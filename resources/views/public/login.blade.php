<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>SERPAY - Micro Payment Systems</title>
    <script src="js/jquery-1.11.1.min.js"></script>

    <script src="http://cbox.cboxprojects.com/public/js/jquery-1.11.1.min.js"></script>

    <link rel="stylesheet" type="text/css" href="http://cbox.cboxprojects.com/public/css/style.css">
    <script src="/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
    <link media="all" type="text/css" rel="stylesheet" href="css/style.css">


    <script>

        $(document).ready(function(e) {

            $(".top_btn").click(function() {
                $('html, body').animate({
                    scrollTop: $(".box").offset().top
                }, 1000);
            });



            $(function() {
                $('a[href*=#]:not([href=#])').click(function() {
                    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

                        var target = $(this.hash);
                        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                        if (target.length) {
                            $('html,body').animate({
                                scrollTop: target.offset().top
                            }, 1000);
                            return false;
                        }
                    }
                });
            });


            $('.adminTab').fadeIn(300);
            $('.entTab').fadeOut(50);
            $('.memTab').fadeOut(50);

            $('.admin').addClass('tabBtnActive');
            $('.entTab').removeClass('tabBtnActive');
            $('.memTab').removeClass('tabBtnActive');

            $('.admin').click(function(e) {

                $('.adminTab').fadeIn(300);
                $('.entTab').fadeOut(300);
                $('.memberTab').fadeOut(300);

                $('.admin').addClass('tabBtnActive');
                $('.enterprise').removeClass('tabBtnActive');
                $('.member').removeClass('tabBtnActive');
            });


            $('.enterprise').click(function(e) {

                $('.adminTab').fadeOut(300);
                $('.entTab').fadeIn(300);
                $('.memberTab').fadeOut(100);

                $('.admin').removeClass('tabBtnActive');
                $('.enterprise').addClass('tabBtnActive');
                $('.member').removeClass('tabBtnActive');
            });

            $('.member').click(function(e) {

                $('.adminTab').fadeOut(300);
                $('.entTab').fadeOut(300);
                $('.memberTab').fadeIn(100);

                $('.admin').removeClass('tabBtnActive');
                $('.enterprise').removeClass('tabBtnActive');
                $('.member').addClass('tabBtnActive');
            });


            $(function() {
                $('a[href*=#]:not([href=#])').click(function() {
                    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

                        var target = $(this.hash);
                        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                        if (target.length) {
                            $('html,body').animate({
                                scrollTop: target.offset().top
                            }, 1000);
                            return false;
                        }
                    }
                });
            });



        });


    </script>

</head>
<style>

    .topSlider .bx-wrapper {
        margin: 0 auto 60px;
        padding: 0;
        position: absolute;
        right: 0px;
        top: 60px;
    }


    .topSlider .bx-viewport {
        background-color: transparent !important;
        border: 0 none !important;
        border-radius: 0;
        height: 327px;
        left: auto !important;
        overflow: visible !important;
        position: absolute !important;
        right: 52px;
        top: 113px;
        width: 443px !important;
        box-shadow: none !important;
    }


    .topSlider .bx-wrapper .bx-caption {
        background-color: transparent !important;
        bottom: 127px;
        color: #000 !important;
        font-size: 28px;
        height: 42px;
        left: -461px;
        line-height: 12px;
        padding: 0 0 10px;
        position: absolute;
        width: 445px;
    }
    .topSlider .bx-wrapper .bx-caption span {
        color: #219af0;
        font-family:'Open Sans', sans-serif;
        font-style: italic;


    }



    .topSlider .bx-pager{
        display:none;
    }

    .mainSlider {

        display:block;
    }

    .homeSlider #bx-pager {
        position: absolute;
        right: 0;
        top: 0;
        width: 240px;
    }

    .homeSlider #bx-pager a {
        display: block;
    }

    .homeSlider .bx-viewport {
        background-color: transparent !important;
        border: 0 none !important;
        border-radius: 0;
        box-shadow: none !important;
    }



    .homeSlider a {
        color: #034987;
        font-family: "Open Sans",sans-serif;
        font-size: 15px;
        letter-spacing: -0.03em;
        line-height: 20px;
        padding: 8px 8px 20px;
        text-decoration: none;
        transition: all 0.3s ease 0s;
    }

    .homeSlider a i{
        color: #fac82e;
        font-size:18px;

    }


    .contentSlider a{
        color:#0079ce;
        font-size:14px;
        display:inline-block;
        position:relative;
        top:0px;
        transition:all 0.3s;
        background-color:transparent !important;
    }


    .contentSlider a:hover{
        color:#0b4b78;
        top:3px;
    }


    .contentSlider a img{

        display:inline-block;
        width:30px;
    }


    .homeSlider a:hover, .homeSlider a.active{
        background-color:#fff5d4;

    }

    .close {
        background-color: #fac82e !important;
        border-radius: 100%;
        color: #de9008;
        display: block;
        font-size: 24px;
        font-weight: 700;
        height: 30px;
        line-height: 1;
        opacity: 1;
        position: absolute;
        right: -15px;
        text-align: center;
        top: -15px;
        width: 30px;
        z-index: 999;
        transition:all 0.3s;
        text-shadow:none !important;
    }

    .close:hover{
        background-color:rgba(252,220,0,1.00) !important;
        color:#f2d479;
        opacity:1 !important;
    }

    #html5-watermark{
        display:none !important;
    }

</style>

<!-- Modal -->
<div class="modal fade" id="developPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none">
    <div class="modal-dialog" style="margin-top:16%; width:40%;">
        <div class="modal-content"  style="border:#fac82e 3px solid;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="modal-body text-center">
                <span style="font-size:24px; margin-top:10px; line-height:15px; display:block;">Sorry,</span>
                <br>
                This page is currently being prepared. We hope to serve you very soon.
            </div>

            <img class="center-block" src="http://cbox.cboxprojects.com/public/images/logo.png" width="200">

        </div>
    </div>
</div>

<body class="p_login">
<div class="box">

    <a class="logo" href="{{ URL::to('/public_portal') }}">
    </a>
    <ul class="buttons_area">
        <li> <a href="#" class="brochureBtn">Brochure</a>
            <ul class="subMenu">
                <li><a href="/files/brochure_en.zip" class="menuBtn featuresBtn">English</a></li>
                <li><a href="#" class="menuBtn featuresBtn">French</a></li>
            </ul>
        </li>
        <li> <a href="{{ URL::to('/public_portal/features') }}" class="featuresBtn"> Features </a> </li>
        <li> <a href="{{ URL::to('/public_portal/contact_us') }}" class="helpBtn"> Contact Us </a> </li>
        <li class="sign_in_item"> <a href="{{ URL::to('/public_portal/login') }}" class="sign_in"> <img width="113" height="38" alt="" src="http://cbox.cboxprojects.com/public/images/sign_in_bg.png"> </a> </li>
        <li class="register_item"> <a href="{{ URL::to('/public_portal/login') }}" class="register"> <img width="124" height="38" alt="" src="http://cbox.cboxprojects.com/public/images/register_bg.png"> </a> </li>
    </ul>
    <div class="tabArea">

        <a name="#admin">
        </a>

        <a name="#enterprise">
        </a>

        <a name="#member">
        </a>

        <a class="tabBtn admin" href="#admin">
            Administrator Login
        </a>

        <a class="tabBtn enterprise" href="#enterprise">
            Business Owner Login
        </a>

        <a class="tabBtn member" href="#member">
            Member Portal Login
        </a>
        <div style="display:block;" class="adminTab tabContent">

            <h1>Administrator Login</h1>
            <p>ADMINISTRATOR_LOGIN_DESCRIPTION </p>

            <a class="adminBtn" href="{{ URL::to('/login') }}/">
            </a>

        </div>
        <div style="display:none;" class="entTab tabContent">

            <h1>Business Owner Login</h1>
            <p>BUSINESS_OWNER_LOGIN_DESCRIPTION </p>

            <a class="adminBtn" style="left: 380px; top:252px;" href="{{ URL::to('/portal/login') }}">
            </a>

            <a class="adminBtn" style="left: 594px; top:252px; width: 313px;" href="{{ URL::to('/portal/register') }}/">
            </a>

        </div>
        <div style="display:none;" class="memberTab tabContent">

            <h1>Member Portal Login</h1>
            <p>MEMBER_PORTAL_LOGIN_DESCRIPTION </p>

            <a class="adminBtn" style="left: 379px; top:251px;" href="{{ URL::to('/member/sign_up') }}#login"></a>
            <a class="adminBtn" style="left: 594px; top:251px; width:313px;" href="{{ URL::to('/member/sign_up') }}#signup"></a>

        </div>
    </div>

    <div id="footer">
        <div class="footerLinks">

            <a href="{{ URL::to('/login') }}" target="_blank">Administrative Portal</a>
            <a href="{{ URL::to('/portal/login') }}" target="_blank">Business Owner Portal</a>
            <a href="{{ URL::to('/member/sign_up') }}" target="_blank">Member Portal</a>
            <a href="{{ URL::to('/public_portal/contact_us') }}">Contact Us</a>
            <a href="{{ URL::to('/public_portal/features') }}">Features</a>

        </div>

        <a class="serpayLogo" href="#pageTop"></a>

        <div class="footerFormArea">

            <form>
                <input class="subject" type="text" placeholder="Subject">
                <input class="email" type="email" placeholder="E-mail Address">
                <input class="sendBtn" type="submit" value="">
            </form>

        </div>

        <div id="footerBottom"></div>

    </div>

    <a class="top_btn" href="#">
    </a>

</div>
</body>
</html>
