<?php $lang = Language::getLanguage();?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>SERPAY - Micro Payment Systems</title>
{{ HTML::script('public/js/jquery-1.11.1.min.js') }}
{{ HTML::script('js/videoplayer/jquery.js') }}
{{ HTML::script('js/videoplayer/html5lightbox.js') }}
{{ HTML::script('bs3/js/bootstrap.min.js') }}
{{ HTML::style('public/css/style.css') }}
{{ HTML::style('css/bootstrap.min.css') }}

<script>

$(document).ready(function(e) {
	
	
	
	$(".top_btn").click(function() {
    $('html, body').animate({
        scrollTop: $(".box").offset().top
    }, 1000);
});



$(function() {
	  $('a[href*=#]:not([href=#])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html,body').animate({
	          scrollTop: target.offset().top
	        }, 1000);
	        return false;
	      }
	    }
	  });
	});
	
	
	
	

	
});


</script>
<!--popup-->

<style>

.close {
    background-color: #fac82e !important;
    border-radius: 100%;
    color: #de9008;
    display: block;
    font-size: 24px;
    font-weight: 700;
    height: 30px;
    line-height: 1;
    opacity: 1;
    position: absolute;
    right: -15px;
    text-align: center;
    top: -15px;
    width: 30px;
    z-index: 999;
	transition:all 0.3s;
	text-shadow:none !important;
}

.close:hover{
	background-color:rgba(252,220,0,1.00) !important;
	color:#f2d479;
	opacity:1 !important;
}

#html5-watermark{
	display:none !important;
}


.pageMainHeading {
 
  margin-bottom: 50px !important;
}
</style>



<!-- Modal -->
<div class="modal fade" id="developPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="margin-top:16%; width:40%;">
    <div class="modal-content"  style="border:#fac82e 3px solid;">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <div class="modal-body text-center">
        <span style="font-size:24px; margin-top:10px; line-height:15px; display:block;">Sorry,</span>
        <br>
This page is currently being prepared. We hope to serve you very soon.
      </div>

      <img class="center-block" src="http://cbox.cboxprojects.com/public/images/logo.png" width="200">

    </div>
  </div>
</div>

<!--popup-->
</head>

<body class="p_features">
<div id="pageTop"></div>

<div class="box">
<a class="logo" href="{{ URL::to('/public_portal') }}">
  </a>


<ul class="buttons_area">
  <li> <a href="#" class="brochureBtn">Brochure</a>
    <ul class="subMenu">
      <li><a href="/files/brochure_en.zip" class="menuBtn featuresBtn">English</a></li>
      <li><a href="#" class="menuBtn featuresBtn">French</a></li>
    </ul>
  </li>
  <li> <a href="{{ URL::to('/public_portal/features') }}" class="featuresBtn"> Features </a> </li>
  <li> <a href="{{ URL::to('/public_portal/contact_us') }}" class="helpBtn"> Contact Us </a> </li>
  <li class="sign_in_item"> <a href="{{ URL::to('/public_portal/login') }}" class="sign_in"> <img width="113" height="38" alt="" src="http://cbox.cboxprojects.com/public/images/sign_in_bg.png"> </a> </li>
  <li class="register_item"> <a href="{{ URL::to('/public_portal/login') }}" class="register"> <img width="124" height="38" alt="" src="http://cbox.cboxprojects.com/public/images/register_bg.png"> </a> </li>
</ul>

  <div class="featuresArea">
  
  <h1 class="pageMainHeading">SERPAY Products & Services</h1>
  
    <div class="row">
      <ul class="ch-grid">
        <li>
          <div class="ch-item ch-img-1"><a href="https://www.youtube.com/embed/FUmwjt1BNn0" class="html5lightbox" data-width="680" data-height="420">
            <div class="ch-info">
              <h3> Device Location Service  </h3>
            </div></a>
          </div>
          <div class="gridBox">
            <h3>Device Location Service</h3>
            <p>{{ $lang->FEATURES_DESCRIPTION_1 }}</p>
            
            <a class="pricingBtn" href="#" onclick="$('#developPopup').modal();">Pricing Plan</a>
           
          </div>
        </li>
        
          <li>
          <div class="ch-item ch-img-2"><a href="https://www.youtube.com/embed/xdY-cpT7MEA" class="html5lightbox" data-width="680" data-height="420">
            <div class="ch-info">
              <h3>Online Payment Service</h3>
            </div></a>
          </div>
          <div class="gridBox">
            <h3>Online Payment Service</h3>
            <p>{{ $lang->FEATURES_DESCRIPTION_2 }}</p>
           
           
             <a class="pricingBtn" href="#" onclick="$('#developPopup').modal();">Pricing Plan</a>
          </div>
        </li>
        
        
        
           <li>
          <div class="ch-item ch-img-3"><a href="https://www.youtube.com/embed/uoWgaeGUijg" class="html5lightbox" data-width="680" data-height="420">
            <div class="ch-info">
              <h3>Offline Payment Service</h3>
            </div></a>
          </div>
          <div class="gridBox">
            <h3>Offline Payment Service</h3>
            <p>{{ $lang->FEATURES_DESCRIPTION_3 }}</p>
            
               <a class="pricingBtn" href="#" onclick="$('#developPopup').modal();">Pricing Plan</a>
           
          </div>
        </li>
        
        
        
        
        
        
         <li>
          <div class="ch-item ch-img-4"><a href="https://www.youtube.com/embed/NjncWbrLHi8" class="html5lightbox" data-width="680" data-height="420">
            <div class="ch-info">
              <h3>Payment Gateway Service</h3>
            </div>
            </a>
          </div>
          <div class="gridBox">
            <h3>Payment Gateway Service</h3>
            <p>{{ $lang->FEATURES_DESCRIPTION_4 }}</p>
            
            <a class="pricingBtn" href="#" onclick="$('#developPopup').modal();">Pricing Plan</a>
           
          </div>
        </li>
        
          <li>
          <div class="ch-item ch-img-5"><a href="https://www.youtube.com/embed/a5-hm04tOlU" class="html5lightbox" data-width="680" data-height="420">
            <div class="ch-info">
              <h3>Clearing / Settling House Service</h3>
            </div>
            </a>
          </div>
          <div class="gridBox">
            <h3>Clearing / Settling House Service</h3>
            <p>{{ $lang->FEATURES_DESCRIPTION_5 }}</p>
           
           
             <a class="pricingBtn" href="#" onclick="$('#developPopup').modal();">Pricing Plan</a>
          </div>
        </li>
        
        
        
           <li>
          <div class="ch-item ch-img-6"><a href="https://www.youtube.com/embed/RPVpAKeJst8" class="html5lightbox" data-width="680" data-height="420">
            <div class="ch-info">
              <h3>Card Management Suite Service</h3>
            </div>
            </a>
          </div>
          <div class="gridBox">
            <h3>Card Management Suite Service</h3>
            <p>{{ $lang->FEATURES_DESCRIPTION_6 }}</p>
            
               <a class="pricingBtn" href="#" onclick="$('#developPopup').modal();">Pricing Plan</a>
           
          </div>
        </li>
        
        
        
        
        
         <li>
          <div class="ch-item ch-img-7"><a href="https://www.youtube.com/embed/15WV6WDb8HM" class="html5lightbox" data-width="680" data-height="420">
            <div class="ch-info">
              <h3>Key Management Suite Service</h3>
            </div>
            </a>
          </div>
          <div class="gridBox">
            <h3>Key Management Suite Service</h3>
            <p>{{ $lang->FEATURES_DESCRIPTION_7 }}</p>
            
            <a class="pricingBtn" href="#" onclick="$('#developPopup').modal();">Pricing Plan</a>
           
          </div>
        </li>
        
          <li>
          <div class="ch-item ch-img-8"><a href="#" onclick="$('#developPopup').modal();">
            <div class="ch-info">
              <h3>RFID Card Internet Charge</h3>
            </div>
            </a>
          </div>
          <div class="gridBox">
            <h3>RFID Card Internet Charge</h3>
            <p>{{ $lang->FEATURES_DESCRIPTION_8 }}</p>
           
           
             <a class="pricingBtn" href="#" onclick="$('#developPopup').modal();">Pricing Plan</a>
          </div>
        </li>
        
        
        
           <li>
          <div class="ch-item ch-img-9"><a href="https://www.youtube.com/embed/H8SR3yUT1c8" class="html5lightbox" data-width="680" data-height="420">
            <div class="ch-info">
              <h3>NFC Mobile Payment Service</h3>
            </div>
            </a>
          </div>
          <div class="gridBox">
            <h3>NFC Mobile Payment Service</h3>
            <p>{{ $lang->FEATURES_DESCRIPTION_9 }}</p>
            
               <a class="pricingBtn" href="#" onclick="$('#developPopup').modal();">Pricing Plan</a>
           
          </div>
        </li>
        
        
        
        
        
         <li>
          <div class="ch-item ch-img-10"> <a href="#" onclick="$('#developPopup').modal();">
            <div class="ch-info">
              <h3>SERPAY Device Rental Service</h3>
            </div>
            </a>
          </div>
          <div class="gridBox">
            <h3>SERPAY Device Rental Service</h3>
            <p>{{ $lang->FEATURES_DESCRIPTION_10 }}</p>
            
            <a class="pricingBtn" href="#" onclick="$('#developPopup').modal();">Pricing Plan</a>
           
          </div>
        </li>
        
          <li>
          <div class="ch-item ch-img-11"> <a href="#" onclick="$('#developPopup').modal();">
            <div class="ch-info">
              <h3>SERPAY Message Pushing Service</h3>
            </div>
            </a>
          </div>
          <div class="gridBox">
            <h3>SERPAY Message Pushing Service</h3>
            <p>{{ $lang->FEATURES_DESCRIPTION_11 }}</p>
           
           
             <a class="pricingBtn" href="#" onclick="$('#developPopup').modal();">Pricing Plan</a>
          </div>
        </li>
        
        
        
           <li>
          <div class="ch-item ch-img-12"><a href="#" onclick="$('#developPopup').modal();">
            <div class="ch-info">
              <h3>SERPAY Advance Security Services</h3>
            </div>
            </a>
          </div>
          <div class="gridBox">
            <h3>SERPAY Advance Security Services</h3>
            <p>{{ $lang->FEATURES_DESCRIPTION_12 }}</p>
            
               <a class="pricingBtn" href="#" onclick="$('#developPopup').modal();">Pricing Plan</a>
           
          </div>
        </li>
        
        
        
        
        
         <li>
          <div class="ch-item ch-img-13"><a href="https://www.youtube.com/embed/duo1Wq48Nf8" class="html5lightbox" data-width="680" data-height="420">
            <div class="ch-info">
              <h3>Client Management Portal</h3>
            </div>
            </a>
          </div>
          <div class="gridBox">
            <h3>Client Management Portal</h3>
            <p>{{ $lang->FEATURES_DESCRIPTION_13 }}</p>
            
            <a class="pricingBtn" href="#" onclick="$('#developPopup').modal();">Pricing Plan</a>
           
          </div>
        </li>
        
          <li>
          <div class="ch-item ch-img-14"><a href="https://www.youtube.com/embed/s2AprS0eZ6I" class="html5lightbox" data-width="680" data-height="420">
            <div class="ch-info">
              <h3>Service Provider Management Portal</h3>
            </div>
            </a>
          </div>
          <div class="gridBox">
            <h3>Service Provider Management Portal</h3>
            <p>{{ $lang->FEATURES_DESCRIPTION_14 }}</p>
           
           
             <a class="pricingBtn" href="#" onclick="$('#developPopup').modal();">Pricing Plan</a>
          </div>
        </li>
        
        
        
           <li>
          <div class="ch-item ch-img-15"><a href="https://www.youtube.com/embed/Q44SKdBe-xk" class="html5lightbox" data-width="680" data-height="420">
            <div class="ch-info">
              <h3>System Administrative Portal</h3>
            </div>
            </a>
          </div>
          <div class="gridBox">
            <h3>System Administrative Portal</h3>
            <p>{{ $lang->FEATURES_DESCRIPTION_15 }}</p>
            
               <a class="pricingBtn" href="#" onclick="$('#developPopup').modal();">Pricing Plan</a>
           
          </div>
        </li>
        
        
        
        
        
        
        
      </ul>
    </div>
  </div>
  
  
  <div id="footer">
    
      <div class="footerLinks">
    
        <a href="{{ URL::to('/login') }}" target="_blank">Administrative Portal</a>
        <a href="{{ URL::to('/portal/login') }}" target="_blank">Business Owner Portal</a>
        <a href="{{ URL::to('/member/sign_up') }}" target="_blank">Member Portal</a>
        <a href="{{ URL::to('/public_portal/contact_us') }}">Contact Us</a>
        <a href="{{ URL::to('/public_portal/features') }}">Features</a>
    
	</div>   
    
      <a class="serpayLogo" href="#pageTop"></a>
      
      
      
      <div class="footerFormArea">

      	 {{Form::open(array('action'=>'BaseController@getContactUs','method'=>'get','class'=>'form-horizontal', 'autocomplete'=>'off','id'=>'createForm1'))}}
        	<input class="subject" name="subject" type="text" placeholder="Subject">
            <input class="email" name="email" type="email" placeholder="E-mail Address">
            <input class="sendBtn" type="submit" value="">


        {{Form::close()}}


      </div>
     
    <div id="footerBottom"></div>
    
     
    
    </div>
  
  
  <a class="top_btn" href="#"> </a> </div>
</body>
</html>
