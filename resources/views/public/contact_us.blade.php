<?php $lang = Language::getLanguage(); ?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>SERPAY - Micro Payment Systems</title>
{{ HTML::script('public/js/jquery-1.11.1.min.js') }}
{{ HTML::script('bs3/js/bootstrap.min.js') }}
{{ HTML::style('public/css/style.css') }}
{{ HTML::style('css/bootstrap.min.css') }}
{{ HTML::script('js/jquery.validate.js') }}
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
<script>
	$.extend($.validator.messages, {
    required: "<?php echo $lang->THIS_FIELD_IS_REQUIRED ?>",
	email: "<?php echo $lang->VALID_EMAIL ?>",
    min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
});

</script>
<script>
var izmir = new google.maps.LatLng(38.4220, 27.1290);
var stockholm = new google.maps.LatLng(59.32522, 18.07002);
var parliament = new google.maps.LatLng(59.327383, 18.06747);
var marker;
var map;
var markers = [];


// Add a marker to the map and push to the array.
function addMarker(location) {
  var marker = new google.maps.Marker({
    position: location,
	animation: google.maps.Animation.DROP,
    map: map
  });
  markers.push(marker);

  var populationOptions = {
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#FF0000',
      fillOpacity: 0.35,
      map: map,
      center: location,
      radius:200
    };
    // Add the circle for this city to the map.
    var cityCircle = new google.maps.Circle(populationOptions);
	markers.push(cityCircle);
}

// Sets the map on all markers in the array.
function setAllMap(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
  setAllMap(null);
}

// Deletes all markers in the array by removing references to them.
function deleteMarkers() {
  clearMarkers();
  markers = [];
}

function showMarkers(lat,lon){

	deleteMarkers();
    var latlngbounds = new google.maps.LatLngBounds();


	var latLng = new google.maps.LatLng(lat,lon);
	// Creating a marker and putting it on the map
	addMarker(latLng);

	latlngbounds.extend(latLng);
	//center

	//map.setCenter(latlngbounds.getCenter());

	map.panTo(new google.maps.LatLng(lat,lon));

	//map.fitBounds(latlngbounds);

}

function initialize() {
  var mapOptions = {
    zoom: 13,
    center: izmir
  };

  map = new google.maps.Map(document.getElementById('map-canvas'),
          mapOptions);


  //google.maps.event.addListener(marker, 'click', toggleBounce);
}


google.maps.event.addDomListener(window, 'load', initialize);



$(document).ready(function(e) {
	
	
	
	$(".top_btn").click(function() {
	    $('html, body').animate({
	        scrollTop: $(".box").offset().top
	    }, 1000);
	});
});


$(function() {


	  $('a[href*=#]:not([href=#])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html,body').animate({
	          scrollTop: target.offset().top
	        }, 1000);
	        return false;
	      }
	    }
	  });

	/*  var form = $("#createForm");
    form.validate(
      {
      errorPlacement: function errorPlacement(error, element) {element.before(error);},
      rules: {
		subject: {required: true},
		full_name: {required: true},
		email: {required: true},
		message: {required: true},
		code: {required: true},
        },
      messages: {

        }

      }
    );
*/

   /* $('#formSubmitBtn').click(function(e) {

       if($('#createForm').valid()) {
			return true;
        }
		return false;
    });
*/


});








</script>
</head>

<body class="p_contact">
<div id="pageTop"></div>

<div class="box"> 
<a class="logo" href="{{ URL::to('/public_portal') }}">
  </a>


    <ul class="buttons_area">
  <li> <a href="#" class="brochureBtn">Brochure</a>
    <ul class="subMenu">
      <li><a href="/files/brochure_en.zip" class="menuBtn featuresBtn">English</a></li>
      <li><a href="#" class="menuBtn featuresBtn">French</a></li>
    </ul>
  </li>
  <li> <a href="{{ URL::to('/public_portal/features') }}" class="featuresBtn"> Features </a> </li>
  <li> <a href="{{ URL::to('/public_portal/contact_us') }}" class="helpBtn"> Contact Us </a> </li>
  <li class="sign_in_item"> <a href="{{ URL::to('/public_portal/login') }}" class="sign_in"> <img width="113" height="38" alt="" src="http://cbox.cboxprojects.com/public/images/sign_in_bg.png"> </a> </li>
  <li class="register_item"> <a href="{{ URL::to('/public_portal/login') }}" class="register"> <img width="124" height="38" alt="" src="http://cbox.cboxprojects.com/public/images/register_bg.png"> </a> </li>
</ul>
  <div class="contactArea">
  
   <h1 class="pageMainHeading">Contact Information</h1>
  
  
  <div class="contactMap">
  
  <h2>Our Location</h2>

  <div id="map-canvas" style="width: 960px; height: 260px;"></div>
  

  </div>
  
  
  <div class="contactLeft">
  <style>
  
  .contactInfoBox {
  position: relative;
  top: 10px !important;
}

.contactArea h2 {
  font-size: 27px !important;
  font-weight: 500 !important;
  margin-top: 40px !important;
  padding-bottom: 9px;
}

.contactMap #map-canvas {
  top: 45px;
}
  
  </style>
  
          <h2>Offices</h2>
          
          <div class="contactInfoBox">
          <h3 style="cursor: pointer;" onclick="showMarkers(14.5824994,121.0617675);">{{$lang->CONTACT_ADDRESS_HEAD_1}}</h3>

          <h4 style="cursor: pointer;" onclick="showMarkers(14.5824994,121.0617675);">{{$lang->CONTACT_ADDRESS_TITLE_1}}</h4>

          <p>{{$lang->CONTACT_ADDRESS_1}}</p>

          <p><span>Tel No: </span> {{$lang->CONTACT_TEL_1}}</p>
          <p><span>Mobile No: </span> {{$lang->CONTACT_MOBILE_1}}</p>
          
          </div>


          
          
          <div class="contactInfoBox">
          <h3 style="cursor: pointer;" onclick="showMarkers(38.4757896,27.1172357);">{{$lang->CONTACT_ADDRESS_HEAD_2}}</h3>
          
          <h4 style="cursor: pointer;" onclick="showMarkers(38.4757896,27.1172357);">{{$lang->CONTACT_ADDRESS_TITLE_2}}</h4>

          <p>{{$lang->CONTACT_ADDRESS_2}}</p>

          <p><span>Tel No: </span> {{$lang->CONTACT_TEL_2}}</p>
          <p><span>Mobile No: </span> {{$lang->CONTACT_MOBILE_2}}</p>
          
          </div>
          
  </div>




  <div class="contactRight">
  
   <h2 style="position: relative; left: 87px;">Contact Form</h2>

    @if(Session::has('message'))
   		<h4 style=" color: #2096D6;margin-left: 100px !important;margin-top: -34px !important;font-size: 14px;">{{Session::pull('message')}}</h4>
    @endif
    @if(Session::has('error'))
   		<h4 style=" color: #D60000;margin-left: 100px !important;margin-top: -34px !important;font-size: 14px;">{{Session::pull('error')}}</h4>
    @endif


  
  <div class="contactForm">
  
   {{Form::open(array('id'=>'createForm'))}}

   {{Form::text('subject',Input::get('subject'), array('required'=>'required','placeholder'=>'Subject')) }}
   {{Form::text('full_name',null, array('required'=>'required','placeholder'=>'Full Name')) }}
   {{Form::text('email',Input::get('email'), array('required'=>'required','placeholder'=>'E - mail')) }}
   {{Form::textarea('message',null, array('required'=>'required','placeholder'=>'Message')) }}
    
     <input type="text" name="code" placeholder="Code" style="width:165px;">

     <?php

	  $image = @imagecreatetruecolor(126, 51) or die("Cannot Initialize new GD image stream");

	  // set background to white and allocate drawing colours
	  $background = imagecolorallocate($image, 0xFF, 0xFF, 0xFF);
	  imagefill($image, 0, 0, $background);
	  $linecolor = imagecolorallocate($image, 0xCC, 0xCC, 0xCC);
	  $textcolor = imagecolorallocate($image, 0x33, 0x33, 0x33);

	  // draw random lines on canvas
	  for($i=0; $i < 6; $i++) {
	    imagesetthickness($image, rand(1,3));
	    imageline($image, 0, rand(0,30), 120, rand(0,30), $linecolor);
	  }

	  // add random digits to canvas
	  $digit = '';
	  for($x = 15; $x <= 95; $x += 20) {
	    $digit .= ($num = rand(0, 9));
	    imagechar($image, rand(3, 5), $x, rand(2, 14), $num, $textcolor);
	  }

	  // record digits in session variable
	  //$_SESSION['digit'] = $digit;
	  Session::put("digit",$digit);

	  // display image and clean up
	  //header('Content-type: image/png');
      $file="/tmp/testimage.png";
	  imagepng($image, $file);
	  //imagepng($image);


	  $imgData = base64_encode(file_get_contents($file));

		// Format the image SRC:  data:{mime};base64,{data};
		$src = 'data: image/png;base64,'.$imgData;

      imagedestroy($image);

	 ?>
     <img class="code" src="{{$src}}"> <a class="codeBtn" href="#" style="display: none;"></a>

     
     {{ Form::submit("", array('class' => 'btn btn-warning pull-left','id'=>'formSubmitBtn','style'=>'margin-left:2px;visibility: 1%;')) }}
     {{ Form::reset("", array('class' => 'btn btn-warning pull-left','id'=>'formSubmitBtn','style'=>'margin-left:2px;visibility: 1%;')) }}



  {{Form::close()}}
  
  </div>
  </div>
    
  </div>
  
  
  <div id="footer">
    
      <div class="footerLinks">
    
      	<a href="{{ URL::to('/login') }}" target="_blank">Administrative Portal</a>
        <a href="{{ URL::to('/portal/login') }}" target="_blank">Business Owner Portal</a>
        <a href="{{ URL::to('/member/sign_up') }}" target="_blank">Member Portal</a>
        <a href="{{ URL::to('/public_portal/contact_us') }}">Contact Us</a>
        <a href="{{ URL::to('/public_portal/features') }}">Features</a>
    
	</div>   
    
      <a class="serpayLogo" href="#pageTop"></a>
      
      

      <div class="footerFormArea">
      
      	 {{Form::open(array('action'=>'BaseController@getContactUs','method'=>'get','class'=>'form-horizontal', 'autocomplete'=>'off','id'=>'createForm1'))}}
        	<input class="subject" name="subject" type="text" placeholder="Subject">
            <input class="email" name="email" type="email" placeholder="E-mail Address">
            <input class="sendBtn" type="submit" value="">


        {{Form::close()}}
      
      
      </div>
     
    <div id="footerBottom"></div>
    
     
    
    </div>
  
  
  <a class="top_btn" href="#"> </a> </div>
</body>
</html>
