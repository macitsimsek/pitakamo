<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>SERPAY - Micro Payment Systems</title>
    {{ HTML::script('public/js/jquery-1.11.1.min.js') }}
    {{ HTML::style('public/css/style.css') }}

    <script src="js/jquery-1.11.1.min.js"></script>

    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <script src="js/jquery.bxslider.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">
    <link rel="stylesheet" type="text/css" href="css/fonts.css">


    <script>

        $(document).ready(function(e) {




            $('.mainSlider').bxSlider({
                mode: 'fade',
                auto: true,
                captions: true
            });


            $('.contentSlider').bxSlider({
                pagerCustom: '#bx-pager',
                mode: 'fade'
            });





            $('.sliderBtn1').addClass('sliderBtnActive');
            $('.sliderBtn2').removeClass('sliderBtnActive');
            $('.sliderBtn3').removeClass('sliderBtnActive');


            $(".top_btn").click(function() {
                $('html, body').animate({
                    scrollTop: $(".box").offset().top
                }, 1000);
            });



            $('.sliderImg1').fadeIn(300);
            $('.sliderImg2').fadeOut(50);
            $('.sliderImg3').fadeOut(50);


            $('.sliderBtn1').click(function(e) {

                $('.sliderImg1').fadeIn(300);
                $('.sliderImg2').fadeOut(50);
                $('.sliderImg3').fadeOut(50);

                $('.sliderBtn1').addClass('sliderBtnActive');
                $('.sliderBtn2').removeClass('sliderBtnActive');
                $('.sliderBtn3').removeClass('sliderBtnActive');



            });


            $('.sliderBtn2').click(function(e) {

                $('.sliderImg1').fadeOut(50);
                $('.sliderImg2').fadeIn(300);
                $('.sliderImg3').fadeOut(50);

                $('.sliderBtn1').removeClass('sliderBtnActive');
                $('.sliderBtn2').addClass('sliderBtnActive');
                $('.sliderBtn3').removeClass('sliderBtnActive');
            });


            $('.sliderBtn3').click(function(e) {

                $('.sliderImg1').fadeOut(50);
                $('.sliderImg2').fadeOut(50);
                $('.sliderImg3').fadeIn(300);



                $('.sliderBtn1').removeClass('sliderBtnActive');
                $('.sliderBtn2').removeClass('sliderBtnActive');
                $('.sliderBtn3').addClass('sliderBtnActive');

            });




            $(function() {
                $('a[href*=#]:not([href=#])').click(function() {
                    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

                        var target = $(this.hash);
                        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                        if (target.length) {
                            $('html,body').animate({
                                scrollTop: target.offset().top
                            }, 1000);
                            return false;
                        }
                    }
                });
            });

        })

    </script>

</head>

<body class="p_home">

<div id="pageTop"></div>

<div class="box">

    <h1 class="homeHeading">The Future of Payment Systems</h1>

    <a class="downloadBtn" href="{{ URL::to("/") }}/files/PioneerAgent.apk"  ></a>
    <style>

        .topSlider .bx-wrapper {
            margin: 0 auto 60px;
            padding: 0;
            position: absolute;
            right: 0px;
            top: 60px;
        }


        .topSlider .bx-viewport {
            background-color: transparent !important;
            border: 0 none !important;
            border-radius: 0;
            height: 327px;
            left: auto !important;
            overflow: visible !important;
            position: absolute !important;
            right: 52px;
            top: 113px;
            width: 443px !important;
            box-shadow: none !important;
        }


        .topSlider .bx-wrapper .bx-caption {
            background-color: transparent !important;
            bottom: 127px;
            color: #000 !important;
            font-size: 28px;
            height: 42px;
            left: -461px;
            line-height: 12px;
            padding: 0 0 10px;
            position: absolute;
            width: 445px;
        }
        .topSlider .bx-wrapper .bx-caption span {
            color: #219af0;
            font-family:'Open Sans', sans-serif;
            font-style: italic;


        }



        .topSlider .bx-pager{
            display:none;
        }

        .mainSlider {

            display:block;
        }

        .homeSlider #bx-pager {
            position: absolute;
            right: 0;
            top: 0;
            width: 240px;
        }

        .homeSlider #bx-pager a {
            display: block;
        }



        .homeSlider .bx-viewport {
            background-color: transparent !important;
            border: 0 none !important;
            border-radius: 0;
            box-shadow: none !important;
        }



        .homeSlider a {
            color: #034987;
            font-family: "Open Sans",sans-serif;
            font-size: 15px;
            letter-spacing: -0.03em;
            line-height: 20px;
            padding: 8px 8px 20px;
            text-decoration: none;
            transition: all 0.3s ease 0s;
        }

        .homeSlider a i{
            color: #fac82e;
            font-size:18px;

        }


        .contentSlider a{
            color:#0079ce;
            font-size:14px;
            display:inline-block;
            position:relative;
            top:0px;
            transition:all 0.3s;
            background-color:transparent !important;
        }


        .contentSlider a:hover{
            color:#0b4b78;
            top:3px;
        }


        .contentSlider a img{

            display:inline-block;
            width:30px;
        }


        .homeSlider a:hover, .homeSlider a.active{
            background-color:#fff5d4;

        }



    </style>
    <style>

        .close {
            background-color: #fac82e !important;
            border-radius: 100%;
            color: #de9008;
            display: block;
            font-size: 24px;
            font-weight: 700;
            height: 30px;
            line-height: 1;
            opacity: 1;
            position: absolute;
            right: -15px;
            text-align: center;
            top: -15px;
            width: 30px;
            z-index: 999;
            transition:all 0.3s;
            text-shadow:none !important;
        }

        .close:hover{
            background-color:rgba(252,220,0,1.00) !important;
            color:#f2d479;
            opacity:1 !important;
        }

        #html5-watermark{
            display:none !important;
        }
        #buttons_area ul
        {
            float: left;
            left: 0;
            opacity: 0;
            position: absolute;

            visibility: hidden;
            z-index: 1;
            -webkit-transition: all .25s ease;
            -moz-transition: all .25s ease;
            -ms-transition: all .25s ease;
            -o-transition: all .25s ease;
            transition: all .25s ease;

        }
        #buttons_area a:hover ul {
            opacity: 1;
            top: 50px;
            visibility: visible;
        }
    </style>

    <!-- Modal -->
    <div class="modal fade" id="developPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="margin-top:16%; width:40%;">
            <div class="modal-content"  style="border:#fac82e 3px solid;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="modal-body text-center">
                    <span style="font-size:24px; margin-top:10px; line-height:15px; display:block;">Sorry,</span>
                    <br>
                    This page is currently being prepared. We hope to serve you very soon.
                </div>

                <img class="center-block" src="http://cbox.cboxprojects.com/public/images/logo.png" width="200">

            </div>
        </div>
    </div>

    <!--popup-->

    <div class="topSlider">

        <ul class="mainSlider">
            <li><img src="http://cbox.cboxprojects.com/public/images/slider/01.png" title="Business Owner Portal"></li>
            <li><img src="http://cbox.cboxprojects.com/public/images/slider/02.png" title="Admin Portal"></li>
            <li><img src="http://cbox.cboxprojects.com/public/images/slider/03.png" title="Member Portal"></li>
            <li><img src="http://cbox.cboxprojects.com/public/images/slider/04.png" title="Card Management Suite"></li>
            <li><img src="http://cbox.cboxprojects.com/public/images/slider/05.png" title="Key Management Suite"></li>
        </ul>

    </div>

    <a class="logo" href="{{ URL::to('/public_portal') }}">
    </a>

    <a class="down_btn" href="#page2">
    </a>

    <ul class="buttons_area">
        <li> <a href="#" class="brochureBtn">DEGISTIRDIM</a>
            <ul class="subMenu">
                <li><a href="/files/brochure_en.zip" class="menuBtn featuresBtn">English</a></li>
                <li><a href="#" class="menuBtn featuresBtn">French</a></li>
            </ul>
        </li>
        <li> <a href="{{ URL::to('/public_portal/features') }}" class="featuresBtn"> Features </a> </li>
        <li> <a href="{{ URL::to('/public_portal/contact_us') }}" class="helpBtn"> Contact Us </a> </li>
        <li class="sign_in_item"> <a href="{{ URL::to('/public_portal/login') }}" class="sign_in"> <img width="113" height="38" alt="" src="http://cbox.cboxprojects.com/public/images/sign_in_bg.png"> </a> </li>
        <li class="register_item"> <a href="{{ URL::to('/public_portal/login') }}" class="register"> <img width="124" height="38" alt="" src="http://cbox.cboxprojects.com/public/images/register_bg.png"> </a> </li>
    </ul>


    <div class="homeSlider">

        <a href="#" id="page2" style="clear:both;"></a>

        <ul class="contentSlider">
            <li><img src="http://cbox.cboxprojects.com/public/images/faq/part1.jpg" /><a href="javascript:$('#developPopup').modal();" onclick="$('#developPopup').modal();">Read More <img src="http://cbox.cboxprojects.com/public/images/right_arrow.png"></a></li>
            <li><img src="http://cbox.cboxprojects.com/public/images/faq/part2.jpg" /><a href="javascript:$('#developPopup').modal();" onclick="$('#developPopup').modal();">Read More <img src="http://cbox.cboxprojects.com/public/images/right_arrow.png"></a></li>
            <li><img src="http://cbox.cboxprojects.com/public/images/faq/part3.jpg" /><a href="javascript:$('#developPopup').modal();" onclick="$('#developPopup').modal();">Read More <img src="http://cbox.cboxprojects.com/public/images/right_arrow.png"></a></li>
            <li><img src="http://cbox.cboxprojects.com/public/images/faq/part4.jpg" /><a href="javascript:$('#developPopup').modal();" onclick="$('#developPopup').modal();">Read More <img src="http://cbox.cboxprojects.com/public/images/right_arrow.png"></a></li>
            <li><img src="http://cbox.cboxprojects.com/public/images/faq/part5.jpg" /><a href="javascript:$('#developPopup').modal();" onclick="$('#developPopup').modal();">Read More <img src="http://cbox.cboxprojects.com/public/images/right_arrow.png"></a></li>
            <li><img src="http://cbox.cboxprojects.com/public/images/faq/part6.jpg" /><a href="javascript:$('#developPopup').modal();" onclick="$('#developPopup').modal();">Read More <img src="http://cbox.cboxprojects.com/public/images/right_arrow.png"></a></li>

        </ul>



        <div id="bx-pager">
            <a data-slide-index="0" href=""><i class="icon-right_arrow"></i> How do I pay for  things with SERPAY? </a>
            <a data-slide-index="1" href=""><i class="icon-right_arrow"></i> What are the main features of SERPAY</a>
            <a data-slide-index="2" href=""><i class="icon-right_arrow"></i> Can SERPAY be customized for my business?</a>
            <a data-slide-index="3" href=""><i class="icon-right_arrow"></i> What if my business model is more complex? How I can customize SERPAY for my business? </a>
            <a data-slide-index="4" href=""><i class="icon-right_arrow"></i> Can I use my credit card in SERPAY? </a>
            <a data-slide-index="5" href=""><i class="icon-right_arrow"></i> What types of installations does SERPAY support? </a>
        </div>



        <a class="down_btn bottomDownBtn" href="#page3">
        </a>

    </div>

    <div class="bottomText" id="page3">


        <div class="homeDevices">
            <h3>SERPAY when and where you need it</h3>
            <p>Use your SERPAY on tablets  and smartphones for when you're on the go, and on the web everywhere in between.</p>

            <a  href="javascript:$('#developPopup').modal();" onclick="$('#developPopup').modal();">Read More <img src="http://cbox.cboxprojects.com/public/images/right_arrow.png"></a>

        </div>

        <div class="easyAdmin">
            <h3>Easy Administration</h3>
            <p>With provision of customized, tailored-fit web portals for different user profiles.</p>

        </div>

        <a class="down_btn bottomDownBtn" href="#footerBottom" style="bottom:-70px; position:absolute; z-index:1;">
        </a>
    </div>

    <div id="footer">

        <div class="footerLinks">

            <a href="{{ URL::to('/login') }}" target="_blank">Administrative Portal</a>
            <a href="{{ URL::to('/portal/login') }}" target="_blank">Business Owner Portal</a>
            <a href="{{ URL::to('/member/sign_up') }}" target="_blank">Member Portal</a>
            <a href="{{ URL::to('/public_portal/contact_us') }}">Contact Us</a>
            <a href="{{ URL::to('/public_portal/features') }}">Features</a>

        </div>

        <a class="serpayLogo" href="#pageTop"></a>

        <div class="footerFormArea">

            {{Form::open(array('action'=>'BaseController@getContactUs','method'=>'get','class'=>'form-horizontal', 'autocomplete'=>'off','id'=>'createForm1'))}}
            <input class="subject" name="subject" type="text" placeholder="Subject">
            <input class="email" name="email" type="email" placeholder="E-mail Address">
            <input class="sendBtn" type="submit" value="">

            {{Form::close()}}

        </div>

        <div id="footerBottom"></div>

    </div>

    <a class="top_btn" href="#">
    </a>

</div>
</body>
</html>