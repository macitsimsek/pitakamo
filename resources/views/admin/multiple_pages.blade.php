<?php
$lang = \App\Classes\Language::getLanguageEntries("EN");?>

@extends('admin.master')
@section('mainContent')
    <style>
        .dataTables_length {
            padding-top: 68px!important;
        }
        #example th:first-child {
            text-align: center;
        }
        .btn-danger{
            background-color: #A09A9A;
            border-color: #A09A9A;
        }
        .btn-danger:hover{
            background-color: #A09A9A;
            border-color: #A09A9A;
        }
    </style>
    <section id="main-content">
        <section class="wrapper">
            @if(!$menu['TYPE'])
            <div class="row">
                <div class="col-md-12">
                    <!--breadcrumbs start -->
                    <ul class="breadcrumb">
                        @foreach($menu['LEVELLING'] as $key=>$value)
                        <li><a href="{{URL::to($value['FULLPATH'])}}">@if($key==0)<i class="fa fa-home"></i>@endif {{ $lang[$value['LANGUAGE_KEY']]}}</a></li>
                        @endforeach
                    </ul>
                    <!--breadcrumbs end -->
                </div>
            </div>

            <section class="panel">
                <div class="panel-body">
                    <div class="pageHeading" style="padding:5px; padding-left: 10px;background-color: #B149DD!important; color: white;font-size: 18px;">{{$lang[$menu['LANGUAGE_KEY']]}}</div>
                </div>
            </section>
            <section class="panel">
                <div class="panel-body">
                    @if(isset($menu['BUTTONS']))
                        @if(isset($menu['BUTTONS']['FIRST']))
                            @foreach($menu['BUTTONS']['FIRST'] as $key=>$value)
                            <div class="btn-group">
                                <a href="{{ URL::to($value['FULLPATH']) }}" class="btn btn-primary" style="border-color: #B149DD!important;background-color: #B149DD!important;"> {{$lang[$value['LANGUAGE_KEY']]}} <i class="fa {{$value['ICON']}}"></i></a>
                            </div>
                            @endforeach
                        @endif
                    @endif
                    @if(isset($menu['BUTTONS']))
                        @if(isset($menu['BUTTONS']['SECOND']))
                        <div class="adv-table editable-table">
                            <div id="editable-sample_wrapper" class="dataTables_wrapper form-inline" role="grid">
                                <div id="example_wrapper" class="dataTables_wrapper">
                                    <table id="example" class="table table-striped table-hover dt-responsive cboxDataTable" cellspacing="0" width="100%">
                                        <script>
                                                <?php
                                                $my_ldap_group_array = array();
                                                foreach($users as $k=>$v) {
                                                    $buttons='';
                                                    foreach($menu['BUTTONS']['SECOND'] as $key=>$val) {
                                                        $buttons .= '<a title="'.$lang[$val['LANGUAGE_KEY']].'" href="'.URL::to(str_replace('{user}','username',$val['FULLPATH'])).'" class="tablePreBtn '.$val['LANGUAGE_KEY'].'"><i class="fa '.$val['ICON'].'"></i></a>';
                                                    }
                                                    $my_ldap_group_array[]="['<div style=\"width:190px;\"><input type=\"checkbox\" name=\"users[]\" value=\"".$v["first_name"]."\"  /> $buttons</div>',
                                    '".addslashes($v["first_name"])."',
                                    '".addslashes($v["middle_name"])."',
                                    '".addslashes($v["last_name"])."',
                                    '".addslashes($v["mobile_number"])."',
                                    '".addslashes($v["gender"])."']";
                                                }
                                                ?>

                                            var dataSet = [
                                                    <?php
                                                    echo implode(",",$my_ldap_group_array);
                                                    ?>
                                                ];
                                            $(document).ready(function() {
                                                $('#example').dataTable( {
                                                    responsive: true,
                                                    "data": dataSet,
                                                    "language": {
                                                        url: '{{URL::asset('js/datatable/EN.json')}}'
                                                    },
                                                    "columns": [
                                                        { "title": "<?php echo $lang['ACTIONS'] ?>"},
                                                        { "title": "<?php echo $lang['FIRST_NAME'] ?>" },
                                                        { "title": "<?php echo $lang['MIDDLE_NAME'] ?>" },
                                                        { "title": "<?php echo $lang['LAST_NAME'] ?>" },
                                                        { "title": "<?php echo $lang['MOBILE_NUMBER'] ?>" },
                                                        { "title": "<?php echo $lang['GENDER'] ?>" }
                                                    ],
                                                    "order": [[ 1, "asc" ]],
                                                    "fnInitComplete": function (oSettings, json) {
                                                        assignEventToButton();
                                                    },
                                                    "fnDrawCallback": function( oSettings ) {

                                                    }
                                                });
                                            });
                                        </script>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @endif
                    @endif
                </div>
            </section>
            @else
                <div class="row dashboardButtonsArea">
                    @foreach ($menu['CHILDREN'] as $key=>$value)
                        <div class="col-md-3">
                            <a id="userManagement" class="dashboardButtons" href="{{ url($value['FULLPATH']) }}" style="display:block;">
                                <section class="panel icon-hover">
                                    <div class="panel-body"> <span class="fa @if(isset($value['ICON'])){{$value['ICON']}}@endif icon-big"></span>
                                        <p>{{$lang[$value['LANGUAGE_KEY']]}}</p>
                                    </div>
                                </section>
                            </a>
                        </div>
                    @endforeach
                </div>
            @endif
        </section>
    </section>

    <script>
        $(function () {
            $("#example").on("click", "a.BLOCK_ACCOUNT", function() {
                $('#smallModalblock').remove();
                $('body').append(blockPopup());
                return false;
            });
            $("#example").on("click", "a.UNBLOCK_ACCOUNT", function() {
                $('#smallModalblock').remove();
                $('body').append(unblockPopup());
                return false;
            });
            $("#example").on("click", "a.RESET_ACCOUNT", function() {
                $('#smallModalblock').remove();
                $('body').append(resetPopup());
                return false;
            });

            $("body").on("click", "#smallModalblock #blockBtnCancel", function() {
                $('#smallModalblock').remove();
                return false;
            });

            $("body").on("click", "#smallModalblock #blockBtnConfirm", function() {
                $('#smallModalblock').remove();
                alert('success')
                return false;
            });

            $("body").on("click", "#smallModalblock #resetBtnCancel", function() {
                $('#smallModalblock').remove();
                return false;
            });

            $("body").on("click", "#smallModalblock #resetBtnConfirm", function() {
                $('#smallModalblock').remove();
                alert('success')
                return false;
            });

            $("body").on("click", "#smallModalblock #unblockBtnCancel", function() {
                $('#smallModalblock').remove();
                return false;
            });

            $("body").on("click", "#smallModalblock #unblockBtnConfirm", function() {
                $('#smallModalblock').remove();
                alert('success')
                return false;
            });
        });
        function resetPopup() {
            return '\n' +
                '        <div style="display: block;background-color: rgba(226, 225, 225, 0.61);" class="modal fade in" id="smallModalblock" tabindex="-1" role="dialog" aria-labelledby="smallModalblock" aria-hidden="false">\n' +
                '            <div class="modal-dialog modal-sm">\n' +
                '                <div class="modal-content">\n' +
                '                    <div class="modal-body">\n' +
                '                        <h4><i style="font-size: 35px;color: red;" class="fa fa-exclamation-triangle" aria-hidden="true"></i></h4>\n' +
                '                        <h5>Do you want to reset this user?</h5>\n' +
                '                    </div>\n' +
                '                    <div class="modal-footer">\n' +
                '                        <button type="button" id="resetBtnCancel" class="btn btn-danger" data-dismiss="modal">Cancel</button>\n' +
                '                        <button type="button" id="resetBtnConfirm" class="btn btn-success pitaPurple">Confirm</button>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '        </div>';
        }
        function blockPopup() {
            return '\n' +
                '        <div style="display: block;background-color: rgba(226, 225, 225, 0.61);" class="modal fade in" id="smallModalblock" tabindex="-1" role="dialog" aria-labelledby="smallModalblock" aria-hidden="false">\n' +
                '            <div class="modal-dialog modal-sm">\n' +
                '                <div class="modal-content">\n' +
                '                    <div class="modal-body">\n' +
                '                        <h4><i style="font-size: 35px;color: red;" class="fa fa-exclamation-triangle" aria-hidden="true"></i></h4>\n' +
                '                        <h5>Do you want to block this user?</h5>\n' +
                '                    </div>\n' +
                '                    <div class="modal-footer">\n' +
                '                        <button type="button" id="blockBtnCancel" class="btn btn-danger" data-dismiss="modal">Cancel</button>\n' +
                '                        <button type="button" id="blockBtnConfirm" class="btn btn-success pitaPurple">Confirm</button>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '        </div>';
        }
        function unblockPopup() {
            return '\n' +
                '        <div style="display: block;background-color: rgba(226, 225, 225, 0.61);" class="modal fade in" id="smallModalblock" tabindex="-1" role="dialog" aria-labelledby="smallModalblock" aria-hidden="false">\n' +
                '            <div class="modal-dialog modal-sm">\n' +
                '                <div class="modal-content">\n' +
                '                    <div class="modal-body">\n' +
                '                        <h4><i style="font-size: 35px;color: red;" class="fa fa-exclamation-triangle" aria-hidden="true"></i></h4>\n' +
                '                        <h5>Do you want to unblock this user?</h5>\n' +
                '                    </div>\n' +
                '                    <div class="modal-footer">\n' +
                '                        <button type="button" id="unblockBtnCancel" class="btn btn-danger" data-dismiss="modal">Cancel</button>\n' +
                '                        <button type="button" id="unblockBtnConfirm" class="btn btn-success pitaPurple">Confirm</button>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '        </div>';
        }
    </script>
@stop