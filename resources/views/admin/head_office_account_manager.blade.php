<?php
$lang = \App\Classes\Language::getLanguageEntries("EN");
$dashboard = \App\Classes\CustomFunctions::getDashboardUrl();
?>
@extends('admin.master')
@section('mainContent')
    {{ HTML::style('css/dataTablesTools.css') }}
    <style> .dataTables_length { padding-top: 68px!important; }
        #example th:first-child {text-align: center;}   </style>
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <!--breadcrumbs start -->
                    <ul class="breadcrumb">
                        <li><a href="{{$dashboard}}"><i class="fa fa-home"></i> {{ $lang['DASHBOARD']}}</a></li>
                        <li @if(!$subpage)class="active"@endif><a href="{{$dashboard.'/'.strtolower($page)}}">{{$lang[$page]}}</a></li>
                        @if($subpage)<li class="active"><a href="{{$dashboard.'/'.strtolower($page).'/'.strtolower($subpage)}}">{{$lang[$subpage]}}</a></li>@endif
                    </ul>
                    <!--breadcrumbs end -->
                </div>
            </div>

            <section class="panel">
                <div class="panel-body">
                    <div class="pageHeading" style="padding:5px; padding-left: 10px;background-color: #B149DD!important; color: white;font-size: 18px;">{{$lang[$page]}}</div>
                </div>
            </section>

            <section class="panel">
                <div class="panel-body">
                    <div class="btn-group"> <a href="{{ URL::to('user_identity_management/user/create') }}" class="btn btn-primary" style="border-color: #B149DD!important;background-color: #B149DD!important;" id="editable-sample_new"> {{$lang['ADD_NEW_ACCOUNT']}} <i class="fa fa-plus"></i></a> </div>
                    <div class="adv-table editable-table">
                        <div id="editable-sample_wrapper" class="dataTables_wrapper form-inline" role="grid">
                            <div id="example_wrapper" class="dataTables_wrapper">
                                <table id="example" class="table table-striped table-hover dt-responsive cboxDataTable" cellspacing="0" width="100%">
                                    <script>
                                            <?php
                                            $my_ldap_group_array = array();
                                            $previewlink=""; $editlink=""; $deletelink=""; $blocklink="";
                                            foreach($users as $k=>$v) {
                                                $previewlink = "<a href=\"./user/".$v["first_name"]."/preview_user?type=".$v["first_name"]."\" class=\"tablePreBtn\" ><i class=\"fa fa-eye\"></i></a>";
                                                $editlink = "<a href=\"./user/".$v["first_name"]."/edit?type=".$v["first_name"]."\" class=\"tableEditBtn\" ><i class=\"fa fa-pencil\"></i></a>";
                                                $deletelink = "<a href=\"./user/".$v["first_name"]."/delete?type=".$v["first_name"]."\" class=\"tableBlockBtn\" ><i class=\"fa fa-ban\"></i></a>";
                                                $blocklink = "<a href=\"./user/".$v["first_name"]."/unblock\" class=\"tablePreBtn\" ><i class=\"fa fa-lock\"></i></a>";
                                                $questionlink = "<a href=\"./user/".$v["first_name"]."/unblock\" class=\"tablePreBtn\" ><i class=\"fa fa-question-circle\"></i></a>";
                                                @$my_ldap_group_array[]="['<div style=\"width:155px;\"><input type=\"checkbox\" name=\"users[]\" value=\"".$v["first_name"]."\"  /> $previewlink $editlink $deletelink $blocklink $questionlink</div>',
                                '".addslashes($v["first_name"])."',
                                '".addslashes($v["middle_name"])."',
                                '".addslashes($v["last_name"])."',
                                '".addslashes($v["mobile_number"])."',
                                '".addslashes($v["gender"])."'
                            ]";
                                            }
                                            ?>
                                        var dataSet = [
                                                <?php
                                                echo implode(",",$my_ldap_group_array);
                                                ?>
                                            ];
                                        $(document).ready(function() {
                                            $('#example').dataTable( {
                                                responsive: true,
                                                stateSave: true,
                                                "data": dataSet,
                                                dom: 'T<"clear">lfrtip',
                                                tableTools:{
                                                    "sSwfPath": "{{URL::asset('js/datatable/TableTools-2.2.3/swf/copy_csv_xls_pdf.swf')}}",
                                                    "aButtons": [

                                                        {
                                                            "sExtends":    "collection",
                                                            "sButtonText": "Export",
                                                            "aButtons":    [ "csv", "xls", "pdf" ]
                                                        }
                                                    ]
                                                },
                                                "language": {
                                                    url: '{{URL::asset('js/datatable/EN.json')}}'
                                                },
                                                "columns": [
                                                    { "title": "<?php echo $lang['ACTIONS'] ?>"},
                                                    { "title": "<?php echo $lang['FIRST_NAME'] ?>" },
                                                    { "title": "<?php echo $lang['MIDDLE_NAME'] ?>" },
                                                    { "title": "<?php echo $lang['LAST_NAME'] ?>" },
                                                    { "title": "<?php echo $lang['MOBILE_NUMBER'] ?>" },
                                                    { "title": "<?php echo $lang['GENDER'] ?>" }
                                                ],
                                                "order": [[ 1, "asc" ]],
                                                "fnInitComplete": function (oSettings, json) {
                                                    assignEventToButton();
                                                },
                                                "fnDrawCallback": function( oSettings ) {

                                                }
                                            });
                                        });
                                    </script>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
    </section>
    <!--main content end-->
@stop