@extends('admin.master')
@section('mainContent')
<?php $lang = \App\Classes\Language::getLanguageEntries("EN");
?>
    <!--alert bar-->
    @if(Session::has('error'))
        <div style="" class="modal fade" id="smallModalUnblock" tabindex="-1" role="dialog" aria-labelledby="smallModalUnblock" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-body">
                        <h4>{{ $lang->WARNING}}</h4>
                        <h5>{{ $lang->YOU_ARE_NOT_AUTHORIZED}}</h5>
                    </div>
                    <div class="modal-footer">
                        <button type="popupCancel button" class="btn btn-default" data-dismiss="modal">{{ $lang->CLOSE}}</button>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <!--alert bar-->


    <!--main content start-->
    <section id="main-content" class="dashboardHome">
        <section class="wrapper">
            <div class="row dashboardButtonsArea">
                @foreach ($menu as $key=>$value)
                    @if($value['LANGUAGE_KEY']!='DASHBOARD')
                    <div class="col-md-3">
                        <a id="userManagement" class="dashboardButtons" href="{{ url($value['FULLPATH']) }}" style="display:block;">
                            <section class="panel icon-hover">
                                <div class="panel-body"> <span class="fa @if(isset($value['ICON'])){{$value['ICON']}}@endif icon-big"></span>
                                    <p>{{$lang[$value['LANGUAGE_KEY']]}}</p>
                                </div>
                            </section>
                        </a>
                    </div>
                    @endif
                @endforeach
            </div>
        </section>

        <script>
            $(function () {
                $(".icon-hover").hover(
                    function () {
                        $(this).find('span').css({"color":"#9d1bd5"})
                    },
                    function () {
                        $(this).find('span').css({"color":"black"})
                    }
                );
            });
        </script>

        {{ HTML::script('js/skycons/skycons.js') }}
        {{ HTML::script('js/calendar/clndr.js') }}
        {{ HTML::script('js/gauge/gauge.js') }}
        {{ HTML::script('js/css3clock/js/css3clock.js') }}
        {{ HTML::script('js/dashboard.js') }}
        {{ HTML::script('js/calendar/moment-2.2.1.js') }}
        {{ HTML::script('js/evnt.calendar.init.js') }}
        {{ HTML::script('js/sparkline/jquery.sparkline.js') }}
        {{ HTML::script('js/draggable-portlet.js') }}
        {{ HTML::script('js/portlet.js') }}
        {{ HTML::script('js/jquery.customSelect.min.js') }}
        {{ HTML::script('js/slick.js') }}

    </section>
    <!--main content end-->

@stop 