<?php
$lang = \App\Classes\Language::getLanguageEntries("EN");
$dashboard = \App\Classes\CustomFunctions::getDashboardUrl();

$haschrildren = false;
$sub_menus = [];
foreach ($menu as $key => $value){
    if($value['parent']['PATH']==strtolower($page)){
        if(isset($value['children'])){
            $haschrildren = true;
            $sub_menus = $value['children'];
        }
    }
}
$backurl= $dashboard.'/'.strtolower($page).'/'.((isset($subpage))?strtolower($subpage).'/':'');
?>
@extends('admin.master')
@section('mainContent')
    {{ HTML::style('css/dataTablesTools.css') }}
    <style>
        .dataTables_length { padding-top: 68px!important; }
        #example th:first-child {
            text-align: center;
        }
         button.accordion {
             color: #B149DD!important;
             background-color: transparent!important;
             cursor: pointer;
             padding: 18px;
             font-size: 22px!important;
             width: 100%;
             border: none;
             border-bottom: 3px solid #B149DD!important;
             text-align: left;
             outline: none;
             transition: 0.4s;
         }
         button.second {
             cursor: pointer;
             padding: 10px;
             font-weight: inherit;
             background-color: transparent!important;
             font-size: 19px!important;
             width: 100%;
             border: none;
             text-align: left;
             outline: none;
             transition: 0.4s;
         }

        button.accordion.active, button.accordion:hover {
            background-color: #ccc;
        }

        button.second.active, button.second:hover {
            background-color: #ccc;
        }

        div.panel {
            padding: 0 18px;
            display: none;
            background-color: white;
        }
    </style>
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <!--breadcrumbs start -->
                    <ul class="breadcrumb">
                        <li><a href="{{$dashboard}}"><i class="fa fa-home"></i> {{ $lang['DASHBOARD']}}</a></li>
                        <li @if(!isset($subpage))class="active"@endif><a href="{{$dashboard.'/'.strtolower($page)}}">{{$lang[$page]}}</a></li>
                        @if(isset($subpage))<li class="active"><a href="{{$dashboard.'/'.strtolower($page).'/'.strtolower($subpage)}}">{{$lang[$subpage]}}</a></li>@endif
                    </ul>
                    <!--breadcrumbs end -->
                </div>
            </div>

            <section class="panel">
                <div class="panel-body">
                    <div class="pageHeading" style="padding:5px; padding-left: 10px;background-color: #B149DD!important; color: white;font-size: 18px;">@if(isset($subpage)){{$lang[$subpage]}}@else{{$lang[$page]}}@endif</div>
                </div>
            </section>
            <section class="panel">
                <div class="panel-body">
                    <div class="col-md-3 text-center">
                        <img src="{{url('images/prof-picture.png')}}" style="height: 160px;">
                    </div>
                    <div class="col-md-9">
                        <button class="accordion">Personal Information<span class="caret"></span></button>
                        <div class="panel">
                            <table class="col-md-12">
                                @foreach($userinfos['personal'] as $key=>$value)
                                    @if(!is_array($value))
                                    <tr style="border-bottom: 1px solid #E2DCDC;">
                                        <td class="col-md-6" style="padding: 15px;font-weight: bold;">{{$key}}</td>
                                        <td class="col-md-6" style="padding: 15px;">{{$value}}</td>
                                    </tr>
                                    @else
                                        @foreach($value as  $key2=>$item)
                                            <tr >
                                                <td class="col-md-6"  style="padding: 15px;font-weight: bold;">@if($key2==0) {{$key}} @endif</td>
                                                <td style="border-bottom: 1px solid #E2DCDC;padding: 15px;" class="col-md-6">{{$item}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                @endforeach
                            </table>
                            <div style="text-align: right;">
                                <a class="btn" style="background-color: #B149DD!important;text-align: right;color:white;" href="{{$backurl}}">BACK</a>
                            </div>
                        </div>
                        <button class="accordion">Organizational Information<span class="caret"></span></button>
                        <div class="panel">
                            <table class="col-md-12">
                                @foreach($userinfos['organizational'] as $key=>$value)
                                    <tr style="border-bottom: 1px solid #E2DCDC;">
                                        <td class="col-md-6" style="padding: 15px;font-weight: bold;">{{$key}}</td>
                                        <td class="col-md-6" style="padding: 15px;">{{$value}}</td>
                                    </tr>
                                @endforeach
                            </table>
                            <div style="text-align: right;">
                                <a class="btn" style="background-color: #B149DD!important;text-align: right;color:white;" href="{{$backurl}}">BACK</a>
                            </div>
                        </div>
                        <button class="accordion">Permissions<span class="caret"></span></button>
                        <div class="panel">
                                @foreach($userinfos['permissions'] as $key=>$value)
                                    @if(!is_array($value))
                                    <button class="second"><span style="margin-right: 20px;" class="fa fa-plus"></span>{{$key}}</button>
                                    @else
                                    <button class="second"><span style="margin-right: 20px;" class="fa fa-plus"></span>{{$key}}</button>
                                        <div style="margin-left: 70px;display: none">
                                            @foreach($value as  $key2=>$item)
                                            <div>{{$item}}</div>
                                            @endforeach
                                        </div>
                                    @endif
                                @endforeach
                            </table>
                            <div style="text-align: right;">
                                <a class="btn" style="background-color: #B149DD!important;text-align: right;color:white;" href="{{$backurl}}">BACK</a>
                            </div>
                        </div>

                        <script>
                            var acc = document.getElementsByClassName("accordion");
                            var i;

                            for (i = 0; i < acc.length; i++) {
                                acc[i].onclick = function(){
                                    this.classList.toggle("active");
                                    var panel = this.nextElementSibling;
                                    if (panel.style.display === "block") {
                                        panel.style.display = "none";
                                    } else {
                                        panel.style.display = "block";
                                    }
                                }
                            }
                            $('.second').click(function () {
                                if($(this).next().is('div')){$(this).next().toggle();}
                                if($(this).find('span').hasClass('fa-plus')){
                                    $(this).find('span').removeClass('fa-plus').addClass('fa-minus');
                                }else{
                                    $(this).find('span').removeClass('fa-minus').addClass('fa-plus');
                                }
                            })
                        </script>
                    </div>
                </div>
                </div>
            </section>
        </section>
    </section>
    <!--main content end-->
@stop