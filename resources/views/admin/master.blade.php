<?php $lang = \App\Classes\Language::getLanguageEntries("EN");
$menu = \App\Classes\CustomFunctions::makePermissionCore(session('token'));
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{URL::to('/')}}<?php echo "/images/favicon.ico?".time();?>" >
    <title>Pitakamo - Micro Payment System</title>

    <!--Core CSS -->
{{ HTML::style('bs3/css/bootstrap.min.css') }}
{{ HTML::style('js/jquery-ui/jquery-ui-1.10.1.custom.min.css') }}
{{ HTML::style('css/font-awesome.css') }}
<!-- Custom styles for this template -->
{{ HTML::style('css/style.css') }}
{{ HTML::style('css/dataTablesTools.css') }}
{{ HTML::style('css/style-responsive.css') }}
{{ HTML::style('css/jquery.dataTables.css') }}
{{ HTML::style('css/dataTables.responsive.css') }}
{{ HTML::style('js/datatable/TableTools-2.2.3/css/dataTables.tableTools.css') }}

<!-- Custom styles for this template -->

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="js/ie8-responsive-file-warning.js"></script><![endif]-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

<!--{{ HTML::script('datatable/js/jquery.js') }}  -->
    {{ HTML::script('js/jquery-1.11.1.min.js') }}
    {{ HTML::script('datatable/js/jquery.dataTables.min.js') }}
    {{ HTML::script('datatable/js/datatables-bootstrap.js') }}
    {{ HTML::script('js/dataTables.responsive.min.js') }}
    {{ HTML::script('js/datatable/TableTools-2.2.3/js/dataTables.tableTools.min.js') }}
    <style>
        .pitaPurple {
            background-color: #B149DD!important;
            border-color: #B149DD!important;
            color: white;
        }
        .pitaCancel {
            background-color: #9C9C90!important;
            border-color: #9C9C90!important;
            color: white;
        }
        button.accordion, a.accordion {
            color: #B149DD!important;
            background-color: transparent!important;
            cursor: pointer;
            padding: 18px;
            font-size: 22px!important;
            width: 100%;
            border: none;
            text-align: left;
            outline: none;
            transition: 0.4s;
        }
        button.second {
            cursor: pointer;
            padding: 10px;
            font-weight: inherit;
            background-color: transparent!important;
            font-size: 19px!important;
            width: 100%;
            border: none;
            text-align: left;
            outline: none;
            transition: 0.4s;
        }

        button.accordion.active, button.accordion:hover,a.accordion.active, a.accordion:hover {
            background-color: #ccc;
        }

        button.second.active, button.second:hover {
            background-color: #ccc;
        }

        div.panel {
            padding: 0 18px;
            display: none;
            background-color: white;
        }
    </style>
</head>

<body style="background-color: #f1f2f7;background-image: none!important;">
<section id="container">
    <!--header start-->
    <header class="header fixed-top clearfix">
        <!--logo start-->
        <div class="brand">
            <a>
                <?php
                $logoPNGUrl = URL::to('/') ."/images/dashboard-logo.png";
                $logoSparkle = URL::to('/') ."/images/sparkle-logo.png";
                $sparkleMenu = URL::to('/') ."/images/sparkle-menu.png";
                $menuBackground = URL::to('/') ."/images/background.jpg";
                $backgroundMember = URL::to('/') ."/images/background-member.png";
                ?>
            <img src="<?php echo $logoSparkle;  ?>" style="height: 110px;position: absolute;"/>
            <img src="<?php echo $logoPNGUrl;  ?>" alt="logo" style="height:65px;padding: 5px;position: absolute;"/>
            <div class="logo-bottom-text"><?=$lang['ADMIN_PORTAL']?></div>
            </a>
        </div>
        <!--logo end-->
        <div style="float: right;margin: 5px 25px 5px 0px;">
            <ul class="nav pull-right top-menu">

                <!-- user login dropdown start-->
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle pitaPurple" href="#">

                        <img style="height: 50px;border-radius: 50%;margin-left: -8px;"src="{{url('/images/prof-picture.png')}}">

                        <div class="personalTopInfo"> <span class="username">{{session('token')['username']}}</span> <b class="caret"></b><br>
                            <span class="positionName"><?=$lang['ADMIN']?></span> </div>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right logout" style="min-width: 230px">
                        <li><a href="{{url('profile')}}"><?=$lang['PROFILE']?></a></li>
                        <li><a href="{{url('logout')}}"><?=$lang['LOGOUT']?></a></li>
                    </ul>
                </li>
                <!-- user login dropdown end -->

            </ul>
        </div>
    </header>
    <!--header end-->
    <!--sidebar start-->
    <aside>
        <div id="sidebar" class="nav-collapse">
            <img src="<?=$menuBackground?>" style="position: absolute;width: 394px;height: 1000px;">
            <img src="<?=$sparkleMenu?>" style="position: absolute;width: 450px;height: 1000px;">
            <!-- sidebar menu start-->
            <div class="leftside-navigation">
                <ul class="sidebar-menu" id="nav-accordion">
                    @foreach ($menu as $key=>$value)
                        @if(empty($value['CHILDREN']))
                        <li @if($key==0)class="active"@endif> <a href="{{ url($value['FULLPATH']) }}"> <i class="fa @if(isset($value['ICON'])){{$value['ICON']}}@endif"></i> <span>{{$lang[$value['LANGUAGE_KEY']]}}</span> </a> </li>
                        @else
                            <li><a href="{{ url($value['FULLPATH']) }}"> <i class="fa @if(isset($value['ICON'])){{$value['ICON']}}@endif"></i> <span>{{$lang[$value['LANGUAGE_KEY']]}}</span> </a>
                                <ul class="sub">
                                @foreach ($value['CHILDREN'] as $key2=>$value2)
                                    <li>
                                        <a style="padding-left: 40px;" href="{{ url($value2['FULLPATH']) }}" > <i class="fa @if(isset($value2['ICON'])){{$value2['ICON']}}@endif"></i> <span>{{$lang[$value2['LANGUAGE_KEY']]}}</span> </a>
                                    </li>
                                @endforeach
                                </ul>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
            <!-- sidebar menu end-->
        </div>
    </aside>
    <!--sidebar end-->
    @yield('mainContent') </section>
<!-- Placed js at the end of the document so the pages load faster -->
<!--Core js-->
{{ HTML::script('js/bootstrap-fileupload/bootstrap-fileupload.js') }}
{{ HTML::script('bs3/js/bootstrap.min.js') }}
{{ HTML::script('js/jquery.scrollTo.min.js') }}
<!--common script init for all pages-->
{{ HTML::script('js/scripts.js') }}
<!--script for this page-->

</body>
</html>
