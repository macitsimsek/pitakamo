<?php
$lang = \App\Classes\Language::getLanguageEntries("EN");
?>
@extends('admin.master')
@section('mainContent')
    <style>
        .form-bottom{
            padding-bottom: 7px;
            border-bottom: 1px solid #F3EAEA;
        }
    </style>
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <!--breadcrumbs start -->
                    <ul class="breadcrumb" style="background-color: #FFFFFF;">
                        @foreach($menu['LEVELLING'] as $key=>$value)
                            <li><a style="color: black" href="{{URL::to($value['FULLPATH'])}}">@if($key==0)<i class="fa fa-home"></i>@endif {{ $lang[$value['LANGUAGE_KEY']]}}</a></li>
                        @endforeach
                    </ul>
                    <!--breadcrumbs end -->
                </div>
            </div>
            <section class="panel">
                <div class="panel-body">
                    <div class="pageHeading" style="padding:5px; padding-left: 10px;background-color: #B149DD!important; color: white;font-size: 18px;">{{$lang[$menu['LANGUAGE_KEY']]}}</div>
                </div>
            </section>
            <section class="panel">
                <div class="panel-body">
                    <div class="col-md-3 text-center">
                        <div id="picture"><img src="{{url($userinfos['PICTURE'])}}" style="height: 160px;"></div>
                        <label class="btn btn-primary pitaPurple" style="margin: 5px;" for="picarea">
                            <input id="picarea" type="file" style="display:none;">
                            {{$lang['CHOOSE_PICTURE']}}
                        </label>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <form class="form-horizontal">
                                <fieldset>
                                    <div class="form-group col-md-12" style="border-bottom: 3px solid #B149DD!important;margin-bottom:0px;">
                                        <div class="col-md-6" style="padding: 13px 0px 13px 0px;"><a class="accordion">Personal Information<span class="caret"></span></a></div>
                                        <div class="col-md-6" style="text-align: right;padding-top: 22px;">
                                            @if(isset($userinfos['EDIT']) && Request::query('option')!='PERSONAL')
                                                <a href="{{url(str_replace('{user}','username',$userinfos['EDIT']['FULLPATH']))}}?option=PERSONAL" style="color: #B149DD!important;">
                                                    <span style="margin-right:5px;" class="fa {{$userinfos['EDIT']['ICON']}}"></span>
                                                    {{$lang[$userinfos['EDIT']['LANGUAGE_KEY']]}}</a>
                                            @endif
                                        </div>
                                    </div>
                                    @if(isset($userinfos['EDIT']) && Request::query('option')=='PERSONAL')
                                    <div class="collapseit col-md-12" style="margin-top: 10px;">
                                        @foreach($userinfos['PERSONAL'] as $key=>$value)
                                            @if(!is_array($value))
                                                <div class="form-group form-bottom">
                                                    <label class="col-md-6 control-label" style="text-align: left;" for="textinput">{{$lang[$key]}}</label>
                                                    <div class="col-md-6">
                                                        <input name="{{$key}}" type="text" value="{{$value}}" placeholder="placeholder" class="form-control input-md">
                                                    </div>
                                                </div>
                                            @else
                                                @foreach($value as $key2=>$item)
                                                    <div class="form-group form-bottom">
                                                        <label class="col-md-6 control-label" style="text-align: left;" for="textinput">@if($key2==0) {{$lang[$key]}} @endif</label>
                                                        <div class="col-md-6">
                                                            <input name="{{$key}}" type="text" value="{{$item}}" placeholder="placeholder" class="form-control input-md">
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        @endforeach
                                        <div class="form-group">
                                            <div class="col-md-12" style="text-align: right">
                                                <a href="{{url($back)}}" style="margin-right: 15px;" class="btn btn-primary pitaCancel">CANCEL</a>
                                                <a href="{{url($back)}}" class="btn btn-primary pitaPurple">SUBMIT</a>
                                            </div>
                                        </div>
                                    </div>
                                    @else
                                    <div class="collapseit col-md-12" style="display: none;margin-top: 10px;">
                                        @foreach($userinfos['PERSONAL'] as $key=>$value)
                                            @if(!is_array($value))
                                                <div class="form-group form-bottom">
                                                    <div class="col-md-6 control-label" style="text-align: left;font-weight: bold;">{{$lang[$key]}}</div>
                                                    <div class="col-md-6 control-label" style="text-align: left;">{{$value}}</div>
                                                </div>
                                            @else
                                                @foreach($value as $key2=>$item)
                                                    <div class="form-group form-bottom">
                                                        <div class="col-md-6 control-label" style="text-align: left;font-weight: bold;">@if($key2==0) {{$lang[$key]}} @endif</div>
                                                        <div class="col-md-6 control-label" style="text-align: left;">{{$item}}</div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        @endforeach
                                    </div>
                                    @endif

                                    <div class="form-group col-md-12" style="border-bottom: 3px solid #B149DD!important;margin-bottom:0px;">
                                        <div class="col-md-6" style="padding: 13px 0px 13px 0px;"><a class="accordion">Organizational Information<span class="caret"></span></a></div>
                                        <div class="col-md-6" style="text-align: right;padding-top: 22px;">
                                            @if(isset($userinfos['EDIT']) && Request::query('option')!='ORGANIZANTIONAL')
                                                <a href="{{url(str_replace('{user}','username',$userinfos['EDIT']['FULLPATH']))}}?option=ORGANIZANTIONAL" style="color: #B149DD!important;">
                                                    <span style="margin-right:5px;" class="fa {{$userinfos['EDIT']['ICON']}}"></span>
                                                    {{$lang[$userinfos['EDIT']['LANGUAGE_KEY']]}}</a>
                                            @endif
                                        </div>
                                    </div>

                                    @if(isset($userinfos['EDIT']) && Request::query('option')=='ORGANIZANTIONAL')
                                        <div class="collapseit col-md-12" style="margin-top: 10px;">
                                            @foreach($userinfos['ORGANIZANTIONAL'] as $key=>$value)
                                                @if(!is_array($value))
                                                    <div class="form-group form-bottom">
                                                        <label class="col-md-6 control-label" style="text-align: left;" for="textinput">{{$lang[$key]}}</label>
                                                        <div class="col-md-6">
                                                            <input name="{{$key}}" type="text" value="{{$value}}" placeholder="placeholder" class="form-control input-md">
                                                        </div>
                                                    </div>
                                                @else
                                                    @foreach($value as $key2=>$item)
                                                        <div class="form-group form-bottom">
                                                            <label class="col-md-6 control-label" style="text-align: left;" for="textinput">@if($key2==0) {{$lang[$key]}} @endif</label>
                                                            <div class="col-md-6">
                                                                <input name="{{$key}}" type="text" value="{{$item}}" placeholder="placeholder" class="form-control input-md">
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endif
                                            @endforeach
                                            <div class="form-group">
                                                <div class="col-md-12" style="text-align: right">
                                                    <a href="{{url($back)}}" style="margin-right: 15px;" class="btn btn-primary pitaCancel">CANCEL</a>
                                                    <a href="{{url($back)}}" class="btn btn-primary pitaPurple">SUBMIT</a>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <div class="collapseit col-md-12" style="display: none;margin-top: 10px;">
                                            @foreach($userinfos['ORGANIZANTIONAL'] as $key=>$value)
                                                @if(!is_array($value))
                                                    <div class="form-group form-bottom">
                                                        <div class="col-md-6 control-label" style="text-align: left;font-weight: bold;">{{$lang[$key]}}</div>
                                                        <div class="col-md-6 control-label" style="text-align: left;">{{$value}}</div>
                                                    </div>
                                                @else
                                                    @foreach($value as $key2=>$item)
                                                        <div class="form-group form-bottom">
                                                            <div class="col-md-6 control-label" style="text-align: left;font-weight: bold;">@if($key2==0) {{$lang[$key]}} @endif</div>
                                                            <div class="col-md-6 control-label" style="text-align: left;">{{$item}}</div>
                                                        </div>
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        </div>
                                    @endif

                                    <div class="form-group col-md-12" style="border-bottom: 3px solid #B149DD!important;margin-bottom:0px;">
                                        <div class="col-md-6" style="padding: 13px 0px 13px 0px;"><a class="accordion">Permissions<span class="caret"></span></a></div>
                                        <div class="col-md-6" style="text-align: right;padding-top: 22px;">
                                            @if(isset($userinfos['EDIT']) && Request::query('option')!='PERMISSIONS')
                                                <a href="{{url(str_replace('{user}','username',$userinfos['EDIT']['FULLPATH']))}}?option=PERMISSIONS" style="color: #B149DD!important;">
                                                    <span style="margin-right:5px;" class="fa {{$userinfos['EDIT']['ICON']}}"></span>
                                                    {{$lang[$userinfos['EDIT']['LANGUAGE_KEY']]}}</a>
                                            @endif
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </section>
    </section>
    <script>
        $('.accordion').click(function () {
            if($(this).parent().parent().next().hasClass('collapseit')){$(this).parent().parent().next().toggle();}
        });

        $("#picarea").change(function(e) {

            for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {

                var file = e.originalEvent.srcElement.files[i];

                var img = document.createElement("img");
                var reader = new FileReader();
                reader.onloadend = function() {
                    img.src = reader.result;
                    img.width = 160;
                    img.className = 'img-circle';
                }
                reader.readAsDataURL(file);
                $("#picture").html(img);
            }
        });
    </script>
    <!--main content end-->
@stop