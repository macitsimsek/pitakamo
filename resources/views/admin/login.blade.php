<?php $lang = \App\Classes\Language::getLanguageEntries("EN");?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="{{URL::to('/')}}<?php echo "/images/favicon.ico?".time();?>" >
<title>Pitakamo - Login</title>


  <!-- BOOTSTRAP CSS -->
{{ HTML::style('bs3/css/bootstrap.min.css') }}
{{ HTML::style('css/bootstrap-reset.css') }}
{{ HTML::style('css/style.css') }}
{{ HTML::style('font-awesome/css/font-awesome.css') }}
{{ HTML::style('css/style-responsive.css') }}
{{ HTML::style('css/main.css') }}
<!-- DATATABLE CSS -->
{{ HTML::style('datatable/css/jquery.dataTables.css') }}
<!-- Scripts are placed here -->
{{ HTML::script('js/jquery-1.11.1.min.js') }}
{{ HTML::script('js/bootstrap.min.js') }}
<!-- DATATABLE JS -->
  {{ HTML::script('datatable/js/jquery.dataTables.min.js') }}
  {{ HTML::script('datatable/js/datatables.fnReloadAjax.js') }}
  {{ HTML::script('datatable/js/datatables-bootstrap.js') }}
</head>

<body style="background-image: url('/images/background.jpg')">
<div class="container">

    <?php
    $logoJPGUrl = URL::to('/') ."/images/logo.jpg";
    $logoPNGUrl = URL::to('/') ."/images/logo.png";

    $logoJPG = public_path().DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."logo.jpg";
    $logoPNG = public_path().DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."logo.png";


    if( !file_exists( $logoJPG) ){
        $logo =$logoPNGUrl;
    }else{
        if( file_exists( $logoPNG) && filemtime($logoPNG)>filemtime($logoJPG) ) {
            $logo = $logoPNGUrl;
        }else{
            $logo = $logoJPGUrl;
        }
    }

    ?>
  {{Form::open(array('class'=>'form-signin'))}}
    @if(Session::has('error'))
        <div id="error" class="alert alert-danger" style="text-align: center;"> {{Session::pull('error')}} </div>
    @endif
  <a href="#" class="loginLogo" ><img src="<?php echo $logo."?".time();  ?>" alt="logo" style="padding: 5px;"/></a>
    <div class="form-group">
        <div class="selectContainer">
            <select class="form-control" name="type" style="text-align: center;font-size: 20px;text-align-last: center;background-color: #01aa02;color: white!important;">
                <option value="" disabled selected><?=$lang['CHOOSE_A_PORTAL']?></option>
                <option value="head"><?=$lang['HEAD_OFFICE_PORTAL']?></option>
                <option value="franchise"><?=$lang['FRANCHISE_PORTAL']?></option>
                <option value="subfranchise"><?=$lang['SUBFRANCHISE_PORTAL']?></option>
            </select>
        </div>
    </div>
  <div class="login-wrap" style="display: none;">
    <div class="user-login-info">
        {{Form::hidden('redirect',isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:'') }}
        {{Form::text('username',null, array('class' => 'form-control','style' => 'font-size: 15px;','autofocus' => '','placeholder'=> $lang['USERNAME'],'required'=>'required' )) }}
        {{Form::password('password', array('class' => 'form-control','style' => 'font-size: 15px;','placeholder'=> $lang['PASSWORD'],'required'=>'required' ))}} </div>
        {{Form::button( $lang['LOGIN'] , array('id'=>'loginBtn','class' => 'btn btn-lg btn-login btn-block',"style"=>"background-color: #9d1bd5;font-weight: bold;"))}}
  </div>
    {{Form::close()}}
</div>
<script>
    $("select.form-control").change(function () {
        $(".login-wrap").show();
    });
    $('#loginBtn').click(function () {
        $.post($('form').attr('action'),$('form').serialize(),function (data) {
            if(data['code']==200){
                $('.loginLogo').before(message(data['code'],data['message']));
                location.href = data['url'];
            }else{
                $('.loginLogo').before(message(data['code'],data['message']));
            }
        })
    });
    function  message(code,message) {
        $('#error').remove();
        var type = '';
        if(code==200){
            type = 'success'
        }else if(code==400){
            type = 'danger'
        }
        return '<div id="error" style="text-align: center;" class="alert alert-'+type+'"> '+message+' </div>';

    }

</script>

<!-- Placed js at the end of the document so the pages load faster -->

<!--Core js-->
{{ HTML::script('js/jquery.js') }}
{{ HTML::script('bs3/js/bootstrap.min.js') }}
</body>
</html>
