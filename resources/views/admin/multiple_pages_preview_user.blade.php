<?php
$lang = \App\Classes\Language::getLanguageEntries("EN");
?>
@extends('admin.master')
@section('mainContent')
    <style>
        .dataTables_length { padding-top: 68px!important; }
        #example th:first-child {
            text-align: center;
        }

        .form-bottom{
            padding-bottom: 15px;
            border-bottom: 1px solid #E2DCDC;
        }

    </style>
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <!--breadcrumbs start -->
                    <ul class="breadcrumb" style="background-color: #FFFFFF;">
                        @foreach($menu['LEVELLING'] as $key=>$value)
                            <li><a style="color: black" href="{{URL::to($value['FULLPATH'])}}">@if($key==0)<i class="fa fa-home"></i>@endif {{ $lang[$value['LANGUAGE_KEY']]}}</a></li>
                        @endforeach
                    </ul>
                    <!--breadcrumbs end -->
                </div>
            </div>

            <section class="panel">
                <div class="panel-body">
                    <div class="pageHeading" style="padding:5px; padding-left: 10px;background-color: #B149DD!important; color: white;font-size: 18px;">{{$lang[$menu['LANGUAGE_KEY']]}}</div>
                </div>
            </section>
            <section class="panel">
                <div class="panel-body">
                    <div class="col-md-3 text-center">
                        <div id="picture"><img src="{{url($userinfos['PICTURE'])}}" style="height: 160px;"></div>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <form class="form-horizontal">
                                <fieldset>
                                    <div class="form-group col-md-12" style="border-bottom: 3px solid #B149DD!important;margin-bottom:0px;">
                                        <div class="col-md-6" style="padding: 13px 0px 13px 0px;"><a class="accordion">Personal Information<span class="caret"></span></a></div>
                                        <div class="col-md-6" style="text-align: right;padding-top: 22px;">
                                            @if(isset($userinfos['EDIT']) && Request::query('option')!='PERSONAL')
                                                <a href="{{url(str_replace('{user}','username',$userinfos['EDIT']['FULLPATH']))}}?option=PERSONAL" style="color: #B149DD!important;">
                                                    <span style="margin-right:5px;" class="fa {{$userinfos['EDIT']['ICON']}}"></span>
                                                    {{$lang[$userinfos['EDIT']['LANGUAGE_KEY']]}}</a>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-12 collapseit" style="display: none;margin-top: 10px;">
                                        @foreach($userinfos['PERSONAL'] as $key=>$value)
                                            @if(!is_array($value))
                                                <div class="form-group form-bottom">
                                                    <div class="col-md-6 control-label" style="text-align: left;font-weight: bold;">{{$lang[$key]}}</div>
                                                    <div class="col-md-6 control-label" style="text-align: left;">{{$value}}</div>
                                                </div>
                                            @else
                                                @foreach($value as $key2=>$item)
                                                    <div class="form-group form-bottom">
                                                        <div class="col-md-6 control-label" style="text-align: left;font-weight: bold;">@if($key2==0) {{$lang[$key]}} @endif</div>
                                                        <div class="col-md-6 control-label" style="text-align: left;">{{$item}}</div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        @endforeach
                                        <div style="text-align: right;">
                                            <a class="btn" style="background-color: #B149DD!important;text-align: right;color:white;" href="{{url($back)}}">BACK</a>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12" style="border-bottom: 3px solid #B149DD!important;margin-bottom:0px;">
                                        <div class="col-md-6" style="padding: 13px 0px 13px 0px;"><a class="accordion">Organizational Information<span class="caret"></span></a></div>
                                        <div class="col-md-6" style="text-align: right;padding-top: 22px;">
                                            @if(isset($userinfos['EDIT']) && Request::query('option')!='ORGANIZANTIONAL')
                                                <a href="{{url(str_replace('{user}','username',$userinfos['EDIT']['FULLPATH']))}}?option=ORGANIZANTIONAL" style="color: #B149DD!important;">
                                                    <span style="margin-right:5px;" class="fa {{$userinfos['EDIT']['ICON']}}"></span>
                                                    {{$lang[$userinfos['EDIT']['LANGUAGE_KEY']]}}</a>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-12 collapseit" style="display: none;margin-top: 10px;">
                                        @foreach($userinfos['ORGANIZANTIONAL'] as $key=>$value)
                                            @if(!is_array($value))
                                                <div class="form-group form-bottom">
                                                    <div class="col-md-6 control-label" style="text-align: left;font-weight: bold;">{{$lang[$key]}}</div>
                                                    <div class="col-md-6 control-label" style="text-align: left;">{{$value}}</div>
                                                </div>
                                            @else
                                                @foreach($value as $key2=>$item)
                                                    <div class="form-group form-bottom">
                                                        <div class="col-md-6 control-label" style="text-align: left;font-weight: bold;">@if($key2==0) {{$lang[$key]}} @endif</div>
                                                        <div class="col-md-6 control-label" style="text-align: left;">{{$item}}</div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        @endforeach
                                        <div style="text-align: right;">
                                            <a class="btn" style="background-color: #B149DD!important;text-align: right;color:white;" href="{{url($back)}}">BACK</a>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12" style="border-bottom: 3px solid #B149DD!important;margin-bottom:0px;">
                                        <div class="col-md-6" style="padding: 13px 0px 13px 0px;"><a class="accordion">Permissions<span class="caret"></span></a></div>
                                        <div class="col-md-6" style="text-align: right;padding-top: 22px;">
                                            @if(isset($userinfos['EDIT']) && Request::query('option')!='PERMISSIONS')
                                                <a href="{{url(str_replace('{user}','username',$userinfos['EDIT']['FULLPATH']))}}?option=PERMISSIONS" style="color: #B149DD!important;">
                                                    <span style="margin-right:5px;" class="fa {{$userinfos['EDIT']['ICON']}}"></span>
                                                    {{$lang[$userinfos['EDIT']['LANGUAGE_KEY']]}}</a>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-12 collapseit" style="display: none;margin-top: 10px;">
                                        @foreach ($core as $key=>$value)
                                            @if($value['LANGUAGE_KEY']!='DASHBOARD')
                                                @if(!isset($value['BUTTONS']) )
                                                    <button type="button" class="second"><span style="margin-right: 20px;" class="fa fa-plus"></span>{{$lang[$value['LANGUAGE_KEY']]}}</button>
                                                @else
                                                    <button type="button" class="second"><span style="margin-right: 20px;" class="fa fa-plus"></span>{{$lang[$value['LANGUAGE_KEY']]}}</button>
                                                    <div class="collapseit col-md-12" style="margin-left: 70px;display: none">
                                                        @if(isset($value['BUTTONS']['FIRST']))
                                                            @foreach($value['BUTTONS']['FIRST'] as $item)
                                                                <div>{{$lang[$item['LANGUAGE_KEY']]}}</div>
                                                            @endforeach
                                                        @endif
                                                        @if(isset($value['BUTTONS']['SECOND']))
                                                            @foreach($value['BUTTONS']['SECOND'] as $item)
                                                                <div>{{$lang[$item['LANGUAGE_KEY']]}}</div>
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                @endif
                                            @endif
                                        @endforeach
                                        <div style="text-align: right;">
                                            <a class="btn" style="background-color: #B149DD!important;text-align: right;color:white;" href="{{url($back)}}">BACK</a>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </section>
    </section>

    <script>
        $('.accordion').click(function () {
            if($(this).parent().parent().next().hasClass('collapseit')){$(this).parent().parent().next().toggle();}
        });

        $('.second').click(function () {
            if($(this).next().hasClass('collapseit')){$(this).next().toggle();}
            if($(this).find('span').hasClass('fa-plus')){
                $(this).find('span').removeClass('fa-plus').addClass('fa-minus');
            }else{
                $(this).find('span').removeClass('fa-minus').addClass('fa-plus');
            }
        });
    </script>
    <!--main content end-->
@stop