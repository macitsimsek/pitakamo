﻿function clog(msg) {
    console.log(msg);
}
function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};
//Start of Page Reload for Debug
//!!!Comment out on production version
//var window_focus = true;
//var autoReload = true;
//var intervalVal = 0;
//$(window).focus(function () {
//    window_focus = true;
//    clearInterval(intervalVal);
//})
//.blur(function () {
//    window_focus = false;
//    intervalVal = setInterval(function () {
//        if (autoReload) {
//            window.location.reload();
//        }
//    }, 5000);
//});

//End of Page Reload for Debug

//HANDLEARS HELPERS
Handlebars.registerHelper('TableWidgetColorClass', function (DESCRIPTION, wType) {
    if (wType && (wType == "cardBlackList" | wType == "cardWhiteList")) {
        return tableWidgetWBListRowColorClassMap[DESCRIPTION];
    } else if (wType && wType == "initializationCodes") {
        return tableWidgetInitCodesRowColorClassMap[DESCRIPTION];
    } else if (wType && wType == "procurementOrders") {
        return tableWidgetProcurementOrdersRowColorClassMap[DESCRIPTION];
    }
    return tableWidgetRowColorClassMap[DESCRIPTION];
});

Handlebars.registerHelper('TableWidgetIconClass', function (DESCRIPTION,wType) {
    if (wType && (wType == "cardBlackList" | wType == "cardWhiteList")) {
        return tableWidgetWBListRowIconClassMap[DESCRIPTION];
    } else if (wType && wType == "initializationCodes") {
        return tableWidgetInitCodesRowIconClassMap[DESCRIPTION];
    } else if (wType && wType == "procurementOrders") {
        return tableWidgetProcurementOrdersRowIconClassMap[DESCRIPTION];
    }
    return tableWidgetRowIconClassMap[DESCRIPTION];
});



Handlebars.registerHelper('DateAgo', function (date) {
    var isNegative,day, hour, min;
    min = Math.round((new Date() - new Date(date)) / 60000);

    if (min< 0) {
        isNegative = true;
        min = -min;
    }
    if (min > 59) {
        hour = Math.round(min / 60);
        min = min % 60;
    }
    if (hour > 23) {
        day = Math.round(hour / 24);
        hour = hour % 24;
    }
    var result = "";
    if (day > 2) {
        result = (day ? day + "d" : "") + (isNegative ? " ahead" : " ago");
    }else if( hour > 2 ){
        result = (hour ? hour + "h" : "") + (isNegative ? " ahead" : " ago");
    } else if (min > 2) {
        result = (min ? min + "min" : "") + (isNegative ? " ahead" : " ago");
    }
    return result;

    //return (day ? day + "d" : "") + (hour ? hour + "h" : "") + (min ? min + "min" : "") + (isNegative ? " ahead" : " ago");

});

Handlebars.registerHelper('widgetDisplay', function (_display) {
    if (_display) {
        return "display:none";
    }
    return "";
});
Handlebars.registerHelper('ifInArraySelect', function (_fields, _field) {
    if (!Array.isArray(_fields)) {
        return "";
    }
    if (_fields.filter(function (a) { return a == _field }).length > 0) {
        return "selected";
    }
    return "";
});
Handlebars.registerHelper('ifEqualSelect', function (_value1, _value2) {
    if (_value1.toString()==_value2.toString()) {
        return "selected";
    }
    return "";
});
Handlebars.registerHelper('isAdmin', function (options) {

    if ($.cookie('role') === 'admin') {
        return options.fn(this);
    }
    return options.inverse(this);
});
$(function () {
    Handlebars.registerPartial("settingsHeaderName", $("#settingsHeaderName-partial").html());
    Handlebars.registerPartial("settingsIntervalSelect", $("#settingsIntervalSelect-partial").html());
    Handlebars.registerPartial("settingsSubmitButtons", $("#settingsSubmitButtons-partial").html());

})

// END OF HANDLEARS HELPERS


//ENUMS

var tableWidgetRowIconClassMap = {
    "Pending": "icon-pending",
    "Work in Progress": "icon-progress",
    "Processed": "icon-processed"
}
var tableWidgetRowColorClassMap = {
    "Pending": "alert-danger",
    "Work in Progress": "alert-info",
    "Processed": "alert-success"
}

var tableWidgetWBListRowIconClassMap = {
    "PENDING EXECUTION ON CARD": "icon-pending",
    "EXECUTED ON CARD": "icon-progress",
    "REVOKED": "icon-processed"
}
var tableWidgetWBListRowColorClassMap = {
    "PENDING EXECUTION ON CARD": "alert-info",
    "EXECUTED ON CARD": "alert-success",
    "REVOKED": "alert-danger"
}

var tableWidgetInitCodesRowIconClassMap = {
    "ENABLED": "icon-processed",
    "DISABLED": "icon-pending",
}
var tableWidgetInitCodesRowColorClassMap = {
    "ENABLED": "alert-info",
    "DISABLED": "alert-danger"
}

var tableWidgetProcurementOrdersRowIconClassMap = {
    "PENDING DELIVERY": "icon-pending",
    "DELIVERED": "icon-processed",
    "CANCELLED": "icon-progress"
}
var tableWidgetProcurementOrdersRowColorClassMap = {
    "PENDING DELIVERY": "alert-info",
    "DELIVERED": "alert-success",
    "CANCELLED": "alert-danger"
}

//END OF ENUMS
