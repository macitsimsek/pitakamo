﻿/// <reference path="../helpers.js" />
/// <reference path="../handlebars-v2.0.0.js" />
/// <reference path="../service.js" />
/// <reference path="../controller.js" />

var latestDeviceApplicationVersionWidget = function (opt) {

    //START OF PRIVATES
    var data;
    var wData, targetPortlet;
    var intervalFunction = 0;
    var pageNum = 1;
    var lastUpdate = new Date();
    var editPage = true;
    //END OF PRIVATES


    //Returns global sam code data
    // if reload refresh data from service
    function getDataFromService(callback) {
        wc.service.getlatestDeviceApplicationVersion(callback);
    }

    //refresh sam code data from service
    //and renders widget
    function refreshWidget() {
        renderLoading();
        setTimeout(function () {
            getDataFromService(function (_data) {
                data = _data;
                renderData();
                setIntervalRefresh();
            });
        }, 100);
    }
    function hasBUID() {
        return wData.BUID != "";
    }
    function setIntervalRefresh() {
        if (intervalFunction !== 0) {
            clearInterval(intervalFunction);
        }
        if (isNumber(wData.SETTINGS.intervalMinutes) && wData.SETTINGS.intervalMinutes > 0) {
            intervalFunction = setInterval(refreshWidget, wData.SETTINGS.intervalMinutes * 60 * 1000);
        }

    }
    function renderSettings() {
        var source = $("#" + wData.TYPE + "SettingsTemplate").html();
        var template = Handlebars.compile(source);
        var html = template(wData);
        $(".widget[widgetType=" + wData.TYPE + "]").append(html);
    }
    function renderLoading() {
        $(".widget[widgetType=" + wData.TYPE + "] .portlet-content").html('<h2 class="text-center"><img src="/js/file-uploader/img/loading.gif"/></h2>');

    }
    //renders widget content
    function renderData() {
        if (!hasBUID()) {
            return;
        }
        lastUpdate = new Date();
        var source = $("#" + wData.TYPE + "ContentTemplate").html();

        var template = Handlebars.compile(source);

        var $widget = $(".widget[widgetType=" + wData.TYPE + "]");
        if (data != undefined) {

            wData.data = data;
            var html = template(wData);
            $widget.find('.portlet-content').html(html);
            $widget.find('.widget-update-time-span').html('0 min');

        } else {
            $widget.find('.portlet-content').html('<h2 class="text-center">No Content!</h2>');
        }
    }
    function initEvents() {
        var $widget = $(".widget[widgetType=" + wData.TYPE + "]");
        if (editPage) {
            $widget.find('.widgetSettingSubmitBtn').click(function () {
                var data = $widget.find('.widgetSettingPanel form').serializeObject();

                wData.SETTINGS = $.extend(wData.SETTINGS, data);
                widgets.updateWidget(wData);
                refreshWidget();

                $widget.find('.portlet-header span.widget-header-name-span').html(wData.SETTINGS.header);
                $widget.find('.widgetSettingPanelOverlay').fadeOut(10);

            });

            $widget.find('.closeWidgetSettingBtn').click(function () {
                $widget.find(".widgetSettingPanelOverlay").fadeOut(10);
                if (!hasBUID()) {
                    removeWidget();
                }
            });
        }

        if ($widget.find('.widget-update-time-span').length > 0) {

            setInterval(function () {
                var diff = new Date().getMinutes() - lastUpdate.getMinutes();
                if (diff < 0) {
                    diff += 60;
                }

                $widget.find('.widget-update-time-span').html(diff + ' min');
            }, 60 * 1000)

        }

    }
    function removeWidget() {
        var $widget = $(".widget[widgetType=" + wData.TYPE + "]");
        $widget.find('.portlet-content').hide();
        $widget.find('.widgetSettingPopup').hide();
        $('#removed-portlet-panel').append($widget);
        wData.SETTINGS.display = false;
        widgets.updateWidget(wData);

    }
    return {
        init: function (_widgetTemplate, _wData, _editPage) {
            wData = _wData;
            editPage = _editPage;

            if (wData.BUID == "" || wData.SETTINGS.display == false) {
                var $panel = $('#removed-portlet-panel');
                $panel.append(_widgetTemplate);
            } else {
                var $panel = $('#main-portlet-panel .dragPortlet').eq(wData.SETTINGS.portletOrder);
                $panel.append(_widgetTemplate);

                refreshWidget();
            }

            var $widget = $(".widget[widgetType=" + wData.TYPE + "]");
            var $portlet = $widget.closest('.portlet');
            $portlet.addClass("appWidget");
            $portlet.addClass("releaseWidget");

            if (editPage) {
                renderSettings();
            }
            initEvents();

            //renderWidget();
        },
        refresh: function () {
            refreshWidget();
        },
        onDrag: function (o) {
            var $widget = $(".widget[widgetType=" + wData.TYPE + "]");
            var $portlet = $widget.closest('.portlet');
            var $dragPortlet = $widget.closest('.dragPortlet');
            if ($dragPortlet.attr('id') == "removed-portlet-panel") {
                wData.SETTINGS.portletOrder = 0;
                wData.SETTINGS.widgetOrder = $dragPortlet.find('.portlet').index($portlet);
                wData.SETTINGS.display = false;
            } else {

                wData.SETTINGS.portletOrder = $('#main-portlet-panel .dragPortlet').index($dragPortlet);
                wData.SETTINGS.widgetOrder = $dragPortlet.find('.portlet').index($portlet);
                wData.SETTINGS.display = true;
            }
            widgets.updateWidget(wData);
            if (o.sender._id() == "removed-portlet-panel") {
                if (!hasBUID()) {
                    $widget.find('.widgetSettingPanelOverlay').fadeIn(50);
                } else {

                }
            }
        },
        removeWidget: function () {
            removeWidget();
        }
    }
}