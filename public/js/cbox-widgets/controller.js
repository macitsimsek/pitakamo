﻿/// <reference path="CBoxService.js" />
/// <reference path="widgetModules/SamIssueCodesWidget.js" />
var widgetController = (function () {
    
    var _widgetService
        , _samIssueCodesWidget
        , _rfIssueCodesWidget
        , _initDeviceListWidget
        , _faultySamIssueList
        , _cardBlackList
        , _cardWhiteList
        , _initializationCodes
        , _procurementOrders
        , _initializedDeviceList
        , _latestDeviceApplicationVersion
        , _allocatedRfCardIds
        , _allocatedSamCardIds
        , _cardEncoding
        , _problematicDeviceTransmission
        , _capLoading
    ;

    var user = $.cookie('username');
    var role = $.cookie('role');
    var buid = $.cookie('buid');
    var widgetList = $.cookie('widget');
    var modules= {
            samIssueCode: function () {
                if (_samIssueCodesWidget == undefined) {
                    _samIssueCodesWidget = tableWidget();
                }
                return _samIssueCodesWidget;
            },
            rfIssueCode: function () {
                if (_rfIssueCodesWidget == undefined) {
                    _rfIssueCodesWidget = tableWidget();
                }
                return _rfIssueCodesWidget;
            },
            initDeviceList: function () {
                if (_initDeviceListWidget == undefined) {
                    _initDeviceListWidget = tableWidget();
                }
                return _initDeviceListWidget;
            },
            faultySamIssueList: function () {
                if (_faultySamIssueList == undefined) {
                    _faultySamIssueList = tableWidget();
                }
                return _faultySamIssueList;
            },
            cardBlackList: function () {
                if (_cardBlackList == undefined) {
                    _cardBlackList = tableWidget();
                }
                return _cardBlackList;
            },
            cardWhiteList: function () {
                if (_cardWhiteList == undefined) {
                    _cardWhiteList = tableWidget();
                }
                return _cardWhiteList;
            },
            initializationCodes: function () {
                if (_initializationCodes == undefined) {
                    _initializationCodes = tableWidget();
                }
                return _initializationCodes;
            },
            procurementOrders: function () {
                if (_procurementOrders == undefined) {
                    _procurementOrders = tableWidget();
                }
                return _procurementOrders;
            },
            initializedDeviceList: function () {
                if (_initializedDeviceList == undefined) {
                    _initializedDeviceList = tableWidget();
                }
                return _initializedDeviceList;
            },
            latestDeviceApplicationVersion: function myfunction() {
                if (_latestDeviceApplicationVersion == undefined) {
                    _latestDeviceApplicationVersion = latestDeviceApplicationVersionWidget();
                }
                return _latestDeviceApplicationVersion;
            },
            allocatedRfCardIds: function () {
                if (_allocatedRfCardIds == undefined) {
                    _allocatedRfCardIds = radioSelectWidget();
                }
                return _allocatedRfCardIds;
            },
            allocatedSamCardIds: function () {
                if (_allocatedSamCardIds == undefined) {
                    _allocatedSamCardIds = radioSelectWidget();
                }
                return _allocatedSamCardIds;
            },
            cardEncoding: function () {
                if (_cardEncoding  == undefined) {
                    _cardEncoding = tableWidget();
                }
                return _cardEncoding;
            },
            problematicDeviceTransmission: function () {
                if (_problematicDeviceTransmission == undefined) {
                    _problematicDeviceTransmission = tableWidget();
                }
                return _problematicDeviceTransmission;
            },
            capLoading: function () {
                if (_capLoading == undefined) {
                    _capLoading = tableWidget();
                }
                return _capLoading;
            }

        }
    return {
        service: function () {
            if (_widgetService == undefined) {
                _widgetService = widgetService();
            }
            return _widgetService;
        }(),
        getModule:function (type) {
            return modules[type]();
        },
        user: function () {
            return user;
        }(),
        role: function () {
            return role;
        }(),
        widgets: function () {
            if (widgetList === undefined || widgetList == "") {
                return [];
            }
            return widgetList.split('-');
        }(),
        isAdmin: function () {
            return role == "admin";
        }(),
        buid: function () {
            return parseInt( buid );
        }()
        
    }
})
var wc = widgetController();
//var wc;
//$(function () {
//    wc = widgetController();
//})
