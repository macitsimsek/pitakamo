﻿
var widgetService = (function () {

    //var widgetsService = "";
    //var samIssueCodeServiceUrl =        "http://54.187.68.215:8888/CboxAppManager/webapi/buid-sam-issue-codes-reqs.json";
    //var faultySamIssueListServiceUrl =  "http://54.187.68.215:8888/CboxWeb/webapi/proc/incomplete-sam-issue";
    //var rfIssueCodeServiceUrl =         "http://54.187.68.215:8888/CboxAppManager/webapi/buid-rf-issue-codes-reqs.json";
    //var initDeviceListServiceUrl =      "http://54.187.68.215:8888/CboxAppManager/webapi/init-devices.json";
    var getWidgetsServiceUrl =          "http://54.187.68.215:8888/CboxAppManager/webapi/get-table-widgets-settings.json";
    var postWidgetsServiceUrl =         "http://54.187.68.215:8888/CboxAppManager/webapi/edit-table-widgets-settings.json";

    var tableWidgetUrls ={
        samIssueCode                    : "http://54.187.68.215:8888/CboxAppManager/webapi/buid-sam-issue-codes-reqs.json",
        faultySamIssueList              : "http://54.187.68.215:8888/CboxWeb/webapi/proc/incomplete-sam-issue.json",
        rfIssueCode                     : "http://54.187.68.215:8888/CboxAppManager/webapi/buid-rf-issue-codes-reqs.json",
        initDeviceList                  : "http://54.187.68.215:8888/CboxAppManager/webapi/init-devices.json",
        cardBlackList                   : "http://54.187.68.215:8888/CboxAppManager/webapi/get-buid-blacklist-records.json",
        cardWhiteList                   : "http://54.187.68.215:8888/CboxAppManager/webapi/get-buid-whitelist-records.json",
        initializationCodes             : "http://54.187.68.215:8888/CboxAppManager/webapi/get-buid-init-codes.json",
        procurementOrders               : "http://54.187.68.215:8888/CboxAppManager/webapi/get-buid-orders-info.json",
        initializedDeviceList           : "http://54.187.68.215:8888/CboxAppManager/webapi/get-buid-initialized-devices.json",
        latestDeviceApplicationVersion  : "http://54.187.68.215:8888/CboxAppManager/webapi/get-device-app-updates.json",

        allocatedCardIdsWidgetApplications: "http://54.187.68.215:8888/CboxWeb/webapi/proc/cbox-device-applications",
        allocatedCardIdsWidgetIds: "http://54.187.68.215:8888/CboxWeb/webapi/proc/max-rf-seq",

        cardEncoding: "http://54.187.68.215:8888/CboxWeb/webapi/proc/buid-card-encoding-reqs",
        problematicDeviceTransmission: "http://54.187.68.215:8888/CboxAppManager/webapi/faulty-device-transmission.json",
        capLoading: "http://54.187.68.215:8888/CboxWeb/webapi/cap/get-cap-request-records"
    }
    var radioSelectWidgetData = {
        allocatedRfCardIds: "http://54.187.68.215:8888/CboxWeb/webapi/proc/cbox-device-applications",
        allocatedSamCardIds: "http://54.187.68.215:8888/CboxWeb/webapi/proc/sam-types"
    }
    var radioSelectWidgetDataIds = {
        allocatedRfCardIds: "http://54.187.68.215:8888/CboxWeb/webapi/proc/max-rf-seq",
        allocatedSamCardIds: "http://54.187.68.215:8888/CboxWeb/webapi/proc/max-sam-seq"
    }

    function getDataFromService(url, params,callback) {
        var returnData;
        var _async = callback != undefined;

        $.ajax({
            async: false,
            type: "GET",
            url: url,
            data: params,
            success: function (_data) {
                if (_data.status == "OK") {
                    returnData = _data.data;
                } else {
                    //alert('service data status is not "OK". Check data from console! ' + _data.developerMessage);
                    console.log(_data);
                    returnData = {}
                }
                if (_async) {
                    callback(returnData);
                }
            }
        });
        return returnData;
    }
    function postDataToService(url, params) {
        var returnData;
        $.ajax({
            contentType: 'application/json',
            accept: 'application/json',
            async: false,
            type: "POST",
            url: url,
            data:JSON.stringify(params),
            success: function (_data) {
                if (_data.status == "OK") {
                    returnData = _data.data;
                } else {
                    alert('service data status is not "OK". Check data from console!');
                    console.log(_data);
                    returnData = {}
                }
            }
        });
        return returnData;
    }
    function getWidgets(user) {
        var params = {
            p_username: user
        }
        return getDataFromService(getWidgetsServiceUrl,params);
    }

    //RETURN PUBLIC FUNCTIONS
    return {
        getWidgets:function (user) {
            return getWidgets(user);
        },
        postWidgets:function (data) {
            postDataToService(postWidgetsServiceUrl, data);
        },
        getTableWidgetData: function (type, BUID,callback) {
            var params = {
                p_buid: BUID
            }
            return getDataFromService(tableWidgetUrls[type], params, callback);
        },
        getlatestDeviceApplicationVersion: function (callback) {
            return getDataFromService(tableWidgetUrls["latestDeviceApplicationVersion"],undefined,callback);
        },
        getradioSelectWidgetData: function (type, BUID,callback) {
            var params = {
                p_buid: BUID
            }
            return getDataFromService(radioSelectWidgetData[type], params, callback);
        },
        getradioSelectWidgetIds: function (type, BUID, AppId, DataParamName) {
            var params = {
                p_buid: BUID
            }

            params[DataParamName] = AppId;

            return getDataFromService(radioSelectWidgetDataIds[type], params);
        }
        
        //getSamIssueCode: function (BUID) {
        //    var params = {
        //        p_buid: BUID
        //    }
        //    return getDataFromService(samIssueCodeServiceUrl,params);
        //},
        //getRfIssueCode: function (BUID) {
        //    var params = {
        //        p_buid: BUID
        //    }
        //    return getDataFromService(rfIssueCodeServiceUrl,params);
        //},
        //getInitDeviceList: function (BUID) {
        //    var params = {
        //        p_buid: BUID
        //    }
        //    return getDataFromService(initDeviceListServiceUrl, params);
        //}
    }

});

