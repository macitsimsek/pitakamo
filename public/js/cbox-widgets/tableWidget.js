﻿/// <reference path="../helpers.js" />
/// <reference path="../handlebars-v2.0.0.js" />
/// <reference path="../service.js" />
/// <reference path="../controller.js" />

var tableWidget = function (opt) {

    //START OF PRIVATES
    var data;
    var wData, targetPortlet;
    var intervalFunction = 0;
    var pageNum = 1;
    var lastUpdate = new Date();
    var editPage = true;
    //END OF PRIVATES


    //Returns global sam code data
    // if reload refresh data from service
    function getDataFromService(callback) {
        wc.service.getTableWidgetData(wData.TYPE,wData.BUID,callback);
    }

    //refresh sam code data from service
    //and renders widget
    function refreshWidget() {
        renderLoading();
        setTimeout(function () {
            getDataFromService(function (_data) {
                data = _data;
                renderData();
                setIntervalRefresh();
            });
        }, 100);
    }
    function hasBUID() {
        return wData.BUID != "";
    }
    function setIntervalRefresh() {
        if (intervalFunction !== 0) {
            clearInterval(intervalFunction);
        }
        if (isNumber(wData.SETTINGS.intervalMinutes) && wData.SETTINGS.intervalMinutes > 0) {
            intervalFunction = setInterval(refreshWidget, wData.SETTINGS.intervalMinutes * 60 * 1000);
        }

    }
    function renderSettings() {
        var source = $("#" + wData.TYPE + "SettingsTemplate").html();
        var template = Handlebars.compile(source);
        var html = template(wData);
        $(".widget[widgetType=" + wData.TYPE + "]").append(html);
    }
    function renderLoading() {
        $(".widget[widgetType=" + wData.TYPE + "] .portlet-content").html('<h2 class="text-center"><img src="/js/file-uploader/img/loading.gif"/></h2>');
        
    }
    //renders widget content
    function renderData() {
        if (!hasBUID()) {
            return;
        }
        lastUpdate = new Date();
        var source = $("#" + wData.TYPE + "ContentTemplate").html();
        var sourcePagination = $("#tablePagination").html();
        var template = Handlebars.compile(source);
        var templatePagination = Handlebars.compile(sourcePagination);

        var $widget = $(".widget[widgetType=" + wData.TYPE + "]");
        if (data != undefined) {
            var slicedData = data;
            if (slicedData != undefined) {
                slicedData = slicedData.slice(wData.SETTINGS.numberOfResults*(pageNum-1), wData.SETTINGS.numberOfResults*pageNum);
            }
            wData.data = slicedData;
            var html = template(wData);
            $widget.find('.portlet-content').html(html);
            $widget.find('.widget-update-time-span').html('0 min');
            var pageCount = Math.ceil(data.length / wData.SETTINGS.numberOfResults);
            if (pageCount > 1) {
                var pageStart = 1;
                var pageEnd = pageCount;

                if (pageCount > 5) {
                    pageStart = pageNum - 2;
                    pageEnd = pageNum + 2;
                    if (pageStart < 1) {
                        pageEnd = pageEnd - pageStart + 1;
                        pageStart = 1;
                    } else if (pageEnd > pageCount) {
                        pageStart = pageStart - (pageEnd - pageCount);
                        pageEnd = pageCount;
                    }
                }

                var pages = [];
                for (var i = pageStart; i <= pageEnd; i++) {
                    pages.push({ number: i, isActive: pageNum == i });
                }

                var htmlPagination = templatePagination(pages);


                $widget.find('.portlet-content').append(htmlPagination);

                $widget.find('.portlet-content .pagination li').click(function () {
                    var _page = pageNum;
                    if ($(this).hasClass('next')) {
                        if (pageNum != pageCount) {
                            _page++;
                        }
                    } else if ($(this).hasClass('previous')) {
                        if (pageNum != 1) {
                            _page--;
                        }
                    } else if ($(this).hasClass('last')) {
                        _page = pageCount;
                    } else if ($(this).hasClass('first')) {
                        _page = 1;
                    } else {
                        _page = parseInt($(this).find('a').html());
                    }
                    pageNum = _page;
                    renderData();
                    return false;
                });
            }
            

        } else {
            $widget.find('.portlet-content').html('<h2 class="text-center">No Content!</h2>');
        }

        //$widget.find('.widgetSettingForm .form-group').first().siblings().hide();

        //Fields To Display
        $widget.find('.panel-body > table > tbody > tr > td').hide();
        $widget.find('.panel-body > table > thead > tr > th').hide();
        for (var i = 0; i < wData.SETTINGS.fields.length; i++) {
            $widget.find('.panel-body > table > tbody > tr > td.' + wData.SETTINGS.fields[i]).show();
            $widget.find('.panel-body > table > thead > tr > th.' + wData.SETTINGS.fields[i]).show();
        }
        
       
    }
    function initEvents() {
        var $widget = $(".widget[widgetType=" + wData.TYPE + "]");
        if (editPage) {
            $widget.find('.widgetSettingSubmitBtn').click(function () {
                var data = $widget.find('.widgetSettingPanel form').serializeObject();

                wData.SETTINGS = $.extend(wData.SETTINGS, data);
                if (wc.isAdmin) {
                    wData.BUID = parseInt(data.BUID);
                    delete data.BUID;
                } else {
                    wData.BUID = wc.buid;
                }

                if (!hasBUID()) {
                    removeWidget();
                } else {
                    if (!isNumber(wData.SETTINGS.numberOfResults)) {
                        widget.SETTINGS.numberOfResults = 10;
                    }
                    widgets.updateWidget(wData);
                    refreshWidget();
                }
                $widget.find('.portlet-header span.widget-header-name-span').html(wData.SETTINGS.header);
                $widget.find('.widgetSettingPanelOverlay').fadeOut(10);

            });

            $widget.find('.closeWidgetSettingBtn').click(function () {
                $widget.find(".widgetSettingPanelOverlay").fadeOut(10);
                if (!hasBUID()) {
                    removeWidget();
                }
            });

            $widget.find('.widget-field-multiselect').select2();

            var $fieldLis = $widget.find('select[name=fields] option');
            for (var i = 0; i < $fieldLis.length; i++) {
                if (wData.staticColumns.indexOf($($fieldLis[i]).val()) > -1) {
                    $widget.find('.select2-search-choice a').eq(i).remove();
                }
            }
        }

        if ($widget.find('.widget-update-time-span').length > 0) {

            setInterval(function () {
                var diff = new Date().getMinutes() - lastUpdate.getMinutes();
                if (diff < 0) {
                    diff += 60;
                }

                $widget.find('.widget-update-time-span').html(diff + ' min');
            }, 60 * 1000)

        }

    }
    function removeWidget() {
        var $widget = $(".widget[widgetType=" + wData.TYPE + "]");
        $widget.find('.portlet-content').hide();
        $widget.find('.widgetSettingPopup').hide();
        $('#removed-portlet-panel').append($widget);
        wData.SETTINGS.display = false;
        widgets.updateWidget(wData);

    }
    return {
        init: function (_widgetTemplate, _wData, _editPage) {
            wData = _wData;
            editPage = _editPage;

            if (wData.BUID == "" || wData.SETTINGS.display == false) {
                var $panel = $('#removed-portlet-panel');
                $panel.append(_widgetTemplate);
            } else {
                var $panel = $('#main-portlet-panel .dragPortlet').eq(wData.SETTINGS.portletOrder);
                $panel.append(_widgetTemplate);

                refreshWidget();
            }

            if (editPage) {
                renderSettings();
            }
            initEvents();

            //renderWidget();
        },
        refresh: function () {
            refreshWidget();
        },
        onDrag: function (o) {
            var $widget = $(".widget[widgetType=" + wData.TYPE + "]");
            var $portlet = $widget.closest('.portlet');
            var $dragPortlet = $widget.closest('.dragPortlet');
            if ($dragPortlet.attr('id') == "removed-portlet-panel") {
                wData.SETTINGS.portletOrder = 0;
                wData.SETTINGS.widgetOrder = $dragPortlet.find('.portlet').index($portlet);
                wData.SETTINGS.display = false;
            } else {

                wData.SETTINGS.portletOrder = $('#main-portlet-panel .dragPortlet').index($dragPortlet);
                wData.SETTINGS.widgetOrder = $dragPortlet.find('.portlet').index($portlet);
                wData.SETTINGS.display = true;
            }
            widgets.updateWidget(wData);
            if (o.sender._id() == "removed-portlet-panel") {
                if (!hasBUID()) {
                    $widget.find('.widgetSettingPanelOverlay').fadeIn(50);
                } else {
                    
                }
            }
        },
        removeWidget: function () {
            removeWidget();
        }
    }
}