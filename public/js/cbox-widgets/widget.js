﻿/// <reference path="helpers.js" />
/// <reference path="handlebars-v2.0.0.js" />
/// <reference path="CBoxService.js" />
/// <reference path="SamIssueCodesWidget.js" />
/// <reference path="controller.js" />
/**
 * @summary     Widget Page a part of scripts
 * @description used to init widgets from service and render widget regarding their saved order. Add general widget event 
 * @version     1.0.0
 * @file        /js/cbox/widget.js
 */

var widgets = (function (opt) {
    var defaults = {
        "editPage": true,
        widgets: [
                            {
                                "WIDGET_TYPE": 1,
                                "TYPE_NAME": "SAM ISSUE CODE WIDGET",
                                "TYPE": "samIssueCode",
                                "BUID": 1101,
                                "fields": ["DESCRIPTION", "ISSUE_ORDER_CODE", "SAM_TYPE_NAME", "ISSUE_REQUEST_CNT", "SUCCESSFUL_ISSUE_CNT"],
                                "staticColumns": ["DESCRIPTION"],
                                "statusColumn": "DESCRIPTION",
                                "SETTINGS": {
                                    "display": false,
                                    "dataWidth": "col-md-12",
                                    "portletOrder": 1,
                                    "widgetOrder": 1,
                                    "header": "Sam Issue Code Widget",
                                    "numberOfResults": 10,
                                    "fields": ["DESCRIPTION", "ISSUE_ORDER_CODE", "SAM_TYPE_NAME", "ISSUE_REQUEST_CNT", "SUCCESSFUL_ISSUE_CNT"],
                                    "intervalMinutes": 0
                                },
                                "LAST_UPDATED": "2015-01-26 20:22:40.0"
                            },
                            {
                                "WIDGET_TYPE": 2,
                                "TYPE_NAME": "RF ISSUE CODE WIDGET",
                                "TYPE": "rfIssueCode",
                                "BUID": 1101,
                                "fields": ["ORDER_STATUS","CARDHOLDER", "APPLICATION", "ISSUE_ORDER_CODE", "ISSUE_REQUEST_CNT", "SUCCESSFUL_ISSUE_CNT", "ISSUE_REQUEST_DATE"],
                                "staticColumns": ["ORDER_STATUS"],
                                "statusColumn": "ORDER_STATUS",
                                "SETTINGS": {
                                    "display": false,
                                    "dataWidth": "col-md-12",
                                    "portletOrder": 2,
                                    "widgetOrder": 1,
                                    "header": "RF Issue Code Widget",
                                    "numberOfResults": 10,
                                    "fields": ["ORDER_STATUS","CARDHOLDER", "APPLICATION", "ISSUE_ORDER_CODE", "ISSUE_REQUEST_CNT", "SUCCESSFUL_ISSUE_CNT", "ISSUE_REQUEST_DATE"],
                                    "intervalMinutes": 0
                                },
                                "LAST_UPDATED": "2015-01-26 20:22:40.0"
                            },
                            {
                                "WIDGET_TYPE": 3,
                                "TYPE_NAME": "Faulty Sam Issue List",
                                "TYPE": "faultySamIssueList",
                                "BUID": 1101,
                                "fields": ["SYSTEM_ID", "ISSUE_ORDER_CODE", "DESCRIPTION", "SAM_TYPE_NAME", "USERNAME", "SAM_ID", "DATE_REPORTED"],
                                "staticColumns": ["DESCRIPTION"],
                                "statusColumn": "DESCRIPTION",
                                "SETTINGS": {
                                    "display": false,
                                    "dataWidth": "col-md-12",
                                    "portletOrder": 3,
                                    "widgetOrder": 1,
                                    "header": "Faulty Sam Issue List",
                                    "numberOfResults": 10,
                                    "fields": ["SYSTEM_ID", "ISSUE_ORDER_CODE", "DESCRIPTION", "SAM_TYPE_NAME", "USERNAME", "SAM_ID", "DATE_REPORTED"],
                                    "intervalMinutes": 0
                                },
                                "LAST_UPDATED": "2015-01-26 20:22:40.0"
                            },
                            {
                                "WIDGET_TYPE": 4,
                                "TYPE_NAME": "Procurement Orders",
                                "TYPE": "procurementOrders",
                                "BUID": 1101,
                                "fields": ["DESCRIPTION", "REQUESTED_QUANTITY", "CREATED_BY", "ORDER_DATE", "SUPPLIER_NAME", "PRODUCT_NAME", "ORDER_CODE", "DELIVERY_DATE"],
                                "staticColumns": ["DESCRIPTION"],
                                "statusColumn": "DESCRIPTION",
                                "SETTINGS": {
                                    "display": false,
                                    "dataWidth": "col-md-12",
                                    "portletOrder": 3,
                                    "widgetOrder": 1,
                                    "header": "Procurement Orders",
                                    "numberOfResults": 10,
                                    "fields": ["DESCRIPTION", "REQUESTED_QUANTITY", "CREATED_BY", "ORDER_DATE", "SUPPLIER_NAME", "PRODUCT_NAME", "ORDER_CODE", "DELIVERY_DATE"],
                                    "intervalMinutes": 0
                                },
                                "LAST_UPDATED": "2015-01-26 20:22:40.0"
                            },
                            {
                                "WIDGET_TYPE": 5,
                                "TYPE_NAME": "Allocated Rf Card Ids",
                                "TYPE": "allocatedRfCardIds",
                                "BUID": 1101,
                                "IdPropName": "MAX_RFCARD_NO",
                                "AppTypeParamName": "p_card_app_type",
                                "SETTINGS": {
                                    "display": false,
                                    "dataWidth": "col-md-12",
                                    "portletOrder": 3,
                                    "widgetOrder": 1,
                                    "header": "Allocated Rf Card Ids",
                                    "intervalMinutes": 0
                                },
                                "LAST_UPDATED": "2015-01-26 20:22:40.0"
                            },
                            {
                                "WIDGET_TYPE": 6,
                                "TYPE_NAME": "Allocated Sam Card Ids",
                                "TYPE": "allocatedSamCardIds",
                                "BUID": 1101,
                                "IdPropName": "MAX_SAM_ID",
                                "AppTypeParamName": "sam_type",
                                "SETTINGS": {
                                    "display": false,
                                    "dataWidth": "col-md-12",
                                    "portletOrder": 3,
                                    "widgetOrder": 1,
                                    "header": "Allocated Sam Card Ids",
                                    "intervalMinutes": 0
                                },
                                "LAST_UPDATED": "2015-01-26 20:22:40.0"
                            },
                            {
                                "WIDGET_TYPE": 7,
                                "TYPE_NAME": "Initialization Codes",
                                "TYPE": "initializationCodes",
                                "BUID": 1101,
                                "fields": ["CREATED_BY", "INIT_COUNT", "CREATE_DATE", "APPLICATION_TYPE", "INIT_PROFILE_NAME", "CODE_STATUS", "SUCCESS_INIT_COUNT", "CARRIER_TYPE", "INIT_CODE"],
                                "staticColumns": ["CODE_STATUS"],
                                "statusColumn": "CODE_STATUS",
                                "SETTINGS": {
                                    "display": false,
                                    "dataWidth": "col-md-12",
                                    "portletOrder": 3,
                                    "widgetOrder": 1,
                                    "header": "Initialization Codes",
                                    "numberOfResults": 10,
                                    "fields": ["CREATED_BY", "R__", "INIT_COUNT", "CREATE_DATE", "APPLICATION_TYPE", "INIT_PROFILE_NAME", "CODE_STATUS", "SUCCESS_INIT_COUNT", "CARRIER_TYPE", "INIT_CODE"],
                                    "intervalMinutes": 0
                                },
                                "LAST_UPDATED": "2015-01-26 20:22:40.0"
                            },
                            {
                                "WIDGET_TYPE": 8,
                                "TYPE_NAME": "Card Black List Records",
                                "TYPE": "cardBlackList",
                                "BUID": 1101,
                                "fields": ["RF_CARD_NO", "BLACKLISTED_DATE", "ENLISTED_BY", "CARRIER_OWNER", "STAFF_CARD_NO", "WORK_SHIFT", "DEVICE_ID", "DEVICE_CARRIER_TYPE", "CARRIER_NUM", "STATUS", "ENLISTED_DATE"],
                                "staticColumns": ["STATUS"],
                                "statusColumn": "STATUS",
                                "SETTINGS": {
                                    "display": false,
                                    "dataWidth": "col-md-12",
                                    "portletOrder": 3,
                                    "widgetOrder": 1,
                                    "header": "Card Black List Records",
                                    "numberOfResults": 10,
                                    "fields": ["RF_CARD_NO", "BLACKLISTED_DATE", "ENLISTED_BY", "CARRIER_OWNER", "STAFF_CARD_NO", "WORK_SHIFT", "DEVICE_ID", "DEVICE_CARRIER_TYPE", "CARRIER_NUM", "STATUS", "ENLISTED_DATE"],
                                    "intervalMinutes": 0
                                },
                                "LAST_UPDATED": "2015-01-26 20:22:40.0"
                            },
                            {
                                "WIDGET_TYPE": 9,
                                "TYPE_NAME": "Card White List Records",
                                "TYPE": "cardWhiteList",
                                "BUID": 1101,
                                "fields": ["RF_CARD_NO", "WHITELISTED_DATE", "ENLISTED_BY", "CARRIER_OWNER", "STAFF_CARD_NO", "WORK_SHIFT", "DEVICE_ID", "DEVICE_CARRIER_TYPE", "CARRIER_NUM", "STATUS_NAME", "ENLISTED_DATE"],
                                "staticColumns": ["STATUS_NAME"],
                                "statusColumn": "STATUS_NAME",
                                "SETTINGS": {
                                    "display": false,
                                    "dataWidth": "col-md-12",
                                    "portletOrder": 3,
                                    "widgetOrder": 1,
                                    "header": "Card White List Records",
                                    "numberOfResults": 10,
                                    "fields": ["RF_CARD_NO", "WHITELISTED_DATE", "ENLISTED_BY", "CARRIER_OWNER", "STAFF_CARD_NO", "WORK_SHIFT", "DEVICE_ID", "DEVICE_CARRIER_TYPE", "CARRIER_NUM", "STATUS_NAME", "ENLISTED_DATE"],
                                    "intervalMinutes": 0
                                },
                                "LAST_UPDATED": "2015-01-26 20:22:40.0"
                            },
                            {
                                "WIDGET_TYPE": 10,
                                "TYPE_NAME": "Latest Device Application Version",
                                "TYPE": "latestDeviceApplicationVersion",
                                "BUID": 1101,
                                "SETTINGS": {
                                    "display": false,
                                    "dataWidth": "col-md-3",
                                    "portletOrder": 3,
                                    "widgetOrder": 1,
                                    "header": "Latest Device Application Version",
                                    "intervalMinutes": 0
                                },
                                "LAST_UPDATED": "2015-01-26 20:22:40.0"
                            },
                            {
                                "WIDGET_TYPE": 11,
                                "TYPE_NAME": "Initialized Device List",
                                "TYPE": "initializedDeviceList",
                                "BUID": 1101,
                                "fields": ["DEVICE_EMEI", "VERSION_NAME", "CARRIER_OWNER", "SAM_ID", "CARRIER_NUM", "CARRIER_TYPE", "ASSIGNED_DEVICE_ID", "DEVICE_GPS", "INITIALIZED_DATE", "DEVICE_MAC", "BUID", "APPLICATION_TYPE", "INIT_CODE"],
                                "staticColumns": [],
                                "statusColumn": "",
                                "SETTINGS": {
                                    "display": false,
                                    "dataWidth": "col-md-12",
                                    "portletOrder": 3,
                                    "widgetOrder": 1,
                                    "header": "Initialized Device List",
                                    "numberOfResults": 10,
                                    "fields": ["DEVICE_EMEI", "VERSION_NAME", "CARRIER_OWNER", "SAM_ID", "CARRIER_NUM", "CARRIER_TYPE", "ASSIGNED_DEVICE_ID", "DEVICE_GPS", "INITIALIZED_DATE", "DEVICE_MAC", "BUID", "APPLICATION_TYPE", "INIT_CODE"],
                                    "intervalMinutes": 0
                                },
                                "LAST_UPDATED": "2015-01-26 20:22:40.0"
                            },
                            {
                                "WIDGET_TYPE": 12,
                                "TYPE_NAME": "Card Encoding",
                                "TYPE": "cardEncoding",
                                "BUID": 1101,
                                "fields": ["STATUS","OPERATION_END_DATE", "ISSUE_ORDER_CODE", "SUCCESSFUL_ISSUE_CNT", "OPERATION_START_DATE", "ISSUE_REQUEST_DATE", "ISSUE_REQUEST_CNT", "CARDHOLDER", "EXECUTED_BY", "LOGGED_BY", "BUID" ],
                                "staticColumns": ["STATUS"],
                                "statusColumn": "STATUS",
                                "SETTINGS": {
                                    "display": false,
                                    "dataWidth": "col-md-12",
                                    "portletOrder": 3,
                                    "widgetOrder": 1,
                                    "header": "Card Encoding",
                                    "numberOfResults": 10,
                                    "fields": ["STATUS", "OPERATION_END_DATE", "ISSUE_ORDER_CODE", "SUCCESSFUL_ISSUE_CNT", "OPERATION_START_DATE", "ISSUE_REQUEST_DATE", "ISSUE_REQUEST_CNT", "CARDHOLDER", "EXECUTED_BY", "LOGGED_BY", "BUID"],
                                    "intervalMinutes": 0
                                },
                                "LAST_UPDATED": "2015-01-26 20:22:40.0"
                            },
                            {
                                "WIDGET_TYPE": 13,
                                "TYPE_NAME": "Problematic Device Transmission",
                                "TYPE": "problematicDeviceTransmission",
                                "BUID": 1101,
                                "fields": ["APPROVAL_STATUS", "TRANSFER_DATE", "DEVICE_ID", "REGISTRY_KEY"],
                                "staticColumns": ["APPROVAL_STATUS"],
                                "statusColumn": "APPROVAL_STATUS",
                                "SETTINGS": {
                                    "display": false,
                                    "dataWidth": "col-md-12",
                                    "portletOrder": 3,
                                    "widgetOrder": 1,
                                    "header": "Problematic Device Transmission",
                                    "numberOfResults": 10,
                                    "fields": ["APPROVAL_STATUS", "TRANSFER_DATE", "DEVICE_ID", "REGISTRY_KEY"],
                                    "intervalMinutes": 0
                                },
                                "LAST_UPDATED": "2015-01-26 20:22:40.0"
                            },
                            {
                                "WIDGET_TYPE": 14,
                                "TYPE_NAME": "Cap Loading Request",
                                "TYPE": "capLoading",
                                "BUID": 1101,
                                "fields": ["DESCRIPTION", "ISSUE_REQUEST_DATE", "ISSUE_ORDER_CODE", "SUCCESSFUL_ISSUE_CNT", "ISSUE_REQUEST_CNT", "SAM_TYPE_NAME", "LOGGED_BY"],
                                "staticColumns": ["DESCRIPTION"],
                                "statusColumn": "DESCRIPTION",
                                "SETTINGS": {
                                    "display": false,
                                    "dataWidth": "col-md-12",
                                    "portletOrder": 3,
                                    "widgetOrder": 1,
                                    "header": "Cap Loading Request",
                                    "numberOfResults": 10,
                                    "fields": ["DESCRIPTION", "ISSUE_REQUEST_DATE", "ISSUE_ORDER_CODE", "SUCCESSFUL_ISSUE_CNT", "ISSUE_REQUEST_CNT", "SAM_TYPE_NAME", "LOGGED_BY"],
                                    "intervalMinutes": 0
                                },
                                "LAST_UPDATED": "2015-01-26 20:22:40.0"
                            },
        ]
    }
    defaults = $.extend(defaults, opt);

    var widgetList = [];
    var orgWidgetList = [];
    var _widgetsSelector = '.portlet';
    var _headersSelector = '.portlet-header';
    var _contentsSelector = '.portlet-content';
    var _widgetMiniPopupTogglerSelectors = ".portlet-toggle";
    var _widgetMiniPopupSelector = ".widgetSettingPopup";
    var _widgetSettingPanelSelector = ".widgetSettingPanelOverlay";
    var _widgetMiniPopupCloseButtonSelector = ".widgetMiniPopupClose";
    var _colorSelector = ".colorSelector";

    function makeDraggable() {
        var oldMouseStart = $.ui.sortable.prototype._mouseStart;
        $.ui.sortable.prototype._mouseStart = function (event, overrideHandle, noActivation) {
            this._trigger("CustomBeforeStart", event, this._uiHash());
            oldMouseStart.apply(this, [event, overrideHandle, noActivation]);
        };

        //$('.dragPortlet').each(function () {
        //    $(this).mousemove(function () {
        //        $(this).find('.portlet-content').hide();
        //    });
        //});
        $(".dragPortlet").sortable({
            connectWith: ".dragPortlet",
            handle: ".portlet-header",
            cancel: ".portlet-toggle",
            cursor: "pointer",
            revert: 0, // animation in milliseconds
            placeholder: "portlet-placeholder ui-corner-all",
            update: function (e, o) {
                if (o.sender !== null) {
                    var widgetType = o.item.attr('widgetType');
                    var module = wc.getModule(widgetType);
                    //module.onDrag(o);
                }
            },
            "CustomBeforeStart": function (e,u) {
                //$(u.item).find('.portlet-content').hide();
                $('.portlet-content').hide();
            }
        })
        //$(".dragPortlet").disableSelection();
    }


    function initEvents() {
        //Hide Show All Widgets
        $('#hideWidgets').click(function () {
            $('.portlet-content').hide();
        });
        $('#showWidgets').click(function () {
            $('.portlet-content').show();
        })

        //Mini popup toggler events
        var togglers = $(_widgetMiniPopupTogglerSelectors);
        togglers.click(function () {
            $(this).closest(_widgetsSelector).find(_widgetMiniPopupSelector).toggle();
        });

        //Mini popup close button event
        $(_widgetMiniPopupCloseButtonSelector).click(function () {
            $(this).closest(_widgetMiniPopupSelector).hide();
        })


        //Color Change Events
        $(_colorSelector).click(function () {
            var colorCode = $(this).css('background-color');
            $(this).closest(_widgetsSelector).find(_headersSelector).css('background-color', colorCode);
        })

        //Widget Header Double Click Toggle Display
        $(_headersSelector).dblclick(function () {
            $(this).closest(_widgetsSelector).find(_contentsSelector).toggle();
        });

        //Minimize Maximize Widget
        $('.widgetMinimizeBtn').click(function () {
            $(this).hide();
            $(this).siblings('.widgetMaximizeBtn').show()
            $(this).closest(_widgetsSelector).find(_contentsSelector).fadeOut(50);
            return false;
        });

        $('.widgetMaximizeBtn').click(function () {
            $(this).hide();
            $(this).siblings('.widgetMinimizeBtn').show()
            $(this).closest(_widgetsSelector).find(_contentsSelector).fadeIn(50);
            return false;
        });

        //Delete Widget Event
        $('.deleteWidgetBtn').click(function () {
            var _module = wc.getModule($(this).closest(_widgetsSelector).attr('widgetType'));
            _module.removeWidget();
            return false;
        });

        //Edit Widget Event,Open Widget Setting Window
        $('.editWidgetBtn').click(function () {
            $(this).closest(_widgetsSelector).find(_widgetSettingPanelSelector).fadeIn(50);
            $(_widgetMiniPopupSelector).fadeOut(10);
            return false;
        });

        $('.refreshWidgetBtn').click(function () {
            var _module = wc.getModule($(this).closest(_widgetsSelector).attr('widgetType'));
            _module.refresh();
            return false;
        });
        $('#save_widgets_btn').click(function () {
            var postList = [];
            for (var i = 0; i < widgetList.length; i++) {
                var $widget = $(".widget[widgetType=" + widgetList[i].TYPE + "]");
                widgetList[i].SETTINGS.dataWidth = $widget.attr('dataWidth');

                var $portlet = $widget.closest('.portlet');
                var $dragPortlet = $widget.closest('.dragPortlet');
                if ($dragPortlet.attr('id') == "removed-portlet-panel") {
                    widgetList[i].SETTINGS.portletOrder = 0;
                    widgetList[i].SETTINGS.widgetOrder = $dragPortlet.find('.portlet').index($portlet);
                    widgetList[i].SETTINGS.display = false;
                } else {

                    widgetList[i].SETTINGS.portletOrder = $('#main-portlet-panel .dragPortlet').index($dragPortlet);
                    widgetList[i].SETTINGS.widgetOrder = $dragPortlet.find('.portlet').index($portlet);
                    widgetList[i].SETTINGS.display = true;
                }


                var postData = {
                    p_username: "arthur",
                    p_buid: parseInt( widgetList[i].BUID ),
                    p_widget_type: parseInt( widgetList[i].WIDGET_TYPE),
                    p_settings:JSON.stringify(widgetList[i].SETTINGS)
                }
                if (!wc.isAdmin) {
                    postData.p_buid = wc.buid;
                }
                postList.push(postData);
            }
            wc.service.postWidgets(postList);
            window.location.reload();
        });

        $('.widgetWidthChangeSelect').change(function () {
            var portlet = $(this).closest('.portlet');
            var val = $(this).val();
            var oldClass = portlet.attr('dataWidth');
            portlet.attr('dataWidth', val);
            portlet.removeClass(oldClass);
            portlet.addClass(val);
        })
    }

    function loadWidgetHtml(_data) {
        var source = $("#widgetTemplate").html();
        var template = Handlebars.compile(source);
        var html = template(_data);
        return html;
    }
    function loadMiniPopupHtmls() {
        var source = $("#widget-setting-popup-template").html();
        var template = Handlebars.compile(source);


        $('.portlet').append(template);
    }

    //Service functions
    function getWidgetsFromService() {
        orgWidgetList = wc.service.getWidgets("arthur");
        //REMOVE
        //orgWidgetList[1].WIDGET_TYPE = 2;
        //console.log(orgWidgetList);
        //REMOVE
        for (var i = 0; i < orgWidgetList.length; i++) {
            if (typeof orgWidgetList[i].SETTINGS == "string") {
                orgWidgetList[i].SETTINGS = JSON.parse(orgWidgetList[i].SETTINGS);
            }
        }
        widgetList = defaults.widgets;

        for (var i = 0; i < widgetList.length; i++) {
            for (var j = 0; j < orgWidgetList.length; j++) {
                if (orgWidgetList[j].WIDGET_TYPE == widgetList[i].WIDGET_TYPE) {
                    widgetList[i] = $.extend(true,{},widgetList[i], orgWidgetList[j]);
                }
            }
        }
        
        widgetList = widgetList.sort(function (a, b) {
            return a.SETTINGS.widgetOrder > b.SETTINGS.widgetOrder;
        });
        


    }

    function initEachWidget() {
        widgetList.forEach(function (_widget) {
            //var targetPortlet = $('.dragPortlet').eq(_widget.portletOrder);
            var widgetTemplate = loadWidgetHtml(_widget);
            var _module = wc.getModule(_widget.TYPE);
            if (_module !== undefined) {
                _module.init(widgetTemplate, _widget, defaults.editPage);
            }
        })
    }

    function initWidgets() {
        getWidgetsFromService();
        initEachWidget();

        if (defaults.editPage) {
            loadMiniPopupHtmls();
            initEvents();
            makeDraggable();
        }
    }

    return {
        init: function (opt) {
            defaults = $.extend(defaults, opt);
            initWidgets();

        },
        getTemplate: function (_data) {
            return loadWidgetHtml(_data);
        },
        updateWidget: function (w) {
            for (var i = 0; i < widgetList.length; i++) {
                if (widgetList[i].WIDGET_TYPE == w.WIDGET_TYPE) {
                    widgetList[i] = $.extend(widgetList[i], w);
                }
            }
            console.log(widgetList);
        }
    }

})()