//date picker start

if (top.location != location) {
    top.location.href = document.location.href ;
}
$(function(){
    window.prettyPrint && prettyPrint();
    $('.default-date-picker').datepicker({
        format: 'dd-mm-yyyy'
    });

    $('.dpYears').datepicker();
    $('.dpMonths').datepicker();
	$("#x_Quantity").mask("9?999999999999",{placeholder:" "});
	$("#x_VERSION_NUMBER").mask("9?999999999999",{placeholder:" "});
	$("#x_BUSINESS_UNIT").mask("9?99999999999",{placeholder:" "});
	$("#x_CODE_INITIALIZATION_COUNT").mask("9?99999999999",{placeholder:" "});
	$("#x_INITIALIZATION_COUNT").mask("9?99999999",{placeholder:" "});
	$("#x_BUUD").mask("9999?",{placeholder:" "});

    var startDate = new Date(2012,1,20);
    var endDate = new Date(2012,1,25);
    $('.dp4').datepicker({
		  onRender: function(date) {
		  	//alert(100);
		    return date.valueOf() < now.valueOf() ? 'disabled' : '';
		  }
		})
        .on('changeDate', function(ev){
            if (ev.date.valueOf() > endDate.valueOf()){
                $('.alert').show().find('strong').text('The start date can not be greater then the end date');
            } else {
                $('.alert').hide();
                startDate = new Date(ev.date);
                $('#startDate').text($('.dp4').data('date'));
            }
            $('.dp4').datepicker('hide');
        });
    $('.dp5').datepicker()
        .on('changeDate', function(ev){
            if (ev.date.valueOf() < startDate.valueOf()){
                $('.alert').show().find('strong').text('The end date can not be less then the start date');
            } else {
                $('.alert').hide();
                endDate = new Date(ev.date);
                $('.endDate').text($('.dp5').data('date'));
            }
            $('.dp5').datepicker('hide');
        });

    // disabling dates
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    var checkin = $('.dpd1').datepicker({
        onRender: function(date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
            if (ev.date.valueOf() > checkout.date.valueOf()) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
            }
            checkin.hide();
            $('.dpd2')[0].focus();
        }).data('datepicker');
    var checkout = $('.dpd2').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
            checkout.hide();
        }).data('datepicker');


	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

	$('#x_dateofBirth').datepicker({
        format: 'dd-mm-yyyy',
		onRender: function(date) {
	    	return date.valueOf() > now.valueOf() ? 'disabled' : '';
	  	}
    });
	$("#x_dateofBirth").mask("99-99-9999");
	$('#x_deliveryDate').mask("99-99-9999");
	$("#x_mobileNumber").mask("9?99999999999999",{placeholder:" "});
	$("#x_CARD_NUMBER").mask("9?99999999999999",{placeholder:" "});
	$("#x_PHONE").mask("9?99999999999999",{placeholder:" "});
	$("#x_CONTACT_NUMBER").mask("9?99999999999999",{placeholder:" "});
	$("#x_GPS_LONGTITUDE").mask("99.9?999999999999",{placeholder:" "});
    //$("#x_NORTH_NAME").mask("99.9?999999999999",{placeholder:" "});
	//$("#x_WEST_NAME").mask("99.9?999999999999",{placeholder:" "});
	//$("#x_EAST_NAME").mask("99.9?999999999999",{placeholder:" "});
	//$("#x_SOUTH_NAME").mask("99.9?999999999999",{placeholder:" "});
	$("#x_GPS_LATITUDE").mask("99.9?999999999999",{placeholder:" "});
	$("#x_numberOfSams").mask("9?99999999999999",{placeholder:" "});
	$("#x_phoneNumber").mask("9?9999999999999999999",{placeholder:" "});


});

//date picker end


//datetime picker start

$(".form_datetime").datetimepicker({format: 'dd MM yyyy - hh:ii'});

$(".form_datetime-component").datetimepicker({
    format: "dd MM yyyy - hh:ii"
});

$(".form_datetime-adv").datetimepicker({
    format: "dd MM yyyy - hh:ii",
    autoclose: true,
    todayBtn: true,
    startDate: '+0d',
    minuteStep: 10
});

$(".form_datetime-meridian").datetimepicker({
    format: "dd MM yyyy - hh:ii P",
    showMeridian: true,
    autoclose: true,
    todayBtn: true
});

//datetime picker end

//timepicker start
$('.timepicker-default').timepicker();


$('.timepicker-24').timepicker({
    autoclose: true,
    minuteStep: 1,
    showSeconds: true,
    showMeridian: false
});

//timepicker end



/*
//spinner start
$('#spinner1').spinner();
$('#spinner2').spinner({disabled: true});
$('#spinner3').spinner({value:0, min: 0, max: 10});
$('#spinner4').spinner({value:0, step: 5, min: 0, max: 200});
//spinner end
*/




function onAddTag(tag) {
    alert("Added a tag: " + tag);
}
function onRemoveTag(tag) {
    alert("Removed a tag: " + tag);
}

function onChangeTag(input,tag) {
    alert("Changed a tag: " + tag);
}



