//////////////////////////////////////////////////////////////////////////////////
// CloudCarousel V1.0.4
// (c) 2010 by R Cecco. <http://www.professorcloud.com>
// MIT License
//
// Reflection code based on plugin by Christophe Beyls <http://www.digitalia.be>
//
// Please retain this copyright header in all versions of the software
//////////////////////////////////////////////////////////////////////////////////

function trace(str){
	console.log(str);
}

(function(jQuery){
	
     jQuery.fn.extend({
        putCarousel: function(options){
    	 
		    //Some variables defines
		   var settings = {};	//array of settings get by server
		   var id_counter = 0;	//counter that counts carousel id
		   var objAddData = {}  //object of additional data. (like filename for example)
		   var arrItemsForEvent = [];
		   var responseMessage = "Error loading carousel , please check the CMS url.";
		   var t = this;
		   var carouselID = jQuery(t).attr("id");
		   
		   if(!options) var options = {};
		   
		   var defaults = {
				catid:'',					
				preset:'',								
				event_onInit:null,			
				event_onLoad:null,			
				event_onItemClick:null,		
				event_onItemMouseOver:null, 
				event_onItemMouseOut:null, 	
				event_onFrontItemChange:null,
				settings:{}
		   };
			
			//set final options - from options or from memory
			var final_options = {};
			jQuery.extend(final_options,defaults,options);
		   			
			var objCarousel = jQuery(this);
			
			//add request data
			var reqData = {};
			if(final_options.catid)
				reqData.catid = final_options.catid;
			if(final_options.preset)
				reqData.preset = final_options.preset;
			
			id_counter++;
			var car_id = objCarousel.attr("id");
			settings = final_options.settings;
			setButtonsEvents(car_id);							
			startCarousel(t,car_id);
			
			
			//-----------------------------------------------
			//Set buttons events of mouseover
			function setButtonsEvents(){
				
				if(settings.button_type != "disabled" && settings.buttons_mouseover == "true"){
					jQuery("#"+carouselID+" .carousel_left_button, #"+carouselID+" .carousel_right_button").each(function(){
						
						var buttonClass = this.className;
						jQuery(this).mouseover(function(){					
							jQuery(this).hide();
							jQuery(this).siblings("."+buttonClass+"_over").show();
						});
						
						jQuery(this).siblings("."+buttonClass+"_over").mouseout(function(){
							jQuery(this).hide();
							var normalID = this.className.replace("_over","");
							jQuery(this).siblings("."+normalID).show();
						});
					});
				}
			}
			
			//-----------------------------------
			//start the gallery - using the container
			function startCarousel(container,carouselID){
			
				//set caption and description containers
				var container_caption = null;
				var container_desc = null;
				
				if(settings.enable_caption == "true")
					container_caption = jQuery("#" + carouselID + " .carousel_caption");
				
				if(settings.enable_desc == "true")
					container_desc = jQuery("#" + carouselID + " .carousel_description");
				
				//set buttons:
				var left_button = null;
				var right_button = null;				
				var left_button_alt = null;
				var right_button_alt = null;
				
				if(settings.button_type != "disabled"){	
					left_button = jQuery("#" + carouselID + " .carousel_left_button");
					right_button = jQuery("#" + carouselID + " .carousel_right_button");					
				}
				
				if(settings.buttons_mouseover == "true"){
					left_button_alt = jQuery("#" + carouselID + " .carousel_left_button_over");
					right_button_alt = jQuery("#" + carouselID + " .carousel_right_button_over");
				}
				
				jQuery("#"+carouselID).CloudCarousel({
					xPos: Number(settings.xpos),
					yPos: Number(settings.ypos),
					reflOpacity:Number(settings.reflection_opacity),
					reflHeight: Number(settings.reflection_height),
					reflGap:Number(settings.reflection_gap),
					yRadius:Number(settings.y_radius),
					xRadius:Number(settings.x_radius),
					minScale:Number(settings.min_scale),
					speed:Number(settings.speed),
					mouseWheel:false,
				    FPS: Number(settings.fps),
				    autoRotate: settings.auto_rotate,
				    autoRotateDelay: Number(settings.auto_rotate_delay),
					bringToFront: eval(settings.bring_to_front),
					buttonLeft:left_button,
					buttonRight:right_button,
					buttonLeft_alt:left_button_alt,
					buttonRight_alt:right_button_alt,
					altBox: container_desc,
					titleBox: container_caption,
				    textShowFrontSpeed:Number(settings.text_back_to_front_delay),
					setOpacityByScale:eval(settings.set_opacity_by_scale),
				    setTextOnMouseOver:eval(settings.text_set_on_mouseover),
					buttonsAppearFadeIn:eval(settings.buttons_appear_fadein),
					linksFrontOnly:eval(settings.link_front_only),
					objAddData:objAddData,					//pass object of additional data (key by id)
					event_onImagesLoaded:onImagesLoaded,
					enableLinks:eval(settings.enable_links),
					reverseControls:eval(settings.button_reverse_dir),
					event_onItemClick:final_options.event_onItemClick,	//pass this event to the carousel
					event_onItemMouseOver:final_options.event_onItemMouseOver,
					event_onItemMouseOut:final_options.event_onItemMouseOut,
					event_onFrontItemChange:final_options.event_onFrontItemChange
				});
			}
			
			
			/**
			 * on images loaded event
			 */
			function onImagesLoaded(arrItems,objCarousel){
				var container = jQuery(t).get(0);
				
				//load onload event:
				if(typeof final_options.event_onLoad == "function")
					final_options.event_onLoad(container,arrItems,objCarousel);
				
				removePreloader();
			}
			
			
			/**
			 * function - run on images first loaded - remove preloader 
			 */
			function removePreloader(){				
					if(settings.preloader_type != "disabled"){						
						var preloader = objCarousel.find(".carousel3d_preloader");	
						var fadeoutSpeed = 400;
						preloader.fadeOut(fadeoutSpeed,function(){jQuery(this).remove()});
				}
			}
			
			return(this);
        }
     });
})(jQuery);

/**
 * ************************************************************************************************************
 * ************************************************************************************************************
 */

(function(jQuery){
	
	function isIE(){
		return ('v'=='\v');
	}
	
	// START Reflection object.
	// Creates a reflection for underneath an image.
	// IE uses an image with IE specific filter properties, other browsers use the Canvas tag.	
	// The position and size of the reflection gets updated by updateAll() in Controller.
	function Reflection(img, reflHeight, opacity) {				
		
		var	reflection, cntx, imageWidth = img.width, imageHeight = img.width, gradient, parent;
	
		parent = jQuery(img.parentNode);

		if (isIE()) {
			this.element = reflection = parent.append("<img class='reflection' style='position:absolute'/>").find(':last')[0];					
			reflection.src = img.src;			
			reflection.style.filter = "flipv progid:DXImageTransform.Microsoft.Alpha(opacity=" + (opacity * 100) + ", style=1, finishOpacity=0, startx=0, starty=0, finishx=0, finishy=" + (reflHeight / imageHeight * 100) + ")";	
			
	} else {	
			
			this.element = reflection = parent.append("<canvas class='reflection' style='position:absolute'/>").find(':last')[0];
			if (!reflection.getContext)
			{
				return;			
			}
			cntx = reflection.getContext("2d");
			try {
				jQuery(reflection).attr({width: imageWidth, height: reflHeight});
				cntx.save();
				cntx.translate(0, imageHeight-1);
				cntx.scale(1, -1);				
				cntx.drawImage(img, 0, 0, imageWidth, imageHeight);				
				cntx.restore();
				cntx.globalCompositeOperation = "destination-out";
				gradient = cntx.createLinearGradient(0, 0, 0, reflHeight);
				gradient.addColorStop(0, "rgba(255, 255, 255, " + (1 - opacity) + ")");
				gradient.addColorStop(1, "rgba(255, 255, 255, 1.0)");
				cntx.fillStyle = gradient;
				cntx.fillRect(0, 0, imageWidth, reflHeight);				
			} catch(e) {			
				return;
			}		
		}
				
		// Store a copy of the alt and title attrs into the reflection
		jQuery(reflection).attr({ 'alt': jQuery(img).attr('alt'), title: jQuery(img).attr('title')} );	
				
	}	//END Reflection object

	// START Item object.
	// A wrapper object for items within the carousel.
	var	Item = function(imgIn, options)
	{							
		this.itemID = 0;
		this.orgWidth = imgIn.width;			
		this.orgHeight = imgIn.height;		
		this.image = imgIn;
		this.reflection = null;
		this.alt = imgIn.alt;
		this.title = imgIn.title;
		this.imageOK = false;		
		this.options = options;	
		this.link = null;
		this.linkOpenNewWindow = false;
		this.imageOK = true;	
		this.filename = "";
				
		//set item id:
		var objImage = jQuery(this.image);
				
		this.itemID = objImage.attr("id");
		
		//set item additional data
		this.filename = objImage.attr("src");
		
		//If the image has link - remove the link from markup, and add it to the object
		//skip link lightbox relation
		var itemLink = objImage.parent();
		
		if(itemLink.hasClass("cloudcarousel_link") && itemLink.attr("rel") != "lightbox"){
			
			this.link = itemLink.prop("href");
			var target = itemLink.prop("target");
			if(target != "" && target.toLowerCase() == "_blank")
				this.linkOpenNewWindow = true;
			
			//annulate the link
			itemLink.attr("href","javascript:void(0);").attr("target","");
			
			//set link cursor:
			if(options.linksFrontOnly == false)
				itemLink.css("cursor","pointer");
		}
		
				
		if (this.options.reflHeight > 0)
		{													
			this.reflection = new Reflection(this.image, this.options.reflHeight, this.options.reflOpacity);					
		}
		
		jQuery(this.image).css('position','absolute');	// Bizarre. This seems to reset image width to 0 on webkit!					

	};// END Item object
	
	
	// Controller object.
	// This handles moving all the items, dealing with mouse clicks etc.	
	var Controller = function(container, images, options)
	{	
		var	items = [], funcSin = Math.sin, funcCos = Math.cos, ctx=this;
		this.controlTimer = 0;
		this.stopped = false;
		//this.imagesLoaded = 0;
		this.container = container;
		this.xRadius = options.xRadius;
		this.yRadius = options.yRadius;
		this.showFrontTextTimer = 0;
		this.autoRotateBackTimer = 0;
		this.autoRotatePaused = false;
		
		if (options.xRadius === 0)
		{
			this.xRadius = (jQuery(container).width()/2.3);
		}
		if (options.yRadius === 0)
		{
			this.yRadius = (jQuery(container).height()/6);
		}

		this.xCentre = options.xPos;
		this.yCentre = options.yPos;
		this.frontIndex = 0;	// Index of the item at the front
		
		// Start with the first item at the front.
		this.rotation = this.destRotation = Math.PI/2;
		this.timeDelay = 1000/options.FPS;
								
		// Turn on the infoBox
		if(options.altBox !== null)
		{
			jQuery(options.altBox).css('display','block');	
			jQuery(options.titleBox).css('display','block');	
		}
		// Turn on relative position for container to allow absolutely positioned elements
		// within it to work.
		jQuery(container).css({ position:'relative', overflow:'hidden'} );
				
		/*
		jQuery(options.buttonLeft).css('display','inline');
		jQuery(options.buttonRight).css('display','inline');
		*/
		
		// Setup the buttons.
		jQuery(options.buttonLeft).bind('mouseup',this,function(event){
			event.data.rotateLeft();
			return false;
		});
		
		if(options.buttonLeft_alt)
			jQuery(options.buttonLeft_alt).bind('mouseup',this,function(event){
				event.data.rotateLeft();
				return false;
			});
		
		jQuery(options.buttonRight).bind('mouseup',this,function(event){
			event.data.rotateRight();
			return false;
		});
		
		if(options.buttonRight_alt){
			jQuery(options.buttonRight_alt).bind('mouseup',this,function(event){															
				event.data.rotateRight();
				return false;
			});			
		}
					
		// You will need this plugin for the mousewheel to work: http://plugins.jquery.com/project/mousewheel
		if (options.mouseWheel)
		{
			jQuery(container).bind('mousewheel',this,function(event, delta){
					event.data.rotate(delta);
					return false;
			});
		}
		
		//--------------------------------------------------
		//set opacity of some object
		function setOpacity(obj,level) {
		  obj.style.opacity = level;
		  obj.style.MozOpacity = level;
		  obj.style.KhtmlOpacity = level;
		  obj.style.filter = "alpha(opacity=" + (level * 100) + ");";
		}
		
		//--------------------------------------------------
		//set text title and description from 
		function setOverlayText(title,desc){
			if(options.titleBox)
				options.titleBox.html(title);
			
			if(options.altBox)
				options.altBox.html(desc);
		}
		
		//bind mouseover event to the images
		jQuery(container).find("img.cloudcarousel").bind('mouseover',this,function(event){
			var	context = event.data;
			
			activateEvent("mouseover",{event:event});
			
			//pause auto rotation
			if ( options.autoRotate !== 'no' ){	
				clearTimeout(context.autoRotateBackTimer);
				context.autoRotatePaused = true;
			}
			
			//set item text
			if(options.setTextOnMouseOver == true){
				clearTimeout(event.data.showFrontTextTimer);
				setOverlayText(jQuery(event.target).attr('title'),jQuery(event.target).attr('alt'));
			}
		});
		
		//bind mouseover event to the images
		jQuery(container).find("img.cloudcarousel").bind('mouseout',this,function(event){
			activateEvent("mouseout",{event:event});
		});
		
		//get response data object from the item.
		function getItemResponseData(item){				
			var responseData = {
				id:item.itemID,
				title:item.title,
				description:item.alt,
				link:item.link,
				filename:item.filename
			};	
			
			//corrent the link
			if(responseData.link == null) 
				responseData.link = "";
				
			return(responseData);
		}
		
		//get event response data
		function getEventResponseData(event){
			var	idx = jQuery(event.target).data('itemIndex');
			var	frontIndex = event.data.frontIndex;			
			var item = items[idx];
			var isFront = (idx == frontIndex)?true:false;
			var responseData = getItemResponseData(item);			
			
			var objResponse = {image:item.image,data:responseData,isFront:isFront};
			return(objResponse);
		}
		
		//activate some event
		function activateEvent(eventName,data){
			
			switch(eventName){
				case "frontchange":
					if(typeof options.event_onFrontItemChange == "function"){
						var frontItemData = ctx.getFrontItem();
						var image = frontItemData.image;
						delete frontItemData.image;
						options.event_onFrontItemChange(frontItemData,image);
					}
				break;
				case "click":
					if(typeof options.event_onItemClick == "function"){
						var objData = getEventResponseData(data.event);
						options.event_onItemClick(objData.data,objData.isFront,objData.image);
					}
				break;
				case "mouseover":
					if(typeof options.event_onItemMouseOver == "function"){
						var objData = getEventResponseData(data.event);
						options.event_onItemMouseOver(objData.data,objData.isFront,objData.image);
					}
				break;
				case "mouseout":
					if(typeof options.event_onItemMouseOut == "function"){
						var objData = getEventResponseData(data.event);
						options.event_onItemMouseOut(objData.data,objData.isFront,objData.image);
					}
				break;
				default:
					alert("wrong event");
					return(false);
				break;
			}
		}
		
		// If we have moved out of a carousel item (or the container itself),
		// restore the text of the front item in 1 second.
		jQuery(container).bind('mouseout',this,function(event){
				var	context = event.data;								
				
				//set timeout to start auto rotate back
				if ( options.autoRotate !== 'no' ){	
					clearTimeout(context.autoRotateBackTimer);				
					context.autoRotateBackTimer = setTimeout( function(){context.unpauseAutoRotate();},1000);
				}				
				
				//set timeout to bring the front text back
				if(options.setTextOnMouseOver == true){
					clearTimeout(context.showFrontTextTimer);				
					context.showFrontTextTimer = setTimeout( function(){context.showFrontText();},options.textShowFrontSpeed);
				}
		});
		
		
		//bind mouseover event to the images
		jQuery(container).find("img.cloudcarousel").bind('click',this,function(event){
				
				setOverlayText(jQuery(event.target).attr('title'),jQuery(event.target).attr('alt'));
				
				var	idx = jQuery(event.target).data('itemIndex');
				var	frontIndex = event.data.frontIndex;
				var	diff = idx - frontIndex;
				var item = items[idx];
				
				//fire click event:
				activateEvent("click",{event:event});
								
				//if the item is fron index or there is an option goto link on evety item, goto link
				if( (options.enableLinks == true) && (diff == 0 || options.linksFrontOnly == false)){
					
					//trace(item.link);
					
					if(item.link){
						if(item.linkOpenNewWindow)	//open link in new window
							window.open(item.link);
						else	//open link in same window
							location.href = item.link;
						return(false);	//don't rotate the carousel if opened link
					}					
				}
				
				//Bring to front
				if(options.bringToFront == false || diff == 0) return(false);
				
				ctx.rotateToItem(item.itemID);
		});
		
		
		// Prevent items from being selected as mouse is moved and clicked in the container.
		jQuery(container).bind('mousedown',this,function(event){
			event.data.container.focus();
			return false;
		});
		container.onselectstart = function () { return false; };		// For IE.

		this.innerWrapper = jQuery(container).wrapInner('<div style="position:absolute;width:100%;height:100%;"/>').children()[0];
	
		// Shows the text from the front most item.
		this.showFrontText = function(){
			if(items[this.frontIndex] === undefined ) return(false);	// Images might not have loaded yet.
			setOverlayText(jQuery(items[this.frontIndex].image).attr('title'),jQuery(items[this.frontIndex].image).attr('alt'));
		};
		
		this.go = function()
		{				
			if(this.controlTimer !== 0) { return; }
			var	context = this;
			this.controlTimer = setTimeout( function(){context.updateAll();},this.timeDelay);
		};
		
		this.stop = function()
		{
			clearTimeout(this.controlTimer);
			this.controlTimer = 0;				
		};
		
		//update front link cursor if there is a link
		this.updateFrontLinkCursor = function(){
			if(options.enableLinks == false) return(false);
			if(options.linksFrontOnly == false) return(false);	//update only if to front image
			var link;
			for(var i=0;i<items.length;i++){
				link = jQuery(items[i].image).parent();
				if(items[i].link){					
					if(i == this.frontIndex)
						link.css("cursor","pointer");	//set pointer to the front
					else 
						link.css("cursor","default");	//set default to all not front
				}				
			}
		}
		
		//get front item image and item data
		this.getFrontItem = function(){
			var frontItem = items[this.frontIndex];
			var objResponse = getItemResponseData(frontItem);
			objResponse.image = frontItem.image;
			return(objResponse);
		}
		
		//get image url
		//rotate right
		this.rotateRight = function(){
			if(options.reverseControls == false)
				this.rotate(1);
			else
				this.rotate(-1);
		}
		
		//rotate the gallery left
		this.rotateLeft = function(){
			if(options.reverseControls == false)
				this.rotate(-1);
			else 
				this.rotate(1);
		}
		
		//rotate the carousel to specifix index
		this.rotateToItem = function(itemID){
			var item = getItemById(itemID);
			if(!item) return(false);
			
			var itemIndex = jQuery(item.image).data("itemIndex");
			var	diff = itemIndex - this.frontIndex;
			if(diff == 0) return(false);
			
			if(diff<0) diff += items.length;
			else 
			if(diff >= items.length) diff -= items.length;
			
			diff2 = -(items.length - diff);	
			
			//rotate the smallest diff
			if(Math.abs(diff) < Math.abs(diff2))
				this.rotate(-diff);
			else 
				this.rotate(-diff2);
		}
		
		//get item by it's ID
		function getItemById(itemID){
			for(var i=0;i<items.length;i++)
				if(String(items[i].itemID) == String(itemID)) return(items[i]);
			return(null);
		}
		
		//get item image url, by item ID
		this.getItemImageUrl = function(itemID,width,height,exact){
			var item = getItemById(itemID);
			if(!item) return(null);
			var imageUrl = options.func_generateImageUrl(options.url_server,item.filename,width,height,exact);
			return(imageUrl);
		}
		
		// Starts the rotation of the carousel. Direction is the number (+-) of carousel items to rotate by.
		this.rotate = function(direction)
		{	
			this.frontIndex -= direction;
			
			//by length correction:
			this.frontIndex %= items.length;
			if(this.frontIndex < 0) this.frontIndex += items.length;
			
			this.destRotation += ( Math.PI / items.length ) * ( 2*direction );
			this.showFrontText();
			this.updateFrontLinkCursor();
			activateEvent("frontchange");
			this.go();
		};
		
		//remove the pause on auto rotate
		this.unpauseAutoRotate = function(){
			this.autoRotatePaused = false;
		}
		
		//on auto rotate step, if the rotation is not paused, rotate one step
		this.onAutoRotateStep = function(){
			//if paused - quit
			if(this.autoRotatePaused == true) return(false);
			
			var dir = (options.autoRotate === 'right')? 1 : -1;
			this.rotate(dir);
		}
		
		//start auto rotation
		this.autoRotate = function()
		{			
			if ( options.autoRotate !== 'no' )
				setInterval( function(){ctx.onAutoRotateStep();}, options.autoRotateDelay );
		};
		
		// This is the main loop function that moves everything.
		this.updateAll = function()
		{											
			var	minScale = options.minScale;	// This is the smallest scale applied to the furthest item.
			var smallRange = (1-minScale) * 0.5;
			var	w,h,x,y,scale,item,sinVal;
						
			var	change = (this.destRotation - this.rotation);				
			var	absChange = Math.abs(change);
	
			this.rotation += change * options.speed;
			if ( absChange < 0.001 ) { this.rotation = this.destRotation; }			
			var	itemsLen = items.length;
			var	spacing = (Math.PI / itemsLen) * 2; 
			//var	wrapStyle = null;
			var	radians = this.rotation;
			var	isMSIE = isIE();
			
			// Turn off display. This can reduce repaints/reflows when making style and position changes in the loop.
			// See http://dev.opera.com/articles/view/efficient-javascript/?page=3			
			this.innerWrapper.style.display = 'none';		
			
			var	style;
			var	px = 'px', reflHeight;	
			var context = this;
			
			for (var i = 0; i<itemsLen ;i++)
			{
				item = items[i];
				
				sinVal = funcSin(radians);
				
				scale = ((sinVal+1) * smallRange) + minScale;
				
				//set opacity effect
				if(options.setOpacityByScale == true)
					setOpacity(items[i].image,scale);
				
				x = this.xCentre + (( (funcCos(radians) * this.xRadius) - (item.orgWidth*0.5)) * scale);
				y = this.yCentre + (( (sinVal * this.yRadius)  ) * scale);		
		
				if (item.imageOK)
				{
					var	img = item.image;
					w = img.width = item.orgWidth * scale;					
					h = img.height = item.orgHeight * scale;
					img.style.left = x + px ;
					img.style.top = y + px;
					img.style.zIndex = "" + (scale * 100)>>0;	// >>0 = Math.foor(). Firefox doesn't like fractional decimals in z-index.
					if (item.reflection !== null)
					{																										
						reflHeight = options.reflHeight * scale;						
						style = item.reflection.element.style;
						style.left = x + px;
						style.top = y + h + options.reflGap * scale + px;
						style.width = w + px;								
						if (isMSIE)
						{											
							style.filter.finishy = (reflHeight / h * 100);				
						}else
						{								
							style.height = reflHeight + px;															
						}																													
					}					
				}
				radians += spacing;
			}
			// Turn display back on.					
			this.innerWrapper.style.display = 'block';

			// If we have a preceptable change in rotation then loop again next frame.
			if ( absChange >= 0.001 )
			{				
				this.controlTimer = setTimeout( function(){context.updateAll();},this.timeDelay);		
			}else
			{
				// Otherwise just stop completely.				
				this.stop();
			}
		}; // END updateAll		
		
		// Create an Item object for each image	
//		func = function(){return;ctx.updateAll();} ;

		//make the buttons appear
		this.showButtons = function(){
			if(!options.buttonLeft) return(false);
				var but_left = options.buttonLeft;
				var but_right = options.buttonRight;
				
				//appear buttons with fade in
				if(options.buttonsAppearFadeIn == true){
					jQuery(but_left).fadeIn();
					jQuery(but_right).fadeIn();
				}
				else{	//appear without fade in
					jQuery(but_left).show();
					jQuery(but_right).show();
				}
		}
		
		
		// Check if images have loaded. We need valid widths and heights for the reflections.
		this.checkImagesLoaded = function()
		{
			var	i,arrItems = [],item,itemData;
			var onInitEventEnabled = (typeof options.event_onImagesLoaded == "function")?true:false;

			//if some of the image is not complete, continue checking.
			for(i=0;i<images.length;i++){
				if ( (images[i].width === undefined) || ( (images[i].complete !== undefined) && (!images[i].complete)  ))
					return;
			}
			
			for(i=0;i<images.length;i++){
				//when the image is loaded
				item = new Item( images[i], options);
				items.push(item);
				jQuery(images[i]).data('itemIndex',i);				
				setOpacity(images[i],1);
				
				//set info for event
				if(onInitEventEnabled == true){
					itemData = getItemResponseData(item);
					itemData.image = images[i];
					arrItems.push(itemData);
				}
			}
			
			//run event - event_onImagesLoaded
			if(onInitEventEnabled == true)
				options.event_onImagesLoaded(arrItems,this);
			
			// If all images have valid widths and heights, we can stop checking.
			clearInterval(this.tt);
						
			this.showButtons();			
			this.showFrontText();			
			this.updateFrontLinkCursor();
			this.autoRotate();
			this.updateAll();
			return(false);
		};

		this.tt = setInterval( function(){ctx.checkImagesLoaded();},20);
	}; // END Controller object
	
	// The jQuery plugin part. Iterates through items specified in selector and inits a Controller class for each one.
	jQuery.fn.CloudCarousel = function(options) {
		
		this.each( function() {			
			
			options = jQuery.extend({}, {						
							   reflHeight:0,							   
							   reflOpacity:0.5,
							   reflGap:0,
							   minScale:0.5,
							   xPos:0,
							   yPos:0,
							   xRadius:0,
							   yRadius:0,
							   altBox:null,
							   titleBox:null,
							   FPS: 30,
							   autoRotate: 'no',
							   autoRotateDelay: 1500,
							   speed:0.2,
							   mouseWheel: false,
							   bringToFront: false,
							   textShowFrontSpeed:1000,
							   setTextOnMouseOver:true,
							   setOpacityByScale:false,
							   buttonsAppearFadeIn:true,
							   linksFrontOnly:false,
							   func_generateImageUrl:null,	//image url function
							   url_server:"",
							   reverseControls:false,
							   enableLinks:false,
							   event_onImagesLoaded:null,
							   event_onItemClick:null,
							   event_onItemMouseOver:null,
							   event_onItemMouseOut:null,
							   event_onFrontItemChange:null							   
			},options );
			// Create a Controller for each carousel.		
			jQuery(this).data('cloudcarousel', new Controller( this, jQuery('.cloudcarousel',jQuery(this)), options) );
		});				
		return this;
	};

})(jQuery);

//SOME ERROR MESSAGES IN CASE THE PLUGIN CAN NOT BE LOADED
function carousel3d_showDoubleJqueryError(sliderID) {
	var errorMessage = "Carousel 3D Error: You have some jquery.js library include that comes after the carousel files js include.";
	errorMessage += "<br> This includes make eliminates the carousel 3d libraries, and make it not work.";
	errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Carousel Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
	errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
	errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>"
		jQuery(sliderID).show().html(errorMessage);
}
