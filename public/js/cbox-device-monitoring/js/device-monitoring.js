﻿//var deviceMonitoringModule = (function () {
var dm = function (opt) {
    var isPortalPage = false;
    if (opt && opt.isPortalPage) {
        isPortalPage = true;
    }
    var buid = ""
        , buids = []
        , devices = []
        , filteredDevices = []
    ;

    var deviceStatuses = [
        {
            name: "OK",
            val:0
        },
        {
            //name: "Critical",
            name: "COK",
            val: 0
        },
        {
            name: "NOK",
            val: 0
        }
    ];
    var buidsParams = [
    {
        name: "BUSINESS_NAME",
        display: "BUSINESS NAME"
    },
    {
        name: "BUID",
        display: "BUID"
    }];

    var businessParamName = buidsParams[0].name;
    var businessDisplayName = buidsParams[0].display;
    var buidParamName = buidsParams[1].name;
    var buidDisplayName = buidsParams[1].display;

    var serviceUrls = {
        buids: "http://54.187.68.215:8888/CboxWeb/webapi/proc/get-all-buid-names",
        deviceHealthInfo: "http://54.187.68.215:8888/CboxAppManager/webapi/device-health-info.json?p_buid="
    }

    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    function getDataFromService(url, params) {
        var returnData;
        $.ajax({
            async: false,
            type: "GET",
            url: url,
            data: params,
            success: function (_data) {
                if (_data.status == "OK") {
                    returnData = _data.data;
                } else {
                    //alert('service data status is not "OK". Check data from console! ' + _data.developerMessage);
                    console.log(_data);
                    returnData = {}
                }
            }
        });
        return returnData;
    }

    function getBuidFromParams() {
        buid = getParameterByName("buid");
    }
    function getBuidFromCookie() {
        buid = $.cookie("buid");
    }
    function getBuidsFromService() {
        buids = getDataFromService(serviceUrls.buids);
    }
    function getDevicesFromService() {
        devices = getDataFromService(serviceUrls.deviceHealthInfo + buid);
        //for (var i = 0; i < 5; i++) {
        //    var dt = new Date();
        //    dt.setMinutes(new Date().getMinutes() - (i * 3));
        //    var d = { "MAC_ID": "00:23:a7:43:2b:2c", "VERSION_NAME": "Ice Cream Sandwich", "DEVICE_NAME": "RECHARGER APP", "GPS_POINTS": "37.422,-122.0840", "DEVICE_ID": "20705011" + i, "CPU": "0.9444444", "SAM_ID": 102050000083, "DATA_SENT": "0", "IMEI": "358173050171662", "DATA_RECIEVED": "0", "INTERFACE_NAME": "GPRS Only", "BATTERY_LIFE": "100", "IN_DATE": dt.toLocaleString(), "MEMORY_INFO": "1,826MB/1,971MB" };
        //    d.GPS_POINTS = "4" + (i + 5).toString() + "." + (3 * i).toString() + ",-11" + (i + 5).toString() + "." + (3 * i + 3).toString();
        //    devices.push(d);
        //}
        filteredDevices = devices;

    }

    function setLastUpdateTime() {
        $('#deviceStatusSummaryDiv h4 span').html(new Date().toLocaleString());
    }

    function setDeviceStatusesForPieChart() {
        deviceStatuses[0].val = 0;
        deviceStatuses[1].val = 0;
        deviceStatuses[2].val = 0;

        for (var i = 0; i < filteredDevices.length; i++) {
            var tDate = new Date(filteredDevices[i].IN_DATE);
            var now = new Date();
            var diff = (now.getTime() - tDate.getTime())/(60*1000);

            if (diff > 10) {
                deviceStatuses[2].val++;
            } else if (diff > 5) {
                deviceStatuses[1].val++;
            } else {
                deviceStatuses[0].val++;
            }
        }

    }
    function drawPieChart() {
        var chart = AmCharts.makeChart("chartDiv", {

            "type": "pie",
            "pathToImages": "/js/amcharts/amcharts/images/",
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "innerRadius": "65%",
            "colors": [
                "#9fd453",
                "#f1bf48",
                "#fb5d51"
            ],
            "labelTickAlpha": 0.1,
            "outlineColor": "",
            "outlineThickness": 0,
            "titleField": "name",
            "valueField": "val",
            "color": "#FFF",
            "fontSize": 20,
            "theme": "default",
            "allLabels": [],
            "balloon": {},
            "legend": {
                "align": "center",
                "markerType": "circle"
            },
            "titles": [],
            "dataProvider": deviceStatuses,
        });

        //$('#deviceStatusSummaryDiv').removeClass('hidden');
    }
    function drawTable() {
        console.log(devices);
        var oTable = $('#deviceTable').dataTable({
            responsive: false,
            data: devices,
            bPaginate: false,
            //dom: '<"clear">lfrtip',
            //dom: '<"row"<"col-md-10"r><"col-md-2 pull-right"f>>tip',
            dom:"",
            tableTools: {
                "sSwfPath": "/js/datatable/TableTools-2.2.3/swf/copy_csv_xls_pdf.swf",
            },
            "language": {
                url: '/js/datatable/EN.json'
            },
            "columns": [
                { "title":"","mRender": function (data, type, full) {
                    return '<input class="select-row-checkbox" type="checkbox" checked value="' + full.DEVICE_ID + '">';
                    }, 'bSortable': false,
                },
                { "title": "Device ID", "mData": "DEVICE_ID", "mRender": function (a) { return '<a style="color:lightblue" href="device_monitoring_utility?device=' + a + '">' + a + '</a>' } },
                { "title": "SAM ID","mData":"SAM_ID" },
                { "title": "Interface", "mData": "INTERFACE_NAME" },
                { "title": "App. Version", "mData": "VERSION_NAME" },
                { "title": "Device Type", "mData": "DEVICE_NAME" },
                //{ "title": "Mac", "mData": "MAC_ID" },
                //{ "title": "Imei", "mData": "IMEI" },
                { "title": "CPU Usage", "mData": "CPU", "mRender": function (a) {return Math.round(a)/100;}},
                { "title": "Memory Usage", "mData": "MEMORY_INFO" },
                { "title": "Battery", "mData": "BATTERY_LIFE" },
                { "title": "Location", "mData": "GPS_POINTS" },
                { "title": "Last Transmission", "mData": "IN_DATE" },
                { "title": "Status", "mRender": function () { return '<div class="ok deviceStatus"></div>'; } }
            ],
            "order": [[1, "asc"]],
            "columnDefs": [

            ],
            "fnInitComplete": function (oSettings, json) {
                $('#deviceTable thead th').eq(0).html('<input id="select-all-checkbox" checked type="checkbox">');
                $('#deviceTable thead th').eq(0).css('padding', "3px 3px 10px 3px");
                $('#deviceTable .select-row-checkbox').click(function () {
                    filterChanged();
                });
                $('#select-all-checkbox').change(function () {
                    var _checked = this.checked;
                    $('.select-row-checkbox').each(function () {
                        this.checked = _checked;
                    }); 
                    filterChanged();
                })
            },
            "fnDrawCallback": function (oSettings) {

            }
        });
        $('#table-search-input').on( 'keyup',function () {
            oTable.fnFilter($(this).val());
            filterChanged();
        });
        
    }
    function filterChanged() {
        var filteredDeviceIds = [];
        $('.select-row-checkbox:checked').each(function () {
            filteredDeviceIds.push($(this).val());
        })
        if (filteredDeviceIds.length != filteredDevices.length) {

            filteredDevices = [];
            devices.forEach(function (a) {
                if (filteredDeviceIds.indexOf(a.DEVICE_ID.toString()) != -1) {
                    filteredDevices.push(a);
                }
            });
            drawMap();
            setDeviceStatusesForPieChart();
            drawPieChart();
        }
    }
    function drawMap() {
        var latlng = [];
        var mapOptions = {
            zoom: 4
        }
        var map = new google.maps.Map(document.getElementById('deviceMapsWrapper'), mapOptions);
        //var styles = [{ "featureType": "landscape", "stylers": [{ "saturation": -100 }, { "lightness": 65 }, { "visibility": "on" }] }, { "featureType": "poi", "stylers": [{ "saturation": -100 }, { "lightness": 51 }, { "visibility": "simplified" }] }, { "featureType": "road.highway", "stylers": [{ "saturation": -100 }, { "visibility": "simplified" }] }, { "featureType": "road.arterial", "stylers": [{ "saturation": -100 }, { "lightness": 30 }, { "visibility": "on" }] }, { "featureType": "road.local", "stylers": [{ "saturation": -100 }, { "lightness": 40 }, { "visibility": "on" }] }, { "featureType": "transit", "stylers": [{ "saturation": -100 }, { "visibility": "simplified" }] }, { "featureType": "administrative.province", "stylers": [{ "visibility": "off" }] }, { "featureType": "water", "elementType": "labels", "stylers": [{ "visibility": "on" }, { "lightness": -25 }, { "saturation": -100 }] }, { "featureType": "water", "elementType": "geometry", "stylers": [{ "hue": "#ffff00" }, { "lightness": -25 }, { "saturation": -97 }] }];
        var styles = [{ featureType: "All", elementType: "geometry.stroke", stylers: [{ visibility: "simplified" }] }, { featureType: "landscape.man_made", elementType: "geometry.fill", stylers: [{ visibility: "off" }] }, { featureType: "landscape.natural", elementType: "All", stylers: [{ visibility: "off" }] }, { featureType: "poi", elementType: "All", stylers: [{ visibility: "off" }] }, { featureType: "transit.line", elementType: "All", stylers: [{ visibility: "off" }] }, { featureType: "landscape", elementType: "All", stylers: [{ visibility: "on" }] }, { featureType: "road.highway", elementType: "All", stylers: [{ visibility: "on" }, { weight: 1.7 }, { saturation: -100 }, { lightness: -42 }, { gamma: 2 }] }, { featureType: "landscape.man_made", elementType: "labels", stylers: [{ visibility: "off" }] }, { featureType: "All", elementType: "geometry", stylers: [{ saturation: -19 }, { visibility: "simplified" }] }, { featureType: "water", elementType: "All", stylers: [{ lightness: -70 }, { saturation: -83 }] }, { featureType: "landscape", elementType: "All", stylers: [{ gamma: .97 }, { saturation: -100 }, { lightness: -72 }] }, { featureType: "road.local", elementType: "All", stylers: [{ lightness: -78 }, { gamma: 2.13 }] }, { featureType: "road.arterial", elementType: "All", stylers: [{ lightness: -74 }, { gamma: 2.13 }] }, { featureType: "All", elementType: "labels.text.stroke", stylers: [{ gamma: .41 }] }, { featureType: "All", elementType: "labels.text.stroke", stylers: [{ gamma: .32 }, { visibility: "off" }] }, { featureType: "All", elementType: "labels.text", stylers: [{ saturation: -100 }, { lightness: -100 }, { gamma: 9.99 }] }, { featureType: "poi", elementType: "All", stylers: [{ visibility: "off" }] }, { featureType: "All", elementType: "geometry", stylers: [{ visibility: "simplified" }] }, { featureType: "All", elementType: "All", stylers: [{ saturation: -82 }, { lightness: -44 }, { gamma: .92 }] }, { featureType: "All", elementType: "labels.text.fill", stylers: [{ lightness: 68 }] }, { featureType: "poi", elementType: "All", stylers: [{ visibility: "off" }] }, { featureType: "transit.line", elementType: "All", stylers: [{ visibility: "simplified" }, { lightness: -60 }] }, { featureType: "landscape.natural", elementType: "All", stylers: [{ visibility: "on" }] }, { featureType: "road.local", elementType: "All", stylers: [{ lightness: -59 }] }, { featureType: "road.arterial", elementType: "All", stylers: [{ saturation: 7 }, { hue: "#0066ff" }, { gamma: .89 }] }, { featureType: "transit", elementType: "All", stylers: [{ lightness: -38 }, { gamma: 1.5 }] }, { featureType: "water", elementType: "All", stylers: [{ hue: "#0022ff" }, { gamma: 1.38 }, { lightness: -32 }, { saturation: -4 }] }, { featureType: "administrative", elementType: "All", stylers: [{ lightness: -64 }] }, { featureType: "administrative.locality", elementType: "All", stylers: [{ lightness: 70 }] }, { featureType: "All", elementType: "All", stylers: [{ saturation: 1 }, { hue: "#0022ff" }] }, { featureType: "transit", elementType: "All" }, { featureType: "road.local", stylers: [{ gamma: .89 }] }, { elementType: "labels.text.fill", stylers: [{ lightness: 16 }, { gamma: 1.03 }] }];
        map.setOptions({ styles: styles });
        var infowindow = new google.maps.InfoWindow();

        for (var i = 0; i < filteredDevices.length; i++) {
            var markerTitle = devices[i].DEVICE_ID.toString();
            var gpsPoints = devices[i].GPS_POINTS.split(',');
            var lat = 0;
            var lng = 0;

            if (Array.isArray(gpsPoints) && gpsPoints.length > 1) {
                lat = parseFloat(gpsPoints[0]);
                lng = parseFloat(gpsPoints[1]);
            }

            var myLatlng = new google.maps.LatLng(lat, lng);
            latlng.push(myLatlng);

            var image = {
                url: '/js/cbox-device-monitoring/media/marker.png',
                scaledSize: new google.maps.Size(24, 24)
            }

            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: markerTitle,
                icon: image,
            });

            google.maps.event.addListener(marker, 'click', (function (marker, markerTitle, infowindow) {
                return function () {
                    infowindow.setContent(markerTitle);
                    infowindow.open(map, marker);
                };
            })(marker, markerTitle, infowindow));
        }

        var latlngbounds = new google.maps.LatLngBounds();
        for (var i = 0; i < latlng.length; i++) {
            latlngbounds.extend(latlng[i]);
        }
        map.setCenter(latlngbounds.getCenter());
        map.fitBounds(latlngbounds);

        
    }
    function sortBuids(sortParam) {
        var sortedBuids = buids.sort(function (a, b) {
            return a[sortParam].localeCompare(b[sortParam]);
        });
        buids = sortedBuids;
    }

    function sortBuidsByName() {
        sortBuids(businessParamName);
    }
    function appendBuidstoSelectList() {
        $.each(buids, function (b) {

            var selected = this[buidParamName] == buid ? " selected":"";
            $('#buidSelectList')
                .append(
                 $("<option"+selected+"></option>")
                .attr("value", this[buidParamName])
                .text(this[businessParamName] + " - " + this[buidParamName]));
        });

    }
    function bindSelectListEvent() {
        $('#buidSelectList').change(function () {
            console.log(this);
            window.location.href= window.location.href.split("?")[0] + "?buid=" + $(this).val();
        })
    }
    function bindToggleWrapper() {
        $('#toggle-wrapper-btn').click(function () {
            if ($(this).html() == "Show Map") {
                $('#deviceMapsDiv').show();
                $('#deviceStatusSummaryDiv').hide();
                $(this).html('Show Chart');
                drawMap();
            } else {
                $('#deviceMapsDiv').hide();
                $('#deviceStatusSummaryDiv').show();
                $(this).html('Show Map');
                drawPieChart();
            }
            return false;
        })
    }
    return {
        init: function () {
            if (isPortalPage) {
                getBuidFromCookie();
                console.log(buid);
                $('#buidSelectList').parent().hide();
            } else {
                getBuidFromParams();
                getBuidsFromService();
                sortBuidsByName();
                appendBuidstoSelectList();
                bindSelectListEvent();
            }
            bindToggleWrapper();
            if (buid != -1 && buid != "") {
                getDevicesFromService();
                setLastUpdateTime();
                setDeviceStatusesForPieChart();
                drawMap();
                drawTable();
            } else {
                $('#deviceMapsDiv').hide();
                $('#buidSelectList').parent().parent().siblings().children().hide();
            }
        },
        //tests
        buid: buid,
        buids:buids
    }
};

