﻿var dm = (function () {
    var deviceId = "";
    var deviceInfo = {};
    var cpuData = [];
    var memoryData = [];
    var sendReceiveData = [];
    var reloadIntervalFunc = 0;
    var serviceUrls = {
        deviceInfo: "http://54.187.68.215:8888/CboxAppManager/webapi/device-single-health-data.json?p_device_id=",
        deviceHealthInfo: "http://54.187.68.215:8888/CboxAppManager/webapi/device-health-info.json?p_buid=",
        cpuData: "http://54.187.68.215:8888/CboxAppManager/webapi/device-bulk-cpu-data.json?p_device_id=",
        sendReceiveData: "http://54.187.68.215:8888/CboxAppManager/webapi/device-sent-receive-data.json?p_device_id=",
        criticalLogs: "http://54.187.68.215:8888/CboxWeb/webapi/sm/get-logging-data?p_search_id=###&p_search_by=1",
        transmissionLogs: "http://54.187.68.215:8888/CboxAppManager/webapi/device-log-data.json?p_device_id=",
        deviceRegistryData: "http://54.187.68.215:8888/CboxAppManager/webapi/device-call-intel-service.json?p_device_id="
    }
    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    function getDataFromService(url, params, callback) {
        var returnData;
        var _async = callback != undefined;

        $.ajax({
            type: "GET",
            url: url,
            data: params,
            async: _async,
            success: function (_data) {
                if (_data.status == "OK") {
                    if (_async) {
                        callback(_data.data);
                    } else {
                        returnData = _data.data;
                    }
                } else {
                    //alert('service data status is not "OK". Check data from console! ' + _data.developerMessage);
                    returnData = {}
                }
            }
        });
        return returnData;
    }

    function getdeviceIdFromParams() {
        deviceId = getParameterByName("device");
    }
    function getDeviceInfoFromService() {
        getDataFromService(serviceUrls.deviceInfo + deviceId, undefined, onDeviceInfoLoad);
    }
    function getCpuDataFromService() {
        getDataFromService(serviceUrls.cpuData + deviceId, undefined, onCpuDataLoad);
    }
    function getSendReceiveDataFromService() {
        getDataFromService(serviceUrls.sendReceiveData + deviceId, undefined, onSendReceiveDataLoad);
    }
    function getCriticalLogsFromService() {
        getDataFromService(serviceUrls.criticalLogs.replace("###",deviceId), undefined, onCriticalLogsDataLoad);
    }
    function getTransmisionLogsFromService() {
        getDataFromService(serviceUrls.transmissionLogs+ deviceId, undefined, onTransmissionLogsDataLoad);
    }
    function getdeviceRegistryDataFromService() {
        getDataFromService(serviceUrls.deviceRegistryData + deviceId, undefined, ondeviceRegistryDataLoad);
    }
    //CHARTS FUNCTIONS
    function setRefreshInterval() {
        var refreshTime = sessionStorage.getItem("refreshTime");
        if (refreshTime) {
            refreshTime = parseInt(refreshTime);
        } else {
            refreshTime = 5;
        }

        $('#refresh-selector option[value=' + refreshTime + ']').attr('selected', true);

        reloadIntervalFunc = setTimeout(function () {
            window.location.reload();
        }, refreshTime * 1000 * 60);

        $('#refresh-selector').change(function () {
            clearTimeout(reloadIntervalFunc);
            var refreshTime = $('#refresh-selector').val();
            sessionStorage.setItem("refreshTime", refreshTime);

            reloadIntervalFunc = setTimeout(function () {
                window.location.reload();
            }, refreshTime * 1000 * 60);

        })
    }
    function appendDeviceInfo() {
        var source = $('#deviceInfoContentTemplate').html();
        var template = Handlebars.compile(source);
        var html = template(deviceInfo);
        $('#deviceInfoList').html(html);
    }
    function setBatteryWidget() {
        var _b = deviceInfo.BATTERY_LIFE;
        $('#batteryUsageColor').css('height', _b * 1.25 + "px");
        $('#batteryUsageText').html(_b);
    }
    function setMap() {
        var gpsPoints = deviceInfo.GPS_POINTS.split(',');
        var lat = 0;
        var lng = 0;

        if (Array.isArray(gpsPoints) && gpsPoints.length > 1) {
            lat = parseFloat(gpsPoints[0]);
            lng = parseFloat(gpsPoints[1]);
        }

        var myLatlng = new google.maps.LatLng(lat, lng);

        var mapOptions = {
            zoom: 8,
            center: myLatlng
        }
        var map = new google.maps.Map(document.getElementById('deviceLocation'), mapOptions);
        //var styles = [{ "featureType": "landscape", "stylers": [{ "saturation": -100 }, { "lightness": 65 }, { "visibility": "on" }] }, { "featureType": "poi", "stylers": [{ "saturation": -100 }, { "lightness": 51 }, { "visibility": "simplified" }] }, { "featureType": "road.highway", "stylers": [{ "saturation": -100 }, { "visibility": "simplified" }] }, { "featureType": "road.arterial", "stylers": [{ "saturation": -100 }, { "lightness": 30 }, { "visibility": "on" }] }, { "featureType": "road.local", "stylers": [{ "saturation": -100 }, { "lightness": 40 }, { "visibility": "on" }] }, { "featureType": "transit", "stylers": [{ "saturation": -100 }, { "visibility": "simplified" }] }, { "featureType": "administrative.province", "stylers": [{ "visibility": "off" }] }, { "featureType": "water", "elementType": "labels", "stylers": [{ "visibility": "on" }, { "lightness": -25 }, { "saturation": -100 }] }, { "featureType": "water", "elementType": "geometry", "stylers": [{ "hue": "#ffff00" }, { "lightness": -25 }, { "saturation": -97 }] }];
        var styles = [{ featureType: "All", elementType: "geometry.stroke", stylers: [{ visibility: "simplified" }] }, { featureType: "landscape.man_made", elementType: "geometry.fill", stylers: [{ visibility: "off" }] }, { featureType: "landscape.natural", elementType: "All", stylers: [{ visibility: "off" }] }, { featureType: "poi", elementType: "All", stylers: [{ visibility: "off" }] }, { featureType: "transit.line", elementType: "All", stylers: [{ visibility: "off" }] }, { featureType: "landscape", elementType: "All", stylers: [{ visibility: "on" }] }, { featureType: "road.highway", elementType: "All", stylers: [{ visibility: "on" }, { weight: 1.7 }, { saturation: -100 }, { lightness: -42 }, { gamma: 2 }] }, { featureType: "landscape.man_made", elementType: "labels", stylers: [{ visibility: "off" }] }, { featureType: "All", elementType: "geometry", stylers: [{ saturation: -19 }, { visibility: "simplified" }] }, { featureType: "water", elementType: "All", stylers: [{ lightness: -70 }, { saturation: -83 }] }, { featureType: "landscape", elementType: "All", stylers: [{ gamma: .97 }, { saturation: -100 }, { lightness: -72 }] }, { featureType: "road.local", elementType: "All", stylers: [{ lightness: -78 }, { gamma: 2.13 }] }, { featureType: "road.arterial", elementType: "All", stylers: [{ lightness: -74 }, { gamma: 2.13 }] }, { featureType: "All", elementType: "labels.text.stroke", stylers: [{ gamma: .41 }] }, { featureType: "All", elementType: "labels.text.stroke", stylers: [{ gamma: .32 }, { visibility: "off" }] }, { featureType: "All", elementType: "labels.text", stylers: [{ saturation: -100 }, { lightness: -100 }, { gamma: 9.99 }] }, { featureType: "poi", elementType: "All", stylers: [{ visibility: "off" }] }, { featureType: "All", elementType: "geometry", stylers: [{ visibility: "simplified" }] }, { featureType: "All", elementType: "All", stylers: [{ saturation: -82 }, { lightness: -44 }, { gamma: .92 }] }, { featureType: "All", elementType: "labels.text.fill", stylers: [{ lightness: 68 }] }, { featureType: "poi", elementType: "All", stylers: [{ visibility: "off" }] }, { featureType: "transit.line", elementType: "All", stylers: [{ visibility: "simplified" }, { lightness: -60 }] }, { featureType: "landscape.natural", elementType: "All", stylers: [{ visibility: "on" }] }, { featureType: "road.local", elementType: "All", stylers: [{ lightness: -59 }] }, { featureType: "road.arterial", elementType: "All", stylers: [{ saturation: 7 }, { hue: "#0066ff" }, { gamma: .89 }] }, { featureType: "transit", elementType: "All", stylers: [{ lightness: -38 }, { gamma: 1.5 }] }, { featureType: "water", elementType: "All", stylers: [{ hue: "#0022ff" }, { gamma: 1.38 }, { lightness: -32 }, { saturation: -4 }] }, { featureType: "administrative", elementType: "All", stylers: [{ lightness: -64 }] }, { featureType: "administrative.locality", elementType: "All", stylers: [{ lightness: 70 }] }, { featureType: "All", elementType: "All", stylers: [{ saturation: 1 }, { hue: "#0022ff" }] }, { featureType: "transit", elementType: "All" }, { featureType: "road.local", stylers: [{ gamma: .89 }] }, { elementType: "labels.text.fill", stylers: [{ lightness: 16 }, { gamma: 1.03 }] }];
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: 'Device Location'
        });
        map.setOptions({ styles: styles });


    }
    function setCpuBarChart() {
        var cpuVal = Math.round(deviceInfo.CPU);

        AmCharts.makeChart("currentCpuChart",
            {
                "type": "gauge",
                "pathToImages": "/js/amcharts/amcharts/images/",
                "faceBorderColor": "#FFFFFF",
                "theme": "black",
                "startEffect": "bounce",

                "arrows": [
                    {
                        "id": "GaugeArrow-1",
                        "innerRadius": "0%",
                        "radius": "75%",
                        "value": cpuVal
                    }
                ],
                "axes": [
                    {
                        "axisThickness": 1,
                        "bottomText": cpuVal + "%",
                        "bottomTextYOffset": -20,
                        "endValue": 100,
                        "id": "GaugeAxis-1",
                        "valueInterval": 10,
                        "bands": [
                            {
                                "color": "#ea3838",
                                "endValue": 100,
                                "id": "GaugeBand-1",
                                "innerRadius": "90%",
                                "startValue": 70
                            },
                            {
                                "color": "#e6c04c",
                                "endValue": 70,
                                "id": "GaugeBand-2",
                                "innerRadius": "90%",
                                "startValue": 40
                            },
                            {
                                "color": "#a5d75e",
                                "endValue": 60,
                                "id": "GaugeBand-3",
                                "innerRadius": "90%",
                                "startValue": 0
                            }
                        ]
                    }
                ],
                "allLabels": [],
                "balloon": {},
                "titles": [
                    {
                        "id": "Title-1",
                        "size": 15,
                        "text": "Current CPU Utilization"
                    }
                ]
            }
        );
    }
    function setMemoryBarChart() {
        var memUsed = parseFloat(deviceInfo.MEMORY_INFO.split('/')[0].replace(',', '.'));
        var memTotal = parseFloat(deviceInfo.MEMORY_INFO.split('/')[1].replace(',', '.'));
        var memUsage = parseInt((memUsed / memTotal) * 100);

        AmCharts.makeChart("memoryChart",
        {
            "type": "gauge",
            "pathToImages": "/js/amcharts/amcharts/images/",
            "faceBorderColor": "#FFFFFF",
            "faceBorderWidth": 16,
            "startEffect": "bounce",
            "fontSize": 13,
            "theme": "black",
            "arrows": [
                {
                    "id": "GaugeArrow-1",
                    "value": memUsage
                }
            ],
            "axes": [
                {
                    "axisThickness": 1,
                    "bottomText": memUsage + "%",
                    "bottomTextYOffset": -20,
                    "endValue": 100,
                    "id": "GaugeAxis-1",
                    "valueInterval": 10,
                    "bands": [
                        {
                            "alpha": 0.91,
                            "balloonText": "",
                            "color": "#ea3838",
                            "endValue": 100,
                            "id": "GaugeBand-1",
                            "innerRadius": "88%",
                            "startValue": 70
                        },
                        {
                            "alpha": 0.89,
                            "color": "#e6c04c",
                            "endValue": 70,
                            "id": "GaugeBand-2",
                            "innerRadius": "84%",
                            "startValue": 40
                        },
                        {
                            "alpha": 0.66,
                            "color": "#a5d75e",
                            "endValue": 40,
                            "id": "GaugeBand-3",
                            "innerRadius": "80%",
                            "startValue": 0
                        }
                    ]
                }
            ],
            "allLabels": [],
            "balloon": {},
            "titles": [
                {
                    "id": "Title-1",
                    "size": 15,
                    "text": "Memory Utilization"
                }
            ]
        });
    }
    function setCpuLineChart() {
        var chart = AmCharts.makeChart("cpuUsageChart",
        {
            "type": "serial",
            "pathToImages": "/js/amcharts/amcharts/images/",
            "categoryField": "DATA_IN_DATE",
            "startDuration": 1,
            "color": "#FFF",
            //"fontSize": 15,
            "handDrawScatter": 8,
            "theme": "dark",
            //"dataDateFormat": "DD-MM-YYYY HH:NN:SS",
            "dataDateFormat": "YYYY-MM-DD HH:NN:SS",
            "height": "100%",
            "categoryAxis": {
                "parseDates": true,
                "minPeriod": "mm",
                "labelRotation": 0,
                //equalSpacing: false,
                //dateFormats: [{ period: 'fff', format: 'JJ:NN:SS' }, { period: 'ss', format: 'JJ:NN:SS' }, { period: 'mm', format: 'JJ:NN' }, { period: 'hh', format: 'JJ:NN' }, { period: 'DD', format: 'MMM DD' }, { period: 'WW', format: 'MMM DD' }, { period: 'MM', format: 'MMM' }, { period: 'YYYY', format: 'YYYY' }]
            },
            "valueAxes": [
                {
                    "id": "ValueAxis-1",
                    "title": "CPU Value"
                }
            ],
            "graphs": [
                {
                    "lineColor": "#FF6600",
                    //"colorField": "color",
                    "fillAlphas": 1,
                    "id": "AmGraph-1",
                    //"lineColorField": "color",
                    "title": "graph 1",
                    "type": "column",
                    "valueField": "val"
                }
            ],
            "titles": [
                {
                    "id": "Title-1",
                    "size": 15,
                    "text": "Hourly CPU Usage"
                }
            ],
            "chartCursor": {
                "categoryBalloonDateFormat": "YYYY-MM-DD HH:NN",
                "cursorPosition": "mouse"
            },
            "chartScrollbar": {
                backgroundColor: "#FFFFFF",
                selectedBackgroundAlpha:0.1,
                graphFillAlpha: 0.1,
                backgroundAlpha:0.1
            },
            "trendLines": [],
            "guides": [],
            "allLabels": [],
            "balloon": {},
            "dataProvider": cpuData
        });
        function zoomChart() {
            chart.zoomToIndexes(chart.dataProvider.length - 20, chart.dataProvider.length - 1);
        }

        chart.addListener("dataUpdated", zoomChart);
        zoomChart();

    }
    function setMemoryLineChart() {

    }
    function setSendReceiveLineChart() {
        var chart = AmCharts.makeChart("dataUsageChart",
        {
            "type": "serial",
            "startEffect": "bounce",
            "theme": "black",
            "height":"100%",
            "pathToImages": "/js/amcharts/amcharts/images/",
            "categoryField": "DATA_IN_DATE",
            "columnWidth": 0.81,
            "dataDateFormat": "YYYY-MM-DD HH:NN:SS",
            "categoryAxis": {
                "autoRotateCount": 0,
                "minPeriod": "mm",
                "parseDates": true,
                "axisAlpha": 0.44
            },
            "valueAxes": [
                {
                    "id": "ValueAxis-1",
                    "title": "Data Package"
                }
            ],
            "graphs": [
                {
                    "connect": false,
                    "fillAlphas": 0.5,
                    "lineAlpha": 1,
                    "title": "Data Package Sent",
                    "valueField": "DATA_SENT"
                },
                {
                    "connect": false,
                    "fillAlphas": 0.5,
                    "lineAlpha": 1,
                    "title": "Data Package Received",
                    "valueField": "DATA_RECIEVED"
                },
            ],
            "titles": [
                {
                    "id": "Title-1",
                    "size": 15,
                    "text": "Data Package Usage"
                }
            ],
            "chartScrollbar": {},
            "chartCursor": {
                "categoryBalloonDateFormat": "YYYY-MM-DD HH:NN"
            },
            "colors": [
                "#24B12A",
                "#EDE452"
            ],
            "trendLines": [],
            "guides": [],
            "allLabels": [],
            "balloon": {},
            "legend": {},
            "dataProvider": sendReceiveData
        });

        function zoomChart() {
            chart.zoomToIndexes(chart.dataProvider.length - 20, chart.dataProvider.length - 1);
        }

        chart.addListener("dataUpdated", zoomChart);
        zoomChart();
    }
    function setCloudSync() {
        var sending = true;
        setInterval(function () {
            if (sending){
                $('.cloud-sync marquee img').hide();
                $('#cloud-sync-img').show();
                sending = false;
            } else {
                $('.cloud-sync marquee img').show();
                $('#cloud-sync-img').hide();
                sending = true;
            }
            
        }, 10000);
    }
    // END OF CHARTS FUNCTIONS


    // SERVICE CALLBACK FUUNCTIONS
    function onDeviceInfoLoad(data) {
        deviceInfo = data[0];
        console.log(deviceInfo);

        appendDeviceInfo();
        setBatteryWidget();
        setMap();
        setCpuBarChart();
        setMemoryBarChart();
        setCloudSync();
    }
    function onCpuDataLoad(data) {
        for (var i = 0; i < data.length; i++) {
            data[i].date = parseDate(data[i].DATA_IN_DATE);
            data[i].val = parseFloat(data[i].CPU);
        }

        data = data.sort(function (a, b) {
            return a.date > b.date;
        });
        cpuData = data;
        //console.log(cpuData);
        setCpuLineChart();
    }
    function onMemoryDataLoad(data) {
        setMemoryLineChart();
    }
    function onSendReceiveDataLoad(data) {
        for (var i = 0; i < data.length ; i++) {
            data[i].date = parseDate(data[i].DATA_IN_DATE);
            data[i].DATA_SENT = getRandomInt(0, 10);
            data[i].DATA_RECIEVED = getRandomInt(0, 10);
        }

        data = data.sort(function (a, b) {
            return a.date - b.date;
        });
        sendReceiveData = data;
        console.log(sendReceiveData);
        setSendReceiveLineChart();
    }
    function onCriticalLogsDataLoad(data) {
        $('#deviceCriticalLogs ul').html('');
        for (var i = 0; i < data.length; i++) {
            $('#deviceCriticalLogs ul').append('<li style="color:rgb(230, 103, 109);" title="' + data[i].LOGGED_TIME + '">' + data[i].DESCRIPTION + '</li>')
        }
    }
    function onTransmissionLogsDataLoad(data) {
        $('#transmissionLogs ul').html('');
        for (var i = 0; i < data.length; i++) {
            //$('#transmissionLogs ul').append('<li title="' + data[i].DEVICE_TIMESTAMP + '">' + data[i].DESCRIPTION + '</li>')
            $('#transmissionLogs ul').append('<li><label>' + data[i].DEVICE_TIMESTAMP + '</label>' + data[i].DESCRIPTION + '</li>');
        }
    }
    function ondeviceRegistryDataLoad(data) {
        var source = $('#deviceRegistryContentTemplate').html();
        var template = Handlebars.compile(source);
        var html = template(data);
        $('#deviceRegistryBox').html(html);
    }
    // END OF SERVICE CALLBACK FUNCTIONS
    function parseDate(d) {
        try {
            var year = d.split('-')[2].split(' ')[0];
            var month = d.split('-')[1];
            var day = d.split('-')[0];
            var time = d.split(' ')[1];
            return new Date(year + "-" + month + "-" + day + " " + time);
            return year + "-" + month + "-" + day + " " + time;
        } catch (e) {
            return "";
        }
        
    }

    return {
        init: function () {
            getdeviceIdFromParams();
            if (deviceId != -1 && deviceId != "") {
                setRefreshInterval();
                getDeviceInfoFromService();
                getCpuDataFromService();
                getSendReceiveDataFromService();
                getCriticalLogsFromService();
                getTransmisionLogsFromService();
                getdeviceRegistryDataFromService();
            }
        },
        //tests
        deviceId: deviceId
    }
})();

$(function () {
    dm.init();
});
