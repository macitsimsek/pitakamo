﻿var dl = (function () {
    var oTable
        , sData
        , deviceData
        , logData
        , params
        , searchId
    ;
    var logService = 'http://54.187.68.215:8888/CboxWeb/webapi/sm/get-logging-data';
    var logDetailsService = 'http://54.187.68.215:8888/CboxWeb/webapi/sm/get-log-details?p_log_code=';
    var deviceInfoService = 'http://54.187.68.215:8888/CboxWeb/webapi/sm/get-device-information?p_device_id=';
    function getDataFromService(url, params, callback) {
        var returnData;
        var _async = callback != undefined;

        $.ajax({
            type: "GET",
            url: url,
            data: params,
            async: _async,
            success: function (_data) {
                if (callback) {
                    callback(_data);
                } else {
                    returnData = _data;
                }
            }
        });
        return returnData;
    }
    function setFormEvents() {
        $('#showDetailArea').click(function (e) {
            serializeForm();
            $('#search-result-div').fadeOut(200);
            $('#device-info').fadeOut(200);
            getDataFromService(logService, params, onDeviceLogsLoaded);
            getDataFromService(deviceInfoService + searchId, undefined,onDeviceInfoLoaded);
        });

        $('.inlineRadioOptions').change(function () {
            $('#search-input').val('');
            if ($('.inlineRadioOptions:checked').val() == "1") {
                $('#search-input').attr('placeholder', 'Device ID');
            } else {
                $('#search-input').attr('placeholder', 'SAM');
            }
        })
    }
    function serializeForm() {
        params = $('#search-form').serializeArray();
        searchId = params.filter(function (i) {
            return i.name == "p_search_id";
        })[0].value;
    }
    //CALLBACK FUNCTIONS
    function onDeviceLogsLoaded(data) {
        $('#search-result-div > div > h2').remove();
        if (data.status == "OK") {

            $('#search-result-table').dataTable({
                bDestroy: true,
                "aaData": data.data,
                dom: '<"clear">lfrtip',
                "aoColumns": [
                    { "sTitle": "Logged Time", "mData": "LOGGED_TIME" },
                    { "sTitle": "Log Type", "mData": "LOG_TYPE", "sClass": "log-code" },
                    { "sTitle": "Severity", "mData": "SEVERITY" },
                ],
                "fnInitComplete": function (oSettings, json) {

                },
                "fnDrawCallback": function (oSettings) {

                    $('#search-result-table tbody tr').on('click', function () {
                        $('#search-result-table tbody tr').removeClass("rowEffect");
                        $(this).addClass("rowEffect");

                        $('#device-logs').fadeOut(200);
                        $('#device-logs ul').html('');
                        var logCode = $(this).find('td.log-code').html();
                        getDataFromService(logDetailsService + logCode, onLogDetailLoaded);
                    });
                }
            });

            $('#search-result-div .adv-table').show();

        } else {
            $('#search-result-div > div').append('<h2 class="text-center">No device log found</h2>');
            $('#search-result-div .adv-table').hide();
        }
        
        $('#search-result-div').fadeIn(200);

    }
    function onDeviceInfoLoaded(data) {
        if (data.status == "OK") {
            deviceData = data.data[0];
            $('#info-device-id').html(deviceData.DEVICE_ID);
            $('#info-device-version').html(deviceData.DEVICE_ANDRIOD_V);
            $('#info-device-sam-id').html(deviceData.SAM_ID);
            $('#info-device-carrier').html(deviceData.CARRIER_TYPE);
            $('#info-device-owner').html(deviceData.OWNER_NAME);
            if (deviceData.APP_TYPE == "RECHARGE DEVICE") {
                $('#info-app-img').attr('src', "/js/cbox-device-log/img/recharger.png");
                $('#info-app-name').html('Recharger');
            } else {
                $('#info-app-img').attr('src', "/js/cbox-device-log/img/validator.png");
                $('#info-app-name').html('Validator');
            }
            $('#device-info').fadeIn(200);
        } else {

        }
    }
    function onLogDetailLoaded(data) {
        if (data.status == "OK") {
            logData = data.data;
            for (var i = 0; i < logData.length; i++) {
                $('#device-logs ul').append('<li class="list-group-item">' + logData[i].SYSTEM_DESCRIPTION + '</li>')
            }

            $('#device-logs').fadeIn(200);
        } else {

        }
    }
    return {
        init: function () {
            setFormEvents();
        }
    }
})().init();
