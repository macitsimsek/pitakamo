<?php

use yajra\Datatables\DatatablesServiceProvider ;

class UserInterfaceController extends Controller {
/***READ***/
	public function getLookAndFeel(){
        $lang = Language::getLanguage();
		$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."setting/entry-list");
	    $settings = json_decode($curl_response,true);
	    foreach($settings as $key1=>$val1){
		    if($key1 == 'data'){
			//print_r($val);exit;
			foreach($val1 as $key2=>$val2){
				if($val2['key'] == "LOOK_FEEL"){
				$value=$val2['value'];//webeo@posta.com
				//echo $value; exit;
				//logo::favicon.ico::TRY::Before::,::2::.::xxx,xxx,xxx::theme1
				$description="Look and feel";//WE Mail Group
					}
				}
			}
		} /*foreach($decoded as $key=>$val)*/
		$form = explode("::", $value);

		$x_siteLogo 			= $form[0];
		$x_favicon 				= $form[1];
		$x_currency 			= $form[2];
		$x_symbolPlacement 		= $form[3];
		$x_decimalSymbol 		= $form[4];
		$x_digitsAfter 			= $form[5];
		$x_digitGroupingSymbol 	= $form[6];
		$x_digitGrouping 		= $form[7];
		$x_theme 				= $form[8];
		return View::make('settings/look_feel',array("key"=>"LOOK_FEEL","value"=>$value,"description"=>$description,"x_siteLogo"=>$x_siteLogo,"x_favicon"=>$x_favicon,"x_currency"=>$x_currency,"x_symbolPlacement"=>$x_symbolPlacement,"x_decimalSymbol"=>$x_decimalSymbol,"x_digitsAfter"=>$x_digitsAfter,"x_digitGroupingSymbol"=>$x_digitGroupingSymbol,"x_digitGrouping"=>$x_digitGrouping,"x_theme"=>$x_theme));

	}

	public function getDashboard(){

		$lang = Language::getLanguage();
		$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."setting/entry-list");
	    $settings = json_decode($curl_response,true);
	    foreach($settings as $key1=>$val1){
		    if($key1 == 'data'){
			//print_r($val);exit;
			foreach($val1 as $key2=>$val2){
				if($val2['key'] == "DASHBOARD"){
						$value=$val2['value'];//webeo@posta.com
					}
				}
			}
		} /*foreach($decoded as $key=>$val)*/

		return View::make('settings/dashboard',array("value"=>$value));
	}

	public function postDashboard(){

		$lang = Language::getLanguage();
		$x_value = Input::get('x_value');
		//echo $x_value; exit;
		$data = array(
				"key"=>"DASHBOARD",
				"value"=>$x_value,
				"description"=>"Dashboard Settings"
				);
				//echo $data['value'];exit;
				$curl_response = CustomFunctions::CallAPI("PUT",Config::get('services.restUrl')."setting/entry-list",$data);

				$decoded = json_decode($curl_response);
				if (isset($decoded->status) && $decoded->status == 'ERROR') {
				App::abort(500, $decoded->developerMessage);

				}

		return Redirect::to('control_panel/user_interface/dashboard')->with('message', $lang->UPDATED);

	}

	public function getUserInterface(){
		return View::make('settings/user_interface');
	}
/***PUT***/	
	public function postLookAndFeel(){
		$lang = Language::getLanguage();
		$logo = Input::file('x_siteLogo');
		$favicon = Input::file('x_favicon');
		
		if(isset($logo) && $logo != ""){
		$x_siteLogo =Input::file('x_siteLogo');
	        $filename = "logo.". $x_siteLogo->getClientOriginalExtension();
	        $destinationPath = public_path().DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR;
	        Input::file('x_siteLogo')->move($destinationPath, $filename);
		}
		if(isset($favicon) && $favicon != ""){
		$x_favicon =Input::file('x_favicon');
	        $filename = "favicon.ico";
			//echo "hi";exit;			
	        $destinationPath = public_path().DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR;
	        Input::file('x_favicon')->move($destinationPath, $filename);	
		}
			$data = array(
				"key"=>"LOOK_FEEL",
				"value"=>"logo"."::"."favicon.ico"."::".Input::get('x_currency')."::".Input::get('x_symbolPlacement')."::".Input::get('x_decimalSymbol')."::".Input::get('x_digitsAfter')."::".Input::get('x_digitGroupingSymbol')."::".Input::get('x_digitGrouping')."::".Input::get('x_theme'),
				"description"=>"Look and feel"
				);
				//echo $data['value'];exit;
				$curl_response = CustomFunctions::CallAPI("PUT",Config::get('services.restUrl')."setting/entry-list",$data);

				$decoded = json_decode($curl_response);
				if (isset($decoded->status) && $decoded->status == 'ERROR') {
				App::abort(500, $decoded->developerMessage);

				}

		return Redirect::to('control_panel/user_interface/look_feel')->with('message', $lang->SETTING_ADDED_NEW);
	}



}
