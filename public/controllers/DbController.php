<?php

use yajra\Datatables\DatatablesServiceProvider ;

class DbController extends Controller {

	public function __construct()
    {

    }



	public function getData()
    {

        DB::setDateFormat('MM/DD/YYYY');

        $query = DB::table('mst_bus')->select(array('bus_id','car_no','depot_code','comp_code'));
		return Datatables::of($query)->make();

        /*
		$posts = DB::select('select bus_id,car_no from mst_bus where rownum<3');

		return  Datatables::of($posts)->make();

        /*
        return Datatables::of($posts)

        ->edit_column('comments', '{{ DB::table(\'comments\')->where(\'post_id\', \'=\', $id)->count() }}')

        ->add_column('actions', '<a href="{{{ URL::to(\'admin/blogs/\' . $id . \'/edit\' ) }}}" class="btn btn-default btn-xs iframe" >{{{ Lang::get(\'button.edit\') }}}</a>
                <a href="{{{ URL::to(\'admin/blogs/\' . $id . \'/delete\' ) }}}" class="btn btn-xs btn-danger iframe">{{{ Lang::get(\'button.delete\') }}}</a>
            ')

        ->remove_column('id')

        ->make();
        */
    }

}

