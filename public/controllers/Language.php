<?php
class Language {
    public static function getLanguage() {
        $message = 'Hello';
		$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."setting/entry-list");
		$decoded = json_decode($curl_response, true);

		if (isset($decoded->status) && $decoded->status == 'ERROR') {
            App::abort(500, $decoded->developerMessage);
		}
		
	foreach($decoded as $key=>$val){
	if($key == 'data'){
		//print_r($val);exit;
		foreach($val as $key1=>$val1){
			if($val1['key']=="DEFAULT_LANGUAGE_SETTINGS"){
				$siteLanguage = $val1['value'];
				
				}
			}
		}/*if($key == 'data')*/
	} /*foreach($decoded as $key=>$val)*/
	
				//echo $siteLanguage; exit;
		 if (Cache::has('language_array'))
		{
		    $object = Cache::get('language_array');
			$object = (object)$object;

        	return $object;
		}

		 $object = array();

		 Session::put('language', $siteLanguage);
		//echo $siteLanguage;exit;
		 $languageSTR = Session::get('language',$siteLanguage);
		//echo $languageSTR; exit;

		$controller = new LanguageController();

        $data = $controller->getItems($languageSTR);
		//print_r($data);

		//$data = DB::table('gen_language_entries')->where('local_str', '=', $languageSTR)->remember(1)->get(); //remember(10)



		foreach ($data as $row) {
		    $object[$row->item] = $row->text;
		}

		Cache::add('language_array', $object, 5);

		//print_r($data);
        $object = (object)$object;

        return $object;
    }
	
	
	public static function getTheme() {
		$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."setting/entry-list");
	    $settings = json_decode($curl_response,true);
	    foreach($settings as $key1=>$val1){
	    if($key1 == 'data'){
		//print_r($val);exit;
		foreach($val1 as $key2=>$val2){
			if($val2['key'] == "LOOK_FEEL"){
			$value=$val2['value'];//webeo@posta.com
				}		
			}
		}
	} /*foreach($decoded as $key=>$val)*/
	$form = explode("::", $value);
		$x_theme 				= $form[8];
		Session::put('siteTheme', $x_theme);
		$siteTheme = Session::get('siteTheme',$x_theme);
		 //echo $siteTheme; exit;
		//echo $x_theme;exit;
		return $siteTheme;
	}
}