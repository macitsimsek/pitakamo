<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	public function getIndex(){

		return View::make('hello');

	}

	public function getLogin(){

		return View::make('login');

	}

	public function postLogin(){

        return "asdasd";

	}



	public function getLogout(){

		Auth::logout();
				$lang = Language::getLanguage();
		return Redirect::to('/login')->with('message', $lang->YOU_ARE_NOW);

	}

	public function getRegister(){

		return View::make('register');

	}

	public function postForgotPassword(){

    	// Declare the rules for the validator
		$rules = array(
			'email' => 'required|email',
		);

		// Create a new validator instance from our dynamic rules
		$validator = Validator::make(Input::all(), $rules);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{   $lang = Language::getLanguage();
			// Ooops.. something went wrong
			return Redirect::to('/login')->withInput()->with('error', $lang->ENTER_YOUR_EMAIL);
		}

		$email = Input::get('email');



		$ldapController = new LdapController();
		$conn = $ldapController->loginLdap();
		$users = $ldapController->getUserList($conn);

		$buldu = false;
		$password = "";
        foreach ($users as $key=>$user){
           	if($user["email"] == $email ){
           		$password = $user["password"];
                $buldu = true;
           	}
        }


		$code = md5($email."__-sYI2sodj/283u-__".$password);

		//print_r($users);
		//var_dump($buldu);
		//echo $email."__-sYI2sodj/283u-__".$password;
		//echo "\r\n";
		//echo $code;
		//exit;

		if($buldu){
         	// Data to be used on the email view
			$data = array(
				'forgotPasswordUrl' => URL::route('forgot-password-confirm', $code),
			);

			// Send the activation code through email
			Mail::send('emails.forgot-password', $data, function($m) use ($email)
			{
				$m->to($email);
				$m->subject($lang->ACCOUNT_PASSWORD_RECOVERY);
			});
		}



        $lang = Language::getLanguage();
		return Redirect::to('/login')->with('message', $lang->PASSWORD_RESET_MAIL_SENDED);
	}


	public function getForgotPasswordConfirm($passwordResetCode){

	/*
    	// Declare the rules for the validator
		$rules = array(
			'passwordResetCode' => 'required',
		);

		// Create a new validator instance from our dynamic rules
		$validator = Validator::make(Input::all(), $rules);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{   $lang = Language::getLanguage();
			// Ooops.. something went wrong
			return Redirect::to('/login')->withInput()->with('error', "TEST");
		}

        */



		$ldapController = new LdapController();
		$conn = $ldapController->loginLdap();
		$users = $ldapController->getUserList($conn);

		// aef1d48bb0970bab97a09c076638078a
		$buldu = false;
		$userName = "";
        foreach ($users as $key=>$user){
        	$code = md5($user["email"]."__-sYI2sodj/283u-__".$user["password"]);
			//echo $user["email"]."__-sYI2sodj/283u-__".$user["password"]."======".$code."<br>";
           	if($code == $passwordResetCode ){
                $buldu = true;
				//print_r($user);
				$userName =  $user["username"];
           	}
        }

		//print_r($users);
		//var_dump($buldu);

		$newPassword =  "1234567";

		$result =  $ldapController->changeUserPassword($conn,$userName,$newPassword);
        //var_dump($result);
		//echo $code;
		//exit;

		if($result){
         	// Data to be used on the email view
			$data = array(
				'newPassword' => $newPassword,
			);

			// Send the activation code through email
			Mail::send('emails.password-changed', $data, function($m) use ($email)
			{
				$m->to($email);
				$m->subject($lang->ACCOUNT_PASSWORD_CHANGED);
			});
		}



        $lang = Language::getLanguage();
		return Redirect::to('/login')->with('message', $lang->PASSWORD_RESET_MAIL_SENDED);
	}


	public function getTest(){

		return View::make('test');

	}

	public function getMain(){


		$dataAry = CustomFunctions::GetJson("GET", "setting/entry-list");
		//print_r($dataAry);exit;
		$settings = -1;
		foreach($dataAry as $data){
           if($data->key == "DASHBOARD"){
              	$settings =  $data->value;
           }
		}
		//echo $settings; exit;
		return View::make('main',array("settings"=>$settings));
	}

}
