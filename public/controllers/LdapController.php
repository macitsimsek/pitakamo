<?php

class LdapController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function loginLdap()
	{
		$config = Config::get('auth.ldap');

		//print_r($config);

		// Connect to the domain controller
		if ( ! $conn = ldap_connect($config['host']))
		{
			throw new \Exception("Could not connect to LDAP host {$config['host']}: ".ldap_error($conn));
		}

		// Required for Windows AD
		ldap_set_option($conn, LDAP_OPT_PROTOCOL_VERSION, 3);
		ldap_set_option($conn, LDAP_OPT_REFERRALS, 0);

		// Enable search of LDAP
		if ( ! @ldap_bind($conn, "cn={$config['dn_user']},{$config['domain']}", $config['dn_pass']))
		{
			throw new \Exception('Could not bind to AD: '."cn={$config['dn_user']},{$config['domain']}: ".ldap_error($conn));
		}

		return $conn;
	}

    public function deleteGroup($conn,$groupName){

		$config = Config::get('auth.ldap');

		if(( @ldap_delete($conn, "cn=".$groupName.",".$config['groupdn'])))  {
			return true;
		} else {
			return false;
		}
	}


/*ASSOCIATE PERMISSON SCHEMA */
	/*public function createGroup($conn,$groupName,$description,$x_associate){

		$config = Config::get('auth.ldap');

   		$info["objectclass"][0] = "posixGroup";
		$info["cn"] = $groupName;
		$info["description"] = $description;
		$info["gidnumber"] = (time()%9999);
		$info["userPassword"] = "{crypt}x";

		if(( @ldap_add($conn, "cn=".$groupName.",".$config['groupdn'], $info)))  {
			return true;
		} else {
			return false;
		}
	}
*/
/*ASSOCIATE PERMISSON SCHEMA */








	public function createGroup($conn,$groupName,$description,$permission){

		$config = Config::get('auth.ldap');

        $gidNumber = (time()%9999);

   		$info["objectclass"][0] = "posixGroup";
		$info["cn"] = $groupName;
		$info["description"] = $description;
		$info["gidnumber"] = $gidNumber;
		$info["userPassword"] = "{crypt}x";

		if(( @ldap_add($conn, "cn=".$groupName.",".$config['groupdn'], $info)))  {

        	$data = array(
						"permissionId"=>$permission,
						"groupId"=>$gidNumber
						);


			$curl_response = CustomFunctions::CallAPI("POST",Config::get('services.restUrl')."permission/group-permission",$data);
			//print_r($data);
			//print_r($curl_response);exit;
			$decoded = json_decode($curl_response);
			if (isset($decoded->status) && $decoded->status == 'ERROR') {
				App::abort(500, $decoded->developerMessage);
			}

			return true;
		} else {
			return false;
		}
	}
	
	
	

	public function changeGroup($conn,$id,$name,$newName,$newDescription,$permission){

    	//echo "id : $id,name : $name,newName : $newName,desc : $newDescription, perm : $permission";
        //exit;
		$config = Config::get('auth.ldap');
	
		$info["cn"][0] = $newName;
		$info["description"][0] = $newDescription;
		/*once cn i yeniden adland�r*/
		$infoReady = array();
				$infoReady = array_filter($info);	
				
	    if(@ldap_rename( $conn, "cn=".$name.",".$config['groupdn'], "cn=".$newName, NULL, TRUE)){
			/*yeni cn ismini al*/
			$newdn = "cn=".$newName.",".$config['groupdn'];
			/*de�i�iklikleri yap*/
			ldap_mod_replace($conn, $newdn, $infoReady);

            // ONCE SILICEZ
	        $data = array(
						"groupId"=>$id
						);
			$curl_response = CustomFunctions::CallAPI("DELETE",Config::get('services.restUrl')."permission/group-permission?p_group_id=".$id);
	        //print_r($data);;
	        //print_r($curl_response);exit;
			$decoded = json_decode($curl_response);
			if (isset($decoded->status) && $decoded->status == 'ERROR') {
	            App::abort(500, $decoded->developerMessage);
			}
	        //exit;

			//SONRA EKLICEZ
	        $data = array(
						"permissionId"=>$permission,
						"groupId"=>$id
						);


			$curl_response = CustomFunctions::CallAPI("POST",Config::get('services.restUrl')."permission/group-permission",$data);
			//print_r($curl_response);exit;
			$decoded = json_decode($curl_response);
			if (isset($decoded->status) && $decoded->status == 'ERROR') {
				App::abort(500, $decoded->developerMessage);
			}

	        //exit;


			return true;
		}else {
			return false;
		}
	}

	public function getGroupList($conn){

		$config = Config::get('auth.ldap');

		$my_ldap_group_array = array();

		/*Following code snippet gets the group information and fill it into group array*/
		if (($search=@ldap_search($conn, $config['groupdn'], "(objectClass=posixGroup)"))) {
			$infoGroup = ldap_get_entries($conn, $search);
            //print_r($infoGroup);
			foreach($infoGroup as $r=>$v) {
				if($v["cn"][0]!="") {
					@$my_ldap_group_array[] = array(	"id"=>$v["gidnumber"][0],
														"name"=>$v["cn"][0],
														"memberlist"=>$v["memberuid"],
														"memberCount"=>$v["memberuid"]["count"],
														"description"=>$v["description"][0]
														);
					}


			}

		}/*End of group information gathering*/

		return $my_ldap_group_array;
	}



	public function getUserList($conn){

		$config = Config::get('auth.ldap');

		$groupAry = self::getGroupList($conn);


		$my_ldap_user_array = array();

		/*Following code snippet gets the group information and fill it into user array*/
	   if (($search=@ldap_search($conn, $config['basedn'], "(objectClass=inetOrgPerson)"))) {
			$infoGroup = ldap_get_entries($conn, $search);
		    //print_r($infoGroup); exit;

			foreach($infoGroup as $r=>$v) {
				if($v["cn"][0]!="") {

					$name = "";
					foreach($groupAry as $k2 => $v2){
			          	if($v["gidnumber"][0]==$v2["id"]){
			               	$name = $v2["name"];
			          	}
					}

					$roomNumber = 0;
					if(isset($v["roomnumber"]) && $v["roomnumber"][0]==1) $roomNumber = 1;

					@$my_ldap_user_array[] = array(
						"username"=>$v["uid"][0],
						"password"=>$v["userpassword"][0],
						"groupId"=>$name,
						"email"=>$v["mail"][0],
						"firstname"=>$v["givenname"][0],
						"departmentnumber"=>$v["departmentnumber"][0],
						"carlicense"=>$v["carlicense"][0],
						"title"=>$v["title"][0],
						"mobile"=>$v["mobile"][0],
						"surname"=>$v["sn"][0],
						"roomnumber"=>$roomNumber
						);
				}
			}

		}/*End of group information gathering*/

		return $my_ldap_user_array;
	}



	public function createUser($conn,$x_firstName,$x_middleName,$x_surname,$x_gender,$x_dateofBirth,$x_identity,$x_address,$x_zipCode,$x_city,$x_country,$x_mobileNumber,$x_email,$filename,$x_companyName,$x_department,$x_position,$x_username,$x_password,$groupId,$groupName){

		$config = Config::get('auth.ldap');
		
		  		// prepare data
		$info["objectclass"][0] = "top";
		$info["objectclass"][1] = "inetOrgPerson";
		$info["objectclass"][2] = "posixAccount";
		$info["objectclass"][3] = "shadowAccount";
		$info["givenName"] = $x_firstName;
		$info["initials"] = $x_middleName;
		$info["sn"] = $x_surname;
		$info["employeeType"] = $x_gender;
		$info["pager"] = $x_dateofBirth;
		$info["carLicense"] = $x_identity;
		$info["mobile"] = $x_mobileNumber;
		$info["homePostalAddress"] = $x_address;
		$info["postalCode"] = $x_zipCode;
		$info["l"] = $x_city;
		$info["st"] = $x_country;
		$info["mail"] = $x_email;
		$info["jpegPhoto"] = $filename;
		$info["o"] = $x_companyName;																
		$info["departmentNumber"] = $x_department;
		$info["title"] = $x_position;
		$info["gidNumber"] = $groupId;
		$info["cn"] = $groupName;
		$info["uid"] = $x_username;
		$info["uidNumber"] = 1003;
		//$info["$x_username"]=$x_username;
		//$x_password="{MD5}".base64_encode(pack("H*",md5($x_password)));
		//$info["gidNumber"] = 0;
		//$info["localityName"] = $x_username;
		//$info["description"] = $x_username;
		$info["homeDirectory"] = "/home/".$x_username;
		$info["loginShell"] = "/bin/bash";
		$info["userPassword"] = "{MD5}".base64_encode(pack("H*",md5($x_password)));
		$info["shadowLastChange"] = 0;
		$info["shadowMax"] = 0;
		$info["shadowWarning"] = 0;

		$info2["memberuid"][]=$x_username;
		$dn="uid=".$x_username.",".$config['basedn'];
		$dnGroup="cn=".$groupName.",".$config['groupdn'];
		/*echo "<pre>";
		print_r($info);
		echo "<pre>";
		echo $dn;
		exit;*/
		$infoReady = array();
				$infoReady = array_filter($info);	
		

		if(( @ldap_add($conn, $dn, $infoReady)))  {
			if(@ldap_mod_add($conn, $dnGroup, $info2)){

               	return true; 
				
			} else {

				return false;
			}
		} else {
			return false;
		}


	}

public function createUserRegister($conn,$x_firstName,$x_middleName,$x_surname,$x_gender,$x_dateofBirth,$x_identity,$x_address,$x_zipCode,$x_city,$x_country,$x_mobileNumber,$x_email,$filename,$x_companyName,$x_department,$x_position,$x_username,$x_password,$groupId,$groupName){

		$config = Config::get('auth.ldap');
		
		  		// prepare data
		$info["objectclass"][0] = "top";
		$info["objectclass"][1] = "inetOrgPerson";
		$info["objectclass"][2] = "posixAccount";
		$info["objectclass"][3] = "shadowAccount";
		$info["givenName"] = $x_firstName;
		$info["initials"] = $x_middleName;
		$info["sn"] = $x_surname;
		$info["employeeType"] = $x_gender;
		$info["pager"] = $x_dateofBirth;
		$info["carLicense"] = $x_identity;
		$info["mobile"] = $x_mobileNumber;
		$info["homePostalAddress"] = $x_address;
		$info["postalCode"] = $x_zipCode;
		$info["l"] = $x_city;
		$info["st"] = $x_country;
		$info["mail"] = $x_email;
		$info["jpegPhoto"] = $filename;
		$info["o"] = $x_companyName;																
		$info["departmentNumber"] = $x_department;		
		$info["title"] = $x_position;
		$info["gidNumber"] = $groupId;
		$info["cn"] = $groupName;
		$info["uid"] = $x_username;
		$info["uidNumber"] = 1003;
		$info["homeDirectory"] = "/home/".$x_username;
		$info["loginShell"] = "/bin/bash";
		$info["userPassword"] = "{MD5}".base64_encode(pack("H*",md5($x_password)));
		$info["shadowLastChange"] = 0;
		$info["shadowMax"] = 0;
		$info["shadowWarning"] = 0;

		$info2["memberUid"][]=$x_username;
		$dn="uid=".$x_username.",".$config['basedn'];
		$dnGroup="cn=newmember,ou=Groups,dc=identity,dc=manager"; //"cn=".$groupName.",".$config['groupdn'];
		
		$infoReady = array();
				$infoReady = array_filter($info);	
			

		if(( @ldap_add($conn, $dn, $infoReady)))  {
			if(@ldap_mod_replace($conn, $dnGroup, $info2)){

               	return true; 
				
			} else {

				return false;
			}
		} else {
			return false;
		}


	}



	public function changeGroupMembers($conn,$groupName,$selectedUsersname){
     	$config = Config::get('auth.ldap');


		//gruptaki tum memberlari sil

        $userAry = self::getUserList($conn);

		if(is_array($userAry)){
         	foreach($userAry as $k=>$v) {
				$info = array();
	         	$info["memberuid"][]=$v["username"];
				@ldap_mod_del($conn, "cn=".$groupName.",".$config['groupdn'], $info);
			}
		}




		$error = true;
		if(is_array($selectedUsersname)){
			foreach($selectedUsersname as $key => $username){
				$info = array();
	         	$info["memberuid"][]=$username;

				//gruba ekle
				if(@ldap_mod_add($conn, " cn=".$groupName.",".$config['groupdn'], $info)){
					//return true;
				} else {
					$error = false;
				}

			}
		}


		return $error;
	}

	public function changeUserData($conn,$userName,$x_firstName,$x_middleName,$x_surname,$x_gender,$x_dateofBirth,$x_identity,$x_address,$x_zipCode,$x_city,$x_country,$x_mobileNumber,$x_email,$x_photo,$x_companyName,$x_department,$x_position,$x_username,$x_password,$groupId,$groupName){

		$config = Config::get('auth.ldap');

        if (($search=@ldap_search($conn, "uid=".$userName.",".$config['basedn'], "cn=*"))) {
        	/*if($pass!="") {
        		$info["userPassword"]="{MD5}".base64_encode(pack("H*",md5($pass)));
			}*/
        //print_r($x_gender);exit;
		$info["givenName"] = $x_firstName;
		$info["initials"] = $x_middleName;
		$info["sn"] = $x_surname;
		$info["employeeType"] = $x_gender;
		$info["departmentNumber"] = $x_dateofBirth;
		$info["carLicense"] = $x_identity;
		$info["mobile"] = $x_mobileNumber;
		$info["homePostalAddress"] = $x_address;
		$info["postalCode"] = $x_zipCode;
		$info["l"] = $x_city;
		$info["st"] = $x_country;
		$info["mail"] = $x_email;
		$info["jpegPhoto"] = $x_photo;
		//var_dump($x_photo);exit;
		$info["o"] = $x_companyName;																
		$info["departmentNumber"] = $x_department;		
		$info["title"] = $x_position;
		$info["gidNumber"] = $groupId;
		$info["cn"] = $groupName;
		$info["uid"] = $userName;
		$info["uidNumber"] = 1003;
		//$info["$x_username"]=$x_username;
		//$x_password="{MD5}".base64_encode(pack("H*",md5($x_password)));
		//$info["gidNumber"] = 0;
		//$info["localityName"] = $x_username;
		//$info["description"] = $x_username;
		$info["homeDirectory"] = "/home/".$userName;
		$info["loginShell"] = "/bin/bash";
		$info["userPassword"] = "{MD5}".base64_encode(pack("H*",md5($x_password)));
		$info["shadowLastChange"] = 0;
		$info["shadowMax"] = 0;
		$info["shadowWarning"] = 0;


           // $info2["memberuid"][]=$userName;

				$infoReady = array();
				$infoReady = array_filter($info);		//Bo� de�erli anahtarlar� diziden ��kar

			    //print_r($infoReady);exit;


              $result = @ldap_mod_replace ($conn,  "uid=".$userName.",".$config['basedn'], $infoReady);
              return true;
        /*
		echo "<pre>";
			print_r($result);
			print_r($info);
				echo "</pre>";
			echo "uid=".$userName.",".$config['basedn'];
			exit;
		*/
			  		//eski grubtan sil
					//@ldap_mod_del($conn, "cn=".$oldGroupName.",".$config['groupdn'], $info2);

			  		//yeni gruba ekle
            		/*if(@ldap_mod_add($conn, " cn=".$groupName.",".$config['groupdn'], $info2)){
		               	return true;
					} else {
						return false;
					}*/
              
              //return false;
        }
	}


	
	public function deleteUser($conn,$userName){

		$config = Config::get('auth.ldap');

		$info2["memberuid"][]=$userName;

//echo $config['basedn'];
//exit;

		if (($search=@ldap_search($conn, $config['basedn'], "(uid=".$userName.")"))) {
    		$info = ldap_get_entries($conn, $search);
/*			print_r($info);exit;*/
							$groupId = $info[0]["gidnumber"][0];

            if (($search=@ldap_search($conn, $config['groupdn'], "(objectclass=posixGroup)"))) {

            	$groupinfo = ldap_get_entries($conn, $search);
				foreach($groupinfo as $r=>$v) {
					if($v["gidnumber"][0]==$groupId) {
						$groupName=$v["cn"][0];
					}
				}
        	}
		}

		//echo $groupName; exit;

		if(( @ldap_delete($conn, "uid=".$userName.",".$config['basedn'])))  {
			if(@ldap_mod_del($conn, "cn=".$groupName.",".$config['groupdn'], $info2)) {
               	return true;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}


		public function deleteMultipleUser($conn,$users){

		$config = Config::get('auth.ldap');

foreach($users as $userName){


		$info2["memberuid"][]=$userName;


		if (($search=@ldap_search($conn, $config['basedn'], "(uid=".$userName.")"))) {
    		$info = ldap_get_entries($conn, $search);
			$groupId = $info[0]["gidnumber"][0];

            if (($search=@ldap_search($conn, $config['groupdn'], "(objectclass=posixGroup)"))) {

            	$groupinfo = ldap_get_entries($conn, $search);
				foreach($groupinfo as $r=>$v) {
					if($v["gidnumber"][0]==$groupId) {
						$groupName=$v["cn"][0];
					}
				}
        	}
		}

		if(( @ldap_delete($conn, "uid=".$userName.",".$config['basedn'])))  {
			@ldap_mod_del($conn, "cn=".$groupName.",".$config['groupdn'], $info2);
			
		} 
		}//foreach($users as $userName){
			return true;
		
	}

	
	

	public function getUserInfo($conn,$userName){

		$config = Config::get('auth.ldap');

		$info2["memberuid"][]=$userName;



		if (($search=@ldap_search($conn, $config['basedn'], "(uid=".$userName.")"))) {
    		$info = ldap_get_entries($conn, $search);
			return $info;
		}


	}

	public function changeUserPassword($conn,$userName,$newPassword){

        	$config = Config::get('auth.ldap');
			echo "uid=".$userName.",".$config['basedn'], "cn=*";

        if (($search=@ldap_search($conn,  "uid=".$userName.",".$config['basedn'], "cn=*"))) {
        	   //	print_r($search);
              $info["userPassword"]="{MD5}".base64_encode(pack("H*",md5($newPassword)));
              if(@ldap_mod_replace ($conn, "uid=".$userName.",".$config['basedn'] , $info)) {
			        return true;
              }
              return false;
             }
        return false;
	}
	
	
	public function changeMultipleUserData($conn,$users,$department,$companyName,$userMessage,$position) {

		$config = Config::get('auth.ldap');
		//echo $conn.",".$department.",".$companyName.",".$userMessage.",".$position;
		//print_r($users);exit;
		
		foreach($users as $userName){

        if (($search=@ldap_search($conn, "uid=".$userName.",".$config['basedn'], "cn=*"))) {
		$info["o"] = $companyName;																
		$info["departmentNumber"] = $department;		
		$info["title"] = $position;
		$infoReady = array();
		$infoReady = array_filter($info);
        $result = @ldap_mod_replace ($conn,  "uid=".$userName.",".$config['basedn'], $infoReady);
			}
		}
              return true;
	}
	
	
	
	public function blockUser($conn,$user){
//echo "$user";exit;
		$config = Config::get('auth.ldap');
        if (($search=@ldap_search($conn, "uid=".$user.",".$config['basedn'], "cn=*"))) {
		$loginExpireDate = 1;//Sysdate - 1
		$info["roomNumber"] = $loginExpireDate;

		//print_r($info);exit;

		$infoReady = array();
		$infoReady = array_filter($info);
              $result = @ldap_mod_replace ($conn,  "uid=".$user.",".$config['basedn'], $infoReady);
              return true;
        }
	}

		public function unBlockUser($conn,$user){
//echo "$user";exit;
		$config = Config::get('auth.ldap');
        if (($search=@ldap_search($conn, "uid=".$user.",".$config['basedn'], "cn=*"))) {
		$loginExpireDate = 2;//Sysdate - 1
		$info["roomNumber"] = $loginExpireDate;

		//print_r($info);exit;

		$infoReady = array();
		$infoReady = array_filter($info);
              $result = @ldap_mod_replace ($conn,  "uid=".$user.",".$config['basedn'], $infoReady);
			  //print_r($result);exit;
              return true;
        }
	}
	

	public function blockUserMultiple($conn,$users){
//echo "$user";exit;
		$config = Config::get('auth.ldap');
		//print_r($users);exit;
		foreach($users as $userName){
        if (($search=@ldap_search($conn, "uid=".$userName.",".$config['basedn'], "cn=*"))) {
		$loginExpireDate = 1;//Sysdate - 1
		$info["roomNumber"] = $loginExpireDate;

		//print_r($info);exit;

		$infoReady = array();
		$infoReady = array_filter($info);
              $result = @ldap_mod_replace ($conn,  "uid=".$userName.",".$config['basedn'], $infoReady);
		}
        }return true;
	}

		public function unBlockUserMultiple($conn,$users){
//echo "$user";exit;
		$config = Config::get('auth.ldap');
		foreach($users as $userName){
        if (($search=@ldap_search($conn, "uid=".$userName.",".$config['basedn'], "cn=*"))) {
		$loginExpireDate = 2;//Sysdate - 1
		$info["roomNumber"] = $loginExpireDate;

		//print_r($info);exit;

		$infoReady = array();
		$infoReady = array_filter($info);
              $result = @ldap_mod_replace ($conn,  "uid=".$userName.",".$config['basedn'], $infoReady);
			  //print_r($result);exit;
		}//foreach($users as $userName){
        }return true;
	}
	

}
