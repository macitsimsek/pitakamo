<?php
class CustomFunctions {

    public static function checkUserPermissionX($modulePath=""){
		//echo Route::getCurrentRoute()->getPath();
		if($modulePath==""){
        	$modulePath =  Route::getCurrentRoute()->getPath();
		}
		$permittedRoutes = Session::get('permittedRouteList');
		$permittedRoutes[] = "main";

		//echo $modulePath;
		//print_r($permittedRoutes);
		//exit;
		foreach($permittedRoutes as $path){
         	$pos = strpos($path, $modulePath);
			if ($pos !== false) { // �� esit imine dikkat!
			    return true;
			}
		}

		return false;

	}
	public static function checkUserPermission($modulePath=""){
		//echo Route::getCurrentRoute()->getPath();
		if($modulePath==""){
        	$modulePath =  Route::getCurrentRoute()->getPath();
		}
		$permittedRoutes = Session::get('permittedRouteList');
		$permittedRoutes[] = "main";
        $tmpAray = explode("/",$modulePath);
        $newArry = array();
		for($i=0;$i<count($tmpAray);$i++){
           $newArry[]=$tmpAray[$i];
		   if($i==1){
              break;
		   }
		}
        $modulePath = implode("/",$newArry);
		//echo $modulePath;
		//print_r($permittedRoutes);
		//exit;
		foreach($permittedRoutes as $path){
         	$pos = strpos($path, $modulePath);
			if ($pos !== false) { // �� esit imine dikkat!
			    return true;
			}
		}

		return false;

	}

	public static function getApplicationRoutes(){
		$curl_response = self::CallAPI("GET",Config::get('services.restUrl')."permission/application-routes");
		//echo $curl_response; exit;
		$decoded = json_decode($curl_response);
		if (isset($decoded->status) && $decoded->status == 'ERROR') {
            App::abort(500, $decoded->developerMessage);
		}
		return $decoded->data;
	}


	public static function GetJson($method, $url,$data=false,$sendException=true){
     	$curl_response = CustomFunctions::CallAPI($method,Config::get('services.restUrl').$url,$data);
		//echo $curl_response; exit;
		$decoded = json_decode($curl_response,false);
        //echo print_r($decoded); exit;
		if (isset($decoded->status) && $decoded->status == 'ERROR' && $sendException==true) {
            App::abort(500, $decoded->developerMessage);
		}

		if(isset($decoded->data) && is_object($decoded->data)){ //tek satir obje geldi demek arraya cevir
           	$tmpAry = array( $decoded->data );
           	return $tmpAry;
		}elseif(isset($decoded->data)){  //array geldi ise
         	return $decoded->data;
		}else{
			if($method=="GET"){
              	return array();
			}else
			if($decoded->status=="OK"){
             	return true;
			}else{
				return false;
			}
		}
	}


	// Method: POST, PUT, GET etc
	// Data: array("param" => "value") ==> index.php?param=value
    public static function CallAPI($method, $url, $data = false){
	    $curl = curl_init();




	    switch ($method)
	    {
	    	case "DELETE":
	            //var_dump($data);
						//exit;
				        curl_setopt($curl, CURLOPT_URL, $url);
						curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
				curl_setopt($curl, CURLOPT_HEADER, false);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
				curl_setopt($curl, CURLOPT_POSTFIELDS,$data);
				 $result = curl_exec($curl);
				 //var_dump($result);

				    curl_close($curl);

				    return $result;

	            break;
	        case "POST":
	            curl_setopt($curl, CURLOPT_POST, 1);


	            if ($data)
	                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
	            break;
	        case "PUT":


				        //var_dump($data);
						//exit;
				        curl_setopt($curl, CURLOPT_URL, $url);
						curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
				curl_setopt($curl, CURLOPT_HEADER, false);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
				curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
				 $result = curl_exec($curl);
				 //var_dump($result);

				    curl_close($curl);

				    return $result;

        		//exit;
               /*
	            curl_setopt($curl, CURLOPT_PUT, 1);
				curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
				print_r(json_encode($data));
				if ($data)
	                curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
				*/
	            break;
	        default:
	            if ($data)
	                $url = sprintf("%s?%s", $url, http_build_query($data));
	    }

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			    'Content-Type: application/json',
			    'Content-Length: ' . strlen(json_encode($data)))
			);

	    // Optional Authentication:
	    //curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	    //curl_setopt($curl, CURLOPT_USERPWD, "username:password");

	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

	    $result = curl_exec($curl);

	    curl_close($curl);

	    return $result;
	}
}