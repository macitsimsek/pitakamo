<?php

use yajra\Datatables\DatatablesServiceProvider ;

class TimeController extends Controller {

	public function getDateTime(){
$timeZone =		array (
'0'=>'Nothing Selected',
'Pacific/Midway'       => "(UTC-11:00) Midway Island",
    'US/Samoa'             => "(UTC-11:00) Samoa",
    'US/Hawaii'            => "(UTC-10:00) Hawaii",
    'US/Alaska'            => "(UTC-09:00) Alaska",
    'US/Pacific'           => "(UTC-08:00) Pacific Time (US &amp; Canada)",
    'America/Tijuana'      => "(UTC-08:00) Tijuana",
    'US/Arizona'           => "(UTC-07:00) Arizona",
    'US/Mountain'          => "(UTC-07:00) Mountain Time (US &amp; Canada)",
    'America/Chihuahua'    => "(UTC-07:00) Chihuahua",
    'America/Mazatlan'     => "(UTC-07:00) Mazatlan",
    'America/Mexico_City'  => "(UTC-06:00) Mexico City",
    'America/Monterrey'    => "(UTC-06:00) Monterrey",
    'Canada/Saskatchewan'  => "(UTC-06:00) Saskatchewan",
    'US/Central'           => "(UTC-06:00) Central Time (US &amp; Canada)",
    'US/Eastern'           => "(UTC-05:00) Eastern Time (US &amp; Canada)",
    'US/East-Indiana'      => "(UTC-05:00) Indiana (East)",
    'America/Bogota'       => "(UTC-05:00) Bogota",
    'America/Lima'         => "(UTC-05:00) Lima",
    'America/Caracas'      => "(UTC-04:30) Caracas",
    'Canada/Atlantic'      => "(UTC-04:00) Atlantic Time (Canada)",
    'America/La_Paz'       => "(UTC-04:00) La Paz",
    'America/Santiago'     => "(UTC-04:00) Santiago",
    'Canada/Newfoundland'  => "(UTC-03:30) Newfoundland",
    'America/Buenos_Aires' => "(UTC-03:00) Buenos Aires",
    'Greenland'            => "(UTC-03:00) Greenland",
    'Atlantic/Stanley'     => "(UTC-02:00) Stanley",
    'Atlantic/Azores'      => "(UTC-01:00) Azores",
    'Atlantic/Cape_Verde'  => "(UTC-01:00) Cape Verde Is.",
    'Africa/Casablanca'    => "(UTC) Casablanca",
    'Europe/Dublin'        => "(UTC) Dublin",
    'Europe/Lisbon'        => "(UTC) Lisbon",
    'Europe/London'        => "(UTC) London",
    'Africa/Monrovia'      => "(UTC) Monrovia",
    'Europe/Amsterdam'     => "(UTC+01:00) Amsterdam",
    'Europe/Belgrade'      => "(UTC+01:00) Belgrade",
    'Europe/Berlin'        => "(UTC+01:00) Berlin",
    'Europe/Bratislava'    => "(UTC+01:00) Bratislava",
    'Europe/Brussels'      => "(UTC+01:00) Brussels",
    'Europe/Budapest'      => "(UTC+01:00) Budapest",
    'Europe/Copenhagen'    => "(UTC+01:00) Copenhagen",
    'Europe/Ljubljana'     => "(UTC+01:00) Ljubljana",
    'Europe/Madrid'        => "(UTC+01:00) Madrid",
    'Europe/Paris'         => "(UTC+01:00) Paris",
    'Europe/Prague'        => "(UTC+01:00) Prague",
    'Europe/Rome'          => "(UTC+01:00) Rome",
    'Europe/Sarajevo'      => "(UTC+01:00) Sarajevo",
    'Europe/Skopje'        => "(UTC+01:00) Skopje",
    'Europe/Stockholm'     => "(UTC+01:00) Stockholm",
    'Europe/Vienna'        => "(UTC+01:00) Vienna",
    'Europe/Warsaw'        => "(UTC+01:00) Warsaw",
    'Europe/Zagreb'        => "(UTC+01:00) Zagreb",
    'Europe/Athens'        => "(UTC+02:00) Athens",
    'Europe/Bucharest'     => "(UTC+02:00) Bucharest",
    'Africa/Cairo'         => "(UTC+02:00) Cairo",
    'Africa/Harare'        => "(UTC+02:00) Harare",
    'Europe/Helsinki'      => "(UTC+02:00) Helsinki",
    'Europe/Istanbul'      => "(UTC+02:00) Istanbul",
    'Asia/Jerusalem'       => "(UTC+02:00) Jerusalem",
    'Europe/Kiev'          => "(UTC+02:00) Kyiv",
    'Europe/Minsk'         => "(UTC+02:00) Minsk",
    'Europe/Riga'          => "(UTC+02:00) Riga",
    'Europe/Sofia'         => "(UTC+02:00) Sofia",
    'Europe/Tallinn'       => "(UTC+02:00) Tallinn",
    'Europe/Vilnius'       => "(UTC+02:00) Vilnius",
    'Asia/Baghdad'         => "(UTC+03:00) Baghdad",
    'Asia/Kuwait'          => "(UTC+03:00) Kuwait",
    'Africa/Nairobi'       => "(UTC+03:00) Nairobi",
    'Asia/Riyadh'          => "(UTC+03:00) Riyadh",
    'Asia/Tehran'          => "(UTC+03:30) Tehran",
    'Europe/Moscow'        => "(UTC+04:00) Moscow",
    'Asia/Baku'            => "(UTC+04:00) Baku",
    'Europe/Volgograd'     => "(UTC+04:00) Volgograd",
    'Asia/Muscat'          => "(UTC+04:00) Muscat",
    'Asia/Tbilisi'         => "(UTC+04:00) Tbilisi",
    'Asia/Yerevan'         => "(UTC+04:00) Yerevan",
    'Asia/Kabul'           => "(UTC+04:30) Kabul",
    'Asia/Karachi'         => "(UTC+05:00) Karachi",
    'Asia/Tashkent'        => "(UTC+05:00) Tashkent",
    'Asia/Kolkata'         => "(UTC+05:30) Kolkata",
    'Asia/Kathmandu'       => "(UTC+05:45) Kathmandu",
    'Asia/Yekaterinburg'   => "(UTC+06:00) Ekaterinburg",
    'Asia/Almaty'          => "(UTC+06:00) Almaty",
    'Asia/Dhaka'           => "(UTC+06:00) Dhaka",
    'Asia/Novosibirsk'     => "(UTC+07:00) Novosibirsk",
    'Asia/Bangkok'         => "(UTC+07:00) Bangkok",
    'Asia/Jakarta'         => "(UTC+07:00) Jakarta",
    'Asia/Krasnoyarsk'     => "(UTC+08:00) Krasnoyarsk",
    'Asia/Chongqing'       => "(UTC+08:00) Chongqing",
    'Asia/Hong_Kong'       => "(UTC+08:00) Hong Kong",
    'Asia/Kuala_Lumpur'    => "(UTC+08:00) Kuala Lumpur",
    'Australia/Perth'      => "(UTC+08:00) Perth",
    'Asia/Singapore'       => "(UTC+08:00) Singapore",
    'Asia/Taipei'          => "(UTC+08:00) Taipei",
    'Asia/Ulaanbaatar'     => "(UTC+08:00) Ulaan Bataar",
    'Asia/Urumqi'          => "(UTC+08:00) Urumqi",
    'Asia/Irkutsk'         => "(UTC+09:00) Irkutsk",
    'Asia/Seoul'           => "(UTC+09:00) Seoul",
    'Asia/Tokyo'           => "(UTC+09:00) Tokyo",
    'Australia/Adelaide'   => "(UTC+09:30) Adelaide",
    'Australia/Darwin'     => "(UTC+09:30) Darwin",
    'Asia/Yakutsk'         => "(UTC+10:00) Yakutsk",
    'Australia/Brisbane'   => "(UTC+10:00) Brisbane",
    'Australia/Canberra'   => "(UTC+10:00) Canberra",
    'Pacific/Guam'         => "(UTC+10:00) Guam",
    'Australia/Hobart'     => "(UTC+10:00) Hobart",
    'Australia/Melbourne'  => "(UTC+10:00) Melbourne",
    'Pacific/Port_Moresby' => "(UTC+10:00) Port Moresby",
    'Australia/Sydney'     => "(UTC+10:00) Sydney",
    'Asia/Vladivostok'     => "(UTC+11:00) Vladivostok",
    'Asia/Magadan'         => "(UTC+12:00) Magadan",
    'Pacific/Auckland'     => "(UTC+12:00) Auckland",
    'Pacific/Fiji'         => "(UTC+12:00) Fiji",
);
//echo system('date +%Z');exit;
//echo ini_get('date.timezone');exit;
$setTimeZone ="";
foreach($timeZone as $timeZonekey=>$timeZonevalue){
	if($timeZonekey==ini_get('date.timezone')){
		$setTimeZone = $timeZonekey;
		break;
		}
	}
$setDateTime = date("d/m/Y - g:i");


		return View::make('settings/date_time',array("timeZone"=>$timeZone,"setTimeZone"=>$setTimeZone,"setDateTime"=>$setDateTime));
	}

	public function getRegionLanguage(){
		$countries = array(
"AF" => "Afghanistan",
"AL" => "Albania",
"DZ" => "Algeria",
"AS" => "American Samoa",
"AD" => "Andorra",
"AO" => "Angola",
"AI" => "Anguilla",
"AQ" => "Antarctica",
"AG" => "Antigua and Barbuda",
"AR" => "Argentina",
"AM" => "Armenia",
"AW" => "Aruba",
"AU" => "Australia",
"AT" => "Austria",
"AZ" => "Azerbaijan",
"BS" => "Bahamas",
"BH" => "Bahrain",
"BD" => "Bangladesh",
"BB" => "Barbados",
"BY" => "Belarus",
"BE" => "Belgium",
"BZ" => "Belize",
"BJ" => "Benin",
"BM" => "Bermuda",
"BT" => "Bhutan",
"BO" => "Bolivia",
"BA" => "Bosnia and Herzegovina",
"BW" => "Botswana",
"BV" => "Bouvet Island",
"BR" => "Brazil",
"BQ" => "British Antarctic Territory",
"IO" => "British Indian Ocean Territory",
"VG" => "British Virgin Islands",
"BN" => "Brunei",
"BG" => "Bulgaria",
"BF" => "Burkina Faso",
"BI" => "Burundi",
"KH" => "Cambodia",
"CM" => "Cameroon",
"CA" => "Canada",
"CT" => "Canton and Enderbury Islands",
"CV" => "Cape Verde",
"KY" => "Cayman Islands",
"CF" => "Central African Republic",
"TD" => "Chad",
"CL" => "Chile",
"CN" => "China",
"CX" => "Christmas Island",
"CC" => "Cocos - Keeling Islands",
"CO" => "Colombia",
"KM" => "Comoros",
"CG" => "Congo - Brazzaville",
"CD" => "Congo - Kinshasa",
"CK" => "Cook Islands",
"CR" => "Costa Rica",
"HR" => "Croatia",
"CU" => "Cuba",
"CY" => "Cyprus",
"CZ" => "Czech Republic",
"CI" => "Cote d'Ivoire",
"DK" => "Denmark",
"DJ" => "Djibouti",
"DM" => "Dominica",
"DO" => "Dominican Republic",
"NQ" => "Dronning Maud Land",
"DD" => "East Germany",
"EC" => "Ecuador",
"EG" => "Egypt",
"SV" => "El Salvador",
"GQ" => "Equatorial Guinea",
"ER" => "Eritrea",
"EE" => "Estonia",
"ET" => "Ethiopia",
"FK" => "Falkland Islands",
"FO" => "Faroe Islands",
"FJ" => "Fiji",
"FI" => "Finland",
"FR" => "France",
"GF" => "French Guiana",
"PF" => "French Polynesia",
"TF" => "French Southern Territories",
"FQ" => "French Southern and Antarctic Territories",
"GA" => "Gabon",
"GM" => "Gambia",
"GE" => "Georgia",
"DE" => "Germany",
"GH" => "Ghana",
"GI" => "Gibraltar",
"GR" => "Greece",
"GL" => "Greenland",
"GD" => "Grenada",
"GP" => "Guadeloupe",
"GU" => "Guam",
"GT" => "Guatemala",
"GG" => "Guernsey",
"GN" => "Guinea",
"GW" => "Guinea-Bissau",
"GY" => "Guyana",
"HT" => "Haiti",
"HM" => "Heard Island and McDonald Islands",
"HN" => "Honduras",
"HK" => "Hong Kong SAR China",
"HU" => "Hungary",
"IS" => "Iceland",
"IN" => "India",
"ID" => "Indonesia",
"IR" => "Iran",
"IQ" => "Iraq",
"IE" => "Ireland",
"IM" => "Isle of Man",
"IL" => "Israel",
"IT" => "Italy",
"JM" => "Jamaica",
"JP" => "Japan",
"JE" => "Jersey",
"JT" => "Johnston Island",
"JO" => "Jordan",
"KZ" => "Kazakhstan",
"KE" => "Kenya",
"KI" => "Kiribati",
"KW" => "Kuwait",
"KG" => "Kyrgyzstan",
"LA" => "Laos",
"LV" => "Latvia",
"LB" => "Lebanon",
"LS" => "Lesotho",
"LR" => "Liberia",
"LY" => "Libya",
"LI" => "Liechtenstein",
"LT" => "Lithuania",
"LU" => "Luxembourg",
"MO" => "Macau SAR China",
"MK" => "Macedonia",
"MG" => "Madagascar",
"MW" => "Malawi",
"MY" => "Malaysia",
"MV" => "Maldives",
"ML" => "Mali",
"MT" => "Malta",
"MH" => "Marshall Islands",
"MQ" => "Martinique",
"MR" => "Mauritania",
"MU" => "Mauritius",
"YT" => "Mayotte",
"FX" => "Metropolitan France",
"MX" => "Mexico",
"FM" => "Micronesia",
"MI" => "Midway Islands",
"MD" => "Moldova",
"MC" => "Monaco",
"MN" => "Mongolia",
"ME" => "Montenegro",
"MS" => "Montserrat",
"MA" => "Morocco",
"MZ" => "Mozambique",
"MM" => "Myanmar [Burma]",
"NA" => "Namibia",
"NR" => "Nauru",
"NP" => "Nepal",
"NL" => "Netherlands",
"AN" => "Netherlands Antilles",
"NT" => "Neutral Zone",
"NC" => "New Caledonia",
"NZ" => "New Zealand",
"NI" => "Nicaragua",
"NE" => "Niger",
"NG" => "Nigeria",
"NU" => "Niue",
"NF" => "Norfolk Island",
"KP" => "North Korea",
"VD" => "North Vietnam",
"MP" => "Northern Mariana Islands",
"NO" => "Norway",
"OM" => "Oman",
"PC" => "Pacific Islands Trust Territory",
"PK" => "Pakistan",
"PW" => "Palau",
"PS" => "Palestinian Territories",
"PA" => "Panama",
"PZ" => "Panama Canal Zone",
"PG" => "Papua New Guinea",
"PY" => "Paraguay",
"YD" => "People's Democratic Republic of Yemen",
"PE" => "Peru",
"PH" => "Philippines",
"PN" => "Pitcairn Islands",
"PL" => "Poland",
"PT" => "Portugal",
"PR" => "Puerto Rico",
"QA" => "Qatar",
"RO" => "Romania",
"RU" => "Russia",
"RW" => "Rwanda",
"RE" => "Reunion",
"BL" => "Saint Barthelemy",
"SH" => "Saint Helena",
"KN" => "Saint Kitts and Nevis",
"LC" => "Saint Lucia",
"MF" => "Saint Martin",
"PM" => "Saint Pierre and Miquelon",
"VC" => "Saint Vincent and the Grenadines",
"WS" => "Samoa",
"SM" => "San Marino",
"SA" => "Saudi Arabia",
"SN" => "Senegal",
"RS" => "Serbia",
"CS" => "Serbia and Montenegro",
"SC" => "Seychelles",
"SL" => "Sierra Leone",
"SG" => "Singapore",
"SK" => "Slovakia",
"SI" => "Slovenia",
"SB" => "Solomon Islands",
"SO" => "Somalia",
"ZA" => "South Africa",
"GS" => "South Georgia and the South Sandwich Islands",
"KR" => "South Korea",
"ES" => "Spain",
"LK" => "Sri Lanka",
"SD" => "Sudan",
"SR" => "Suriname",
"SJ" => "Svalbard and Jan Mayen",
"SZ" => "Swaziland",
"SE" => "Sweden",
"CH" => "Switzerland",
"SY" => "Syria",
"ST" => "Sao Tome and Principe",
"TW" => "Taiwan",
"TJ" => "Tajikistan",
"TZ" => "Tanzania",
"TH" => "Thailand",
"TL" => "Timor-Leste",
"TG" => "Togo",
"TK" => "Tokelau",
"TO" => "Tonga",
"TT" => "Trinidad and Tobago",
"TN" => "Tunisia",
"TR" => "Turkey",
"TM" => "Turkmenistan",
"TC" => "Turks and Caicos Islands",
"TV" => "Tuvalu",
"UM" => "U.S. Minor Outlying Islands",
"PU" => "U.S. Miscellaneous Pacific Islands",
"VI" => "U.S. Virgin Islands",
"UG" => "Uganda",
"UA" => "Ukraine",
"SU" => "Union of Soviet Socialist Republics",
"AE" => "United Arab Emirates",
"GB" => "United Kingdom",
"US" => "United States",
"ZZ" => "Unknown or Invalid Region",
"UY" => "Uruguay",
"UZ" => "Uzbekistan",
"VU" => "Vanuatu",
"VA" => "Vatican City",
"VE" => "Venezuela",
"VN" => "Vietnam",
"WK" => "Wake Island",
"WF" => "Wallis and Futuna",
"EH" => "Western Sahara",
"YE" => "Yemen",
"ZM" => "Zambia",
"ZW" => "Zimbabwe",
"AX" => "Aland Islands",
);
        $curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."language/languages");
		$languages = json_decode($curl_response,true);
		$curl_response2 = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."setting/entry-list");
		$settings = json_decode($curl_response2,true);
		foreach($settings as $key1=>$val1){
				if($key1 == 'data'){
				//print_r($val);exit;
				foreach($val1 as $key2=>$val2){
					//print_r($val);exit;
					if($val2['key'] == "DEFAULT_LANGUAGE_SETTINGS"){
					$setLanguage=$val2['value'];
					$setCountry=$val2['description'];
					}				
					
					}
				}/*if($key == 'data')*/
	} /*foreach($decoded as $key=>$val)*/
		
//print_r($languages['data']);exit;
/*
Array ( [0] => Array ( [localStr] => EN [name] => English ) [1] => Array ( [localStr] => TR [name] => T�rk�e ) )
*/
	return View::make('settings/region_language',array("countries"=>$countries,"languages"=>$languages['data'],"setLanguage"=>$setLanguage,"setCountry"=>$setCountry));
	}
	
	
	
	public function postDateTime()
	{
		$lang = Language::getLanguage();
		
		Input::get('x_dateTime');
		
		$timezone = Input::get('x_region');
		
		
		return Redirect::to('control_panel/language_time/date_time')->with('message',$lang->SETTING_ADDED_NEW);
		
	}
	
	
	
	
	
	public function postRegionLanguage()
	{
		
		$lang = Language::getLanguage();
		$data = array(
				"key"=>"DEFAULT_LANGUAGE_SETTINGS",
				"value"=>Input::get('x_languages'),
				"description"=>Input::get('x_country')
				
				);
	$curl_response = CustomFunctions::CallAPI("PUT",Config::get('services.restUrl')."setting/entry-list",$data);

				$decoded = json_decode($curl_response);
				if (isset($decoded->status) && $decoded->status == 'ERROR') {
		            App::abort(500, $decoded->developerMessage);
				
        }
		 if (Cache::has('language_array')){
			 Cache::forget('language_array');
			 }
		return Redirect::to('control_panel/language_time/region_language')->with('message',$lang->SETTING_ADDED_NEW);
		
	}
		
			

	public function getWarningMessageCustomization(){
		$lang = Language::getLanguage();	
		$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."error-code/entry-list");
		$warningMessages = json_decode($curl_response,true);
		//print_r($warningMessages); exit; 
		return View::make('settings/warning_message_customization',array("warningMessages"=>$warningMessages));
	}
	
	public function getWarningMessageCustomizationEdit($key){
		$lang = Language::getLanguage();	
		$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."error-code/entry-list");
		$warningMessages = json_decode($curl_response,true);
		
		
		foreach ($warningMessages as $keyFRC=>$valueFRC) {
		
		if($keyFRC == 'data'){
			foreach ($valueFRC as $k=>$v) {
				
					
					if($v['codeId'] == $key){
					$message=$v['usageArea'];
					$description=$v['description'];
				}
			}					
		}
	}
		
		
		return View::make('settings/warning_message_customization_edit',array("key"=>$key,"message"=>$message,"description"=>$description));
	}
	
	
	public function postWarningMessageCustomizationEdit($key){
		
		
		
		$lang = Language::getLanguage();
		
		$data = array(
				"codeId"=>$key,
				"usageArea"=>Input::get('x_message'),
				"description"=>Input::get('x_description')		
				);
				//print_r($data);exit;
				$curl_response = CustomFunctions::CallAPI("PUT",Config::get('services.restUrl')."error-code/entry-list",$data);
				
				$decoded = json_decode($curl_response);
				if (isset($decoded->status) && $decoded->status == 'ERROR') {
				App::abort(500, $decoded->developerMessage);
				
				}
				return Redirect::to('control_panel/language_time/warning_message_customization')->with('message', $lang->SETTING_ADDED_NEW);

		
	}

}
