<?php

use yajra\Datatables\DatatablesServiceProvider ;

class SettingsController extends Controller {

	public function getSettings(){

        $curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."setting/entry-list");
		$decoded = json_decode($curl_response);
		if (isset($decoded->status) && $decoded->status == 'ERROR') {
            App::abort(500, $decoded->developerMessage);
		}
		return $decoded->data;

	}


	public function getIndex(){

		$settings = self::getSettings();
		
		return View::make('settings/software_settings', array("settings"=>$settings));

	}


	public function getEdit($key)
	{	
	$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."setting/entry-list");
	$settings = json_decode($curl_response,true);
	foreach($settings as $key1=>$val1){
	if($key1 == 'data'){
		//print_r($val);exit;
		foreach($val1 as $key2=>$val2){
			//print_r($val);exit;
			if($val2['key'] == $key){
			$value=$val2['value'];
			$description=$val2['description'];
				}		
			}
		}/*if($key == 'data')*/
	} /*foreach($decoded as $key=>$val)*/
		return View::make('settings/software_settings_edit', array("key"=>$key,"value"=>$value,"description"=>$description));
	}

	public function postEdit($key)
	{
        $lang = Language::getLanguage();
		$item = $key;
        $item = Str::upper($item);		
		
		if($key === "EMAILING_DETAILS"){
			$data = array(
				"key"=>$key,
				"value"=>Input::get('x_setting')."::".Input::get('x_setting1')."::".Input::get('x_setting2'),
				"description"=>Input::get('x_description')
				
				);
				}elseif($key === "SITE_LOGO"){
			$x_photo =Input::file('x_setting');
	        $filename = "logo.". $x_photo->getClientOriginalExtension();
	        $destinationPath = public_path().DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR;
	        Input::file('x_setting')->move($destinationPath, $filename);
			$data = array(
				"key"=>$key,
				"value"=> $filename,
				"description"=>Input::get('x_description')
				);
					}				
				else{
				$data = array(
				"key"=>$key,
				"value"=>Input::get('x_setting'),
				"description"=>Input::get('x_description')
				
				);
				}

			/*	echo Config::get('services.restUrl')."settings/entry-list";
                var_dump($data);
				exit;*/
				
	            $curl_response = CustomFunctions::CallAPI("PUT",Config::get('services.restUrl')."setting/entry-list",$data);

				$decoded = json_decode($curl_response);
				if (isset($decoded->status) && $decoded->status == 'ERROR') {
		            App::abort(500, $decoded->developerMessage);
				
        }
		 if (Cache::has('language_array')){
			 Cache::forget('language_array');
			 }
		return Redirect::to('settings/software_settings')->with('message', str_replace("__USERNAME__",$item, $lang->SETTING_EDITED));

	}



}
