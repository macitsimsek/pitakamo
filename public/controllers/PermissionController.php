<?php

use yajra\Datatables\DatatablesServiceProvider ;

class PermissionController extends Controller {

	function __construct() {

   }


	public function getItems(){
       	$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."permission/permission-schema");
		//echo $curl_response; exit;
		$decoded = json_decode($curl_response);
		if (isset($decoded->status) && $decoded->status == 'ERROR') {
            App::abort(500, $decoded->developerMessage);
		}
		return $decoded->data;
	}

	public function getIndex(){
		$data = self::getItems();
        //print_r($data); exit;
		return View::make('permission.schema', array("data"=>$data));
	}

	public function getCreate()
	{

        return View::make('permission.schema_create');
	}

	public function postCreate()
	{
		$name = Input::get('x_name');
    	$description = Input::get('x_description');

        $data = array(
					"permissionId"=>"0",
					"name"=>$name,
					"description"=>$description
					);


		$curl_response = CustomFunctions::CallAPI("POST",Config::get('services.restUrl')."permission/permission-schema",$data);
		//print_r($curl_response);exit;
		$decoded = json_decode($curl_response);
		if (isset($decoded->status) && $decoded->status == 'ERROR') {
			App::abort(500, $decoded->developerMessage);
		}
        $lang = Language::getLanguage();
		return Redirect::to('user_identity_management/schema')->with('message', str_replace("__USERNAME__",$name, $lang->CREATED));

	}


	public function postDelete($key)
	{
		$lang = Language::getLanguage();


		$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."permission/group-permission?p_group_id=-1");
		//echo $curl_response; exit;
		$decoded = json_decode($curl_response);
        //echo print_r($decoded); exit;
		$data = $decoded->data;
		//print_r($data); exit;
		foreach($data as $row){
			//echo $row->permissionId;
         	if($row->permissionId == $key){ //bagli grup buldu

            	return Redirect::back()->with('error',$lang->SCHEMA_HAS_GROUP);
         	}
		}


         $curl_response = CustomFunctions::CallAPI("DELETE",Config::get('services.restUrl')."permission/permission-schema?p_permission_id=".$key);
                //print_r($data);;
                //print_r($curl_response);exit;
				$decoded = json_decode($curl_response);
				if (isset($decoded->status) && $decoded->status == 'ERROR') {
		            //App::abort(500, $decoded->developerMessage);
					return Redirect::to('user_identity_management/schema')->with('error', str_replace("__USERNAME__",$key, $lang->NOT_DELETED));
				}
		 return Redirect::to('user_identity_management/schema')->with('message', str_replace("[__USERNAME__]",$key, $lang->DELETED));

	}

	public function getEdit($key)
	{
		$langAry = array();

		$data = self::getItems();
		$items = array();
		$name = "";
		$description = "";
        foreach($data as $row){
            if($row->permissionId == $key ){
               	$name = $row->name;
				$description = $row->description;
            }
        }

		return View::make('permission.schema_edit', array("key"=>$key,"name"=>$name,"description"=>$description));
	}

	public function postEdit($key)
	{

    	$name = Input::get('x_name');
    	$description = Input::get('x_description');



		$data = array(
					"permissionId"=>$key,
					"name"=>$name,
					"description"=>$description
					);

		//echo Config::get('services.restUrl')."lang/entry";
		//var_dump($data);
		$curl_response = CustomFunctions::CallAPI("PUT",Config::get('services.restUrl')."permission/permission-schema",$data);
		//print_r($data);;
		//print_r($curl_response);exit;
		$decoded = json_decode($curl_response);
		if (isset($decoded->status) && $decoded->status == 'ERROR') {
			App::abort(500, $decoded->developerMessage);
		}

        $lang = Language::getLanguage();
		return Redirect::to('user_identity_management/schema')->with('message', $lang->UPDATED);

	}


    public function getMemberIndex($key)
	{
		$data = self::getItems();
		$permissionArry = array();
        foreach($data as $row){
        	$permissionArry[$row->permissionId] = $row->name;
        }

		$routeObjAry = CustomFunctions::getApplicationRoutes();
        //print_r($routeObjAry);exit;

        $data = array(
					"permissionId"=>$key
					);
		$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."permission/permission-routes?p_permission_id=".$key);
		//echo $curl_response; exit;
		$decoded = json_decode($curl_response);
		if (isset($decoded->status) && $decoded->status == 'ERROR') {
            App::abort(500, $decoded->developerMessage);
		}
		$selectedPermissionArry = array();
		if(isset($decoded->data)){
            $selectedPermissionArry =  $decoded->data;
		}

		return View::make('permission.schema_mapping', array("permissionId"=>$key,"permissionArry"=>$permissionArry,"selectedPermissionArry"=>$selectedPermissionArry,"routeObjAry"=>$routeObjAry));
	}

	public function getMemberPreview($key)
	{
		$data = self::getItems();
		$permissionArry = array();
		$name = "";
		$description = "";
        foreach($data as $row){
        	$permissionArry[$row->permissionId] = $row->name;
			if($row->permissionId == $key ){
               	$name = $row->name;
				$description = $row->description;
            }
        }


		$data = CustomFunctions::getApplicationRoutes();
        foreach($data as $row){
        	$routeObjAry[$row->routeId] = $row->languageKey;
        }
		//print_r($routeObjAry);exit;


        $data = array(
					"permissionId"=>$key
					);
		$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."permission/permission-routes?p_permission_id=".$key);
		//echo $curl_response; exit;
		$decoded = json_decode($curl_response);
		if (isset($decoded->status) && $decoded->status == 'ERROR') {
            App::abort(500, $decoded->developerMessage);
		}
		//print_r($decoded);exit;
		$selectedPermissionArry = array();
		if(isset($decoded->data)){
            $selectedPermissionArry =  $decoded->data;
		}

		//print_r($selectedPermissionArry);exit;

        $ldap = new LdapController();
		$groups = $ldap->getGroupList($ldap->loginLdap());
		$groupAry = array();
		foreach($groups as $k => $v){
        	$groupAry[$v["id"]] = $v["name"];
		}

		$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."permission/group-permission?p_group_id=-1");
		//echo $curl_response; exit;
		$decoded = json_decode($curl_response);
        //echo print_r($decoded); exit;
		$data = $decoded->data;
		//print_r($data); exit;
		$selectedGroupArry = array();
		foreach($data as $row){
			//echo $row->permissionId;
         	if($row->permissionId == $key){ //bagli grup buldu

            	$selectedGroupArry[] = isset($groupAry[$row->groupId])?$groupAry[$row->groupId]:"";
         	}
		}


		return View::make('permission.schema_preview', array(	"name"=>$name,
																"description"=>$description,
																"permissionId"=>$key,
																"permissionArry"=>$permissionArry,
																"selectedGroupArry"=>$selectedGroupArry,
																"selectedPermissionArry"=>$selectedPermissionArry,
																"routeObjAry"=>$routeObjAry));
	}

	public function postMemberIndex($id){

		//$selectedRouteIdList = Input::get('my_multi_select2');
		$selectedRouteIdList = Input::get('x_selected_nodes');
		$selectedRouteIdList = explode(",",$selectedRouteIdList);
        //print_r($selectedRouteIdList); exit;
		$result=false;

		//once eskileri sil
		$curl_response = CustomFunctions::CallAPI("DELETE",Config::get('services.restUrl')."permission/permission-routes?p_permission_id=".$id);
        //echo $id;
		//print_r($curl_response);exit;
		$decoded = json_decode($curl_response);
		if (isset($decoded->status) && $decoded->status == 'ERROR') {
            App::abort(500, $decoded->developerMessage);
		}

        foreach($selectedRouteIdList as $key=>$val){
        		$data = array(
					"routeId"=>$val,
					"permissionId"=>$id
					);

                $curl_response = CustomFunctions::CallAPI("POST",Config::get('services.restUrl')."permission/permission-routes",$data);
                //print_r($curl_response);exit;
				$decoded = json_decode($curl_response);
				if (isset($decoded->status) && $decoded->status == 'ERROR') {
		            App::abort(500, $decoded->developerMessage);
				}
				$result=true;
        }

		$lang = Language::getLanguage();
		if($result==true){
        	return Redirect::to('user_identity_management/schema')->with('message', $lang->UPDATED);
		}else{
            return Redirect::back()->with('error', $lang->NOT_UPDATED);
		}

	}

}
