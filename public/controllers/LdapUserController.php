<?php

class LdapUserController extends LdapController {

	public function getIndex(){
        $conn = parent::loginLdap();
		$userAry = parent::getUserList($conn);
		//print_r($userAry);exit;
    	return View::make('ldap.user', array("users"=>$userAry));
	}
	
	public function postIndex(){
        $conn = parent::loginLdap();
		//print_r($_POST);exit;
			if (Input::has('edit'))
			{	return $this->getMultipleEdit();
			}elseif (Input::has('posted'))
			{	return $this->postMultipleEdit();
			}else if (Input::has('deletemtp'))
			{	return $this->postMultipleDelete();
			}else if (Input::has('block'))
			{	return $this->postBlockMultiple();
			}else if (Input::has('unblock'))
			{	return $this->postUnBlockMultiple();
			}
	}

	public function getEdit($user)
	{
        // Title
        //$title = Lang::get('admin/blogs/title.create_a_new_blog');

		$conn = parent::loginLdap();
		$userInfo  = parent::getUserInfo($conn,$user);

		//print_r($userInfo);
		
		$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."master-table/system-operator");
		$decoded = json_decode($curl_response,true);
		foreach($decoded as $key1=>$val1){
 		if($key1 == 'data'){
			$value = $val1['businessName'];
			$companynames[$value] =$val1['businessName'];
		}
		}
		$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."master-table/departments");
		$decoded = json_decode($curl_response,true);
		foreach($decoded as $key1=>$val1){
	    if($key1 == 'data'){
			$value = $val1['departmentName'];
			$departments[$value] 	=$val1['departmentName'];
		}
	} /*foreach($decoded as $key=>$val)*/
	
		
		
		
		$groupAry = parent::getGroupList($conn);
		$groupOptions = array();

		foreach($groupAry as $k => $v){
			
            $groupOptions[$v["id"]] = $v["name"];
		}

		$groupId=@$userInfo[0]["gidnumber"][0];
		$x_firstName = @$userInfo[0]["givenname"][0];
		$x_middleName =@$userInfo[0]["initials"][0];
		$x_surname =@$userInfo[0]["sn"][0];
		$x_gender =@$userInfo[0]["employeetype"][0];
		$x_dateofBirth =@$userInfo[0]["pager"][0];
		$x_identity =@$userInfo[0]["carlicense"][0];
		$x_address =@$userInfo[0]["homepostaladdress"][0];
		$x_zipCode =@$userInfo[0]["postalcode"][0];
		$x_city =@$userInfo[0]["l"][0];
		$x_country =@$userInfo[0]["st"][0];
		$x_mobileNumber =@$userInfo[0]["mobile"][0];
		$x_email =@$userInfo[0]["mail"][0];
		$x_photo =@$userInfo[0]["jpegphoto"][0];
		$x_companyName =@$userInfo[0]["o"][0];
		$x_department =@$userInfo[0]["departmentnumber"][0];
		$x_position =@$userInfo[0]["title"][0];
		

        // Show the page
        //return View::make('admin/blogs/create_edit', compact('title'));
        return View::make('ldap/user_edit', array(	'user' => $user,
													'groupId' => $groupId,
													'group_options'=>$groupOptions,
													'x_firstName'=>$x_firstName,
													'x_middleName'=>$x_middleName,
													'x_surname'=>$x_surname,
													'x_gender'=>$x_gender,
													'x_dateofBirth'=>$x_dateofBirth,
													'x_identity'=>$x_identity,
													'x_address'=>$x_address,
													'x_zipCode'=>$x_zipCode,
													'x_city'=>$x_city,
													'x_country'=>$x_country,
													'x_mobileNumber'=>$x_mobileNumber,
													'x_email'=>$x_email,
													'x_photo'=>$x_photo,
													'x_companyName'=>$x_companyName,
													'x_department'=>$x_department,
													'x_position'=>$x_position,
													'companynames'=>$companynames,
													'departments'=>$departments));
	}

	public function postEdit($user)
	{
        // Title
        //$title = Lang::get('admin/blogs/title.create_a_new_blog');

		$conn = parent::loginLdap();
		$lang = Language::getLanguage();

		$userName = $user;
		
		$x_firstName = Input::get('x_firstName');
		$x_middleName =Input::get('x_middleName');
		$x_surname =Input::get('x_surname');
		$x_gender =Input::get('x_gender');
		//print_r($x_gender);exit;
		$x_dateofBirth =Input::get('x_dateofBirth');
		$x_identity =Input::get('x_identity');
		$x_address =Input::get('x_address');
		$x_zipCode =Input::get('x_zipCode');
		$x_city =Input::get('x_city');
		$x_country =Input::get('x_country');
		$x_mobileNumber =Input::get('x_mobileNumber');
		$x_email =Input::get('x_email');
		//$x_photo =Input::get('x_photo');

        if (Input::hasFile('x_photo'))
		{
		   	$x_photo =Input::file('x_photo');
	        $filename = $userName . '.' . $x_photo->getClientOriginalExtension();
	        $filename = strtolower($filename);
			$destinationPath = public_path().DIRECTORY_SEPARATOR."files".DIRECTORY_SEPARATOR;
	        Input::file('x_photo')->move($destinationPath, $filename);
		}else{
           	$filename ="";
		}



		$x_companyName =Input::get('x_companyName');
		$x_department =Input::get('x_department');
		$x_position =Input::get('x_position');
	 	//$userName = Input::get('userName');
		$x_username =Input::get('x_username');
		
		$x_password =Input::get('x_password');
		//$x_confirmPassword =Input::get('x_confirmPassword');
		//$x_selectaGroup =Input::get('x_selectaGroup');
		//$x_createAnother =Input::get('x_createAnother');
		//$x_inlineCheckbox =Input::get('x_inlineCheckbox');
		$groupId = Input::get('oldGroupId');
        //$oldGroupId = Input::get('oldGroupId');   

		$groupAry = parent::getGroupList($conn);

		$groupName = "" ;

        foreach($groupAry as $k => $v){
            if($v["id"] == $groupId){
             	$groupName = $v["name"];
            }
			/*if($v["id"] == $oldGroupId){
             	$oldGroupName = $v["name"];
            }*/
		}


		if(parent::changeUserData($conn,$userName,$x_firstName,$x_middleName,$x_surname,$x_gender,$x_dateofBirth,$x_identity,$x_address,$x_zipCode,$x_city,$x_country,$x_mobileNumber,$x_email,$filename,$x_companyName,$x_department,$x_position,$x_username,$x_password,$groupId,$groupName)==true){
        	return Redirect::to('user_identity_management/user')->with('message', str_replace("__USERNAME__",$user, $lang->USER_UPDATED));
		}else{
            return Redirect::back()->with('error', str_replace("__USERNAME__",$user, $lang->NOT_EDITED));
		}
	}





	public function getCreate()
	{
		$conn = parent::loginLdap();
		$groupAry = parent::getGroupList($conn);
		
	
	$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."master-table/system-operator");
		$decoded = json_decode($curl_response,true);
		foreach($decoded as $key1=>$val1){
 		if($key1 == 'data'){
			$value = $val1['businessName'];
			$companynames[$value] =$val1['businessName'];
		}
		}
		$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."master-table/departments");
		$decoded = json_decode($curl_response,true);
		foreach($decoded as $key1=>$val1){
	    if($key1 == 'data'){
			$value = $val1['departmentName'];
			$departments[$value] 	=$val1['departmentName'];
		}
	} /*foreach($decoded as $key=>$val)*/
	
		

		$groupOptions = array();
		foreach($groupAry as $k => $v){
            $groupOptions[$v["id"]] = $v["name"];
		}
        return View::make('ldap/user_create', array('group_options'=>$groupOptions,'departments'=>$departments,'companynames'=>$companynames));
	}
	


	public function postCreate()
	{
		
		$conn = parent::loginLdap();
		$lang = Language::getLanguage();
		
	/*	var_dump(Input::file('x_photo'));
		exit;*/
		
		$x_firstName = Input::get('x_firstName');
		$x_middleName =Input::get('x_middleName');
		$x_surname =Input::get('x_surname');
		$x_gender =Input::get('x_gender');
		$x_dateofBirth =Input::get('x_dateofBirth');
		$x_identity =Input::get('x_identity');
		$x_address =Input::get('x_address');
		$x_zipCode =Input::get('x_zipCode');
		$x_city =Input::get('x_city');
		$x_country =Input::get('x_country');
		$x_mobileNumber =Input::get('x_mobileNumber');
		$x_email =Input::get('x_email');
		$x_companyName =Input::get('x_companyName');
		$x_department =Input::get('x_department');
		$x_position =Input::get('x_position');
	 	$x_username =Input::get('x_username');
		$x_password =Input::get('x_password');
		$x_photo =Input::file('x_photo');
        $filename = $x_username . '.' . $x_photo->getClientOriginalExtension();
        $filename = strtolower($filename);
		$destinationPath = public_path().DIRECTORY_SEPARATOR."files".DIRECTORY_SEPARATOR;
        Input::file('x_photo')->move($destinationPath, $filename);
		$groupId = Input::get('x_selectaGroup');
		$createAnother = Input::get('x_inlineCheckbox');

	    $groupAry = parent::getGroupList($conn);
        foreach($groupAry as $k => $v){
            if($v["id"] == $groupId){
             	$groupName = $v["name"];
            }
		}
		

		if(parent::createUser($conn,$x_firstName,$x_middleName,$x_surname,$x_gender,$x_dateofBirth,$x_identity,$x_address,$x_zipCode,$x_city,$x_country,$x_mobileNumber,$x_email,$filename,$x_companyName,$x_department,$x_position,$x_username,$x_password,$groupId,$groupName)==true){
		if(isset($createAnother)&&$createAnother=="x_inlineCheckbox"){	
        	return Redirect::to('user_identity_management/user/create')->with('message', str_replace("__USERNAME__",$x_username, $lang->USER_CREATED));}else{return Redirect::to('user_identity_management/user')->with('message', str_replace("__USERNAME__",$x_username, $lang->USER_CREATED));}
		}else{
            return Redirect::back()->with('error', str_replace("__USERNAME__",$x_username, $lang->USER_NOT_CREATED));
		}

	}





	public function getDelete($user)
	{
        // Title
        //$title = Lang::get('admin/blogs/title.create_a_new_blog');

		$conn = parent::loginLdap();
		$userAry = parent::getUserList($conn);


        // Show the page
        //return View::make('admin/blogs/create_edit', compact('title'));
        return View::make('user_identity_management/user_delete', array('username' => $user));
	}

	public function postDelete($user)
	{
        // Title
        //$title = Lang::get('admin/blogs/title.create_a_new_blog');

		$conn = parent::loginLdap();
		$userAry = parent::getUserList($conn);

        $lang = Language::getLanguage();

        if(parent::deleteUser($conn,$user)==true){
        	return Redirect::to('user_identity_management/user')->with('message', str_replace("__USERNAME__",$user, $lang->DELETED));
		}else{
            return Redirect::to('user_identity_management/user')->with('error', str_replace("__USERNAME__",$user, $lang->NOT_DELETED));
		}
	}

/***********************USER_REGISTRATION*************************/
	public function getCreateUserRegistration()
	{
		$conn = parent::loginLdap();
		return View::make('register');
	}

public function postCreateUserRegistration()
	{
		$conn = parent::loginLdap();
		$lang = Language::getLanguage();
		$x_firstName = Input::get('x_firstName');
		$x_middleName =Input::get('x_middleName');
		$x_surname =Input::get('x_surname');
		$x_gender =Input::get('x_gender');
		$x_dateofBirth =Input::get('x_dateofBirth');
		$x_identity =Input::get('x_identity');
		$x_address =Input::get('x_address');
		$x_zipCode =Input::get('x_zipCode');
		$x_city =Input::get('x_city');
		$x_country =Input::get('x_country');
		$x_mobileNumber =Input::get('x_mobileNumber');
		$x_email =Input::get('x_email');
		$x_companyName =Input::get('x_companyName');
		$x_department =Input::get('x_department');
		$x_position =Input::get('x_position');
	 	$x_username =Input::get('x_username');
		$x_password =Input::get('x_password');
		$x_photo =Input::file('x_photo');
        $filename = $x_username . '.' . $x_photo->getClientOriginalExtension();
        $filename = strtolower($filename);
		$destinationPath = public_path().DIRECTORY_SEPARATOR."files".DIRECTORY_SEPARATOR;
        Input::file('x_photo')->move($destinationPath, $filename);

		$groupId="6062";
		$groupName="UNCOMFIRMED";

	   
		if(parent::createUserRegister($conn,$x_firstName,$x_middleName,$x_surname,$x_gender,$x_dateofBirth,$x_identity,$x_address,$x_zipCode,$x_city,$x_country,$x_mobileNumber,$x_email,$filename,$x_companyName,$x_department,$x_position,$x_username,$x_password,$groupId,$groupName)==true){

        	return Redirect::to('login')->with('message', str_replace("__USERNAME__",$x_username, $lang->USER_CREATED));
		}else{
            return Redirect::back()->with('error', str_replace("__USERNAME__",$x_username, $lang->USER_NOT_CREATED));
		}

	}
/***********************USER_REGISTRATION*************************/


/***********************MULTIPLE_EDIT*************************/
public function getMultipleEdit()
	{
		$conn = parent::loginLdap();
		$lang = Language::getLanguage();
		$users = Input::get('users');
		if(!isset($users) || $users == ""){
			 return Redirect::to('user_identity_management/user')->with('error', $lang->NO_OPERATION_HAS_BEEN_DONE);
			 exit;
			}
		$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."master-table/system-operator");
		$decoded = json_decode($curl_response,true);
		foreach($decoded as $key1=>$val1){
 		if($key1 == 'data'){
			$value = $val1['businessName'];
			$companynames[$value] =$val1['businessName'];
		}
		}
		$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."master-table/departments");
		$decoded = json_decode($curl_response,true);
		foreach($decoded as $key1=>$val1){
	    if($key1 == 'data'){
			$value = $val1['departmentName'];
			$departments[$value] 	=$val1['departmentName'];
		}
	} /*foreach($decoded as $key=>$val)*/
		
		
		
		
        return View::make('ldap/user_edit_multiple', array('users' => $users,'departments'=>$departments,'companynames'=>$companynames));

	
       

	}
public function postMultipleEdit()
	{
		$conn = parent::loginLdap();
		$lang = Language::getLanguage();
		$department = Input::get('x_department');
		$position = Input::get('x_position');
		$companyName = Input::get('x_companyName');
		$users = Input::get('users');
		$userMessage = implode(",", $users);
		
		if(parent::changeMultipleUserData($conn,$users,$department,$companyName,$userMessage,$position)==true){
        	return Redirect::to('user_identity_management/user')->with('message', str_replace("__USERNAME__",$userMessage, $lang->USER_UPDATED));
		}else{
            return Redirect::back()->with('error', str_replace("__USERNAME__",$userMessage, $lang->NOT_EDITED));
		}
		
	
       

	}
	/***********************MULTIPLE_EDIT*************************/

  /***********************MULTIPLE_DELETE*************************/
public function postMultipleDelete()
	{	$lang = Language::getLanguage();
		$conn = parent::loginLdap();
		$userAry = parent::getUserList($conn);
		$users = Input::get('users');
		if(!isset($users) || $users == ""){
			 return Redirect::to('user_identity_management/user')->with('error', $lang->NO_OPERATION_HAS_BEEN_DONE);
			 exit;
			}
        $lang = Language::getLanguage();
		$userMessage = implode(",", $users);

        if(parent::deleteMultipleUser($conn,$users)==true){
        	return Redirect::to('user_identity_management/user')->with('message', str_replace("__USERNAME__",$userMessage, $lang->DELETED));
		}else{
            return Redirect::to('user_identity_management/user')->with('error', str_replace("__USERNAME__",$userMessage, $lang->NOT_DELETED));
		}
		   

	}
	/***********************MULTIPLE_DELETE*************************/
	
	/***********************BLOCK_USER****************************/

		public function postBlock($user)
	{
		$conn = parent::loginLdap();
        $lang = Language::getLanguage();
        if(parent::blockUser($conn,$user)==true){
        	return Redirect::to('user_identity_management/user')->with('message', str_replace("__USERNAME__",$user, $lang->BLOCKED));
		}else{
            return Redirect::to('user_identity_management/user')->with('error', str_replace("__USERNAME__",$user, $lang->NOT_BLOCKED));
		}
	}

		public function postUnBlock($user)
	{
		$conn = parent::loginLdap();
        $lang = Language::getLanguage();
        if(parent::unBlockUser($conn,$user)==true){
        	return Redirect::to('user_identity_management/user')->with('message', str_replace("__USERNAME__",$user, $lang->OPERATION_COMPLETED));
		}else{
            return Redirect::to('user_identity_management/user')->with('error', str_replace("__USERNAME__",$user, $lang->OPERATION_NOT_COMPLETED));
		}
	}
	/***********************BLOCK_USER****************************/
	/***********************MULTIPLE_BLOCK_USER****************************/

		public function postBlockMultiple()
	{
		$conn = parent::loginLdap();
        $lang = Language::getLanguage();
		$users = Input::get('users');
		if(!isset($users) || $users == ""){
			 return Redirect::to('user_identity_management/user')->with('error', $lang->NO_OPERATION_HAS_BEEN_DONE);
			 exit;
			}
		$userMessage = implode(",", $users);
		
        if(parent::blockUserMultiple($conn,$users)==true){
        	return Redirect::to('user_identity_management/user')->with('message', str_replace("__USERNAME__",$userMessage, $lang->BLOCKED));
		}else{
            return Redirect::to('user_identity_management/user')->with('error', str_replace("__USERNAME__",$userMessage, $lang->NOT_BLOCKED));
		}
	}

		public function postUnBlockMultiple()
	{
		$conn = parent::loginLdap();
        $lang = Language::getLanguage();
		$users = Input::get('users');
		if(!isset($users) || $users == ""){
			 return Redirect::to('user_identity_management/user')->with('error', $lang->NO_OPERATION_HAS_BEEN_DONE);
			 exit;
			}
		$userMessage = implode(",", $users);
				
        if(parent::unBlockUserMultiple($conn,$users)==true){
        	return Redirect::to('user_identity_management/user')->with('message', str_replace("__USERNAME__",$userMessage, $lang->OPERATION_COMPLETED));
		}else{
            return Redirect::to('user_identity_management/user')->with('error', str_replace("__USERNAME__",$userMessage, $lang->OPERATION_NOT_COMPLETED));
		}
	}
	/***********************MULTIPLE_BLOCK_USER****************************/	
	/***********************PREVIEW_USER****************************/
	
	
	public function getPreviewUser($user)
	{
        // Title
        //$title = Lang::get('admin/blogs/title.create_a_new_blog');

		$conn = parent::loginLdap();
		$userInfo  = parent::getUserInfo($conn,$user);

		//print_r($userInfo);
		
		$groupAry = parent::getGroupList($conn);
		$groupOptions = array();

		foreach($groupAry as $k => $v){
			
            $groupOptions[$v["id"]] = $v["name"];
		}

		$groupId=@$userInfo[0]["gidnumber"][0];
		$x_firstName = @$userInfo[0]["givenname"][0];
		$x_middleName =@$userInfo[0]["initials"][0];
		$x_surname =@$userInfo[0]["sn"][0];
		$x_gender =@$userInfo[0]["employeetype"][0];
		$x_dateofBirth =@$userInfo[0]["pager"][0];
		$x_identity =@$userInfo[0]["carlicense"][0];
		$x_address =@$userInfo[0]["homepostaladdress"][0];
		$x_zipCode =@$userInfo[0]["postalcode"][0];
		$x_city =@$userInfo[0]["l"][0];
		$x_country =@$userInfo[0]["st"][0];
		$x_mobileNumber =@$userInfo[0]["mobile"][0];
		$x_email =@$userInfo[0]["mail"][0];
		$x_photo =@$userInfo[0]["jpegphoto"][0];
		$x_companyName =@$userInfo[0]["o"][0];
		$x_department =@$userInfo[0]["departmentnumber"][0];
		$x_position =@$userInfo[0]["title"][0];
		

        // Show the page
        //return View::make('admin/blogs/create_edit', compact('title'));
        return View::make('ldap/preview_user', array(	'user' => $user,
													'groupId' => $groupId,
													'group_options'=>$groupOptions,
													'x_firstName'=>$x_firstName,
													'x_middleName'=>$x_middleName,
													'x_surname'=>$x_surname,
													'x_gender'=>$x_gender,
													'x_dateofBirth'=>$x_dateofBirth,
													'x_identity'=>$x_identity,
													'x_address'=>$x_address,
													'x_zipCode'=>$x_zipCode,
													'x_city'=>$x_city,
													'x_country'=>$x_country,
													'x_mobileNumber'=>$x_mobileNumber,
													'x_email'=>$x_email,
													'x_photo'=>$x_photo,
													'x_companyName'=>$x_companyName,
													'x_department'=>$x_department,
													'x_position'=>$x_position));
	}
	/***********************PREVIEW_USER****************************/
	
}