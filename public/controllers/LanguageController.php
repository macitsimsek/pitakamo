<?php

use yajra\Datatables\DatatablesServiceProvider ;

class LanguageController extends Controller {


	public function getLanguageTime(){
		return View::make('settings/language_time');
	}
		

	public function getLanguages(){

	/*
		$languages = DB::select( DB::raw("SELECT local_str,UTL_I18N.STRING_TO_RAW(name) name FROM  gen_languages order by local_str ")  );
		//print_r($languages); exit;
        return $languages;
		*/
        //echo Config::get('services.restUrl')."lang";

        $curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."language/languages");
		//echo $curl_response; exit;
		$decoded = json_decode($curl_response);
		if (isset($decoded->status) && $decoded->status == 'ERROR') {
            App::abort(500, $decoded->developerMessage);
		}
		return $decoded->data;

	}

	public function getItems($lang){
       	$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."language/entries?p_local_str=".$lang);
		//echo Config::get('services.restUrl')."language/entries?localStr=".$lang;exit;
		//echo $curl_response; exit;
		$decoded = json_decode($curl_response);
		if (isset($decoded->status) && $decoded->status == 'ERROR') {
            App::abort(500, $decoded->developerMessage);
		}
		return $decoded->data;
	}

	public function getIndex(){

		$langAry = array();

		$languages = self::getLanguages();
		$items = array();
        foreach($languages as $row){
             $items1 = self::getItems($row->localStr);
			 $items = array_merge($items,$items1);
        }

		foreach ($items as $row) {
			@$langAry[$row->item][$row->localStr] = $row->text;
		}

		return View::make('settings.language', array("languages"=>$languages,"items"=>$langAry));

	}

	public function getBulkIndex(){

		$langAry = array();

		$languages = self::getLanguages();
		$items = array();
        foreach($languages as $row){
             $items1 = self::getItems($row->localStr);
			 $items = array_merge($items,$items1);
        }

		foreach ($items as $row) {
			@$langAry[$row->item][$row->localStr] = $row->text;
		}

		return View::make('settings.language_bulk', array("languages"=>$languages,"items"=>$langAry));

	}

	public function postBulkIndex(){

        $txt = Input::get('x_bulk_list');
        $txt = str_replace("+"," ",rawurldecode($txt));
		//echo $txt;

        $pairs = explode("&",$txt);
		//print_r($pairs);
		foreach($pairs as $pair){
           //print_r($pair);
		   list($key,$value) = explode("=",$pair);
		   list($keyword,$localStr) = explode("-",$key);
		   echo $keyword." ".$localStr." ".$value."\n<br>";


			$data = array(
				"localStr"=>$localStr,
				"messageGroup"=>"",
				"item"=>$keyword,
				"text"=>$value
				);

				//echo Config::get('services.restUrl')."lang/entry";
                //var_dump($data);
                $curl_response = CustomFunctions::CallAPI("PUT",Config::get('services.restUrl')."language/entries",$data);
                //print_r($data);;
                //print_r($curl_response);exit;
				$decoded = json_decode($curl_response);
				if (isset($decoded->status) && $decoded->status == 'ERROR') {
		            App::abort(500, $decoded->developerMessage);
				}


		}

		Cache::forget('language_array');
        $lang = Language::getLanguage();
		return Redirect::to('control_panel/language_time/language_customization')->with('message', $lang->UPDATED);






	}

	public function getCreate()
	{
		$languages = self::getLanguages();
        return View::make('settings/language_create', array("languages"=>$languages));
	}

	public function postCreate()
	{
		$lang = Language::getLanguage();
        $item = Input::get('x_key');
        $item = Str::upper($item);
		$item = preg_replace('/\s+/', ' ',$item);	//birden fazla bo�lu�u temizle	
		$item = str_replace(' ', '_', $item);	//bo�luklar� _ ile de�i�tir

		function strToHex($string)
		{
			if($string == null || $string == ''){
				return '';
			}
		    $hex='';
		    for ($i=0; $i < strlen($string); $i++)
		    {
		        $hex .= dechex(ord($string[$i]));
		    }

		    return $hex;
		}

	    $languages = self::getLanguages();

		foreach ($languages as $col) {
				$text = Input::get('x_val_'.$col->localStr);

                $data = array(
				"localStr"=>$col->localStr,
				"messageGroup"=>"",
				"item"=>$item,
				"text"=>$text
				);

				//echo Config::get('services.restUrl')."lang/entry";

                $curl_response = CustomFunctions::CallAPI("POST",Config::get('services.restUrl')."language/entries",$data);
                //print_r($curl_response);exit;
				$decoded = json_decode($curl_response);
				if (isset($decoded->status) && $decoded->status == 'ERROR') {
		            App::abort(500, $decoded->developerMessage);
				}
         }

		/*
        DB::beginTransaction();

		foreach ($languages as $col) {
			try {
            	$text = Input::get('x_val_'.$col->localStr);

                $data = array(
				"localStr"=>$col->localStr,
				"messageGroup"=>"",
				"item"=>$item,
				"text"=>$text
				);

                $curl_response = CustomFunctions::CallAPI("POST",Config::get('services.restUrl')."language/entries",$data);

	           $result = DB::insert("insert into gen_language_entries (local_str,item,text) values (?,?,?)", array($col->localStr,$item,$text));
			   //karakter sorunun boyle cozebildim
			   DB::statement("update gen_language_entries set text = UTL_I18N.RAW_TO_CHAR('".strToHex($text)."','UTF8') where local_str = '".$col->localStr."' and item='".$item."' ");
			   if($result==false){
			   		DB::rollback();
	            	return Redirect::to('settings/language')->with('error',str_replace("__USERNAME__",$item, $lang->NOT_CREATED));
			   }
			}catch(PDOException $e){
					DB::rollback();
	            	return Redirect::to('settings/language')->with('error', str_replace("__USERNAME__",$item, $lang->NOT_CREATED));
			}


		}
        DB::commit();
		*/
		Cache::forget('language_array');
		return Redirect::to('control_panel/language_time/language_customization')->with('message', str_replace("__USERNAME__",$item, $lang->CREATED));

	}


	public function postDelete($key)
	{
		$lang = Language::getLanguage();

         $curl_response = CustomFunctions::CallAPI("DELETE",Config::get('services.restUrl')."language/entries?p_item=".$key);
                //print_r($data);;
                //print_r($curl_response);exit;
				$decoded = json_decode($curl_response);
				if (isset($decoded->status) && $decoded->status == 'ERROR') {
		            //App::abort(500, $decoded->developerMessage);
					return Redirect::to('settings/language')->with('error', str_replace("__USERNAME__",$key, $lang->NOT_DELETED));
				}

		 Cache::forget('language_array');
		 return Redirect::to('control_panel/language_time/language_customization')->with('message', str_replace("__USERNAME__",$key, $lang->DELETED));
        /*
		DB::beginTransaction();
		
			try {
               $result = DB::delete("delete from gen_language_entries where item=?", array($key));
			   if($result==false){
			   		DB::rollback();
	            	return Redirect::to('settings/language')->with('error', str_replace("__USERNAME__",$key, $lang->NOT_DELETED));
			   }
			}catch(PDOException $e){
					DB::rollback();
	            	return Redirect::to('settings/language')->with('error', str_replace("__USERNAME__",$key, $lang->NOT_DELETED));
			}
			DB::commit();
		return Redirect::to('settings/language')->with('message', str_replace("__USERNAME__",$key, $lang->DELETED));
		*/

	}

	public function getEdit($key)
	{
		$langAry = array();

		$languages = self::getLanguages();
		$items = array();
        foreach($languages as $row){
             $items1 = self::getItems($row->localStr);
			 $items = array_merge($items,$items1);
        }

		//$items = DB::select( DB::raw("SELECT UTL_I18N.STRING_TO_RAW(text) text,item,local_str FROM gen_language_entries where item = ? ") , array($key) );
        //print_r($items);


		foreach ($items as $row) {
			@$langAry[$row->item][$row->localStr] = $row->text;
		}

        //print_r($langAry);exit;

		return View::make('settings/language_edit', array("key"=>$key,"languages"=>$languages,"items"=>$langAry));
	}

	public function postEdit($key)
	{
        $item = $key;
        $item = Str::upper($item);

		$languages = self::getLanguages();
		$lang = Language::getLanguage();

		foreach ($languages as $col) {
         	$text = Input::get('x_val_'.$col->localStr);

			$data = array(
				"localStr"=>$col->localStr,
				"messageGroup"=>"",
				"item"=>$item,
				"text"=>$text
				);

				//echo Config::get('services.restUrl')."lang/entry";
                //var_dump($data);
                $curl_response = CustomFunctions::CallAPI("PUT",Config::get('services.restUrl')."language/entries",$data);
                //print_r($data);;
                //print_r($curl_response);exit;
				$decoded = json_decode($curl_response);
				if (isset($decoded->status) && $decoded->status == 'ERROR') {
		            App::abort(500, $decoded->developerMessage);
				}
        }

		/*
        DB::beginTransaction();

		foreach ($languages as $col) {
			try {
            	$text = Input::get('x_val_'.$col->localStr);
	           DB::statement("update gen_language_entries set text = UTL_I18N.RAW_TO_CHAR('".strToHex($text)."','UTF8') where local_str = '".$col->localStr."' and item='".$item."' ");

			}catch(PDOException $e){
					DB::rollback();
	            	return Redirect::to('settings/language')->with('error',str_replace("__USERNAME__",$item, $lang->NOT_EDITED));
			}


		}
        DB::commit();
		*/

		//Removing An Item From The Cache

		Cache::forget('language_array');

		return Redirect::to('control_panel/language_time/language_customization')->with('message', str_replace("__USERNAME__",$item, $lang->USERNAME_EDITED));

	}
	
	
/*****************ADD DELETE UPLOAD PACKAGES****************/
	
	
	public function getAddLanguage()
	{   //echo gethostname();exit;
	
	$languages = self::getLanguages();
	$remove=array();
	foreach($languages as $col){$remove[]=$col->localStr;}	//print_r($remove);exit;
	
		$languagesList = array(
'aa' => 'Afar',
'ab' => 'Abkhaz',
'ae' => 'Avestan',
'af' => 'Afrikaans',
'ak' => 'Akan',
'am' => 'Amharic',
'an' => 'Aragonese',
'ar' => 'Arabic',
'as' => 'Assamese',
'av' => 'Avaric',
'ay' => 'Aymara',
'az' => 'Azerbaijani',
'ba' => 'Bashkir',
'be' => 'Belarusian',
'bg' => 'Bulgarian',
'bh' => 'Bihari',
'bi' => 'Bislama',
'bm' => 'Bambara',
'bn' => 'Bengali',
'bo' => 'Tibetan Standard, Tibetan, Central',
'br' => 'Breton',
'bs' => 'Bosnian',
'ca' => 'Catalan; Valencian',
'ce' => 'Chechen',
'ch' => 'Chamorro',
'co' => 'Corsican',
'cr' => 'Cree',
'cs' => 'Czech',
'cu' => 'Old Church Slavonic, Church Slavic, Church Slavonic, Old Bulgarian, Old Slavonic',
'cv' => 'Chuvash',
'cy' => 'Welsh',
'da' => 'Danish',
'de' => 'German',
'dv' => 'Divehi; Dhivehi; Maldivian;',
'dz' => 'Dzongkha',
'ee' => 'Ewe',
'el' => 'Greek, Modern',
'en' => 'English',
'eo' => 'Esperanto',
'es' => 'Spanish; Castilian',
'et' => 'Estonian',
'eu' => 'Basque',
'fa' => 'Persian',
'ff' => 'Fula; Fulah; Pulaar; Pular',
'fi' => 'Finnish',
'fj' => 'Fijian',
'fo' => 'Faroese',
'fr' => 'French',
'fy' => 'Western Frisian',
'ga' => 'Irish',
'gd' => 'Scottish Gaelic; Gaelic',
'gl' => 'Galician',
'gn' => 'Guaraní',
'gu' => 'Gujarati',
'gv' => 'Manx',
'ha' => 'Hausa',
'he' => 'Hebrew (modern)',
'hi' => 'Hindi',
'ho' => 'Hiri Motu',
'hr' => 'Croatian',
'ht' => 'Haitian; Haitian Creole',
'hu' => 'Hungarian',
'hy' => 'Armenian',
'hz' => 'Herero',
'ia' => 'Interlingua',
'id' => 'Indonesian',
'ie' => 'Interlingue',
'ig' => 'Igbo',
'ii' => 'Nuosu',
'ik' => 'Inupiaq',
'io' => 'Ido',
'is' => 'Icelandic',
'it' => 'Italian',
'iu' => 'Inuktitut',
'ja' => 'Japanese (ja)',
'jv' => 'Javanese (jv)',
'ka' => 'Georgian',
'kg' => 'Kongo',
'ki' => 'Kikuyu, Gikuyu',
'kj' => 'Kwanyama, Kuanyama',
'kk' => 'Kazakh',
'kl' => 'Kalaallisut, Greenlandic',
'km' => 'Khmer',
'kn' => 'Kannada',
'ko' => 'Korean',
'kr' => 'Kanuri',
'ks' => 'Kashmiri',
'ku' => 'Kurdish',
'kv' => 'Komi',
'kw' => 'Cornish',
'ky' => 'Kirghiz, Kyrgyz',
'la' => 'Latin',
'lb' => 'Luxembourgish, Letzeburgesch',
'lg' => 'Luganda',
'li' => 'Limburgish, Limburgan, Limburger',
'ln' => 'Lingala',
'lo' => 'Lao',
'lt' => 'Lithuanian',
'lu' => 'Luba-Katanga',
'lv' => 'Latvian',
'mg' => 'Malagasy',
'mh' => 'Marshallese',
'mi' => 'Maori',
'mk' => 'Macedonian',
'ml' => 'Malayalam',
'mn' => 'Mongolian',
'mr' => 'Marathi (Mara?hi)',
'ms' => 'Malay',
'mt' => 'Maltese',
'my' => 'Burmese',
'na' => 'Nauru',
'nb' => 'Norwegian Bokmål',
'nd' => 'North Ndebele',
'ne' => 'Nepali',
'ng' => 'Ndonga',
'nl' => 'Dutch',
'nn' => 'Norwegian Nynorsk',
'no' => 'Norwegian',
'nr' => 'South Ndebele',
'nv' => 'Navajo, Navaho',
'ny' => 'Chichewa; Chewa; Nyanja',
'oc' => 'Occitan',
'oj' => 'Ojibwe, Ojibwa',
'om' => 'Oromo',
'or' => 'Oriya',
'os' => 'Ossetian, Ossetic',
'pa' => 'Panjabi, Punjabi',
'pi' => 'Pali',
'pl' => 'Polish',
'ps' => 'Pashto, Pushto',
'pt' => 'Portuguese',
'qu' => 'Quechua',
'rm' => 'Romansh',
'rn' => 'Kirundi',
'ro' => 'Romanian, Moldavian, Moldovan',
'ru' => 'Russian',
'rw' => 'Kinyarwanda',
'sa' => 'Sanskrit (Sa?sk?ta)',
'sc' => 'Sardinian',
'sd' => 'Sindhi',
'se' => 'Northern Sami',
'sg' => 'Sango',
'si' => 'Sinhala, Sinhalese',
'sk' => 'Slovak',
'sl' => 'Slovene',
'sm' => 'Samoan',
'sn' => 'Shona',
'so' => 'Somali',
'sq' => 'Albanian',
'sr' => 'Serbian',
'ss' => 'Swati',
'st' => 'Southern Sotho',
'su' => 'Sundanese',
'sv' => 'Swedish',
'sw' => 'Swahili',
'ta' => 'Tamil',
'te' => 'Telugu',
'tg' => 'Tajik',
'th' => 'Thai',
'ti' => 'Tigrinya',
'tk' => 'Turkmen',
'tl' => 'Tagalog',
'tn' => 'Tswana',
'to' => 'Tonga (Tonga Islands)',
'tr' => 'Turkish',
'ts' => 'Tsonga',
'tt' => 'Tatar',
'tw' => 'Twi',
'ty' => 'Tahitian',
'ug' => 'Uighur, Uyghur',
'uk' => 'Ukrainian',
'ur' => 'Urdu',
'uz' => 'Uzbek',
've' => 'Venda',
'vi' => 'Vietnamese',
'vo' => 'Volapük',
'wa' => 'Walloon',
'wo' => 'Wolof',
'xh' => 'Xhosa',
'yi' => 'Yiddish',
'yo' => 'Yoruba',
'za' => 'Zhuang, Chuang',
'zh' => 'Chinese',
'zu' => 'Zulu');
foreach($remove as $key){
	$key=strtolower($key);
if (array_key_exists($key, $languagesList)) {
	//echo $languagesList[$key];exit;
    unset($languagesList[$key]);
}	}
		return View::make('settings/add_language',array("languagesList"=>$languagesList));
		}
	public function getDeleteLanguage()
	{
		$languages = self::getLanguages();
        //print_r($languages);exit;
		$langAry = array();
		foreach ($languages as $col) {
				if($col->localStr != "EN" && $col->localStr !="TR" ){
                   $langAry[$col->localStr] = $col->localStr .  " - " .$col->name ;
				}
		}
		Cache::forget('language_array');
		return View::make('settings/delete_language',array("langAry"=>$langAry) );
	}
	public function getUploadLanguage()
	{	$languages = self::getLanguages();
	$remove=array();
	foreach($languages as $col){$remove[]=$col->localStr;}	//print_r($remove);exit;
	
		$languagesList = array(
'aa' => 'Afar',
'ab' => 'Abkhaz',
'ae' => 'Avestan',
'af' => 'Afrikaans',
'ak' => 'Akan',
'am' => 'Amharic',
'an' => 'Aragonese',
'ar' => 'Arabic',
'as' => 'Assamese',
'av' => 'Avaric',
'ay' => 'Aymara',
'az' => 'Azerbaijani',
'ba' => 'Bashkir',
'be' => 'Belarusian',
'bg' => 'Bulgarian',
'bh' => 'Bihari',
'bi' => 'Bislama',
'bm' => 'Bambara',
'bn' => 'Bengali',
'bo' => 'Tibetan Standard, Tibetan, Central',
'br' => 'Breton',
'bs' => 'Bosnian',
'ca' => 'Catalan; Valencian',
'ce' => 'Chechen',
'ch' => 'Chamorro',
'co' => 'Corsican',
'cr' => 'Cree',
'cs' => 'Czech',
'cu' => 'Old Church Slavonic, Church Slavic, Church Slavonic, Old Bulgarian, Old Slavonic',
'cv' => 'Chuvash',
'cy' => 'Welsh',
'da' => 'Danish',
'de' => 'German',
'dv' => 'Divehi; Dhivehi; Maldivian;',
'dz' => 'Dzongkha',
'ee' => 'Ewe',
'el' => 'Greek, Modern',
'en' => 'English',
'eo' => 'Esperanto',
'es' => 'Spanish; Castilian',
'et' => 'Estonian',
'eu' => 'Basque',
'fa' => 'Persian',
'ff' => 'Fula; Fulah; Pulaar; Pular',
'fi' => 'Finnish',
'fj' => 'Fijian',
'fo' => 'Faroese',
'fr' => 'French',
'fy' => 'Western Frisian',
'ga' => 'Irish',
'gd' => 'Scottish Gaelic; Gaelic',
'gl' => 'Galician',
'gn' => 'Guaraní',
'gu' => 'Gujarati',
'gv' => 'Manx',
'ha' => 'Hausa',
'he' => 'Hebrew (modern)',
'hi' => 'Hindi',
'ho' => 'Hiri Motu',
'hr' => 'Croatian',
'ht' => 'Haitian; Haitian Creole',
'hu' => 'Hungarian',
'hy' => 'Armenian',
'hz' => 'Herero',
'ia' => 'Interlingua',
'id' => 'Indonesian',
'ie' => 'Interlingue',
'ig' => 'Igbo',
'ii' => 'Nuosu',
'ik' => 'Inupiaq',
'io' => 'Ido',
'is' => 'Icelandic',
'it' => 'Italian',
'iu' => 'Inuktitut',
'ja' => 'Japanese (ja)',
'jv' => 'Javanese (jv)',
'ka' => 'Georgian',
'kg' => 'Kongo',
'ki' => 'Kikuyu, Gikuyu',
'kj' => 'Kwanyama, Kuanyama',
'kk' => 'Kazakh',
'kl' => 'Kalaallisut, Greenlandic',
'km' => 'Khmer',
'kn' => 'Kannada',
'ko' => 'Korean',
'kr' => 'Kanuri',
'ks' => 'Kashmiri',
'ku' => 'Kurdish',
'kv' => 'Komi',
'kw' => 'Cornish',
'ky' => 'Kirghiz, Kyrgyz',
'la' => 'Latin',
'lb' => 'Luxembourgish, Letzeburgesch',
'lg' => 'Luganda',
'li' => 'Limburgish, Limburgan, Limburger',
'ln' => 'Lingala',
'lo' => 'Lao',
'lt' => 'Lithuanian',
'lu' => 'Luba-Katanga',
'lv' => 'Latvian',
'mg' => 'Malagasy',
'mh' => 'Marshallese',
'mi' => 'Maori',
'mk' => 'Macedonian',
'ml' => 'Malayalam',
'mn' => 'Mongolian',
'mr' => 'Marathi (Mara?hi)',
'ms' => 'Malay',
'mt' => 'Maltese',
'my' => 'Burmese',
'na' => 'Nauru',
'nb' => 'Norwegian Bokmål',
'nd' => 'North Ndebele',
'ne' => 'Nepali',
'ng' => 'Ndonga',
'nl' => 'Dutch',
'nn' => 'Norwegian Nynorsk',
'no' => 'Norwegian',
'nr' => 'South Ndebele',
'nv' => 'Navajo, Navaho',
'ny' => 'Chichewa; Chewa; Nyanja',
'oc' => 'Occitan',
'oj' => 'Ojibwe, Ojibwa',
'om' => 'Oromo',
'or' => 'Oriya',
'os' => 'Ossetian, Ossetic',
'pa' => 'Panjabi, Punjabi',
'pi' => 'Pali',
'pl' => 'Polish',
'ps' => 'Pashto, Pushto',
'pt' => 'Portuguese',
'qu' => 'Quechua',
'rm' => 'Romansh',
'rn' => 'Kirundi',
'ro' => 'Romanian, Moldavian, Moldovan',
'ru' => 'Russian',
'rw' => 'Kinyarwanda',
'sa' => 'Sanskrit (Sa?sk?ta)',
'sc' => 'Sardinian',
'sd' => 'Sindhi',
'se' => 'Northern Sami',
'sg' => 'Sango',
'si' => 'Sinhala, Sinhalese',
'sk' => 'Slovak',
'sl' => 'Slovene',
'sm' => 'Samoan',
'sn' => 'Shona',
'so' => 'Somali',
'sq' => 'Albanian',
'sr' => 'Serbian',
'ss' => 'Swati',
'st' => 'Southern Sotho',
'su' => 'Sundanese',
'sv' => 'Swedish',
'sw' => 'Swahili',
'ta' => 'Tamil',
'te' => 'Telugu',
'tg' => 'Tajik',
'th' => 'Thai',
'ti' => 'Tigrinya',
'tk' => 'Turkmen',
'tl' => 'Tagalog',
'tn' => 'Tswana',
'to' => 'Tonga (Tonga Islands)',
'tr' => 'Turkish',
'ts' => 'Tsonga',
'tt' => 'Tatar',
'tw' => 'Twi',
'ty' => 'Tahitian',
'ug' => 'Uighur, Uyghur',
'uk' => 'Ukrainian',
'ur' => 'Urdu',
'uz' => 'Uzbek',
've' => 'Venda',
'vi' => 'Vietnamese',
'vo' => 'Volapük',
'wa' => 'Walloon',
'wo' => 'Wolof',
'xh' => 'Xhosa',
'yi' => 'Yiddish',
'yo' => 'Yoruba',
'za' => 'Zhuang, Chuang',
'zh' => 'Chinese',
'zu' => 'Zulu');
foreach($remove as $key){
	$key=strtolower($key);
if (array_key_exists($key, $languagesList)) {
	//echo $languagesList[$key];exit;
    unset($languagesList[$key]);
}	}
	
		return View::make('settings/upload_language',array("languagesList"=>$languagesList));
		//print_r(Session::all()); exit;
		
		}	
	public function postAddLanguage(){
		$x_languageKey =Input::get('x_languageKey');
		$x_languageName =Input::get('x_languageName');
		$x_photo =Input::file('x_photo');

        $filename = $x_languageKey . '.' . $x_photo->getClientOriginalExtension();
        //$filename = strtolower($filename);
		$destinationPath = public_path().DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."languages".DIRECTORY_SEPARATOR;
        Input::file('x_photo')->move($destinationPath, $filename);

			$data = array(
				"localStr"=>$x_languageKey,
				"name"=>$x_languageName
				);

				//echo Config::get('services.restUrl')."lang/entry";

                $curl_response = CustomFunctions::CallAPI("POST",Config::get('services.restUrl')."language/languages",$data);
                //print_r($curl_response);exit;
				$decoded = json_decode($curl_response);
				if (isset($decoded->status) && $decoded->status == 'ERROR') {
		            App::abort(500, $decoded->developerMessage);
				}
		Cache::forget('language_array');
		$lang = Language::getLanguage();
		return Redirect::to('control_panel/language_time/language_customization')->with('message', str_replace("__USERNAME__",$x_languageKey, $lang->CREATED));
	}
	public function postDeleteLanguage()
	{
		 $x_languageKey =Input::get('x_deleteLanguage');
		//echo $x_languageKey; exit;

		$lang = Language::getLanguage();
         //echo Config::get('services.restUrl')."language/languages?p_local_str=".$x_languageKey;
		 //exit;
         $curl_response = CustomFunctions::CallAPI("DELETE",Config::get('services.restUrl')."language/languages?p_local_str=".$x_languageKey);
                //print_r($data);;
                //print_r($curl_response);exit;
				$decoded = json_decode($curl_response);
				if (isset($decoded->status) && $decoded->status == 'ERROR') {
		            //App::abort(500, $decoded->developerMessage);
					return Redirect::back()->with('error', str_replace("__USERNAME__",$key, $lang->NOT_DELETED));
				}
		 Cache::forget('language_array');
         return Redirect::to('control_panel/language_time/language_customization')->with('message', str_replace("__USERNAME__",$x_languageKey, $lang->DELETED));

	}
	public function postUploadLanguage()
	{

		set_time_limit(600); //60 seconds = 1 minute
		ini_set('max_execution_time', 600);

		$x_languageKey =Input::get('x_languageKey');
		$x_languageName =Input::get('x_languageName');
		$x_photo =Input::file('x_photo');
		$x_file =Input::file('x_file');

        $lang = Language::getLanguage();

		if($x_file->getClientOriginalExtension()!="csv"){
        	return Redirect::back()->with('error', $lang->UNSUPPORTED_FILE_TYPE);
		}


        $filename = $x_languageKey . '.' . $x_photo->getClientOriginalExtension();
        //$filename = strtolower($filename);
		$destinationPath = public_path().DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."languages".DIRECTORY_SEPARATOR;
		//echo $destinationPath; exit;
        Input::file('x_photo')->move($destinationPath, $filename);

			$data = array(
				"localStr"=>$x_languageKey,
				"name"=>$x_languageName
				);

				//echo Config::get('services.restUrl')."lang/entry";

                $curl_response = CustomFunctions::CallAPI("POST",Config::get('services.restUrl')."language/languages",$data);
                //print_r($curl_response);exit;
				$decoded = json_decode($curl_response);
				if (isset($decoded->status) && $decoded->status == 'ERROR') {
		            App::abort(500, $decoded->developerMessage);
				}

		//hersey ok se sirasi ile dosyayi don
		$row = 1;
		if (($handle = fopen($x_file, "r")) !== FALSE) {
		    while (($data = fgetcsv($handle, 1000, "\t")) !== FALSE) {

               if(count($data)>1 && $row>1){
               	  //	print_r($data);
                  if(trim($data[0])=="" && $data[1]!=""){
	                    $key = $data[1];
						$val = $data[2];
					}else{
	                    $key = $data[0];
	                    $val = $data[1];
				  }
				 //echo $key."-".$val."<br>";
				 $data = array(
					"localStr"=>$x_languageKey,
					"messageGroup"=>"",
					"item"=>$key,
					"text"=>$val
					);

					//echo Config::get('services.restUrl')."lang/entry";
	                //var_dump($data);
	                $curl_response = CustomFunctions::CallAPI("PUT",Config::get('services.restUrl')."language/entries",$data);
	                //print_r($data);;
	                //print_r($curl_response);exit;
					$decoded = json_decode($curl_response);
					if (isset($decoded->status) && $decoded->status == 'ERROR') {
			            App::abort(500, $decoded->developerMessage);
					}
               }


		        //echo "<p> $row satirindaki $num alan: <br /></p>\n";
		        $row++;

		     }
		    fclose($handle);
		 }

		Cache::forget('language_array');

		return Redirect::to('control_panel/language_time/language_customization')->with('message', str_replace("__USERNAME__",$x_languageKey, $lang->CREATED));
	}


}
