<?php

use yajra\Datatables\DatatablesServiceProvider ;

class ProcurementManagementController extends Controller {
/********GET REQUIRED DATA - METHODS*********/
	public function getSamType(){
		
		$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."proc/sam-types");
		$getSamType = json_decode($curl_response);
		if (isset($getSamType->status) && $getSamType->status == 'ERROR') {
		App::abort(500, $getSamType->developerMessage);
		}
		return $getSamType->data;
		}
	public function getBMID(){
		$data = CustomFunctions::GetJson("GET", "proc/bmid");
		return $data;
		}
	public function getAllOrdersInfo($key){
		$data = CustomFunctions::GetJson("GET", "proc/all-orders-info?p_bmid=$key");
		return $data;
		}
	public function getCardApplication(){
		$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."proc/card-applications");
		$getCardApplication = json_decode($curl_response);
		if (isset($getCardApplication->status) && $getCardApplication->status == 'ERROR') {
		App::abort(500, $getCardApplication->developerMessage);
		}
		return $getCardApplication->data;
		}
	public function getProduct(){
		$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."proc/products");
		$getProduct = json_decode($curl_response);
		if (isset($getProduct->status) && $getProduct->status == 'ERROR') {
		App::abort(500, $getProduct->developerMessage);
		}
		return $getProduct->data;
		}
	public function getSamOrderDetail($p_bmid,$sam_type){
		$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."proc/prev-sam-order-details?p_bmid=".$p_bmid."&sam_type=".$sam_type."");
		$getSamOrderDetail = json_decode($curl_response,true);
		if (isset($getSamOrderDetail->status) && $getSamOrderDetail->status == 'ERROR') {
		App::abort(500, $getSamOrderDetail->developerMessage);
		}
		return $getSamOrderDetail;
		}
	public function getSupplier($p_bmid,$prod_id){
		$data = CustomFunctions::GetJson("GET", "proc/suppliers?p_bmid=".$p_bmid."&prod_id=".$prod_id,false,false);
		return $data;
		}
		/*****/
	public function getProductStatus1($p_bmid,$prod_id){
		$data = CustomFunctions::GetJson("GET", "proc/product-status?p_bmid=".$p_bmid."&prod_id=".$prod_id."");
		return $data;
		}
		/*****/
	public function getProductStatus(){
		$data = CustomFunctions::GetJson("GET", "proc/order-status");
		return $data;
		}
	public function getProductPreviousOrders($p_bmid,$prod_id){
		$data = CustomFunctions::GetJson("GET", "proc/product-order-prev?p_bmid=".$p_bmid."&order_id=".$prod_id,false,false);
		return $data;
		}	
	public function getProductSupplierPreview($p_bmid){
		$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."proc/supplier-prev?p_bmid=".$p_bmid);
		$getProductSupplierPreview = json_decode($curl_response,true);
		//print_r($getProductSupplierPreview);exit;
		if (isset($getProductSupplierPreview->code) && $getProductSupplierPreview->code == 900){
			$getProductSupplierPreview = array();
			}
		if (isset($getProductSupplierPreview->status) && $getProductSupplierPreview->status == 'ERROR') {
		App::abort(500, $getProductSupplierPreview->developerMessage);
		}
		return $getProductSupplierPreview;
		}
	public function getProductSupplier(){
		$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."proc/product-supplier");
		$getProductSupplier = json_decode($curl_response);
		if (isset($getProductSupplier->status) && $getProductSupplier->status == 'ERROR') {
		App::abort(500, $getProductSupplier->developerMessage);
		}
		return $getProductSupplier->data;
		}
	public function getProductOrder($p_bmid,$prod_id){
		$data = CustomFunctions::GetJson("GET", "proc/product-order?p_bmid=".$p_bmid."&prod_id=".$prod_id,false,false);
		return $data;
		}
	public function getOrderRequest(){
		$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."proc/order-request");
		$getOrderRequest = json_decode($curl_response);
		if (isset($getOrderRequest->status) && $getOrderRequest->status == 'ERROR') {
		App::abort(500, $getOrderRequest->developerMessage);
		}
		return $getOrderRequest->data;
		}
	public function getCheckSamOrderCode(){
		$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."proc/check-sam-order-code");
		$getCheckSamOrderCode = json_decode($curl_response);
		if (isset($getCheckSamOrderCode->status) && $getCheckSamOrderCode->status == 'ERROR') {
		App::abort(500, $getCheckSamOrderCode->developerMessage);
		}
		return $getCheckSamOrderCode->data;
		}
	public function getSamId(){
		$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."proc/sam-id");
		$getSamId = json_decode($curl_response);
		if (isset($getSamId->status) && $getSamId->status == 'ERROR') {
		App::abort(500, $getSamId->developerMessage);
		}
		return $getSamId->data;
		}
	public function getSamTransaction(){
		$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."proc/set-sam-transaction");
		$getSamTransaction = json_decode($curl_response);
		if (isset($getSamTransaction->status) && $getSamTransaction->status == 'ERROR') {
		App::abort(500, $getSamTransaction->developerMessage);
		}
		return $getSamTransaction->data;
		}
/**********READ**********/	
	public function getProcurementPlatform($key1,$key2){
						$lang = Language::getLanguage();
						$getBMID	= self::getBMID();
						
						/*
						print_r($getAllOrdersInfo[0]);exit;
						stdClass Object ( [address] => Cameroon [createdBy] => WE Name WE Surname [deliveryDate] => 2014-11-29 00:00:00.0 [description] => PENDING DELIVERY [orderCode] => 124619 [orderDate] => 2014-11-06 11:08:23.0 [orderDescription] => ssas [phone] => 52525 [productName] => CREDIT SAM [requestedQuantity] => 400 [supplierName] => NXP )
						*/
						$getProduct = self::getProduct();
						$BMID = $key1;
						$image = array();
						$getSupplierPrev="";
						$getProductOrder = array();
						
						
				$dataBMID[1]=$lang->PLEASE_SELECT;
                 	foreach($getBMID as $data){
						$dataBMID[$data->bmid] = $data->bmid;
					}
				
				
				
				foreach($getProduct as $getProductImage){
				$image[] .= $getProductImage->productId;
				}
							$getSupplierPr = self::getSupplier($key1,$key2);
			foreach($getSupplierPr as $getSupplierPre){
			$getSupplierPrev =$getSupplierPre->supplierName;}
			
				if($BMID != 1&& $key2 !=0){
					$getProductOrder = self::getProductOrder($key1,$key2);
					}
					
									if($BMID == 1&& $key2 ==0){
					$getAllOrdersInfo = array();
					}else{
					$getAllOrdersInfo =self::getAllOrdersInfo($key1);
					}
				
				
				
		
			return View::make('procurement/procurement_platform',array("key1"=>$key1,"key2"=>$key2,"dataBMID"=>$dataBMID,"image"=>$image,"getProductOrder"=>$getProductOrder,"getSupplierPrev"=>$getSupplierPrev,"getAllOrdersInfo"=>$getAllOrdersInfo));
			}
	
	public function getIssueRequestManager(){
		
			return View::make('procurement/issue_request_manager');
			}
	
	public function getInventoryManagement($key1,$key2,$key3){
						$lang = Language::getLanguage();
						$getBMID	= self::getBMID();
						$ProductStatus = array();
						if($key1!=1 && $key2!=0 ) {
						$getProductStatus = self::getProductStatus($key1,$key2);
								foreach($getProductStatus as $getProductStatusData){
				$ProductStatus[$getProductStatusData->issuedStatusType] = $getProductStatusData->statusDescription;
				}
						}
						//print_r($getBMID);exit;
						$getProduct = self::getProduct();
						$BMID = $key1;
						
						$image = array();
						$getProductOrder = array();
						
						
				$dataBMID[1]=$lang->PLEASE_SELECT;
                 	foreach($getBMID as $data){
						$dataBMID[$data->bmid] = $data->bmid;
					}
				
				
				
				foreach($getProduct as $getProductImage){
				$image[] .= $getProductImage->productId;
				}
				
				if($BMID != 1&& $key2 !=0){
					$getProductOrder = self::getProductOrder($key1,$key2);}
				
				//print_r($getProductOrder);exit;
				
		


				
		
			return View::make('procurement/inventory_management',array("key1"=>$key1,"key2"=>$key2,"key3"=>$key3,"dataBMID"=>$dataBMID,"image"=>$image,"getProductOrder"=>$getProductOrder,"ProductStatus"=>$ProductStatus));
			}
			
	public function getProductSuppliers($key){
						$lang = Language::getLanguage();
						$getProductSupplierPreview = array();
						$getBMID					= self::getBMID();
						$BMID = $key;
						//echo $BMID;exit;
						if($BMID != 1){
						$getProductSupplierPreview  = self::getProductSupplierPreview($BMID);}

				$dataBMID[1]=$lang->PLEASE_SELECT;
                 	foreach($getBMID as $data){
						$dataBMID[$data->bmid] = $data->bmid;
					}
                //print_r($dataBMID);exit;

				
		
			return View::make('procurement/product_suppliers',array("key"=>$key,"dataBMID"=>$dataBMID,"getProductSupplierPreview"=>$getProductSupplierPreview));
			}
			
/*	public function getRfIssueRequest(){
						$getBMID	= self::getBMID();
				$i=0;
				while($i < count($getBMID)) {
				$dataBMID[] = $getBMID->bmid;
				$i++;
				}  		
		
		
			return View::make('procurement/rf_issue_request',array("dataBMID"=>$dataBMID));
			}*/
			
	public function getSamIssueRequest($key1,$key2){
						$lang = Language::getLanguage();
						$getSamType = self::getSamType();
						$getBMID	= self::getBMID();
						$getSamOrderDetail = array();
						
				if($key1 != 1 && $key2 != 0){		
				$getSamOrderDetail = self::getSamOrderDetail($key1,$key2);}
				
				$dataBMID[1]=$lang->PLEASE_SELECT;
                 	foreach($getBMID as $data){
						$dataBMID[$data->bmid] = $data->bmid;
					}
				
				$dataSamType[0]=$lang->PLEASE_SELECT;
				
				$x=1;
				foreach($getSamType as $SamType){
				$dataSamType[$x] = $SamType->samTypeName;
				$x++;
				}

				
			return View::make('procurement/sam_issue_request',array("key1"=>$key1,"key2"=>$key2,"dataBMID"=>$dataBMID,"dataSamType"=>$dataSamType,"getSamOrderDetail"=>$getSamOrderDetail));
			}
	
	public function getCreateASupplier($key){
			$getProduct = self::getProduct();	
			
				foreach($getProduct as $Product){
				$dataProduct[$Product->productId] = $Product->productName;
				}
		
			return View::make('procurement/create_a_supplier',array("key"=>$key,"dataProduct"=>$dataProduct));
			}
			
	public function getCreateAnOrder($key1,$key2){
		
		//http://54.187.68.215:8888/cbox-webservice/webapi/proc/product-previous-orders?p_bmid=222222&prod_id=1
		
			$getSupplier = self::getSupplier($key1,$key2);
			$getProduct = self::getProduct();
			$getProductPreviousOrders = self::getProductPreviousOrders($key1,$key2);
			$getSuppliers = array();
			
			$x_orderCode 			= "";
			$x_orderDate			= "";
			$x_requestedQuantity 	= "";
			$x_createdBy			= "";
			$x_supplierId			= "";
			$x_productId			= "";
			$getSupplierPrev		= "";

			foreach($getProductPreviousOrders as $getProductPreviousOrders1){
			
			if(isset($getProductPreviousOrders1->orderCode)){
			$x_orderCode 			= $getProductPreviousOrders1->orderCode;
			$x_orderDate			= $getProductPreviousOrders1->orderDate;
			$x_requestedQuantity 	= $getProductPreviousOrders1->requestedQuantity;
			$x_createdBy 			= $getProductPreviousOrders1->createdBy;
			$x_supplierId			= $getProductPreviousOrders1->supplierId;
			$x_productId			= $getProductPreviousOrders1->productId;
			$getSupplierPr = self::getSupplier($key1,$x_productId);
			foreach($getSupplierPr as $getSupplierPre){
			$getSupplierPrev =$getSupplierPre->supplierName;}

			}
			}
			
			foreach($getProduct as $getProductData){
				$getProducts[$getProductData->productId] = $getProductData->productName;
				}
				
				foreach($getSupplier as $getSupplierData){
					$key = $getSupplierData->supplierId;
						$getSuppliers[$key] = $getSupplierData->supplierName;
				}
				

			return View::make('procurement/create_an_order',array(	"key1"=>$key1,
																	"key2"=>$key2,
																	"x_requestedQuantity"=>$x_requestedQuantity,
																	"x_orderDate"=>$x_orderDate,
																	"x_orderCode"=>$x_orderCode,
																	"getProductPreviousOrders"=>$getProductPreviousOrders,
																	"getSuppliers"=>$getSuppliers,
																	"getProducts"=>$getProducts,
																	"x_createdBy"=>$x_createdBy,
																	"getSupplierPrev"=>$getSupplierPrev));
			}
			
			
	
	public function getEditAnOrder($key1,$key2,$key3){

			$getProductOrder = self::getProductOrder($key1,$key2,$key3);
			$getProductStatus = self::getProductStatus();

if(is_array($getProductOrder)){
                 	foreach($getProductOrder as $datakey=>$dataval){
						if($datakey == "data"){
							
								
			$x_deliveryDate 		= $dataval->deliveryDate;
			$x_orderStatus 			= $dataval->orderStatus;
			$x_requestedQuantity	= $dataval->requestedQuantity;
			$x_orderDescription 	= $dataval->orderDescription;
							
						}
					}
				}else{
            $x_deliveryDate 		= $getProductOrder->deliveryDate;
			$x_orderStatus 			= $getProductOrder->orderStatus;
			$x_requestedQuantity	= $getProductOrder->requestedQuantity;
			$x_orderDescription 	= $getProductOrder->orderDescription;
				}


			foreach($getProductStatus as $getProductStatusData){
				$ProductStatus[$getProductStatusData->statusType] = $getProductStatusData->description;
				}
if(!isset($ProductStatus))
$ProductStatus=array();
			
		//echo $x_orderStatus; exit;
			return View::make('procurement/edit_an_order',array("key1"=>$key1,"key2"=>$key2,"key3"=>$key3,"x_deliveryDate"=>$x_deliveryDate,"x_orderStatus"=>$x_orderStatus,"x_requestedQuantity"=>$x_requestedQuantity,"x_orderDescription"=>$x_orderDescription,"ProductStatus"=>$ProductStatus));
			}
	
	public function getPreviewAnOrder($key1,$key2,$key3){
		//key1 BMID key2 Sam type  key3 Orderid

			$getProductPreviousOrders = self::getProductPreviousOrders($key1,$key2);
			//print_r($getProductPreviousOrders);exit;

			$x_productId 			= $getProductPreviousOrders->productId;
			$x_supplierId 			= $getProductPreviousOrders->supplierId;
			$x_orderDescription 	= $getProductPreviousOrders->orderDescription;
			$x_deliveryDate			= $getProductPreviousOrders->deliveryDate;
			$x_requestedQuantity 	= $getProductPreviousOrders->requestedQuantity;
		
			
			return View::make('procurement/preview_an_order',array("x_productId"=>$x_productId,"x_supplierId"=>$x_supplierId,"x_orderDescription"=>$x_orderDescription,"x_deliveryDate"=>$x_deliveryDate,"x_requestedQuantity"=>$x_requestedQuantity));
			}	
			
	public function getEditASupplier($key1,$key2){
		
			$getProductSupplierPreview  = self::getProductSupplierPreview($key1);
			foreach ($getProductSupplierPreview as $key=>$val ) {
				
			if($key == "data"){
			foreach($val as $getProductSupplierPreview2 ){
		
				  if($getProductSupplierPreview2['supplierId'] === $key2){
						  $x_supplierName = $getProductSupplierPreview2['supplierId'];
						  $x_address = $getProductSupplierPreview2['address'];
						  $x_phoneNumber = $getProductSupplierPreview2['phone'];
						  $x_factoryLocation = $getProductSupplierPreview2['factory'];							
					  }
			}
			}
			}
					  
							//echo $x_supplierName.$x_address.$x_phoneNumber.$x_factoryLocation; exit;

		
			return View::make('procurement/edit_a_supplier',array("key1"=>$key1,"key2"=>$key2,"x_supplierName"=>$x_supplierName,"x_address"=>$x_address,"x_phoneNumber"=>$x_phoneNumber,"x_factoryLocation"=>$x_factoryLocation));
			}	
	
	
	
	
	
	
/***CREATE EDIT***/		
	public function postProductSuppliers($key){
			$lang = Language::getLanguage();
			$key = Input::get('x_BMID');
			
			
			return Redirect::to('procurement_management/product_suppliers/'.$key.'/list');
			}
			
	public function postRfIssueRequest(){
			$lang = Language::getLanguage();
			return Redirect::to('procurement_management/issue_request_manager/')->with('message',$lang->SYSTEM_ACCOUNT_EMAIL_CREATED);
			}
			
	public function postSamIssueRequest($key1,$key2){
			$lang = Language::getLanguage();
			
						$set1=Input::get('x_BMID');
						if(isset($set1)&&$set1!=1){
						$key1=$set1;}
						
						$set2=Input::get('x_SamType');
						if(isset($set2)&&$set2!=0){
						$key2=$set2;}
						
						$x_numberOfSams=Input::get('x_numberOfSams');
					

						
						
			if($key1 != 1 && $key2 != 0 && $x_numberOfSams != ""){
					$data = array(
						"bmid"=>$key1,
						"requestCount"=>Input::get('x_numberOfSams'),
						"samType"=>$key2
						
						);
			$curl_response = CustomFunctions::CallAPI("POST",Config::get('services.restUrl')."proc/order-request",$data);
						$decoded = json_decode($curl_response);

						if (isset($decoded->status) && $decoded->status == 'ERROR') {
							App::abort(500, $decoded->developerMessage);
						}
				return Redirect::to('procurement_management/issue_request_manager/'.$key1.'/'.$key2.'/sam_issue_request')->with('message',$lang->ISSUE_REQUEST_MANAGER_ADDED);
				}else{
					return Redirect::to('procurement_management/issue_request_manager/'.$key1.'/'.$key2.'/sam_issue_request');
					}
			
			}
	
	public function postCreateASupplier($key){
			$lang = Language::getLanguage();
			$data = array(
				"bmid" 				=>$key,
				"productId" 		=>Input::get('x_selectAProduct'),
				"supplierName" 		=>Input::get('x_supplierName'),
				"supplierAddress" 	=>Input::get('x_address'),
				"supplierFactory"	=>Input::get('x_factoryLocation'),
				"phoneNo" 			=>Input::get('x_phoneNumber'),
				);
				//print_r($data);exit;
		
			$curl_response = CustomFunctions::CallAPI("POST",Config::get('services.restUrl')."proc/suppliers",$data);
						$decoded = json_decode($curl_response);
						if (isset($decoded->status) && $decoded->status == 'ERROR') {
							App::abort(500, $decoded->developerMessage);
						}
				return Redirect::to('procurement_management/product_suppliers/'.$key.'/list')->with('message',$lang->SUPPLIER_CREATED);
			
			}

			
	public function postEditASupplier($key1,$key2){
			$lang = Language::getLanguage();
			

			$data = array(
				"bmid" => $key1,
				"supplierId" => $key2,
				"supplierAddress" 	=>Input::get('x_address'),
				"supplierFactory"	=>Input::get('x_factoryLocation'),
				"phoneNo" 			=>Input::get('x_phoneNumber'),
				);
				
				//print_r($data);exit;
		
		$curl_response = CustomFunctions::CallAPI("PUT",Config::get('services.restUrl')."proc/suppliers",$data);
		
		$decoded = json_decode($curl_response);
		if (isset($decoded->status) && $decoded->status == 'ERROR') {
		App::abort(500, $decoded->developerMessage);
		
		}
		
		return Redirect::to('procurement_management/product_suppliers/'.$key1.'/list')->with('message',$lang->SUPPLIER_EDITED);
		
	}
	
	
	public function postDeleteASupplier($key1,$key2){
				$lang = Language::getLanguage();
				$curl_response = CustomFunctions::CallAPI("DELETE",Config::get('services.restUrl')."proc/suppliers?supp_id=".$key2);
				$decoded = json_decode($curl_response);
				//echo $decoded->status;exit;
				if (isset($decoded->status) && $decoded->status == 'ERROR') {
		            //App::abort(500, $decoded->developerMessage);
					return Redirect::to('procurement_management/product_suppliers/'.$key1.'/list')->with('error', str_replace("__USERNAME__",$key1, $lang->SUPPLIER_NOT_DELETED));
				}
		 return Redirect::to('procurement_management/product_suppliers/'.$key1.'/list')->with('message', $lang->SUPPLIER_DELETED);
				
	}
	
	public function postCreateAnOrder($key1,$key2){
			$lang = Language::getLanguage();
			if($key2 == Input::get('x_productType') ){
			$data = array(
				"bmid"     			=>$key1,
				"productType"   	=>Input::get('x_productType'),
				"productSupplier"   =>Input::get('x_productSupplier'),
				"orderDescription"  =>Input::get('x_orderDescription'),
				"deliveryDate"    	=>Input::get('x_deliveryDate'),
				"orderNo"    		=>Input::get('x_Quantity'),
				"creator"       	=>Session::get('firstName', '').' '. Session::get('lastName', ''),
				"supplier"     		=>Input::get('x_productSupplier'),
				);
			

				//print_r($data);exit;  Array ( [bmid] => 222222 [productType] => 1 [productSupplier] => 167781 [orderDescription] => or des [deliveryDate] => 10-25-2014 [orderNo] => 4 [creator] => WE Name WE Surname [supplier] => 167781 )
		
			$curl_response = CustomFunctions::CallAPI("POST",Config::get('services.restUrl')."proc/product-order",$data);
						$decoded = json_decode($curl_response);
						if (isset($decoded->status) && $decoded->status == 'ERROR') {
							App::abort(500, $decoded->developerMessage);
						}
				return Redirect::to('procurement_management/procurement_platform/'.$key1.'/'.$key2.'/list')->with('message',$lang->ORDER_CREATED);
			}else{
				return Redirect::to('procurement_management/procurement_platform/'.$key1.'/'.Input::get('x_productType').'/create_an_order');		
				//return $this->getCreateAnOrder($key1,$key2);
				}
			
			}
	
	public function postEditAnOrder($key1,$key2,$key3){
			$lang = Language::getLanguage();

			
			
			

			$data = array(
					"bmid"  => $key1,
					"orderId"  => $key3,
					"orderDescription" =>Input::get('x_orderDescription'),
					"deliveryDate" =>Input::get('x_deliveryDate'),
					"orderNo" => Input::get('x_requestedQuantity'),
					"orderStatus" => Input::get('x_orderStatus'),
					"creator" =>Session::get('firstName', '').' '.Session::get('lastName', '')
				);

		//echo $key1.' '.$key2.' '.$key3;
		//print_r($data);exit;
		
		$curl_response = CustomFunctions::CallAPI("PUT",Config::get('services.restUrl')."proc/product-order",$data);
		//print_r($curl_response);exit;
		$decoded = json_decode($curl_response);
		if (isset($decoded->status) && $decoded->status == 'ERROR') {
		App::abort(500, $decoded->developerMessage);
		
		}
	
			return Redirect::to('procurement_management/procurement_platform/'.$key1.'/'.$key2.'/list')->with('message',$lang->ORDER_EDITED);
			}
			
	
	public function postDeleteAnOrder($key1,$key2,$key3){
			//echo "proc/product-order?p_bmid=".$key1."&order_id=".$key3;exit;
			//proc/product-order?p_bmid=222222&order_id=940759
		
		$lang = Language::getLanguage();
		$curl_response = CustomFunctions::CallAPI("DELETE",Config::get('services.restUrl')."proc/product-order?p_bmid=".$key1."&order_id=".$key3);
				$decoded = json_decode($curl_response);
				//echo $decoded->status;exit;
				if (isset($decoded->status) && $decoded->status == 'ERROR') {
		            //App::abort(500, $decoded->developerMessage);
					return Redirect::to('procurement_management/procurement_platform/'.$key1.'/'.$key2.'/list')->with('error',$lang->ORDER_DELETED);
				}
		 return Redirect::to('procurement_management/procurement_platform/'.$key1.'/'.$key2.'/list')->with('message',$lang->ORDER_DELETED);
				
	}
	
	
	public function postProcurementPlatform($key1,$key2){
		//print_r($_POST);exit;
			$lang = Language::getLanguage();
			
						$set1=Input::get('x_BMID');
						if(isset($set1)&&$set1!=1){
						$key1=$set1;}
						
						$set2=Input::get('x_SamType');
						if(isset($set2)&&$set2!=0){
						$key2=$set2;}
						
					return Redirect::to('procurement_management/procurement_platform/'.$key1.'/'.$key2.'/list');
					
			
			}
		
	public function postInventoryManagement($key1,$key2,$key3){
		//print_r($_POST);exit;
			$lang = Language::getLanguage();
			
						$set1=Input::get('x_BMID');
						if(isset($set1)&&$set1!=1){
						$key1=$set1;}
						
						$set2=Input::get('x_SamType');
						if(isset($set2)&&$set2!=0){
						$key2=$set2;}
						
						$set3=Input::get('x_orderStatus');
						if(isset($set3)&&$set3!=0){
						$key3=$set3;}
						
					return Redirect::to('procurement_management/inventory_management/'.$key1.'/'.$key2.'/'.$key3.'/list');
					
			
			}
	
	

}
		


		