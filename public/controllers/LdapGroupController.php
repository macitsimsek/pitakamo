<?php

class LdapGroupController extends LdapController {

	public function getIndex(){
        $conn = parent::loginLdap();
		$groupAry = parent::getGroupList($conn);

		//print_r($groupAry);exit;
    	return View::make('ldap.group', array("groups"=>$groupAry));
	}

    public function getMemberIndex($id){
        $conn = parent::loginLdap();
		$groupAry = parent::getGroupList($conn);
		$userAry = parent::getUserList($conn);

		$groupOptions = array();
        $memberList = null;
		foreach($groupAry as $k => $v){

            $groupOptions[$v["id"]] = $v["name"];
			if($id==$v["id"]){
             	$memberList = $v["memberlist"];
			}

		}

		//print_r($groupAry);exit;
    	return View::make('ldap.group_member', array("groupId" => $id,"memberList"=>$memberList,"groups"=>$groupAry,"users"=>$userAry,"group_options"=>$groupOptions));
	}

	public function postMemberIndex($id){

		$selectedUsersname = Input::get('selectedUsers');
        //print_r($selectedUsersname); exit;

        $conn = parent::loginLdap();
        $groupAry = parent::getGroupList($conn);
		$groupName = "";
		foreach($groupAry as $k => $v){
			if($id==$v["id"]){
             	$groupName = $v["name"];
			}
		}
		$lang = Language::getLanguage();


		if(parent::changeGroupMembers($conn,$groupName,$selectedUsersname)==true){
        	return Redirect::to('user_identity_management/group')->with('message', str_replace("__GROUP__",$id, $lang->GROUP_UPDATED));
		}else{
            return Redirect::to('user_identity_management/group')->with('error', str_replace("__GROUP__",$id, $lang->GROUP_NOT_UPDATED));
		}

	}

	public function getEdit($id)
	{
        // Title
        //$title = Lang::get('admin/blogs/title.create_a_new_blog');

		$conn = parent::loginLdap();
		$groupAry = parent::getGroupList($conn);

		$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."permission/group-permission?p_group_id=".$id);
		//echo $curl_response; exit;
		$decoded = json_decode($curl_response);
        //echo print_r($decoded); exit;
		$selectedPermission = -1;
		if(isset($decoded->data)){
         	$selectedPermission = $decoded->data->permissionId;
		}
		//echo $selectedPermission;exit;

        $curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."permission/permission-schema");
		//echo $curl_response; exit;
		$decoded = json_decode($curl_response);
		if (isset($decoded->status) && $decoded->status == 'ERROR') {
            App::abort(500, $decoded->developerMessage);
		}

        $permissions= array();
		if(isset($decoded->data)){
			//print_r($decoded->data); exit;
         	foreach($decoded->data as $row){
                $permissions[$row->permissionId] = $row->name;
         	}
		}
        // print_r($permissions); exit;

     	foreach($groupAry as $k => $v){
          	if($v["id"]==$id){
               	$name = $v["name"];
				$description=$v["description"];
				
          	}
		}

        // Show the page
        //return View::make('admin/blogs/create_edit', compact('title'));
        return View::make('ldap/group_edit', array('id' => $id, 'name' => $name,'description'=>$description,"permissions"=>$permissions,"selectedPermission"=>$selectedPermission));
	}

	public function postEdit($id)
	{
        // Title
        //$title = Lang::get('admin/blogs/title.create_a_new_blog');

		$conn = parent::loginLdap();
		$groupAry = parent::getGroupList($conn);
		$name = "";
		$newName = Input::get('x_groupName');
		$permission = Input::get('x_associate');
		$description="";
		$lang = Language::getLanguage();
		$newDescription= Input::get('x_description');

		foreach($groupAry as $k => $v){
          	if($v["id"]==$id){
               	$name = $v["name"];
          	}
		}
	
		//echo $conn." , ".$name." , ".$newName." , ".$description." , ".$newDescription;
		//exit;

        if(parent::changeGroup($conn,$id,$name,$newName,$newDescription,$permission)==true){
        	return Redirect::to('user_identity_management/group')->with('message', str_replace("__USERNAME__",$name, $lang->CHANGED));
		}else{
            return Redirect::to('user_identity_management/group')->with('error', str_replace("__USERNAME__",$name, $lang->NOT_CHANGED));
		}
	}

/*ASSOCIATE PERMISSON SCHEMA */
/*
	public function getCreate()
	{
        

		$groupOptions = array("1","2");


		

        // Show the page
        //return View::make('admin/blogs/create_edit', compact('title'));
        return View::make('ldap/group_create')->with('group_options',$groupOptions);
		
		
	}

	public function postCreate()
	{
        $name = Input::get('x_groupName');
		$description = Input::get('x_description');
		$x_associate = Input::get('x_associate');
$lang = Language::getLanguage();
		$conn = parent::loginLdap();

		if(parent::createGroup($conn,$name,$description,$x_associate)==true){
        	return Redirect::to('user_identity_management/group')->with('message', str_replace("__USERNAME__",$name, $lang->CREATED));
		}else{
            return Redirect::to('user_identity_management/group')->with('error', str_replace("__USERNAME__",$name, $lang->NOT_CREATED));
		}

	}
*/
/*ASSOCIATE PERMISSON SCHEMA */

	public function getCreate()
	{
			$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."permission/permission-schema");
		//echo $curl_response; exit;
		$decoded = json_decode($curl_response,true);
		$permissions= array();
		foreach($decoded as $key=>$val){
	if($key == 'data'){
		
		foreach($val as $key1=>$val1){
			
				$permissions[$val1['permissionId']] = $val1['name'];
			
			}
		}
	} 
     

	
        return View::make('ldap/group_create', array("permissions"=>$permissions));
	}

	public function postCreate()
	{
        $name = Input::get('x_groupName');
		$description = Input::get('x_description');
		$permission= Input::get('x_associate');
		$lang = Language::getLanguage();
		$conn = parent::loginLdap();

		if(parent::createGroup($conn,$name,$description,$permission)==true){
        	return Redirect::to('user_identity_management/group')->with('message', str_replace("__USERNAME__",$name, $lang->CREATED));
		}else{
            return Redirect::to('user_identity_management/group')->with('error', str_replace("__USERNAME__",$name, $lang->NOT_CREATED));
		}

	}





	public function getDelete($id)
	{
        // Title
        //$title = Lang::get('admin/blogs/title.create_a_new_blog');

		$conn = parent::loginLdap();
		$groupAry = parent::getGroupList($conn);
		$name = "";
		$memberCount = "";

		foreach($groupAry as $k => $v){
          	if($v["id"]==$id){
               	$name = $v["name"];
               	$memberCount = $v["memberCount"];
          	}
		}

        // Show the page
        //return View::make('admin/blogs/create_edit', compact('title'));
        return View::make('ldap/group_delete', array('id' => $id, 'name' => $name,"memberCount"=>$memberCount));
	}

	public function postDelete($id)
	{
        // Title
        //$title = Lang::get('admin/blogs/title.create_a_new_blog');

		$conn = parent::loginLdap();
		$lang = Language::getLanguage();
		$groupAry = parent::getGroupList($conn);
		$userAry = parent::getUserList($conn);
		$name = "";

		foreach($groupAry as $k => $v){
          	if($v["id"]==$id){
               	$name = $v["name"];
          	}
		}
	/*E�er user array i grup �yesi bir kullan�c� i�eriyorsa, yakala ve uyar*/
	foreach ($userAry as $key => $val) {
       if ($val['groupId'] === $name) {
           return Redirect::to('user_identity_management/group')->with('error',str_replace("__USERNAME__",$name, $lang->HAS_MEMBERS));
       }
   }

        if(parent::deleteGroup($conn,$name)==true){
        	return Redirect::to('user_identity_management/group')->with('message', str_replace("__USERNAME__",$name, $lang->DELETED));
		}else{
            return Redirect::to('user_identity_management/group')->with('error',str_replace("__USERNAME__",$name, $lang->NOT_DELETED));
		}
	}
	
	
	
	 public function getPreview($id){


        $conn = parent::loginLdap();
		$groupAry = parent::getGroupList($conn);
		$userAry = parent::getUserList($conn);

		$groupOptions = array();
        $memberList = null;
		foreach($groupAry as $k => $v){

            $groupOptions[$v["id"]] = $v["name"];
			if($id==$v["id"]){
             	$memberList = $v["memberlist"];
				$name = $v["name"];
				$description=$v["description"];
			}

		}

        $dataAry = CustomFunctions::GetJson("GET", "permission/group-permission?p_group_id=".$id);
		$selectedPermission = -1;
		foreach($dataAry as $data){
           $selectedPermission = $data->permissionId;
		}

		$dataAry = CustomFunctions::GetJson("GET", "permission/permission-schema");
        $permissions= array();
		foreach($dataAry as $data){
           $permissions[$data->permissionId] = $data->name;
		}

		$selectedPermission = isset($permissions[$selectedPermission])?$permissions[$selectedPermission]:"";

		//print_r($groupAry);exit;
    	return View::make('ldap/group_preview', array("groupId" => $id,"memberList"=>$memberList,"groups"=>$groupAry,"users"=>$userAry,"group_options"=>$groupOptions,'name' => $name,'description'=>$description, "selectedPermission"=>$selectedPermission));
	}

	
	

}



