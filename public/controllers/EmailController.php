<?php

use yajra\Datatables\DatatablesServiceProvider ;

class EmailController extends Controller {


/**********READ**********/	
	public function getSettings(){

        $curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."setting/entry-list");
		$decoded = json_decode($curl_response);
		if (isset($decoded->status) && $decoded->status == 'ERROR') {
            App::abort(500, $decoded->developerMessage);
		}
		return $decoded->data;

	}

	

	public function getEmailingConfiguration(){
		$lang = Language::getLanguage();
		$settings = self::getSettings();
		//print_r($settings); exit;
		return View::make('settings/emailing_configuration');
	}
	
	public function getOutgoingEmail(){
		$lang = Language::getLanguage();	
		$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."setting/entry-list");
	    $settings = json_decode($curl_response,true);
	    foreach($settings as $key1=>$val1){
	    if($key1 == 'data'){
		//print_r($val);exit;
		foreach($val1 as $key2=>$val2){
		//print_r($val1);exit; [description] => smtp email settings [key] => EMAILING_DETAILS [value] => cbox@cbox.com::123456::smtp.cbox.xom
			if($val2['key'] == "EMAILING_DETAILS"){
			$value=$val2['value'];//webeo@posta.com
			//Cbox Server::cboxproject@gmail.com::Cbox::SECURE_SMTP::smtp.gmail.com::465::-1::tls::cboxproject@gmail.com::123::Lorem ipsum dolor sit amet
			$description=$val2['description'];//WE Mail Group
				}		
			}
		}
	} /*foreach($decoded as $key=>$val)*/
		$form = explode("::", $value);
//Array ( [0] => Cbox Server [1] => cboxproject@gmail.com [2] => Cbox [3] => SECURE_SMTP [4] => smtp.gmail.com [5] => 465 [6] => -1 [7] => tls [8] => cboxproject@gmail.com [9] => 123 [10] => Lorem ipsum dolor sit amet )
		$x_name 			= $form[0];
		$x_fromAdress 		= $form[1];
		$x_emailPrefix 		= $form[2];
		$x_protocol 		= $form[3];
		$x_hostName 		= $form[4];
		$x_smtpPort 		= $form[5];
		$x_timeout 			= $form[6];
		$x_tls 				= $form[7];
		$x_username 		= $form[8];
		$x_password 		= $form[9];
		$x_description 		= $form[10];
		return View::make('settings/outgoing_email',array("key"=>"EMAILING_DETAILS","value"=>$value,"description"=>$description,"x_name"=>$x_name,"x_fromAdress"=>$x_fromAdress,"x_emailPrefix"=>$x_emailPrefix,"x_protocol"=>$x_protocol,"x_hostName"=>$x_hostName,"x_smtpPort"=>$x_smtpPort,"x_timeout"=>$x_timeout,"x_tls"=>$x_tls,"x_username"=>$x_username,"x_password"=>$x_password,"x_description"=>$x_description));

	}

	public function getSystemAccountEmail(){
		$lang = Language::getLanguage();
		$settings = self::getSettings();
		//print_r($settings); exit;
		//Array ( [0] => stdClass Object ( [description] => test [key] => DEFAULT_LANGUAGE_SETTINGS [value] => EN ) [1] => stdClass Object ( [description] => description [key] => SITE_LOGO [value] => logo.png ) [2] => stdClass Object ( [description] => 100 TL [key] => CURRENCY [value] => TL ) [3] => stdClass Object ( [description] => time region select description [key] => TIME_REGION [value] => TR ) [4] => stdClass Object ( [description] => Renk seçimi [key] => SITE_THEME [value] => CBOX ) [5] => stdClass Object ( [description] => 10.000,23 [key] => DECIMAL_SETTING_DESCRIPTION [value] => . ) [6] => stdClass Object ( [description] => smtp email settings [key] => EMAILING_DETAILS [value] => cbox@cbox.com::123456::smtp.cbox.xom ) [7] => stdClass Object ( [description] => WE Mail Group [key] => SYSTEM_ACCOUNT_EMAILS [value] => weboa@mail.com ) [8] => stdClass Object ( [description] => WE Mail Group [key] => SYSTEM_ACCOUNT_EMAILS [value] => webeo@posta.com ) )
		
		return View::make('settings/system_account_emails',array("settings"=>$settings));
	}
	

/**********CREATE**********/	
	
	public function getSystemAccountEmailCreate(){
		$lang = Language::getLanguage();	
		return View::make('settings/system_account_emails_create');
	}
	
	public function postSystemAccountEmailCreate(){
		$lang = Language::getLanguage();	
		$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."setting/entry-list");
	    $settings = json_decode($curl_response,true);
		$rest = array();
		foreach($settings as $key1=>$val1){
	    if($key1 == 'data'){
			foreach($val1 as $tableData){
				if (strpos($tableData['key'],'SYSTEM_ACCOUNT_EMAILS_') !== false) {
					
    					$rest[] = substr($tableData['key'], -4);
						//print_r($rest);exit;
					}
				}
			}
		}
		asort($rest);
		$i = end($rest);
		if($i == ""){
		$i = 1000;
			}else{
		$i = $i+1;}
		
		$lang = Language::getLanguage();
		$x_email=Input::get('x_email');
		$data = array(
				"key"=>"SYSTEM_ACCOUNT_EMAILS_".$i,
				"value"=>Input::get('x_email'),
				"description"=>Input::get('x_accountName')
				);
		//print_r($data); exit;
		$curl_response = CustomFunctions::CallAPI("POST",Config::get('services.restUrl')."setting/entry-list",$data);
				$decoded = json_decode($curl_response);
				if (isset($decoded->status) && $decoded->status == 'ERROR') {
		            App::abort(500, $decoded->developerMessage);
				}
       	return Redirect::to('control_panel/emailing_configuration/system_account_emails')->with('message', str_replace("__USERNAME__",$x_email, $lang->SYSTEM_ACCOUNT_EMAIL_CREATED));
		
		
	}

/**********EDIT**********/
	

public function getSystemAccountEmailEdit($key){
		//echo $key; exit; //webeo@posta.com
		$lang = Language::getLanguage();
		
		$curl_response = CustomFunctions::CallAPI("GET",Config::get('services.restUrl')."setting/entry-list");
		$settings = json_decode($curl_response,true);
	foreach($settings as $key1=>$val1){
	if($key1 == 'data'){
		foreach($val1 as $k){
		if((strpos($k['key'],'SYSTEM_ACCOUNT_EMAILS_') !== false) && $k['key'] ==$key ){
		$key=$key;
		$value=$k['value'];
		$description=$k['description'];
		}
		}
	}/*if($key == 'data')*/
	} /*foreach($decoded as $key=>$val)*/
	
		return View::make('settings/system_account_emails_edit',array("key"=>$key,"value"=>$value,"description"=>$description));
	}
	
	
	
	public function postSystemAccountEmailEdit($key){
		$lang = Language::getLanguage();
		$item = $key;

			$data = array(
				"key"=>$key,
				"value"=>Input::get('x_email'),
				"description"=>Input::get('x_accountName')
				);
		
		$curl_response = CustomFunctions::CallAPI("PUT",Config::get('services.restUrl')."setting/entry-list",$data);
		
		$decoded = json_decode($curl_response);
		if (isset($decoded->status) && $decoded->status == 'ERROR') {
		App::abort(500, $decoded->developerMessage);
		
		}
		
		return Redirect::to('control_panel/emailing_configuration/system_account_emails')->with('message', str_replace("__USERNAME__",$item, $lang->SETTING_EDITED));
		
	}
	
	

	
	public function postOutgoingEmail(){
		    $lang = Language::getLanguage();
		//Array ( [0] => Cbox Server [1] => cboxproject@gmail.com [2] => Cbox [3] => SECURE_SMTP [4] => smtp.gmail.com [5] => 465 [6] => -1 [7] => tls [8] => cboxproject@gmail.com [9] => 123 [10] => Lorem ipsum dolor sit amet )
			$tls = Input::get('x_tls');
			if($tls==""){
				$tls=0;
				}
			$data = array(
				"key"=>"EMAILING_DETAILS",
				"value"=>Input::get('x_name')."::".Input::get('x_fromAdress')."::".Input::get('x_emailPrefix')."::".Input::get('x_protocol')."::".Input::get('x_hostName')."::".Input::get('x_smtpPort')."::".Input::get('x_timeout')."::".$tls."::".Input::get('x_username')."::".Input::get('x_password')."::".Input::get('x_description'),
				"description"=>Input::get('x_description')			
				);
				//echo $data['value'];exit;
				$curl_response = CustomFunctions::CallAPI("PUT",Config::get('services.restUrl')."setting/entry-list",$data);
				
				$decoded = json_decode($curl_response);
				if (isset($decoded->status) && $decoded->status == 'ERROR') {
				App::abort(500, $decoded->developerMessage);
				
				}
				return Redirect::to('control_panel/emailing_configuration/outgoing_email')->with('message', $lang->SETTING_ADDED_NEW);
				
				}
				
				
				
/**********DELETE**********/				
	public function postSystemAccountEmailDelete($key){
		//echo $key; exit;
		
		$lang = Language::getLanguage();
		$curl_response = CustomFunctions::CallAPI("DELETE",Config::get('services.restUrl')."setting/entry-list?p_key=".$key);
				$decoded = json_decode($curl_response);
				//echo $decoded->status;exit;
				if (isset($decoded->status) && $decoded->status == 'ERROR') {
		            //App::abort(500, $decoded->developerMessage);
					return Redirect::to('control_panel/emailing_configuration/system_account_emails')->with('error', str_replace("__USERNAME__",$key, $lang->NOT_DELETED));
				}
		 return Redirect::to('control_panel/emailing_configuration/system_account_emails')->with('message', str_replace("__USERNAME__",$key, $lang->DELETED));
				
	}

}
		


		