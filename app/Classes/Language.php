<?php
namespace App\Classes;

use App;
use App\Classes\CustomFunctions;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class Language {

	public static function saveEntry($request) {
        if(!Session::has('token')){
            //$token = Session::get('token'); Normal uses
            $token["access_token"] = $request["access_token"]; // trying use for postman
            $data =[
                "P_LOCAL_STR" => $request["P_LOCAL_STR"],
                "P_MESSAGE_GROUP" => $request["P_MESSAGE_GROUP"],
                "P_ITEM" => $request["P_ITEM"],
                "P_TEXT" => urldecode($request["P_TEXT"])
            ];
            $curl_response = CustomFunctions::CallAPI("POST",
                Config::get('services.restUrl')."core/save-entry?access_token=".$token["access_token"],$data,true);
            return $curl_response;
        }else{
            App::abort(500, "Please Login");
        }
	}

    public static function deleteEntry($request) {
        if(!Session::has('token')){
            //$token = Session::get('token');
            $token["access_token"] = $request["access_token"]; // trying use for postman
            $data =[
                "P_ITEM" => $request["P_ITEM"]
            ];
            $curl_response = CustomFunctions::CallAPI("POST",
                Config::get('services.restUrl')."core/delete-entry?access_token=".$token["access_token"],$data,true);
            return $curl_response;
        }else{
            App::abort(500, "Please Login");
        }
    }

    public static function getLanguages() {
        $curl_response = CustomFunctions::CallAPI("GET",
            Config::get('services.restUrl')."core/get-languages", false,true);
        return $curl_response;
    }

    public static function getLanguageEntries($lang) {
	    if (isset($lang)){
            $data =[
                "P_LOCAL_STR" => $lang
            ];
            $curl_response = CustomFunctions::CallAPI("POST",
                Config::get('services.restUrl')."core/get-language-entries", $data,true);
            if($curl_response['code']==200){
                $array = [];
                foreach ($curl_response["data"] as $item){
                    $array[$item["ITEM"]] = $item['TEXT'];
                }
                return $array;
            }else{
                App::abort(500, $curl_response['userMessage']);
            }
        }else{
            App::abort(500, "Please Enter Lang");
        }
    }
}

