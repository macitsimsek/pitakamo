<?php
namespace App\Classes;

use App\Classes\CustomFunctions;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class Token {

    private static $client_id = "webapp";
    private static $client_secret = "RWt6mPHvfip2D0f8ILiZlWOE074EbeEe";

    public static function getToken($type,$username,$password) {
        $data =[
            "grant_type" => "password",
            "client_id" => Token::$client_id,
            "client_secret" => Token::$client_secret,
            "username" => $username,
            "password" => $password
        ];
        $curl_response = CustomFunctions::CallAPI("POST",
            Config::get('services.restUrl').$type."/oauth/token",$data,false);
        if(!isset($curl_response['error'])){
            $typeNumber = 0;
            $array = [];
            if($type=='head'){
                $typeNumber = 4300;
            }elseif($type=='franchise'){
                $typeNumber = 4200;
            }elseif($type=='subfranchise'){
                $typeNumber = 4100;
            }
            $array['access_token']  = $curl_response['access_token'];
            $array['expires_in']    = time()+round((0.8*$curl_response['expires_in']));
            $array['refresh_token'] = $curl_response['refresh_token'];
            $array['username']      = $username;
            $array['type']          = $type;
            $array['typeNumber']    = $typeNumber;
            Session::set('token', $array);
            return $array;
        }else{
            return $curl_response;
        }
    }

    public static function refreshToken($token) {
        $data =[
            "grant_type" => "refresh_token",
            "client_id" => Token::$client_id,
            "client_secret" => Token::$client_secret,
            "refresh_token" => $token
        ];
        $curl_response = CustomFunctions::CallAPI("POST",
            Config::get('services.restUrl')."refresh/oauth/token",$data,false);
        if(!isset($curl_response['error'])){
            $session = Session::get('token');
            $session['access_token']  = $curl_response['access_token'];
            $session['refresh_token'] = $curl_response['refresh_token'];
            $session['expires_in']    = time()+round((0.8*$curl_response['expires_in']));
            Session::set('token', $session);
            return ['code'=>200,'data'=>$session];
        }else{
            return ['code'=>400,'message'=>'Please Login'];
        }
    }

    public static function checkToken() {
        if(Session::has('token')){
            $array = Session::get('token');
            if(time()>$array['expires_in']){
                return Token::refreshToken($array['refresh_token']);
            }
            return ['code'=>200,'data'=>$array];
        }else{
            return ['code'=>400,'message'=>'Please Login'];
        }
    }

    public static function removeToken() {
        if(Session::has('token')){
            $data = ['access_token'=>Session::get('token')];
            Session::forget('token');
            CustomFunctions::CallAPI("POST",Config::get('services.restUrl')."/oauth/token/revoke",$data,false);
        }else{
            Session::forget('token');
        }
    }
}

