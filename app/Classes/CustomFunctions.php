<?php
namespace App\Classes;

use App;
use App\Classes\Language;
use Illuminate\Support\Facades\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class CustomFunctions {

    public static function makePermissionCore($token){
        $permissions    = CustomFunctions::getPermissions($token)['data'];
        $core           = CustomFunctions::makeEverythingPermisions($permissions);
        return $core;
    }

    public static function redirectUrl(){
        $token          = Session::get('token');
        $permissions    = CustomFunctions::getPermissions($token)['data'];
        foreach ($permissions as $data){
            if($data['ROUTE_ID']==0){
                return Request::root().DIRECTORY_SEPARATOR.$data['PATH'].DIRECTORY_SEPARATOR;
                break;
            }
        }
    }

    public static function getDashboardUrl(){
        $token          = Session::get('token');
        $permissions    = CustomFunctions::getPermissions($token)['data'];

        foreach ($permissions as $data){
            if($data['ROUTE_ID']==0){
                return Request::root().DIRECTORY_SEPARATOR.$data['PATH'].DIRECTORY_SEPARATOR;
                break;
            }
        }
    }

    public static function getPermissions($token){
        $data =[
            "p_username"    => $token['username'],
            "p_group_id"    => $token['typeNumber']
        ];
        $curl_response = CustomFunctions::CallAPI("POST",
            Config::get('services.restUrl')."core/get-user-page-permissions?access_token=".$token["access_token"], $data,true);
        if(!isset($curl_response['error'])){
            return ['code'=>200,'data'=>$curl_response['data']];
        }elseif(isset($curl_response['code']) && $curl_response['code']!=200){
            return ['code'=>400,'message'=>$curl_response['userMessage']];
        }else{
            return ['code'=>400,'message'=>$curl_response['error_description']];
        }
	}

    public static function makeEverythingPermisions($permissions){
        $parent = [];
        $dashboard = [];
        $main = '';
        foreach ($permissions as $values){
            if($values['TYPE']==0){
                $main = $values['PATH'];
                $dashboard = [
                    'TYPE'          =>true,
                    'PATH'          =>$main,
                    'FULLPATH'      =>$main.DIRECTORY_SEPARATOR,
                    'LANGUAGE_KEY'  =>$values['LANGUAGE_KEY'],
                    'ICON'          =>$values['ICON_CLASS']];
                $parent[$values['PATH']] = $dashboard;
                break;
            }
        }
        foreach ($permissions as $values){
            if($values['TYPE']==1){
                $children   = [];
                $fButtons   = [];
                $sButtons   = [];
                foreach ($permissions as $values2){//start to check childrens
                    if($values2['UPPER_ROUTE_ID']==$values['ROUTE_ID']){
                        if($values2['TYPE']==2){
                            $fSmButtons   = [];
                            $sSmButtons   = [];
                            foreach ($permissions as $values3){//start to check childrens
                                if($values3['UPPER_ROUTE_ID']==$values2['ROUTE_ID']){
                                    if($values3['TYPE']==5){
                                        $fSmButtons[] = [
                                            'PATH'          =>$values3['PATH'],
                                            'FULLPATH'      =>$main.DIRECTORY_SEPARATOR.$values['PATH'].DIRECTORY_SEPARATOR.$values3['PATH'],
                                            'LANGUAGE_KEY'  =>$values3['LANGUAGE_KEY'],
                                            'ICON'          =>$values3['ICON_CLASS']];
                                    }
                                    if($values3['TYPE']==6){
                                        $sSmButtons[] = [
                                            'PATH'          =>$values3['PATH'],
                                            'FULLPATH'      =>$main.DIRECTORY_SEPARATOR.$values['PATH'].DIRECTORY_SEPARATOR.$values3['PATH'],
                                            'LANGUAGE_KEY'  =>$values3['LANGUAGE_KEY'],
                                            'ICON'          =>$values3['ICON_CLASS']];
                                    }
                                }
                            }//if children has buttons

                            $levelling = [[
                                    'FULLPATH'      =>  $dashboard['PATH'],
                                    'LANGUAGE_KEY'  =>  $dashboard['LANGUAGE_KEY'],
                                    'ICON'          =>  $dashboard['ICON']], [
                                    'FULLPATH'      =>  $dashboard['PATH'].DIRECTORY_SEPARATOR.$values['PATH'],
                                    'LANGUAGE_KEY'  =>  $values['LANGUAGE_KEY'],
                                    'ICON'          =>  $values['ICON_CLASS']], [
                                    'FULLPATH'      =>  $dashboard['PATH'].DIRECTORY_SEPARATOR.$values['PATH'].DIRECTORY_SEPARATOR.$values2['PATH'],
                                    'LANGUAGE_KEY'  =>  $values2['LANGUAGE_KEY'],
                                    'ICON'          =>  $values2['ICON_CLASS']]];
                            if(!empty($fSmButtons) or !empty($sSmButtons)){
                                $children[$values2['PATH']] = [
                                    'TYPE'          =>false,
                                    'PATH'          =>$values2['PATH'],
                                    'FULLPATH'      =>$main.DIRECTORY_SEPARATOR.$values['PATH'].DIRECTORY_SEPARATOR.$values2['PATH'],
                                    'LANGUAGE_KEY'  =>$values2['LANGUAGE_KEY'],
                                    'ICON'          =>$values2['ICON_CLASS'],
                                    'BUTTONS'       =>['FIRST'=>$fSmButtons,'SECOND'=>$sSmButtons],
                                    'LEVELLING'     =>$levelling
                                ];
                            }else{
                                $children[$values2['PATH']] = [
                                    'TYPE'          =>false,
                                    'PATH'          =>$values2['PATH'],
                                    'FULLPATH'      =>$main.DIRECTORY_SEPARATOR.$values['PATH'].DIRECTORY_SEPARATOR.$values2['PATH'],
                                    'LANGUAGE_KEY'  =>$values2['LANGUAGE_KEY'],
                                    'ICON'          =>$values2['ICON_CLASS'],
                                    'LEVELLING'     =>$levelling
                                ];
                            }
                        }else if($values2['TYPE']==3){//if parent has buttons
                            $fButtons[] = [
                                'PATH'          =>$values2['PATH'],
                                'FULLPATH'      =>$main.DIRECTORY_SEPARATOR.$values['PATH'].DIRECTORY_SEPARATOR.$values2['PATH'],
                                'LANGUAGE_KEY'  =>$values2['LANGUAGE_KEY'],
                                'ICON'          =>$values2['ICON_CLASS']];
                        }else if($values2['TYPE']==4){
                            $sButtons[] = [
                                'PATH'          =>$values2['PATH'],
                                'FULLPATH'      =>$main.DIRECTORY_SEPARATOR.$values['PATH'].DIRECTORY_SEPARATOR.$values2['PATH'],
                                'LANGUAGE_KEY'  =>$values2['LANGUAGE_KEY'],
                                'ICON'          =>$values2['ICON_CLASS']];
                        }
                    }
                }
                $levelling = [[
                    'FULLPATH'      =>  $dashboard['PATH'],
                    'LANGUAGE_KEY'  =>  $dashboard['LANGUAGE_KEY'],
                    'ICON'          =>  $dashboard['ICON']], [
                    'FULLPATH'      =>  $dashboard['PATH'].DIRECTORY_SEPARATOR.$values['PATH'],
                    'LANGUAGE_KEY'  =>  $values['LANGUAGE_KEY'],
                    'ICON'          =>  $values['ICON_CLASS']]];

                if(!empty($children)){//if parent has children make one more array value which is children
                    if(!empty($fButtons) or !empty($sButtons)){
                        $parent[$values['PATH']] = [
                            'TYPE'          =>true,
                            'PATH'          =>$values['PATH'],
                            'FULLPATH'      =>$main.DIRECTORY_SEPARATOR.$values['PATH'],
                            'LANGUAGE_KEY'  =>$values['LANGUAGE_KEY'],
                            'ICON'          =>$values['ICON_CLASS'],
                            'CHILDREN'      =>$children,
                            'BUTTONS'       =>['FIRST'=>$fButtons,'SECOND'=>$sButtons],
                            'LEVELLING'     =>$levelling
                        ];
                    }else{
                        $parent[$values['PATH']] = [
                            'TYPE'          =>true,
                            'PATH'          =>$values['PATH'],
                            'FULLPATH'      =>$main.DIRECTORY_SEPARATOR.$values['PATH'],
                            'LANGUAGE_KEY'  =>$values['LANGUAGE_KEY'],
                            'ICON'          =>$values['ICON_CLASS'],
                            'CHILDREN'      =>$children,
                            'LEVELLING'     =>$levelling
                        ];
                    }
                }else{
                    if(!empty($fButtons) or !empty($sButtons)){
                        $parent[$values['PATH']] = [
                            'TYPE'          =>false,
                            'PATH'          =>$values['PATH'],
                            'FULLPATH'      =>$main.DIRECTORY_SEPARATOR.$values['PATH'],
                            'LANGUAGE_KEY'  =>$values['LANGUAGE_KEY'],
                            'ICON'          =>$values['ICON_CLASS'],
                            'BUTTONS'       =>['FIRST'=>$fButtons,'SECOND'=>$sButtons],
                            'LEVELLING'     =>$levelling
                        ];
                    }else{
                        $parent[$values['PATH']] = [
                            'TYPE'          =>false,
                            'PATH'          =>$values['PATH'],
                            'FULLPATH'      =>$main.DIRECTORY_SEPARATOR.$values['PATH'],
                            'LANGUAGE_KEY'  =>$values['LANGUAGE_KEY'],
                            'ICON'          =>$values['ICON_CLASS'],
                            'LEVELLING'     =>$levelling
                        ];
                    }
                }
            }
        }
        return $parent;
    }

    public static function checkUserPermissionX($modulePath=""){
		//echo Route::getCurrentRoute()->getPath();
		if($modulePath==""){
        	$modulePath =  Route::getCurrentRoute()->getPath();
		}
		$permittedRoutes = Session::get('permittedRouteList');
		$permittedRoutes[] = "main";

		foreach($permittedRoutes as $path){
         	$pos = strpos($path, $modulePath);
			if ($pos !== false) { // �� esit imine dikkat!
			    return true;
			}
		}

		return false;

	}
	public static function checkUserPermission($modulePath=""){
		//echo Route::getCurrentRoute()->getPath();
		if($modulePath==""){
        	$modulePath =  Route::getCurrentRoute()->getPath();
		}
		$permittedRoutes = Session::get('permittedRouteList');
		$permittedRoutes[] = "main";
        $tmpAray = explode("/",$modulePath);
        $newArry = array();
		for($i=0;$i<count($tmpAray);$i++){
           $newArry[]=$tmpAray[$i];
		   if($i==1){
              break;
		   }
		}
        $modulePath = implode("/",$newArry);
		//echo $modulePath;
		//print_r($permittedRoutes);
		//exit;
		foreach($permittedRoutes as $path){
         	$pos = strpos($path, $modulePath);
			if ($pos !== false) { // esit imine dikkat!
			    return true;
			}
		}
		return false;
	}

	public static function getApplicationRoutes(){
		$curl_response = self::CallAPI("GET",Config::get('services.restUrl')."permission/application-routes");
		//echo $curl_response; exit;
		$decoded = json_decode($curl_response);
		if (isset($decoded->status) && $decoded->status == 'ERROR') {
            App::abort(500, $decoded->developerMessage);
		}
		return $decoded->data;
	}


	public static function GetJson($method, $url,$data=false,$sendException=true){
     	$curl_response = CustomFunctions::CallAPI($method,Config::get('services.restUrl').$url,$data);
		//echo $curl_response; exit;
		$decoded = json_decode($curl_response,false);
        //echo print_r($decoded); exit;
		if (isset($decoded->status) && $decoded->status == 'ERROR' && $sendException==true) {
            App::abort(500, $decoded->developerMessage);
		}

		if(isset($decoded->data) && is_object($decoded->data)){ //tek satir obje geldi demek arraya cevir
           	$tmpAry = array( $decoded->data );
           	return $tmpAry;
		}elseif(isset($decoded->data)){  //array geldi ise
         	return $decoded->data;
		}else{
			if($method=="GET"){
              	return array();
			}else
			if($decoded->status=="OK"){
             	return true;
			}else{
				return false;
			}
		}
	}


    private static function generateBody($data ,$isJson){
	    if($isJson){
	        return json_encode($data);
        }else{
	        return http_build_query($data);
        }
    }

    public static function CallAPI($method, $url, $data = false,$isJson=true){
	    $curl = curl_init();

	    switch ($method)
	    {
	    	case "DELETE":
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
				curl_setopt($curl, CURLOPT_HEADER, false);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
				curl_setopt($curl, CURLOPT_POSTFIELDS,$data);
				 $result = curl_exec($curl);
                curl_close($curl);
                return $result;
	            break;
	        case "POST":
	            curl_setopt($curl, CURLOPT_POST, 1);
	            if($isJson)
                    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	            if ($data)
	                curl_setopt($curl, CURLOPT_POSTFIELDS, CustomFunctions::generateBody($data ,$isJson));
	            break;
	        case "PUT":
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
				curl_setopt($curl, CURLOPT_HEADER, false);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
				curl_setopt($curl, CURLOPT_POSTFIELDS, CustomFunctions::generateBody($data ,$isJson));
				 $result = curl_exec($curl);
                curl_close($curl);
                return $result;
	            break;
	        default:
	            if ($data)
	                $url = sprintf("%s?%s", $url, CustomFunctions::generateBody($data ,$isJson));
	    }

	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

	    $result = curl_exec($curl);

	    curl_close($curl);

	    return json_decode($result, true);
	}
}