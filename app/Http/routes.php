<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});


Route::get('/token/{type?}',            'UserController@getToken');
Route::get('/check-token',              'UserController@checkToken');

//LANGUAGES

Route::get('/language-add',             'BaseController@saveEntry');
Route::get('/language-delete-entry',    'BaseController@deleteEntry');
Route::get('/language-entries',         'BaseController@getLanguageEntries');
Route::get('/languages',                'BaseController@getLanguages');

//if logged in dont show this pages always put it top because it can understan login is a variable of getpages
Route::group(['middleware' => 'App\Http\Middleware\LoginMiddleware'], function()
{
    Route::get('login', 				    'UserController@getLogin');
    Route::post('login', 				    'UserController@postLogin');
    Route::get('/admin-portal/login', 	    'UserController@getAdminLogin');
    Route::post('/admin-portal/login', 	    'UserController@postAdminLogin');

});

//this routes requires login
Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware'], function()
{
    Route::get('/logout',                       'UserController@logout');
    Route::get('/admin-portal',                 'BaseController@getDashboard');
    Route::get('/admin-portal/start',           'BaseController@getDashboardUrl');
    Route::get('/admin-portal/{page}/{subpage?}','BaseController@getPages');
    Route::get('/admin-portal/{page}/{user?}/{type?}','BaseController@getPageByButtons');
    Route::get('/admin-portal/{page}/{subpage}/{user?}/{type?}','BaseController@getSubPageByButtons');

    //TOKENS

    Route::get('/refresh-token/{token}',    'UserController@refreshToken');
    Route::get('/remove-token',             'UserController@removeToken');

    //PATHS

    Route::get('/insert-ui-path',           'UserController@insertUIPath');
    Route::get('/edit-ui-path',             'UserController@editUIPath');
    Route::get('/delete-ui-path',           'UserController@deleteUIPath');
    Route::get('/get-user-page-permissions','UserController@getUserPagePermissions');
    Route::get('/get-payment-type',         'UserController@getPaymentType');
    Route::get('/get-payment-status-type',  'UserController@getPaymentStatusType');
    Route::get('/get-gender-types',         'UserController@getGenderTypes');
    Route::get('/list-membership-types',    'UserController@listMembershipTypes');
});

//TEST URLS
Route::get('/test/get-user-page-permissions',   'UserController@testGetUserPagePermissions');
Route::get('/test/get-menu',                    'UserController@testMakeMenu');
Route::get('/test/redirect',                    'BaseController@getDashboardUrl');
Route::get('/test/checktoken',              'UserController@testcheckToken');