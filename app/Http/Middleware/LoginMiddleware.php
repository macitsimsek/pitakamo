<?php

namespace App\Http\Middleware;

use App\Classes\CustomFunctions;
use App\Classes\Token;
use Closure;

class LoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $result = Token::checkToken();
        if($result['code']==200){
            $url = CustomFunctions::getDashboardUrl($request);
            return redirect($url);
        }
        return $next($request);
    }
}
