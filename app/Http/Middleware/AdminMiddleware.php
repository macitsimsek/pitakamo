<?php

namespace App\Http\Middleware;

use App\Classes\Token;
use Closure;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $result = Token::checkToken();
        if($result['code']==400){
            return redirect('admin-portal/login')->with('error', $result['message']);
        }
        return $next($request);
    }
}
