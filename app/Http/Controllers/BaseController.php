<?php
namespace App\Http\Controllers;
use App\Classes\CustomFunctions;
use App\Classes\Language;
use App\Classes\Token;
use Illuminate\Http\Request;
use View;

class BaseController extends Controller {

	/**
	* Setup the layout used by the controller.
	*
	* @return void
	*/

    public function getDashboardUrl(Request $request) {
        return CustomFunctions::redirectUrl();
    }

    public function getDashboard() {
        $core = CustomFunctions::makePermissionCore(session('token'));
        return View :: make('admin/dashboard',['menu'=>$core]);
    }

    public function getPages($page,$subpage=null) {
        $user = [
            'first_name'    =>'Oh',
            'middle_name'   =>'My',
            'last_name'     =>'God',
            'mobile_number' =>'0555456123455',
            'gender'        =>'Male'
        ];
        $users=[];
        for($i=0;$i<25;$i++){
            $users[] =  $user;
        }

        $core = CustomFunctions::makePermissionCore(session('token'));
        if($subpage){
            return View :: make('admin/multiple_pages',['menu'=>$core[$page]['CHILDREN'][$subpage],'users' => $users]);
        }else{
            return View :: make('admin/multiple_pages',['menu'=>$core[$page],'users' => $users]);
        }
    }


    public function getPageByButtons(Request $request,$page,$user=null,$type=null) {
        $userinfos =  [
            'PICTURE' => 'images/prof-picture.png',
            'PERSONAL' => [
                'FIRST_NAME'            =>'Ted',
                'MIDDLE_NAME'           =>'test',
                'LAST_NAME'             =>'test',
                'MOBILE_NUMBER'         =>'213465345425234',
                'ADDRESS'               =>['Oh','My','last_name'],
                'BIRTH'                 =>'02/05/1225',
                'PLACE_OF_BIRTH'        =>'test',
                'GENDER'                =>'Male',
                'NATIONALITY'           =>'Brazil',
                'ID_TYPE'               =>'Test',
                'ID_NO'                 =>'Test',
                'NATURE_OF_BUSINESS'    =>'Test'
            ],
            'ORGANIZANTIONAL' => [
                'COMPANY_NAME'          =>'Oh',
                'DEPARTMENT'            =>'My',
                'POSITION'              =>'Test'
            ]
        ];
        $core = CustomFunctions::makePermissionCore(session('token'));
        foreach ($core[$page]['BUTTONS']['SECOND'] as $BUTTON) {
            if(str_replace('{user}/','',$BUTTON['PATH'])=='edit'){
                $userinfos['EDIT'] = $BUTTON; break;
            }
        }
        if($type    ==  'preview_user'){
            return View :: make('admin/multiple_pages_preview_user',
                ['core'=>$core,'menu'=>$core[$page],'userinfos'=>$userinfos,'back'=>$core[$page]['FULLPATH']]);
        }else if($type  ==  'edit'){
            return View :: make('admin/multiple_pages_edit',
                ['menu'=>$core[$page],'userinfos'=>$userinfos,'back'=>$core[$page]['FULLPATH']]);
        }
    }


    public function getSubPageByButtons($page,$subpage,$user=null,$type=null) {
        $userinfos =  [
            'PICTURE' => 'images/prof-picture.png',
            'PERSONAL' => [
                'FIRST_NAME'            =>'Ted',
                'MIDDLE_NAME'           =>'test',
                'LAST_NAME'             =>'test',
                'MOBILE_NUMBER'         =>'213465345425234',
                'ADDRESS'               =>['Oh','My','last_name'],
                'BIRTH'                 =>'02/05/1225',
                'PLACE_OF_BIRTH'        =>'test',
                'GENDER'                =>'Male',
                'NATIONALITY'           =>'Brazil',
                'ID_TYPE'               =>'Test',
                'ID_NO'                 =>'Test',
                'NATURE_OF_BUSINESS'    =>'Test'
            ],
            'ORGANIZANTIONAL' => [
                'COMPANY_NAME'          =>'Oh',
                'DEPARTMENT'            =>'My',
                'POSITION'              =>'Test'
            ]
        ];
        $core = CustomFunctions::makePermissionCore(session('token'));
        foreach ($core[$page]['CHILDREN'][$subpage]['BUTTONS']['SECOND'] as $BUTTON) {
            if(str_replace($subpage.'/{user}/','',$BUTTON['PATH'])=='edit'){
                $userinfos['EDIT'] = $BUTTON; break;
            }
        }
        if($type    ==  'preview_user'){
            return View :: make('admin/multiple_pages_preview_user',['core'=>$core,'menu'=>$core[$page]['CHILDREN'][$subpage],'userinfos'=>$userinfos,'back'=>$core[$page]['CHILDREN'][$subpage]['FULLPATH']]);
        }else if($type  ==  'edit'){
            return View :: make('admin/multiple_pages_edit',
                ['menu'=>$core[$page],'userinfos'=>$userinfos,'back'=>$core[$page]['FULLPATH']]);
        }
    }


    public function getHeadOfficeAccountManager() {
        return view('admin/head_office_account_manager', []);
    }

    public function getHeadOfficePre() {
        return view('admin/head_office_preview');
    }

    public function getToken(Request $request) {
        return Token::getToken($request->type,$request->username,$request->password);
    }

    public function saveEntry(Request $request) {
        return Language::saveEntry($request);
    }

    public function deleteEntry(Request $request) {
        return Language::deleteEntry($request);
    }

    public function getLanguageEntries() {
        return Language::getLanguageEntries('EN');
    }

    public function getLanguages() {
        return Language::getLanguages();
    }
}