<?php
namespace App\Http\Controllers;
use App\Classes\Language;
use App\Classes\Token;
use Illuminate\Http\Request;
use App\Classes\CustomFunctions;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use View;

class UserController extends Controller {

	/**
	* Setup the layout used by the controller.
	*
	* @return void
	*/

    public function postLogin() {

    }

    public function getLogin() {
        return View :: make('login');
    }

    public function getAdminLogin() {
        return View :: make('admin/login');
    }

    public function postAdminLogin(Request $request) {
        $result         =   Token::getToken($request->type,$request->username,$request->password);
        $lang           =   'EN';
        $langEntries    =   Language::getLanguageEntries($lang);
        if(isset($result['access_token'])){
            return ['code'=>200,'url'=>CustomFunctions::redirectUrl(),'message'=>$langEntries['LOGIN_SUCCESSFUL']];
        }else{
            return ['code'=>400,'message'=>$langEntries['BAD_CREDENTIALS']];
        }
    }

    public function refreshToken($token) {
        return Token::refreshToken($token);
    }

    public function getToken($type=null) {
        if($type==null) { $type2 = $type = 'head';}
        elseif($type=='system') {$type2=$type;$type= 'head';}
        else{ $type2= $type;}
        return Token::getToken($type,$type2.'@cboxprojects.com','qwerty');
    }

    public function checkToken() {
        return Token::checkToken();
    }

    public function logout(Request $request) {
        Token::removeToken();
        return redirect('/admin-portal/login');
    }

	public function insertUIPath(Request $request) {
        if(Session::has('token')){
            $token = Session::get('token'); //Normal uses
            $data =[
                "P_PATH_ID"             => $request["P_PATH_ID"],
                "P_PARENT_PATH_ID"      => $request["P_PARENT_PATH_ID"],
                "P_PATH_STRING"         => $request["P_PATH_STRING"],
                "P_DISPLAY_SEQUENCE"    => $request["P_DISPLAY_SEQUENCE"],
                "P_DISPLAY_NAME"        => $request["P_DISPLAY_NAME"]
            ];
            $curl_response = CustomFunctions::CallAPI("POST",
                Config::get('services.restUrl')."core/instert-ui-path?access_token=".$token, $data,true);
            return $curl_response;
        }else{
            return ["message"=>"please login"];
        }
	}

    public function editUIPath(Request $request) {
        if(Session::has('token')){
            $token = Session::get('token'); //Normal uses
            $data =[
                "P_PATH_ID"             => $request["P_PATH_ID"],
                "P_PARENT_PATH_ID"      => $request["P_PARENT_PATH_ID"],
                "P_PATH_STRING"         => $request["P_PATH_STRING"],
                "P_DISPLAY_SEQUENCE"    => $request["P_DISPLAY_SEQUENCE"],
                "P_DISPLAY_NAME"        => $request["P_DISPLAY_NAME"]
            ];
            $curl_response = CustomFunctions::CallAPI("POST",
                Config::get('services.restUrl')."core/edit-ui-path?access_token=".$token, $data,true);
            return $curl_response;
        }else{
            return ["message"=>"please login"];
        }
    }

    public function deleteUIPath(Request $request) {
        if(Session::has('token')){
            $token = Session::get('token'); //Normal uses
            $data =[
                "P_PATH_ID"             => $request["P_PATH_ID"]
            ];
            $curl_response = CustomFunctions::CallAPI("POST",
                Config::get('services.restUrl')."core/delete-ui-path?access_token=".$token, $data,true);
            return $curl_response;
        }else{
            return ["message"=>"please login"];
        }
    }

    public function getPaymentType() {
        $curl_response = CustomFunctions::CallAPI("GET",
            Config::get('services.restUrl')."core/get-payment-types", false,true);
        return $curl_response;
    }

    public function getPaymentStatusType() {
        $curl_response = CustomFunctions::CallAPI("GET",
            Config::get('services.restUrl')."core/get-payment-status-types", false,true);
        return $curl_response;
    }

    public function getGenderTypes() {
        $curl_response = CustomFunctions::CallAPI("GET",
            Config::get('services.restUrl')."core/get-gender-types", false,true);
        return $curl_response;
    }

    public function listMembershipTypes() {
        $curl_response = CustomFunctions::CallAPI("GET",
            Config::get('services.restUrl')."core/list-membership-types", false,true);
        return $curl_response;
    }




    public function testGetUserPagePermissions(Request $request) {
        $permissions    = CustomFunctions::getPermissions(session('token'))['data'];
        return $permissions;
    }

    public function testMakeMenu(Request $request) {
        $permissions    = CustomFunctions::getPermissions(session('token'))['data'];
        $menu           = CustomFunctions::makeEverythingPermisions($permissions);
        return $menu;
    }

    public function testcheckToken(Request $request) {
        return Token::checkToken();
    }

}